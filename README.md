First, run ```build.bash```. Then you can use ```run.bash``` to run the SVP planner (```pointing_planner```) with default parameters.

Otherwise you can source ```setup.bash```, which will source the ROS workspace ```setup.bash```, to use it through the ROS CLI (```rosrun```, ```rosparam```...)

