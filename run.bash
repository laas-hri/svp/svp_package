#!/usr/bin/env bash

dir=`dirname $0`

source $dir/setup.bash
roslaunch pointing_planner pointing_planner.launch
