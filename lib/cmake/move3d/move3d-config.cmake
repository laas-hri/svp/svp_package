get_filename_component(move3d_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

list(APPEND CMAKE_MODULE_PATH ${move3d_CMAKE_DIR})

find_package(Qt5 REQUIRED COMPONENTS Core)
list(REMOVE_AT CMAKE_MODULE_PATH -1)

find_library(GBM_LIBRARY gb)
find_library(SOFTMOTION_LIBRARY softMotion)

if(NOT TARGET move3d)
    include("${move3d_CMAKE_DIR}/move3d-targets.cmake")
endif()

set(move3d_LIBRARIES move3d ${GBM_LIBRARY} ${SOFTMOTION_LIBRARY})
