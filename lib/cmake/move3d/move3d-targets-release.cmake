#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "move3d" for configuration "Release"
set_property(TARGET move3d APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(move3d PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "assimp"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libmove3d.so"
  IMPORTED_SONAME_RELEASE "libmove3d.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS move3d )
list(APPEND _IMPORT_CHECK_FILES_FOR_move3d "${_IMPORT_PREFIX}/lib/libmove3d.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
