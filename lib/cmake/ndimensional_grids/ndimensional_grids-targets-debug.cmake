#----------------------------------------------------------------
# Generated CMake target import file for configuration "Debug".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "ndimensional_grids" for configuration "Debug"
set_property(TARGET ndimensional_grids APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(ndimensional_grids PROPERTIES
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/lib/libndimensional_grids.so"
  IMPORTED_SONAME_DEBUG "libndimensional_grids.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS ndimensional_grids )
list(APPEND _IMPORT_CHECK_FILES_FOR_ndimensional_grids "${_IMPORT_PREFIX}/lib/libndimensional_grids.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
