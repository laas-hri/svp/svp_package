# - Config file for the ndimensional_grids package
# It defines the following variables
#  ndimensional_grids_INCLUDE_DIRS - include directories for ndimensional_grids
#  ndimensional_grids_LIBRARIES    - libraries to link against

# Compute paths
get_filename_component(ndimensional_grids_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
set(ndimensional_grids_INCLUDE_DIRS "${ndimensional_grids_CMAKE_DIR}/../../../include/")

include(CMakeFindDependencyMacro)

if(NOT DEFINED ${EIGEN3_INCLUDE_DIRS})
    FIND_PACKAGE(Eigen3 REQUIRED)
  set(EIGEN3_INCLUDE_DIRS ${Eigen_INCLUDE_DIR})
endif(NOT DEFINED ${EIGEN3_INCLUDE_DIRS})

# Our library dependencies (contains definitions for IMPORTED targets)
if(NOT TARGET ndimensional_grids )
    include("${ndimensional_grids_CMAKE_DIR}/ndimensional_grids-targets.cmake")
endif()

# These are IMPORTED targets created by ndimensional_grids-targets.cmake
set(ndimensional_grids_LIBRARIES ndimensional_grids)
