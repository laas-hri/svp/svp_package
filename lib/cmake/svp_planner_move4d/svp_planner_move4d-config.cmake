# - Config file for the svp_planner package
# It defines the following variables
#  svp_planner_move4d_INCLUDE_DIRS - include directories for svp_planner
#  svp_planner_move4d_LIBRARIES    - libraries to link against

# Compute paths
get_filename_component(svp_planner_move4d_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
set(svp_planner_move4d_INCLUDE_DIRS "${svp_planner_move4d_CMAKE_DIR}/../../../include/")

include(CMakeFindDependencyMacro)

find_dependency(Eigen3 REQUIRED)
if(NOT ${EIGEN3_FOUND})
    FIND_PACKAGE(EIGEN REQUIRED)
  set(EIGEN3_INCLUDE_DIRS ${Eigen_INCLUDE_DIR})
endif(NOT DEFINED ${EIGEN3_INCLUDE_DIRS})

find_dependency(ndimensional_grids REQUIRED)
find_dependency(move3d REQUIRED)
find_dependency(move4d REQUIRED)
find_dependency(Boost REQUIRED)
find_dependency(svp_planner REQUIRED)

# Our library dependencies (contains definitions for IMPORTED targets)
if(NOT TARGET svp_planner_move4d )
    include("${svp_planner_move4d_CMAKE_DIR}/svp_planner_move4d-targets.cmake")
endif()

# These are IMPORTED targets created by svp_planner_move4d-targets.cmake
set(svp_planner_move4d_LIBRARIES -Wl,--no-as-needed svp_planner_move4d -Wl,--as-needed)
