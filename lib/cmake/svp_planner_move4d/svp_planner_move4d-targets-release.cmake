#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "svp_planner_move4d" for configuration "Release"
set_property(TARGET svp_planner_move4d APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(svp_planner_move4d PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libsvp_planner_move4d.so"
  IMPORTED_SONAME_RELEASE "libsvp_planner_move4d.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS svp_planner_move4d )
list(APPEND _IMPORT_CHECK_FILES_FOR_svp_planner_move4d "${_IMPORT_PREFIX}/lib/libsvp_planner_move4d.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
