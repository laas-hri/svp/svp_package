# - Config file for the svp_planner package
# It defines the following variables
#  svp_planner_INCLUDE_DIRS - include directories for svp_planner
#  svp_planner_LIBRARIES    - libraries to link against

# Compute paths
get_filename_component(svp_planner_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
set(svp_planner_INCLUDE_DIRS "${svp_planner_CMAKE_DIR}/../../../include/")

include(CMakeFindDependencyMacro)

#find_dependency(Eigen3 REQUIRED)
if(NOT DEFINED ${EIGEN3_INCLUDE_DIRS})
    FIND_PACKAGE(Eigen3 REQUIRED)
  set(EIGEN3_INCLUDE_DIRS ${Eigen_INCLUDE_DIR})
endif(NOT DEFINED ${EIGEN3_INCLUDE_DIRS})

find_dependency(ndimensional_grids REQUIRED)

# Our library dependencies (contains definitions for IMPORTED targets)
if(NOT TARGET svp_planner )
    include("${svp_planner_CMAKE_DIR}/svp_planner-targets.cmake")
endif()

# These are IMPORTED targets created by svp_planner-targets.cmake
set(svp_planner_LIBRARIES svp_planner)
