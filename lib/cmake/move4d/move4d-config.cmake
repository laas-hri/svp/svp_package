get_filename_component(move4d_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

list(APPEND CMAKE_MODULE_PATH ${move4d_CMAKE_DIR})

# NOTE Had to use find_package because find_dependency does not support COMPONENTS or MODULE until 3.8.0

find_package(move3d REQUIRED)
list(REMOVE_AT CMAKE_MODULE_PATH -1)

if(NOT TARGET move4d)
    include("${move4d_CMAKE_DIR}/move4d-targets.cmake")
endif()

set(move4d_LIBRARIES move4d)
