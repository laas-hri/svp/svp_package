#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "move4d" for configuration "Release"
set_property(TARGET move4d APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(move4d PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "move3d"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libmove4d.so"
  IMPORTED_SONAME_RELEASE "libmove4d.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS move4d )
list(APPEND _IMPORT_CHECK_FILES_FOR_move4d "${_IMPORT_PREFIX}/lib/libmove4d.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
