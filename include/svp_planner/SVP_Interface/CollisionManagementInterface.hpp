#ifndef SVP_INTERFACE_COLLISIONMANAGEMENTINTERFACE_HPP
#define SVP_INTERFACE_COLLISIONMANAGEMENTINTERFACE_HPP

#include <Eigen/Core>

namespace svp
{
namespace interface
{
class CollisionManagementInterface
{
  public:
    // Constructors - Destructors
    CollisionManagementInterface() {}

    // Methods
    virtual void getCollision()=0;
    virtual void initCollisions()=0;
    virtual bool moveCheckCollisions(bool hr, Eigen::Vector3d pos)=0; //hr==false ==> Human, hr==true ==> Robot

};
} // namespace interface
} // namespace svp
#endif // SVP_INTERFACE_COLLISIONMANAGEMENTINTERFACE_HPP