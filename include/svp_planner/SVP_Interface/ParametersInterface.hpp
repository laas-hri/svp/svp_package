#ifndef SVP_INTERFACE_PARAMETERSINTERFACE_HPP
#define SVP_INTERFACE_PARAMETERSINTERFACE_HPP

#include "SVP_Interface/LoggerInterface.hpp"
#include "SVP_Interface/TargetsInterface.hpp"
#include <Eigen/Core>
#include <array>

namespace svp
{
namespace interface
{
class ParametersInterface
{
  public:
    TargetsInterface *targets=nullptr;

    // Using
    using PlannerParamsVal =
        std::array<float, 10>; // Correspondonds to the number of parameters contained in
                               // PlannerParametersNames declared below

    // Enum classes
    enum class PlannerParametersNames {
        // Try_no_move
        mobrob = 0,
        mobhum,
        // Vis
        vis_threshold,
        kvisib,
        // Dist
        maxdist,
        kdist,
        // Prox
        proxmin,
        proxmax,
        // Angle
        desired_angle_deltahr,
        desired_angle_deltahr_tolerance
    };

    // Constructors - Destructors
    ParametersInterface()
        : plannerParametersValues{}, usePhysicalTarget(false), physicalTarget(0,0),
          indexFirstOptionalTarget(0)
    {
    }

    ~ParametersInterface()
    {
        delete targets;
    }

    // Methods
    // Init
    virtual void initParameters(LoggerInterface *pLog, TargetsInterface *pTargets) = 0;
    // Get-Set PlannerParametersValues
    virtual void setPlannerParametersValues(const PlannerParamsVal &values);
    virtual void setPlannerParametersValue(PlannerParametersNames name, float val);
    virtual PlannerParamsVal &getPlannerParametersValues();
    virtual float getPlannerParametersValue(PlannerParametersNames name);

    // physicalTarget
    virtual void setUsePhysicalTarget(bool val);
    virtual void setPhysicalTarget(Eigen::Vector2d vect);
    virtual bool getUsePhysicalTarget();
    virtual Eigen::Vector2d getPhysicalTarget();

    // targets
    virtual void setTargets(TargetsInterface *vectT, uint firstOpT);
    virtual void setTargets(TargetsInterface *vectT);
    virtual void setIndexFirstOptionalTarget(uint firstOpT);
    virtual TargetsInterface *getTargets();
    virtual uint getIndexFirstOptionalTarget();

    // fetch parameters
    virtual void fetchParameters() = 0;

    // Attributes
  protected:
    PlannerParamsVal plannerParametersValues;

    bool usePhysicalTarget;
    Eigen::Vector2d physicalTarget; ///< where the human has to get in the end

    uint indexFirstOptionalTarget; // the targets before this index in targets are mandatory
    LoggerInterface *log=nullptr;
};
} // namespace interface
} // namespace svp

#endif // SVP_INTERFACE_PARAMETERSINTERFACE_HPP