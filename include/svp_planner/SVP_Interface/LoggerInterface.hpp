#ifndef SVP_INTERFACE_LOGGERINTERFACE_HPP
#define SVP_INTERFACE_LOGGERINTERFACE_HPP

#include <log4cxx/logger.h>
#include <sstream>

namespace svp
{
namespace interface
{
class LoggerInterface
{
  public:
    // You need to declare your logger here

    // Constructors -  Destructors
    LoggerInterface()= default;

    // Methods
    virtual void initLogger() = 0;

    virtual void sendInfoMsg(const std::istringstream &msg) = 0;
    virtual void sendDebugMsg(const std::istringstream &msg) = 0;
    virtual void sendErrorMsg(const std::istringstream &msg) = 0;
    virtual void sendTraceMsg(const std::istringstream &msg) = 0;
}; // class Logger
} // namespace interface
} // namespace svp

#endif // SVP_INTERFACE_LOGGERINTERFACE_HPP