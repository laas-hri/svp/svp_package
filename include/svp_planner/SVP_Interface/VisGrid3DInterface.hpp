#ifndef SVP_INTERFACE_VISGRID3DINTERFACE_HPP
#define SVP_INTERFACE_VISGRID3DINTERFACE_HPP

namespace svp
{
namespace interface
{
class VisibilityGrid3DInterface
{
  public:
    using SpaceCoord = std::array<float, 3>;
    using ArrayCoord = std::array<size_t, 3>;
    using out_of_grid = std::out_of_range;

    // Constructors - Destructors
    VisibilityGrid3DInterface(ParametersInterface* paramsP)
    {
        params=paramsP;
    }

    ~VisibilityGrid3DInterface()
    {
        delete params;
    }

    // Methods
    virtual void initGrid() = 0;
    virtual void loadGrid() = 0;

    virtual SpaceCoord getCellSize() = 0;
    virtual ArrayCoord getCellCoord(unsigned int i) = 0;
    virtual ArrayCoord getCellCoord(SpaceCoord spc) = 0;
    virtual SpaceCoord getCellCenter(ArrayCoord arc) = 0;

    virtual std::vector<float> getCellVisibilities(SpaceCoord spc) = 0;

    virtual unsigned int getNumberOfCells() = 0;
    virtual std::array<float, 3> getOriginCorner() = 0;
protected:
    ParametersInterface* params=nullptr;
};
} // namespace interface
} // namespace svp

#endif // SVP_INTERFACE_VISGRID3DINTERFACE_HPP