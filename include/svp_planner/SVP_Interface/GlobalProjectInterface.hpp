#ifndef SVP_INTERFACE_GLOBALPROJECTINTERFACE_HPP
#define SVP_INTERFACE_GLOBALPROJECTINTERFACE_HPP

#include <map>
#include <string>
#include <vector>

namespace svp
{
namespace interface
{
class GlobalProjectInterface
{
  public:
    // Init
    virtual void init() = 0;

    // Jobs
    virtual int getNumberOfJobs() = 0;

    // Bounds
    virtual std::vector<double> getActiveSceneBounds() = 0;

    // ENV
    virtual void setEnvIsRunning(bool val) = 0;

    // Cost details
    virtual void clearCostDetails() = 0;
    virtual void setCostDetails(const std::map<std::string, double> &m) = 0;
    virtual std::map<std::string, double> getCostDetails() = 0;

    // Target results
    virtual void updateTargetResults(const std::string &targetName) = 0;
    virtual void updateOtherVisibleResults(const std::string &targetName) = 0;
};
} // namespace interface
} // namespace svp

#endif // SVP_INTERFACE_GLOBALPROJECTINTERFACE_HPP
