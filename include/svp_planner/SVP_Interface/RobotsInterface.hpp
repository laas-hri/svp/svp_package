#ifndef SVP_INTERFACE_ROBOTSINTERFACE_HPP
#define SVP_INTERFACE_ROBOTSINTERFACE_HPP

#include "SVP_Planner/Declarations.hpp"
#include <Eigen/Geometry>
namespace svp
{
namespace interface
{
class RobotsInterface
{
  public:
    using Cell = svp::DeclarationsSVP::Cell;

    // Methods
    // Constructors - Destructors
    RobotsInterface()= default;

    // Methods
    // Init
    virtual void initRobots() = 0;

    // Name
    virtual std::string getHumanName() = 0;
    virtual std::string getRobotName() = 0;

    // StartPos
    virtual Eigen::Vector2d getStartPosHuman() = 0;
    virtual Eigen::Vector2d getStartPosRobot() = 0;
    virtual void setStartPosHuman(float xPos, float yPos) = 0;
    virtual void setStartPosRobot(float xPos, float yPos) = 0;


    // Pos
    virtual void setHumanPosition(const Cell &cell) = 0;
    virtual void setRobotPosition(const Cell &cell) = 0;

    // Cost
    virtual void setHumanCost(Cell *cell) = 0;
    virtual void setRobotCost(Cell *cell) = 0;

    virtual double getHumanCost() = 0;
    virtual double getRobotCost() = 0;

    // State Value
    virtual void setHumanState(unsigned int state_id, float value) = 0;
    virtual void setRobotState(unsigned int state_id, float value) = 0;

    virtual float getHumanState(unsigned int state_id) = 0;
    virtual float getRobotState(unsigned int state_id) = 0;

    // Perspective Vector Pos
    virtual Eigen::Vector3d getHumanPerspectiveVectorPos() = 0;
    virtual Eigen::Vector3d getRobotPerspectiveVectorPos() = 0;

    // Perspective Matrix Pos
    virtual Eigen::Affine3d getHumanPerspectiveMatrixPos() = 0;
    virtual Eigen::Affine3d getRobotPerspectiveMatrixPos() = 0;
}; // class RobotsInterface

} // namespace interface
} // namespace svp

#endif // SVP_INTERFACE_ROBOTSINTERFACE_HPP