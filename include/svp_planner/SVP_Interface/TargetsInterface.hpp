#ifndef SVP_INTERFACE_TARGETSINTERFACE_HPP
#define SVP_INTERFACE_TARGETSINTERFACE_HPP

#include <Eigen/Core>
#include <string>
#include <vector>

namespace svp
{
namespace interface
{
class TargetsInterface
{
  public:
    // Using
    using TNames = typename std::vector<std::string>;
    using TPos3d = typename std::vector<Eigen::Vector3d>;
    using TPosBase2d = typename std::vector<Eigen::Vector2d>;

    // Constructors - Destructors
    TargetsInterface()= default;


    // Methods
    // Get
    virtual int getTargetsNumber();
    virtual const std::string &getTargetName(int i);
    virtual const Eigen::Vector3d &getTargetPos3d(int i);
    virtual const Eigen::Vector2d &getTargetBasePos2d(int i);

    // Set
    virtual void setTargetsPosNames(TNames names, TPos3d pos3d, TPosBase2d pos2d);
    virtual void setTargetsNames(TNames names);
    virtual void setTargetsPos3d(TPos3d pos);
    virtual void setTargetsBasePos2d(TPosBase2d pos);

    // Clear
    virtual void clearTargets();

    // Add Target
    virtual void addTarget(std::string name, Eigen::Vector3d pos3d, Eigen::Vector2d pos2d);
    virtual void addTargetName(std::string name);
    virtual void addTargetPos3d(Eigen::Vector3d pos);
    virtual void addTargetBasePos2d(Eigen::Vector2d pos);

  protected:
    TNames TargetsNames;
    TPos3d TargetsPos3d;
    TPosBase2d TargetsBasePos2d;
};

} // namespace interface
} // namespace svp
#endif // SVP_INTERFACE_TARGETSINTERFACE_HPP
