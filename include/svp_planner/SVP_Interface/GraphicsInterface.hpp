#ifndef SVP_INTERFACE_GRAPHICSINTERFACE_HPP
#define SVP_INTERFACE_GRAPHICSINTERFACE_HPP

#include "ndimensional_grids/ndgrid/NDGrid.hpp"
#include "SVP_Planner/CostGrid.hpp"

namespace svp
{
namespace interface
{
class GraphicsInterface
{
public:
    // Constructors - Destructors
    GraphicsInterface() {}

    // Methods
    virtual void initGraphics()=0;
    virtual void add2DGrid(const ndimensional_grids::nDimGrid<float, 2>& grid2d, const std::string& name)=0;
    virtual void add2DGrid(const CostGrid& costGrid2d, const std::string& name)=0;
};

} // namespace interface
} // namespace svp

#endif // SVP_INTERFACE_GRAPHICSINTERFACE_HPP
