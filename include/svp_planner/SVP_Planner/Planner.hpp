#ifndef SVP_PLANNER_PLANNER_HPP
#define SVP_PLANNER_PLANNER_HPP

#ifndef SVP_PLANNER_HPP
#define SVP_PLANNER_HPP

#include "SVP_Interface/GlobalProjectInterface.hpp"
#include "SVP_Interface/LoggerInterface.hpp"
#include "SVP_Interface/ParametersInterface.hpp"
#include "SVP_Interface/RobotsInterface.hpp"
#include "SVP_Interface/TargetsInterface.hpp"
#include "SVP_Interface/VisGrid3DInterface.hpp"
#include "SVP_Interface/CollisionManagementInterface.hpp"
#include "SVP_Interface/GraphicsInterface.hpp"

#include "SVP_Planner/Declarations.hpp"
#include "SVP_Planner/Grids.hpp"
#include "SVP_Planner/LinkedBalls2D.hpp"
#include "SVP_Planner/CostGrid.hpp"
#include "SVP_Planner/Geometry.hpp"
#include <atomic>
#include <memory>
#include <mutex>

namespace svp
{

class Planner
{
  public:
    // Using
    using Cost = DeclarationsSVP::Cost;
    using Cell = DeclarationsSVP::Cell;
    using Grid = DeclarationsSVP::Grid;
    using Grid2d = DeclarationsSVP::Grid2d;

    // Constructors - Destructors
    Planner(interface::GlobalProjectInterface *globProjP,
            interface::LoggerInterface *logP, interface::ParametersInterface *paramsP,
            interface::RobotsInterface *robotsP,
            interface::VisibilityGrid3DInterface *visGrid3dP, Grids *gridsP,
            interface::TargetsInterface *targetsP,
            interface::CollisionManagementInterface *collP,
            interface::GraphicsInterface* graphP);

    ~Planner();

    // Methods
    // init
    void reinit();
    void initCollisionGrids();
    void initCollisionGrid(ndimensional_grids::nDimGrid<bool,2> &grid, bool hr); //hr==0 ==> Human, hr==1 ==> Robot

    Cell run();

    void getParameters();

    bool isTooFar(Cell *c, Cell *from);
    bool areAgentTooFarFromEachOther(Cell *c);
    const Eigen::Vector3d approxPerspJointPos(const Eigen::Vector2d &pos, double perspHeight);

    Grid::SpaceCoord conv2to4d(const Grid2d::SpaceCoord &c);
    Grid::ArrayCoord conv2to4d(const Grid2d::ArrayCoord &c);
    Grid2d::SpaceCoord conv4to2d(const Grid::SpaceCoord &c);
    Grid2d::ArrayCoord conv4to2d(const Grid::ArrayCoord &c);

    Cell *createCell(Grid::ArrayCoord coord, Grid::SpaceCoord pos);

    float getRouteDirTime(Cell *c, uint i);

    void setRobots(Cell *cell); // r,h,cell

    void moveHumanToFaceTarget(Cell *c, Eigen::Vector3d &perspJointPos, unsigned int target_id);
    void moveRobotToHalfAngle(Cell *c, Eigen::Vector3d &perspJointPos, unsigned int target_id);

    void resetFromCurrentInitPos();

    Cell searchHumanPositionOnly(Grid::SpaceCoord cellSize4d, std::vector<double> envSize4d,
                                 Grid::SpaceCoord pos4d);
    Cost targetCostWithoutHRI(Cell *c, unsigned int i, float visib);
    Cost computeCostWithoutHRI(Cell *c);

    Cell searchRobotPosition(const Cell& start, const std::vector<double>& envSize2d);
    Cost computeCostWithHRI(Cell *c);
    Cost targetCostWithHRI(Cell *c, unsigned int i, float visibHuman, float visibRobot);

    std::vector<float> getVisibilities(bool hr, const Eigen::Vector2d &pos2d); // hr=0 => Human, hr=1 => Robot
    bool isHumanNeedToMove(Cell* c);

    std::vector<unsigned int> computeAllRobotPosPossibilities(const Eigen::Vector2d& targetPos, const Eigen::Vector2d& humanPos, Grid2d& grid);
    std::array<Eigen::Vector2d, 2> computeRobotPos(const Eigen::Vector2d& targetPos, const Eigen::Vector2d& humanPos, float diffAngHR, float dist_proxemics);
    bool addPossibility(const Eigen::Vector2d& humanPos, const Eigen::Vector2d& robotPos, const Eigen::Vector2d& targetPos, std::vector<unsigned int>& robotPossibilities, const Grid2d& grid);
    bool isPossibilityAlreadyInVector(const std::vector<unsigned int>& robotPossibilities, unsigned int posId);

    svp::interface::ParametersInterface *params;
    Grids *grids;
  protected:
    static void _computeCellWithoutHRI(uint id, Planner *svpP, Grid2d *grid, std::atomic_uint *next,
                             Planner::Cell *start, Planner::Cell **best, uint *iter_of_best,
                             std::mutex *best_mtx);

    // Attributes
    svp::Geometry *geometry=nullptr;
    svp::interface::GlobalProjectInterface *globProj=nullptr;
    svp::interface::LoggerInterface *log=nullptr;
    svp::interface::RobotsInterface *robots=nullptr;
    svp::interface::CollisionManagementInterface *coll=nullptr;
    svp::interface::GraphicsInterface *graphics=nullptr;

    std::vector<float> routeDirTimes;
    std::shared_ptr<LinkedBalls2d> balls;
    float robotPerspHeight=1.153;
    float humanPerspHeight=1.7; //< heights of the perspective joints of the agents*/

    // CostGrids
    CostGrid* cGridHumanOnly=nullptr;
    CostGrid* cGridHumanOnlyInvalid=nullptr;
    CostGrid* humanDistHumanOnly=nullptr;
    CostGrid* visibHumanOnly=nullptr;

    CostGrid* rPossibilitiesGrid=nullptr;
    CostGrid* robotDistGrid=nullptr;
    CostGrid* deltaAngHRGridMandatoryTarget=nullptr;
    CostGrid* deltaAngHRGridOptionalTarget=nullptr;
    CostGrid* cGridRobot=nullptr;
    CostGrid* cGridRobotInvalid=nullptr;

    float kDeltaHR=0;
    const float DEG_MAX=4; // Can be modified depending on the parameters used to generate visibilities
};

} // namespace svp
#endif // SVP_PLANNER_HPP
#endif
