#ifndef SVP_PLANNER_MYCOST_HPP
#define SVP_PLANNER_MYCOST_HPP

#include <array>
#include <cmath>
#include <cstdlib>
#include <sstream>

namespace svp
{
template <size_t NCONS, size_t NCOST, typename CONSTRAINTS, typename COSTS> class MyCost
{
  public:
    // Using
    using Constraints = CONSTRAINTS;
    using Costs = COSTS;
    using Type = MyCost<NCONS, NCOST, CONSTRAINTS, COSTS>;

    // Attributes
    std::array<float, NCONS> constraints;
    std::array<float, NCOST> costs;

    // Constructors - Destructors
    MyCost() : costs{}, constraints{} {}

    // Methods
    float &constraint(Constraints c) { return constraints.at((int)c); }

    float &cost(Costs c) { return costs.at((int)c); }

    bool isValid()
    {
        for (float c : constraints)
            if (c > 0.f)
                return false;

        return true;
    }

    bool operator<(const Type &other) const
    {
        return (constraints != other.constraints ? constraints < other.constraints
                                                 : costs < other.costs);
        /*if (constraints != other.constraints)
                return constraints<other.constraints
          else
                return costs<other.costs*/
    }

    bool operator<=(const Type &other) const { return (*this) == other || (*this) < other; }

    bool operator==(const Type &other) const
    {
        return constraints == other.constraints && costs == other.costs;
    }

    double toDouble()
    {
        // Declarations
        double v(0);
        int nconst = constraints.size();
        int ncost = costs.size();

        // Operations
        for (int i = 0; i < nconst; ++i) {
            // assert(constraints[i]<=1.);
            v += constraints[i] * std::pow(10, nconst + ncost - i);
        }

        for (int i = 0; i < ncost; ++i) {
            // assert(costs[i]<=1.);
            v += costs[i] * std::pow(10, ncost - i - 1);
        }

        return v;
    }

    std::string toString()
    {
        // Declarations
        std::ostringstream oss;

        // Operations
        oss << "constr: ";
        for (auto k : constraints)
            oss << k << " ";

        oss << "\ncosts: ";
        for (auto c : costs)
            oss << c << " ";

        oss << "\n =" << toDouble();
        return oss.str();
    }
};

} // namespace svp

#endif // SVP_PLANNER_MYCOST_HPP