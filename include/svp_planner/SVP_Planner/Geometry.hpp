#ifndef SVP_PLANNER_GEOMETRY_HPP
#define SVP_PLANNER_GEOMETRY_HPP

#include "SVP_Interface/ParametersInterface.hpp"
#include "SVP_Planner/Declarations.hpp"

namespace svp
{
    class Geometry
    {
    public:
        // Constructors - Destructors
        Geometry(svp::interface::ParametersInterface* paramsP);

        ~Geometry();

        // Legacy functions
        double angle(const Eigen::Vector2d &v);
        double angle(const Eigen::Vector2d &v1, const Eigen::Vector2d &v2);
        // End legacy functions
        float computeAngle(const Eigen::Vector2d& posA, const Eigen::Vector2d& posB, const Eigen::Vector2d& posC);
        std::vector<float> computeAngles(const Eigen::Vector2d& posHuman, const Eigen::Vector2d& posRobot, const Eigen::Vector2d& posTarget);

        float radToDeg(float valRad);
        float degToRad(float valDeg);

    private:
        svp::interface::ParametersInterface* params=nullptr;
    };

} // namespace svp
#endif // SVP_PLANNER_GEOMETRY_HPP