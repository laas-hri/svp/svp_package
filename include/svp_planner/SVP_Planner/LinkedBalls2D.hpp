#ifndef SVP_PLANNER_LINKEDBALLS2D_HPP
#define SVP_PLANNER_LINKEDBALLS2D_HPP

#include <Eigen/Core>
#include <complex>
#include <iostream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

namespace svp
{
class LinkedBalls2d
{
  public:
    std::string name;
    std::vector<std::pair<float, std::vector<Eigen::Vector2d>>> balls_values;
};
} // namespace svp

#endif // SVP_PLANNER_LINKEDBALLS2D_HPP