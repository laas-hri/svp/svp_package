#ifndef SVP_PLANNER_DECLARATIONS_HPP
#define SVP_PLANNER_DECLARATIONS_HPP

#include "SVP_Planner/MyCell.hpp"
#include "SVP_Planner/MyCost.hpp"
#include <ndimensional_grids/ndgrid/NDGrid.hpp>

namespace svp
{
struct DeclarationsSVP {
    enum class MyConstraints { COL = 0, VIS, DIST, RTIME, ANGLE };
    enum class MyCosts { COST = 0, TIME, VISIB };
    using Cost = MyCost<5, 3, MyConstraints, MyCosts>;
    using Cell = MyCell<Cost>;
    using Grid = typename ndimensional_grids::nDimGrid<Cell *, 4>;
    using Grid2d = typename ndimensional_grids::nDimGrid<Cell *, 2>;
};
} // namespace svp

#endif // SVP_PLANNER_DECLARATIONS_HPP