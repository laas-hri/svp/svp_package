#ifndef SVP_PLANNER_MYCELL_HPP
#define SVP_PLANNER_MYCELL_HPP

#include <Eigen/Core>
#include <ndimensional_grids/ndgrid/NDGrid.hpp>

namespace svp
{
template <typename C> class MyCell
{
  public:
    // Using
    using Grid = typename ndimensional_grids::nDimGrid<MyCell<C> *, 4>;
    using ArrayCoord = typename Grid::ArrayCoord;
    using SpaceCoord = typename Grid::SpaceCoord;
    using CostType = C;

    // Attributes
    bool col;
    bool vis;
    uint target;
    C cost;
    ArrayCoord coord;
    SpaceCoord pos; // pos[0:1] = robot, pos[2:3] = human
    bool open;

    // Constructors - Destructors
    MyCell(ArrayCoord coord, SpaceCoord pos) : cost{}, coord(coord), pos(pos), open(false) {}

    // Operators
    bool operator<(const MyCell<C> &other) const { return (cost < other.cost); }

    // Methods
    inline Eigen::Vector2d getPos(int i) const
    {
        return Eigen::Vector2d(pos[i * 2], pos[i * 2 + 1]);
    }

    inline std::array<float, 2> posRobot() const { return {{pos[0], pos[1]}}; }

    inline std::array<float, 2> posHuman() const { return {{pos[2], pos[3]}}; }

    inline Eigen::Vector2d vPosRobot() const { return Eigen::Vector2d(pos[0], pos[1]); }

    inline Eigen::Vector2d vPosHuman() const { return Eigen::Vector2d(pos[2], pos[3]); }
};

} // namespace svp

#endif // SVP_PLANNER_MYCELL_HPP
