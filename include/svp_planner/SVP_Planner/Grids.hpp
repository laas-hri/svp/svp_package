#ifndef SVP_PLANNER_GRIDS_HPP
#define SVP_PLANNER_GRIDS_HPP

#include "SVP_Interface/VisGrid3DInterface.hpp"
#include <ndimensional_grids/ndgrid/NDGrid.hpp>
#include <ndimensional_grids/ndgrid/NDGridAlgo.hpp>

namespace svp
{
class Grids
{
  public:
    // Attributes
    ndimensional_grids::nDimGrid<bool, 2> freespaceHuman,
        freespaceRobot; ///< cell is true if in free space
    ndimensional_grids::Dijkstra<ndimensional_grids::nDimGrid<bool, 2>, float> distGridH, distGridR,
        distGridPhysicalTarget;
    svp::interface::VisibilityGrid3DInterface *visGrid;
};
} // namespace svp

#endif // SVP_PLANNER_GRIDS_HPP