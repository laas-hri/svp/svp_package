#ifndef SVP_PLANNER_COSTGRID_HPP
#define SVP_PLANNER_COSTGRID_HPP

#include <ndimensional_grids/ndgrid/NDGrid.hpp>
#include <Eigen/Core>

namespace svp
{
    class CostGrid : public ndimensional_grids::nDimGrid<float, 2>
    {
    public:
        CostGrid(SpaceCoord origin, ArrayCoord size, SpaceCoord cellSize);
        CostGrid(ArrayCoord size, std::vector<double> envSize);
        CostGrid(double samplingRate, bool adapt, std::vector<double> envSize);
        CostGrid(SpaceCoord cellSize, bool adapt, std::vector<double> envSize);
        CostGrid();

        void showInfos();
        void initGridValues();
        void showGrid();

        void setValue(unsigned int i, float val);
        void setValue(SpaceCoord spc, float val);
    };
}

#endif // SVP_PLANNER_COSTGRID_HPP

/*
val==-1 => constraints are not respected
val>0 && val<inf => everything OK.
val==inf => this slot is off-limit, no need to compute it
val==0 => cost not computed at all
 */