#ifndef POINTINGPLANNERMODULE_HPP
#define POINTINGPLANNERMODULE_HPP

#include <move4d/API/moduleBase.hpp>
#include "svp_planner_move4d/Completion.hpp"
#include "SVP_Planner/Planner.hpp"
#include "SVP_Planner/Grids.hpp"
#include "svp_planner_move4d/pointingPlanner/PlannerM4D.hpp"

namespace svp
{
//struct PlanningData;

class SVP_PlannerModule : public move4d::ModuleBase
{
    // MOVE3D_STATIC_LOGGER;
    

  public:
  	SVP_PlannerModule();
    static SVP_PlannerModule *__instance;
    static std::string name() { return "SVP_Planner"; }

    void initialize() override;
    void run() override;

  private:
  	Planner* _planner{};
  	PlannerM4D* _plannerM4D{};
	interface::ParametersInterface* _params{};
  	interface::GlobalProjectInterface* _globProj{};
 	interface::RobotsInterface* _robots{};
	interface::TargetsInterface* _targ{};
	interface::VisibilityGrid3DInterface* _visGrid3d{};
	interface::CollisionManagementInterface* _coll{};
 	interface::LoggerInterface* _log{};
 	interface::GlobalProjectInterface* _globProject{};
	interface::GraphicsInterface* _graph{};
 	Grids* _grids{};
};

} // namespace svp

#endif // POINTINGPLANNERMODULE_HPP
