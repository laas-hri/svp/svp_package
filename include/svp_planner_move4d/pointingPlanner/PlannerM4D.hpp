#ifndef SVP_PLANNER_M4DD_HPP
#define SVP_PLANNER_M4DD_HPP

#include "SVP_Planner/Planner.hpp"
#include <move4d/API/Device/robot.hpp>
#include "svp_planner_move4d/visibilityGrid/VisibilityGrid.hpp"
#include "svp_planner_move4d/visibilityGrid/VisibilityGridLoader.hpp"

namespace svp
{
    using namespace move4d;
    class PlannerM4D
    {
    public:
        PlannerM4D(Planner* planP);
        float computeStateVisiblity(move4d::RobotState &state);


    protected:
        std::vector<float> _getVisibilites(move4d::Robot *r, const Eigen::Vector2d &pos2d);

    private:
        Planner* plan=nullptr;
        visibility::VisibilityGrid3Dimensions* visGrid3d=nullptr;
    };
}

#endif // SVP_PLANNER_M4DD_HPP