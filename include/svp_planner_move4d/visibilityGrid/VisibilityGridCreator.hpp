#ifndef SVP_VISIBILITYGRID_VISIBILITYGRIDCREATOR_HPP
#define SVP_VISIBILITYGRID_VISIBILITYGRIDCREATOR_HPP

#include <move4d/API/moduleBase.hpp>
#include <move4d/Logging/Logger.h>

namespace svp{
namespace visibility {
    class VisibilityGrid3Dimensions;

    class VisibilityGridCreator : public move4d::ModuleBase {
        MOVE3D_STATIC_LOGGER;
    protected:
        VisibilityGridCreator();

    public:
        virtual ~VisibilityGridCreator();

        static std::string name() { return "VisibilityGridCreator"; }

        virtual void run() override;

    protected:
        void computeVisibilities(uint nb_threads);

        void writeGridsToFile(const std::string &name);

    private:
        VisibilityGrid3Dimensions *_grid;
        static VisibilityGridCreator *__instance;
    };
} // namespace visibility
} // namespace svp

#endif //SVP_VISIBILITYGRID_VISIBILITYGRIDCREATOR_HPP
