#ifndef SVP_VISIBILITYGRID_VISIBILITYGRID_HPP
#define SVP_VISIBILITYGRID_VISIBILITYGRID_HPP

#include <Eigen/Core>
#include <move4d/API/Grids/TwoDGrid.hpp>
#include <move4d/API/forward_declarations.hpp>
#include <unordered_map>

#include <boost/serialization/array.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <move4d/API/Grids/NDGrid.hpp>

#include <move4d/API/Device/robot.hpp>

namespace svp
{
namespace visibility
{

class VisibilityGrid3Dimensions : public move4d::API::nDimGrid<std::unordered_map<move4d::Robot *, float>, 3>
{
  public:
    VisibilityGrid3Dimensions(SpaceCoord origin, ArrayCoord size, SpaceCoord cellSize);
    VisibilityGrid3Dimensions(ArrayCoord size, std::vector<double> envSize);
    VisibilityGrid3Dimensions(double samplingRate, bool adapt, std::vector<double> envSize);
    VisibilityGrid3Dimensions(SpaceCoord cellSize, bool adapt, std::vector<double> envSize);
    VisibilityGrid3Dimensions();

    void merge(const std::map<move4d::Robot *, move4d::API::nDimGrid<float, 3>> &grids);
    void add(const std::string &robotName, std::vector<float> &values);

    std::map<move4d::Robot *, move4d::API::nDimGrid<float, 3>> split() const;
    move4d::API::nDimGrid<float, 3> computeGridOf(move4d::Robot *r) const;

    float getVisibility(move4d::Robot *agent, Eigen::Vector2d &pos2d, move4d::Robot *target);
    reference getCell(move4d::Robot *agent, const Eigen::Vector2d &pos2d);
    using move4d::API::nDimGrid<std::unordered_map<move4d::Robot *, float>, 3>::getCell;

  protected:
    friend class boost::serialization::access;
    template <class Archive> void serialize(Archive &ar, const unsigned int version)
    {
        boost::serialization::split_member(ar, *this, version);
    }

    template <class Archive> void load(Archive &ar, const unsigned int version)
    {
        ar >> boost::serialization::make_array(m_nbOfCell.data(), m_nbOfCell.size());
        ar >> boost::serialization::make_array(m_cellSize.data(), m_cellSize.size());
        ar >> boost::serialization::make_array(m_originCorner.data(), m_originCorner.size());

        this->values_.assign(static_cast<uint>(m_nbOfCell[0]) * m_nbOfCell[1] * m_nbOfCell[2],
                             value_type());
        ulong n_rob;

        ar >> n_rob;
        for (ulong i = 0; i < n_rob; ++i) {
            std::string name;
            ar >> name;
            std::vector<float> values;
            ar >> values;
            this->add(name, values);
        }
    }
    template <class Archive> void save(Archive &ar, const unsigned int version) const
    {
        ar << boost::serialization::make_array(m_nbOfCell.data(), m_nbOfCell.size());
        ar << boost::serialization::make_array(m_cellSize.data(), m_cellSize.size());
        ar << boost::serialization::make_array(m_originCorner.data(), m_originCorner.size());

        if (this->getNumberOfCells()) {
            ulong nb_rob = values_[0].size();
            ar << nb_rob; // nb of robots
            for (auto it : values_[0]) {
                std::string name = it.first->getName();
                ar << name;
                std::vector<float> values = computeGridOf(it.first).getValues();
                ar << values;
            }
        }
    }
};
} // namesapce visibility
} // namespace svp

#endif // SVP_VISIBILITYGRID_VISIBILITYGRID_HPP
