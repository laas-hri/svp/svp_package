#ifndef SVP_VISIBILITYGRID_VISIBILITYGRIDLOADER_HPP
#define SVP_VISIBILITYGRID_VISIBILITYGRIDLOADER_HPP

#include <move4d/API/moduleBase.hpp>
#include <move4d/Logging/Logger.h>

namespace svp
{
namespace visibility
{

class VisibilityGrid3Dimensions;

class VisibilityGridLoader : public move4d::ModuleBase
{
    MOVE3D_STATIC_LOGGER;

  //protected:
    

  public:
    VisibilityGridLoader();
    ~VisibilityGridLoader() override;
    static std::string name() { return "VisibilityGridLoader"; }

    void initialize() override;

    VisibilityGrid3Dimensions *grid() const;
    static VisibilityGridLoader __instance;

  private:
    bool loadBinary();
    bool loadText();
    VisibilityGrid3Dimensions *_grid = nullptr;
    
};
} // namespace visibility
} // namespace move4d

#endif // SVP_VISIBILITYGRID_VISIBILITYGRIDLOADER_HPP
