// MOVE4D IMPLEMENTATION

#ifndef COMPLETION_HPP
#define COMPLETION_HPP

// Interfaces
#include "svp_planner/SVP_Interface/GlobalProjectInterface.hpp"
#include "svp_planner/SVP_Interface/LoggerInterface.hpp"
#include "svp_planner/SVP_Interface/ParametersInterface.hpp"
#include "svp_planner/SVP_Interface/RobotsInterface.hpp"
#include "svp_planner/SVP_Interface/VisGrid3DInterface.hpp"
#include "svp_planner/SVP_Interface/TargetsInterface.hpp"
#include "svp_planner/SVP_Interface/CollisionManagementInterface.hpp"
#include "svp_planner/SVP_Interface/GraphicsInterface.hpp"

// Geometry
#include <move4d/utils/Geometry.h>

// GlobalProject
#include <move4d/API/project.hpp>
#include <move4d/planner/cost_space.hpp>

// Parameters
#include <move4d/API/Parameter.hpp>

// RobotsInterface
#include <move4d/API/Device/hriAgent.hpp>
#include <move4d/API/Device/robot.hpp>
#include <move4d/HRI_costspace/HRICS_Modules.hpp>
#include <move4d/HRI_costspace/HRICS_costspace.hpp>

// VisibilityGrid3D
#include <svp_planner_move4d/visibilityGrid/VisibilityGrid.hpp>
#include <svp_planner_move4d/visibilityGrid/VisibilityGridLoader.hpp>

// Collision
#include <move4d/API/Collision/collisionInterface.hpp>
#include <move4d/API/Collision/CylinderCollision.hpp>

// Graphics
#include <move4d/API/Graphic/DrawablePool.hpp>
#include <move4d/API/Grids/NDGrid.hpp>

using namespace move4d;

namespace svp
{
namespace move4d_if
{
// GlobalProject
class GlobalProject : public interface::GlobalProjectInterface
{
  public:
    void init() override
    {
    }

    int getNumberOfJobs() override { return global_Project->getNumberOfJobs(); }

    std::vector<double> getActiveSceneBounds() override
    {
        return global_Project->getActiveScene()->getBounds();
    }

    void setEnvIsRunning(bool val) override { ENV.setBool(Env::isRunning, val); }

    void clearCostDetails() override
    {
        global_costSpace->setCostDetails(std::map<std::string, double>{});
    }

    void setCostDetails(const std::map<std::string, double> &m) override
    {
        global_costSpace->setCostDetails(std::move(m));
    }

    std::map<std::string, double> getCostDetails() override
    {
        return global_costSpace->getCostDetails();
    }

    void updateOtherVisibleResults(const std::string &targetName) override
    {
        API::Parameter::lock_t lock;
        API::Parameter &otherVisParam =
        API::Parameter::root(lock)["PointingPlanner"]["result"]["other_visible"];
        otherVisParam=API::Parameter(API::Parameter::ArrayValue);

        otherVisParam.append(targetName);
    }

    void updateTargetResults(const std::string &targetName) override
    {
        API::Parameter::lock_t lock;
        API::Parameter &targetParam = API::Parameter::root(lock)["PointingPlanner"]["result"]["target"];
        targetParam=API::Parameter(API::Parameter::ArrayValue);

        targetParam.append(targetName);
    }
};

// Logger
class Logger : public interface::LoggerInterface
{
  public:
    // YOU NEED TO DECLARE ATTRIBUTES USED FOR THE LOGGER HERE
    MOVE3D_STATIC_LOGGER;
    // INIT_MOVE3D_STATIC_LOGGER(Logger,"move4d.visibilitygrid.pointingplanner.data");

    void initLogger() override {}

    void sendInfoMsg(const std::istringstream &msg) override
    {
        // M3D_INFO(msg);
    }

    void sendDebugMsg(const std::istringstream &msg) override
    {
        // M3D_DEBUG(msg);
    }

    void sendErrorMsg(const std::istringstream &msg) override
    {
        // M3D_ERROR(msg);
    }

    void sendTraceMsg(const std::istringstream &msg) override
    {
        // M3D_TRACE(msg);
    }
};

// Parameters
class Parameters : public interface::ParametersInterface
{
    void initParameters(interface::LoggerInterface *pLog, interface::TargetsInterface *pTargets) override
    {
        log = pLog;
        targets=pTargets;
        targets->clearTargets();
    }
    
    void fetchParameters() override
    {
        // Declarations
        unsigned int i(0);
        API::Parameter::lock_t lock;
        Robot *t;

        // Operations
        std::cout<<"Parameters::fetchParameters start"<<std::endl;
        // Try_no_move
        setPlannerParametersValue(
            PlannerParametersNames::mobrob,
            API::Parameter::root(lock)["PointingPlanner"]["mobrob"].asDouble());

        setPlannerParametersValue(
            PlannerParametersNames::mobhum,
            API::Parameter::root(lock)["PointingPlanner"]["mobhum"].asDouble());

        // Vis
        setPlannerParametersValue(
                PlannerParametersNames::vis_threshold,
                API::Parameter::root(lock)["PointingPlanner"]["vis_threshold"].asDouble());

        setPlannerParametersValue(
                PlannerParametersNames::kvisib,
                API::Parameter::root(lock)["PointingPlanner"]["kvisib"].asDouble());

        // Dist
        setPlannerParametersValue(
                PlannerParametersNames::maxdist,
                API::Parameter::root(lock)["PointingPlanner"]["maxdist"].asDouble());

        setPlannerParametersValue(
                PlannerParametersNames::kdist,
                API::Parameter::root(lock)["PointingPlanner"]["kdist"].asDouble());

        // Prox
        setPlannerParametersValue(
                PlannerParametersNames::proxmin,
                API::Parameter::root(lock)["PointingPlanner"]["proxmin"].asDouble());

        setPlannerParametersValue(
                PlannerParametersNames::proxmax,
                API::Parameter::root(lock)["PointingPlanner"]["proxmax"].asDouble());

        // Angle
        setPlannerParametersValue(
                PlannerParametersNames::desired_angle_deltahr,
                API::Parameter::root(lock)["PointingPlanner"]["desired_angle_deltahr"].asDouble());
        setPlannerParametersValue(
                PlannerParametersNames::desired_angle_deltahr_tolerance,
                API::Parameter::root(lock)["PointingPlanner"]["desired_angle_deltahr_tolerance"].asDouble());

        // Human
        if (API::Parameter::root(lock)["PointingPlanner"].hasKey("human")) {
            std::string human_name =
                API::Parameter::root(lock)["PointingPlanner"]["human"].asString();
            Robot *h = global_Project->getActiveScene()->getRobotByName(human_name);
            if (h == nullptr) {
                std::cout<<"ERROR: human not found ("<<human_name<<std::endl;
                // log->sendErrorMsg("ERROR: human not found ("<<human_name);
            }

        } else {
            std::cout<<"ERROR: no human set"<<std::endl;
            // log->sendErrorMsg("no human set");
        }

        // Target(s)
        API::Parameter &ptargets = API::Parameter::root(lock)["PointingPlanner"]["targets"];
        std::cout<<"Found "<< ptargets.size() <<" target(s)"<<std::endl;
        targets->clearTargets();
        for(i=0; i<ptargets.size(); ++i)
        {
            t=global_Project->getActiveScene()->getRobotByName(ptargets[i].asString());
            if(!t)
            {
                /*log->sendErrorMsg("no object with name "<<ptargets[i].asString()<<" known to be
           set as a pointing target"); throw API::unknown_robot(ptargets[i].asString());*/
                std::cout<<"no object with name "<<ptargets[i].asString()<<" known to be set as a pointing target\n";
            }
            else
            {
                 std::cout<<"Target "<<i<<" is named "<<t->getName()<<std::endl;

                //log->sendErrorMsg("target: "<<t->getName());
                targets->addTarget(t->getName(), t->getJoint(0)->getVectorPos(), m3dGeometry::getConfBase2DPos(*t->getCurrentPos()));
            }
            setIndexFirstOptionalTarget(targets->getTargetsNumber());

            API::Parameter &poptTargets = API::Parameter::root(lock)["PointingPlanner"]["optional_targets"]; 
            std::cout<<"Found "<< poptTargets.size() <<" optional target(s)"<<std::endl;
            for(i=0; i<poptTargets.size(); ++i)
            {
                t=global_Project->getActiveScene()->getRobotByName(poptTargets[i].asString());
                
                if(!t)
                {
                    /*log->sendErrorMsg("no object with name "<<poptTargets[i].asString()<<" known to
           be set as a pointing target"); throw API::unknown_robot(ptargets[i].asString());*/
                    std::cout<<"no object with name "<<poptTargets[i].asString()<<" known to be set as a pointing target\n";
                }
                else
                {
                    std::cout<<"Optional target "<<i<<" is named "<<t->getName()<<std::endl;
                    //log->sendDebugMsg("optional target: "<<t->getName());
                    targets->addTarget(t->getName(), t->getJoint(0)->getVectorPos(), m3dGeometry::getConfBase2DPos(*t->getCurrentPos()));
                }
            }
        }

        std::cout<<"DEBUG Parameters::fetchParameters firstOptionalTarget="<<getIndexFirstOptionalTarget()<<std::endl;
        std::cout<<"Parameters::fetchParameters end"<<std::endl;
    }
};

// Robots
class Robots : public interface::RobotsInterface
{
  private:
    // Declarations
    Robot *human{};
    Robot *robot{};

  public:
    // Operations

    // Init
    void initRobots() override
    {
        human = global_Project->getActiveScene()->getRobotByNameContaining("HUMAN");
        robot = global_Project->getActiveScene()->getActiveRobot();
    }

    // Name
    std::string getHumanName() override { return human->getName(); }

    std::string getRobotName() override { return robot->getName(); }

    // StartPos
    Eigen::Vector2d getStartPosHuman() override
    {
        return m3dGeometry::getConfBase2DPos(*human->getInitialPosition());
    }

    Eigen::Vector2d getStartPosRobot() override
    {
        return m3dGeometry::getConfBase2DPos(*robot->getInitialPosition());
    }

    void setStartPosHuman(float xPos, float yPos) override
    {
        RobotState humanState=*human->getInitialPosition();
        humanState[6]=xPos;
        humanState[7]=yPos;
        human->setInitialPosition(humanState);
    }

    void setStartPosRobot(float xPos, float yPos) override
    {
        RobotState robotState=*robot->getInitialPosition();
        robotState[6]=xPos;
        robotState[7]=yPos;
        robot->setInitialPosition(robotState);
    }

    // Pos
    void setHumanPosition(const Cell &cell) override
    {
        // Declarations
        RobotState humanState;
        unsigned int i(0);

        // Operations
        humanState = *human->getCurrentPos();

        for (i = 0; i < 2; ++i) {
            humanState[6 + i] = cell.posHuman()[i];
        }
        human->setAndUpdate(humanState);
    }

    void setRobotPosition(const Cell &cell) override
    {
        // Declarations
        RobotState robotState;
        unsigned int i(0);

        // Operations
        robotState = *robot->getCurrentPos();

        for (i = 0; i < 2; ++i) {
            robotState[6 + i] = cell.posRobot()[i];
        }
        robot->setAndUpdate(robotState);
    }

    // Cost
    void setHumanCost(Cell *cell) override
    {
        // Declarations
        RobotState humanState;

        // Operations
        humanState = *human->getCurrentPos();
        humanState.setCost(cell->cost.toDouble());
        int val=human->setAndUpdate(humanState);
    }

    void setRobotCost(Cell *cell) override
    {
        // Declarations
        RobotState robotState;

        // Operations
        robotState = *robot->getCurrentPos();
        robotState.setCost(cell->cost.toDouble());
        int val=robot->setAndUpdate(robotState);
    }

    double getHumanCost() override { return human->getCurrentPos()->getCost(); }

    double getRobotCost() override { return robot->getCurrentPos()->getCost(); }

    // State Value
    void setHumanState(unsigned int state_id, float value) override
    {
        RobotState humanState = *human->getCurrentPos();
        humanState[state_id] = value;
        int val=human->setAndUpdate(humanState);
    }

    void setRobotState(unsigned int state_id, float value) override
    {
        RobotState robotState = *robot->getCurrentPos();
        robotState[state_id] = value;
        int val=robot->setAndUpdate(robotState);
    }

    float getHumanState(unsigned int state_id) override
    {
        return human->getCurrentPos()->at(state_id, 0);
    }

    float getRobotState(unsigned int state_id) override
    {
        return robot->getCurrentPos()->at(state_id, 0);
    }

    // Perspective Vector Pos
    Eigen::Vector3d getHumanPerspectiveVectorPos() override
    {
        return human->getHriAgent()->perspective->getVectorPos();
    }

    Eigen::Vector3d getRobotPerspectiveVectorPos() override
    {
        return robot->getHriAgent()->perspective->getVectorPos();
    }

    // Perspective Matrix Pos
    Eigen::Affine3d getHumanPerspectiveMatrixPos() override
    {
        return human->getHriAgent()->perspective->getMatrixPos();
    }

    Eigen::Affine3d getRobotPerspectiveMatrixPos() override
    {
        return robot->getHriAgent()->perspective->getMatrixPos();
    }
};

// VisibilityGrid3D
class VisibilityGrid3D : public interface::VisibilityGrid3DInterface
{
  public:
    VisibilityGrid3D(interface::ParametersInterface* paramsP): interface::VisibilityGrid3DInterface(paramsP)
    {}
    // Methods
    void initGrid() override
    {
    }

    void loadGrid() override
    {
        visibilityGrid = dynamic_cast<svp::visibility::VisibilityGridLoader *>(
                             ModuleRegister::getInstance()->module(
                                 svp::visibility::VisibilityGridLoader::name()))
                             ->grid();
    }

    SpaceCoord getCellSize() override { return visibilityGrid->getCellSize(); }

    ArrayCoord getCellCoord(unsigned int i) override { return visibilityGrid->getCellCoord(i); }

    ArrayCoord getCellCoord(SpaceCoord spc) override { return visibilityGrid->getCellCoord(spc); }

    SpaceCoord getCellCenter(ArrayCoord arc) override { return visibilityGrid->getCellCenter(arc); }

    unsigned int getNumberOfCells() override {return visibilityGrid->getNumberOfCells();}

    std::array<float, 3> getOriginCorner() override {return visibilityGrid->getOriginCorner();}

    std::vector<float> getCellVisibilities(SpaceCoord spc) override
    {
        // Declarations
        std::vector<float> visib={};
        Robot *target = nullptr;
        
        // Operations
        svp::visibility::VisibilityGrid3Dimensions::reference cell = visibilityGrid->getCell(spc);
        try
        {
            for (unsigned int i = 0; i < params->targets->getTargetsNumber(); i++)
            {
                target = global_Project->getActiveScene()->getRobotByName(params->targets->getTargetName(i));
                visib.push_back(1.f-cell[target]);
            }
        } catch(svp::visibility::VisibilityGrid3Dimensions::out_of_grid &){
            std::cout<<"OUT OF GRID, assigning standard values"<<std::endl;
            visib.assign(params->targets->getTargetsNumber(),1.f);
        }
        return visib;
    }

    svp::visibility::VisibilityGrid3Dimensions *visibilityGrid;
};

// CollisionManagement
class CollisionManagement : public interface::CollisionManagementInterface
{
public:
    CollisionManagement()=default;

    void initCollisions() override
    {
        API::CollisionInterface *coll=global_Project->getCollision();
        assert(coll);
        cylinderColl=new API::CylinderCollision(coll);
    }

    void getCollision() override
    {
        cylinderColl= new API::CylinderCollision(global_Project->getCollision());
    }

    bool moveCheckCollisions(bool hr, Eigen::Vector3d pos) override // hr==false => Human, hr==true => Robot
    {
        Robot* actor;

        if (!hr)
            actor = global_Project->getActiveScene()->getRobotByNameContaining("HUMAN");
        else
            actor = global_Project->getActiveScene()->getActiveRobot();

        return cylinderColl->moveCheck(actor, pos, API::CollisionInterface::CollisionChecks(API::CollisionInterface::COL_ENV | API::CollisionInterface::COL_OBJECTS));
    }

protected:
    API::CylinderCollision* cylinderColl;
};

// GraphicsInterface
class Graphics: public interface::GraphicsInterface
{
public:
    Graphics()=default;

    void initGraphics()
    {
    }

    void add2DGrid(const ndimensional_grids::nDimGrid<float, 2>& grid2d, const std::string& name)
    {
        std::cout<<"void show2DGrid(const ndimensional_grids::nDimGrid<float, 2>& grid2d) not implemented, can't be used"<<std::endl;
    }

    void add2DGrid(const CostGrid& costGrid2d, const std::string& name)
    {
        // Convert grid to appropriate format
        move4d::API::nDimGrid<float, 3>::SpaceCoord gridOrigin={costGrid2d.getOriginCorner()[0], costGrid2d.getOriginCorner()[1], 0.f};
        move4d::API::nDimGrid<float, 3>::ArrayCoord gridSize={costGrid2d.shape()[0], costGrid2d.shape()[1], 1};
        move4d::API::nDimGrid<float, 3>::SpaceCoord gridCellSize={costGrid2d.getCellSize()[0], costGrid2d.getCellSize()[1], 0.1};
        move4d::API::nDimGrid<float, 3> grid2d(gridOrigin, gridSize, gridCellSize);

        if (grid2d.getNumberOfCells()==costGrid2d.getNumberOfCells())
        {
            for (unsigned int i=0; i<costGrid2d.getNumberOfCells(); i++)
            {
                if (costGrid2d[i]>std::numeric_limits<float>::max())
                    (float&)grid2d[i]=-10;
                else
                    (float&)grid2d[i]=costGrid2d[i];
            }

            std::cout<<"Values added to grid"<<std::endl;
        }
        else
        {
            std::cout<<"Error, can't add values to Grid"<<std::endl;
        }

        // Add Grid
        Graphic::DrawablePool* dPoolInst=Graphic::DrawablePool::getInstance();
        if (dPoolInst)
        {
            if (dPoolInst->sAddGrid3Dfloat(std::shared_ptr<Graphic::Grid3Dfloat>(new Graphic::Grid3Dfloat{name, grid2d, true})))
                std::cout<<"Graphics::add2DGrid "<<name<<" ADD OK"<<std::endl;

            else
                std::cout<<"Graphics::add2DGrid "<<name<<" ADD FAILURE"<<std::endl;
        }
        else
            std::cout<<"ERROR No Graphics::DrawablePool instance found"<<std::endl;
    }
};

} // namespace move4d_if
} // namespace svp
#endif // COMPLETION_HPP
