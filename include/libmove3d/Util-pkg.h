/* 
 *  This file includes the prototypes of the functions defined in the
 *  directory util and the files defining the structure appearing in 
 *  these prototypes.
 */

#ifndef UTIL_PKG_H
#define UTIL_PKG_H

/* struct */
#include "libmove3d/p3d_sys.h"
#include "libmove3d/dyna_list.h"
#include "libmove3d/ebt.h"
#include "libmove3d/stat.h"

/* globals */


/* proto */
#include "libmove3d/util/proto/util_proto.h"

#endif /* #ifndef UTIL_PKG_H */

