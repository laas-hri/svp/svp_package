/*
 *  This file includes the prototypes of the functions defined in the
 *  directory graspPlanning and the files defining the structure appearing in
 *  these prototypes.
 */
#ifdef GRASP_PLANNING
#ifndef GRASP_PLANNING_PKG_H
#define GRASP_PLANNING_PKG_H

#include "libmove3d/p3d_sys.h"
#include "libmove3d/p3d_type.h"
#include "libmove3d/p3d_matrix.h"
#include "libmove3d/p3d.h"


#include "libmove3d/graspPlanning/include/graspPlanning.h"
#include "libmove3d/graspPlanning/include/gpContact.h"
#include "libmove3d/graspPlanning/include/gpGrasp.h"
#include "libmove3d/graspPlanning/include/gp_volInt.h"
#include "libmove3d/graspPlanning/include/gpKdTree.h"
#include "libmove3d/graspPlanning/include/gpConvexHull.h"
#include "libmove3d/graspPlanning/include/gp_grasp_io.h"
#include "libmove3d/graspPlanning/include/gpWorkspace.h"
#include "libmove3d/graspPlanning/include/gpPlacement.h"

#include "libmove3d/graspPlanning/proto/gp_grasping_utils_proto.h"
#include "libmove3d/graspPlanning/proto/gp_grasp_generation_proto.h"
#include "libmove3d/graspPlanning/proto/gp_extensionsM3D_proto.h"
#include "libmove3d/graspPlanning/proto/gp_force_closure_proto.h"
#include "libmove3d/graspPlanning/proto/gp_geometry_proto.h"
#include "libmove3d/graspPlanning/proto/gp_inertia_axes_proto.h"
#include "libmove3d/graspPlanning/proto/gp_volInt_proto.h"
#include "libmove3d/graspPlanning/proto/FormgraspPlanning_proto.h"



#endif
#endif
