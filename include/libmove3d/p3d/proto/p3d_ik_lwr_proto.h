

extern int ikLWRArmSolverUnique(double fixedAngle, double posArray[4][4], double phiArray[7], int solution);

extern int ikLWRArmSolver(double fixedAngle, double posArray[4][4], double phiArray[8][7], int valid[8]);
