#ifndef P3D_COPY_ROBOT_H
#define P3D_COPY_ROBOT_H

p3d_rob* copyRobStructure(p3d_rob* robotPt);
void deleteRobStructure(p3d_rob* robotPt);

#endif // P3D_ROBOT_H
