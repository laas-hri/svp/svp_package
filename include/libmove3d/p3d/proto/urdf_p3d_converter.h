#ifdef USE_ROS_URDF
#include <urdf/model.h>
namespace urdf{
typedef Model URDFModel;
}
#else
#include <robotModelParser/urdf_model.h>
#endif

/**
 * \file urdf_p3d_converter.h
 * \brief Fonction qui parcourt une structure URDF C++ et crée un modèle p3d associé.
 * \author F. Lancelot
 * \version 0.1
 * \date 26 août 2011
 *
 * Programme qui parcourt une structure URDF C++ et crée un modèle p3d associé.
 * Le modèle URDF ne semble pas proposer de structures pour sauvegarder les vertices et indices.
 *
 */


int urdf_p3d_converter(const std::string &path, urdf::URDFModel* model, char* modelName, double scale);

int blender_p3d_import(const std::string &path, double scale);
