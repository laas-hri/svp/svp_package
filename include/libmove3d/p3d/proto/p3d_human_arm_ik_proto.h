/*
 *   This file was automatically generated by version 1.7 of cextract.
 *   Manual editing not recommended.

 */
#ifndef __CEXTRACT__

extern void get_translation ( p3d_matrix4 M, p3d_vector3 p );
extern double angle_normal ( double x );
extern double norm ( p3d_vector3 v );
extern double iszero ( float x );
extern int solve_trig1 ( double a, double b, double c, double theta[2] );
extern void rmatmult ( p3d_matrix4 A, p3d_matrix4 B, p3d_matrix4 C );
extern void invertrmatrix ( p3d_matrix4 N, p3d_matrix4 M );
extern void hmatmult ( p3d_matrix4 A, p3d_matrix4 B, p3d_matrix4 C );
extern void crossproduct ( p3d_vector3 r, p3d_vector3 a, p3d_vector3 b );
extern void rotation_principal_axis_to_matrix ( char axis, double angle, p3d_matrix4 m );
extern void make_frame ( p3d_vector3 p, double p_scale, p3d_vector3 q, p3d_matrix4 R, int invert );
extern void evalcircle ( double angle, p3d_vector3 p );
extern int law_of_cosines ( double *angle, double a, double b, double c );
extern double get_circle_equation ( p3d_vector3 ee, p3d_vector3 axis, p3d_vector3 pos_axis, double upper_len, double lower_len, p3d_vector3 c, p3d_vector3 u, p3d_vector3 v, p3d_vector3 n );
extern int solve_R_angle ( p3d_vector3 g, p3d_vector3 s, p3d_vector3 t, p3d_matrix4 TT, double *angl );
extern int SetGoal ( p3d_matrix4 GG, double *rangle );
extern void solve_R1 ( p3d_vector3 p, p3d_vector3 q, p3d_vector3 p2, p3d_vector3 q2, double p_scale, p3d_matrix4 R1, int orientacio );
extern void SolveR1 ( double angle, p3d_matrix4 R1, int orientacio );
extern void SolveR1R2 ( double rangle, p3d_matrix4 R1, p3d_matrix4 R2, int orientacio );
extern int compute_angles ( double angles_R1[3], double angles_R2[3], int orientacio );
extern void initialization ( p3d_matrix4 T1, p3d_matrix4 T2, p3d_vector3 a, p3d_vector3 p );
extern int compute_inverse_kinematics_R7_human_arm ( double *q, p3d_matrix4 Tgrip, p3d_matrix4 Tbase, p3d_matrix4 Tdiff, double s_angle, double dis1, double dis2, int orientacio );

#endif /* __CEXTRACT__ */
