/*
 *   This file was automatically generated by version 1.7 of cextract.
 *   Manual editing not recommended.

 */
#ifndef __CEXTRACT__

extern int p3d_get_poly_entity_type ( p3d_poly *p );
extern p3d_primitive *p3d_get_poly_primitive_data ( p3d_poly *p );
extern int p3d_get_poly_color ( p3d_poly *p );
extern void p3d_get_poly_pt ( p3d_poly *p, int i, double *x, double *y, double *z );
extern void p3d_get_box_obj ( p3d_obj *o, double *x1, double *x2, double *y1, double *y2, double *z1, double *z2 );
extern void p3d_get_BB_obj ( p3d_obj *o, double *x1, double *x2, double *y1, double *y2, double *z1, double *z2 );
void p3d_get_oriented_BB_vertices(p3d_obj *o,double vertices[8][3]);
extern p3d_oriented_BB *p3d_get_oriented_BB_obj(p3d_obj *o);
extern void p3d_get_BB_rob ( p3d_rob *r, double *x1, double *x2, double *y1, double *y2, double *z1, double *z2 );
extern void p3d_get_BB_rob_max_size ( p3d_rob *r, double *maxsize );
extern void p3d_get_env_size ( double *x, double *y, double *z );
extern void p3d_get_env_box ( double *x1, double *x2, double *y1, double *y2, double *z1, double *z2 );
extern double p3d_get_env_graphic_dmax ( void );
extern double p3d_get_env_object_tolerance ( void );
extern double p3d_get_env_dmax ( void );
extern int p3d_get_obstacle_npt ( int i );
extern int p3d_get_obstacle_npoly ( void );
extern void p3d_get_obstacle_pt ( int num, int i, double *x, double *y, double *z );
extern void p3d_get_obstacle_box ( double *x1, double *x2, double *y1, double *y2, double *z1, double *z2 );
extern p3d_obj *p3d_get_obst_by_name ( char *name );
extern double p3d_get_this_robot_radius ( pp3d_rob r );
extern double p3d_get_robot_radius ( void );
extern void p3d_get_robot_box ( double *x1, double *x2, double *y1, double *y2, double *z1, double *z2, double *t1, double *t2, double *u1, double *u2, double *v1, double *v2 );
extern void p3d_get_robot_box_deg ( double *x1, double *x2, double *y1, double *y2, double *z1, double *z2, double *t1, double *t2, double *u1, double *u2, double *v1, double *v2 );
extern void p3d_get_robot_pos ( double *q );
extern void p3d_get_robot_pos_deg ( double *q );
extern void p3d_get_robot_pos(p3d_rob * r, double *q);
extern int p3d_get_robot_njnt ( void );
extern char *p3d_get_robot_name ( void );
extern p3d_rob *p3d_get_current_robot( void );
extern int p3d_get_robot_jnt_type ( int i );
extern void p3d_get_robot_jnt ( int i, double *val );
extern void p3d_get_robot_jnt_deg ( int i, double *val );
extern int p3d_get_robot_ndof ( void );
extern void p3d_get_robot_dof ( p3d_rob * robotPt, int i, double *val );
extern void p3d_get_robot_dof_VelocityMax(p3d_rob * robotPt, int i, double *val);
extern void p3d_get_robot_dof_AccelerationMax(p3d_rob * robotPt, int i, double *val);
extern void p3d_get_robot_dof_deg ( pp3d_rob robotPt, int i, double *val );
extern void p3d_get_robot_dof_bounds ( p3d_rob * robotPt, int i, double *vmin, double *vmax );
extern void p3d_get_robot_dof_rand_bounds ( p3d_rob * robotPt, int i, double *vmin, double *vmax );
extern void p3d_get_robot_dof_bounds_deg ( p3d_rob * robotPt, int i, double *vmin, double *vmax );
extern void p3d_get_robot_config_into ( p3d_rob* robotPt, configPt * config );
extern void p3d_get_robot_config_deg_into ( p3d_rob* robotPt, configPt * config );
extern configPt p3d_get_robot_config ( p3d_rob* robotPt );
extern configPt p3d_get_robot_config_deg ( p3d_rob* robotPt );
extern configPt p3d_get_robot_min_config ( p3d_rob* robotPt );
extern configPt p3d_get_robot_max_config ( p3d_rob* robotPt );
extern void p3d_get_robot_jnt_rad ( int i, double *val );
extern void p3d_get_robot_jnt_bounds ( int i, double *vmin, double *vmax );
extern void p3d_get_robot_jnt_rand_bounds ( int i, double *vmin, double *vmax );
extern void p3d_get_robot_jnt_bounds_deg ( int i, double *vmin, double *vmax );
extern int p3d_get_body_npt ( int i );
extern int p3d_get_body_npoly ( void );
extern void p3d_get_body_pt ( int num, int i, double *x, double *y, double *z );
extern void p3d_get_body_box ( double *x1, double *x2, double *y1, double *y2, double *z1, double *z2 );
extern p3d_obj *p3d_get_body_by_name ( char *name );
extern int p3d_get_parent_jnt_by_name ( char *name );
extern int p3d_get_rob_nid_by_name ( char *name );
extern int p3d_get_body_nid_by_name ( char *name );
extern int p3d_get_traj_ncourbes ( void );
extern double p3d_get_cur_traj_rangeparam ( void );
extern void p3d_get_plane_normalv_in_world_pos ( p3d_poly *p, poly_index face_index, p3d_vector3 normv );
extern p3d_rob* p3d_get_robot_by_name(const char *name);
extern p3d_rob* p3d_get_robot_by_name_containing(const char *name);
extern int p3d_get_robot_jnt_index_by_name( p3d_rob* robotPt, const char *name );
extern p3d_jnt* p3d_get_robot_jnt_by_name(p3d_rob* robot, const char *name);
extern p3d_obj * p3d_get_robot_body_by_name(p3d_rob* robot, const char *name);
extern int p3d_get_first_joint_pose(p3d_rob *robotPt, p3d_matrix4 pose);
extern int p3d_get_body_pose(p3d_rob *robotPt, int index, p3d_matrix4 pose);
extern int p3d_get_freeflyer_pose(p3d_rob *robotPt, p3d_matrix4 pose);
extern int p3d_get_freeflyer_pose2(p3d_rob *robotPt, double *x, double *y, double *z, double *rx, double *ry, double *rz);
extern int p3d_get_collision_tolerance_inhibition(p3d_rob *robotPt);
extern const char* p3d_get_localpath_type(p3d_localpath_type lp_type);
extern configPt p3d_get_random_bonebreaker_conf_for_given_grasp(p3d_rob *robotPt,p3d_rob* objectPt,p3d_matrix4 Matt);
extern configPt p3d_get_random_bonebreaker_grasp(p3d_rob *robotPt,p3d_rob* objectPt,double object_ray);
#endif /* __CEXTRACT__ */
