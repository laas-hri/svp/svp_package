/* 
 *  This file includes the prototypes of the functions defined in the
 *  directory graphic and the files defining the structure appearing in 
 *  these prototypes.
 */

#ifndef GRAPHIC_PKG_H
#define GRAPHIC_PKG_H

#ifdef UNIX
#endif

/* struct */
#include "libmove3d/p3d_sys.h"
#include "libmove3d/p3d_type.h"
#include "libmove3d/p3d_matrix.h"
#include "libmove3d/polyhedre.h"
#include "libmove3d/p3d_poly.h"
#include "libmove3d/p3d.h"

#include "libmove3d/traj.h"
#include "libmove3d/localpath.h"
#include "libmove3d/device.h"

#include "libmove3d/g3d_states.h"
#include "libmove3d/g3d_window.h"

#if defined( MOVE3D_QT_LIBRARY )
#include "libmove3d/graphic/proto/g3d_newWindow.hpp"
#include <iostream>
#endif

#ifndef G3D_FONT_FILE_PATH
#define G3D_FONT_FILE_PATH "/usr/share/fonts/truetype/ttf-dejavu/DejaVuSansMono-Bold.ttf"
#endif
typedef void(*g3d_callback)();

/// add a callback to be executed when g3d_draw() is called
/// @warning the order of execution is not guaranteed. If this is an issue, need to fix g3d_del_draw_env_callback function
extern void g3d_add_draw_env_callback(g3d_callback CB);
/// remove a callback added with g3d_add_draw_env_callback
/// @warning this will break order of execution of the callbacks
extern void g3d_del_draw_env_callback(g3d_callback CB);
extern g3d_callback *g3d_get_draw_env_callbacks(int *nb);

/* QT OpenGL*/
extern void g3d_draw_env(int opengl_context);
extern void g3d_draw_object_moved(p3d_obj *o, int coll, G3D_Window* win, int opengl_context);




/* globals */

extern int G3D_DRAW_TRACE;
extern int G3D_DRAW_OCUR_SPECIAL;
extern int boxlist;	/* liste opengl pour la boite materialisant
			   l'environnment */
extern int p3d_numcoll;	/* Variables externes pour le CC */
extern std::vector<p3d_matrix4*> global_FramesToDraw;

#if defined( QT_GL ) && defined( CXX_PLANNER )
#include "qtWindow/qtOpenGL/Widget.hpp"
#endif

/* proto */
#include "libmove3d/graphic/proto/graphic_proto.h"


#ifdef USE_SHADERS
 #include "libmove3d/graphic/proto/g3d_extensions_proto.h"

 //! array of OpenGL IDs of the existing shader programs:
 extern GLuint G3D_PROGRAMS[100];

 //! number of existing shaders:
 extern unsigned int G3D_NB_PROGRAMS;

 //! index (in the array 'programs') of the current program (the one that is used now):
 extern unsigned int G3D_CURRENT_PROGRAM;
#endif


#endif /* #ifndef GRAPHIC_PKG_H */
