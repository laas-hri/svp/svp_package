/* 
 *  This file includes the prototypes of the functions defined in the
 *  directory planner and the files defining the structure appearing in 
 *  these prototypes.
 */

#ifndef PLANNER_PKG_H
#define PLANNER_PKG_H

/* struct */
#include "libmove3d/p3d_sys.h"
#include "libmove3d/p3d_type.h"
#include "libmove3d/p3d_matrix.h"
#include "libmove3d/polyhedre.h"
#include "libmove3d/p3d_poly.h"
#include "libmove3d/p3d.h"

#include "libmove3d/elastic.h"
#include "libmove3d/traj.h"
#include "libmove3d/localpath.h"
#include "libmove3d/device.h"

#include "libmove3d/environment.h"

//start path deform
#include "libmove3d/dyna_list.h"
//end path deform
#include "libmove3d/roadmap.h"
/* globals */
extern pp3d_graph  XYZ_TAB_GRAPH[];

extern int (*ext_p3d_run_rrt)(p3d_rob* robotPt);


/* proto */
#include "libmove3d/planner/proto/planner_proto.h"
#include "libmove3d/planner/Diffusion/proto/Diffusion_proto.h"
#include "libmove3d/planner/astar/proto/astar_proto.h"
#include "libmove3d/planner/dfs/proto/dfs_proto.h"

#ifdef MULTILOCALPATH
 #include "libmove3d/planner/proto/p3d_softMotion_traj_proto.h"
#endif

#endif	/* #ifndef PLANNER_PKG_H */
