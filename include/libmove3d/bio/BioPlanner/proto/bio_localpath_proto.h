/*
 *   This file was automatically generated by version 1.7 of cextract.
 *   Manual editing not recommended.
 *
 *   Created: Wed Jun 23 14:30:20 2004
 */
#ifndef __CEXTRACT__

extern void bio_col_set_step_deplacement(double deplacement);
extern void bio_col_get_step_deplacement(double *deplacement);

extern double bio_compute_localpath_validation_const_step(p3d_rob *robotPt,
							  p3d_localpath *localpathPt,
							  double maxdep);

extern int bio_col_test_localpath_step ( p3d_rob *robotPt, p3d_localpath *localpathPt, int *ntest, double *Kpath, configPt *q_atKpath );

extern void bio_evaluate_traj(FILE *contacts_file);

extern void bio_reset_current_q_inv(p3d_rob *robotPt);

extern void bio_set_current_q_inv(p3d_rob *robotPt, p3d_localpath *localpathPt, configPt q_inv); 
extern int bio_get_current_q_inv(p3d_rob *robotPt, configPt q_invPt);


/**
 * isBioTrajInCol
 * Function testing the validity of a trajectory in a bio environment
 * TODO.
 * Note: the incremental step is based on the viual step in draw traj.
 * ie: p3d_get_env_graphic_dmax();
 */
extern int isBioTrajInCol(p3d_rob* robotPt, p3d_traj* trajPt, 
			  int* nTestPt);

#endif /* __CEXTRACT__ */
