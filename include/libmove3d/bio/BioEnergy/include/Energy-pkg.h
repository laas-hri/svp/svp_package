/* 
 *  This file includes all the prototypes of the functions defined in the
 *  directory BioEnergy and the files defining the structure appearing in 
 *  these prototypes.
 */

#ifndef ENERGY_PKG_H
#define ENERGY_PKG_H


#include "libmove3d/bio/BioEnergy/include/bio_minimization.h" 
#include "libmove3d/bio/BioEnergy/include/bio_nmode.h" 


/* proto */
#include "libmove3d/bio/BioEnergy/proto/benergy_proto.h"



#endif /* #ifndef ENERGY_PKG_H */
