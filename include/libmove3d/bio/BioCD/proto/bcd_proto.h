/* 
 *    File generated automatically. Do not edit by hand.
 */ 

#include "libmove3d/bio/BioCD/proto/bcd_init_proto.h"
#include "libmove3d/bio/BioCD/proto/bcd_resize_proto.h"
#include "libmove3d/bio/BioCD/proto/bcd_shordist_proto.h"
#include "libmove3d/bio/BioCD/proto/bcd_hierarchies_proto.h"
#include "libmove3d/bio/BioCD/proto/bcd_util_proto.h"
#include "libmove3d/bio/BioCD/proto/bcd_test_proto.h"
