#ifndef PSF_UTIL
#define PSF_UTIL

typedef double matrix4[4][4];
typedef double vector4[4];

typedef struct {
  int nb_file_name;
  const char** name_list;
} file_name_list;

#endif

