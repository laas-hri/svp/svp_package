/* 
 *    File generated automatically. Do not edit by hand.
 */ 

#include "libmove3d/bio/BioStructures/proto/psf_defs_bio_proto.h"
#include "libmove3d/bio/BioStructures/proto/psf_get_set_proto.h"
#include "libmove3d/bio/BioStructures/proto/psf_to_p3d_proto.h"
#include "libmove3d/bio/BioStructures/proto/psf_rw_proto.h"
#include "libmove3d/bio/BioStructures/proto/psf_util_proto.h"
#include "libmove3d/bio/BioStructures/proto/aaa_rw_proto.h"
#ifdef WITH_XFORMS
#include "libmove3d/bio/BioStructures/proto/FORM_aaa_proto.h"
#include "libmove3d/bio/BioStructures/proto/FORM_files_selector_proto.h"
#include "libmove3d/bio/BioStructures/proto/FORM_joint_lig_proto.h"
#endif
#include "libmove3d/bio/BioStructures/proto/biostruct_util_proto.h"
