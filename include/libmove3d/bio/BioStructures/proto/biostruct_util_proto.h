#include "libmove3d/P3d-pkg.h"
#include "libmove3d/Bio-pkg.h"

extern int bio_set_bio_jnt_types(void);
extern void bio_set_list_AA_first_jnt(void);
extern int bio_set_bio_jnt_AAnumbers(void);
extern void  bio_set_AAtotal_number(void);
extern void bio_set_nb_flexible_sc(void);
extern void bio_set_list_firstjnts_flexible_sc(void);
extern void bio_set_nb_dof_ligand(void);
extern void bio_set_num_subrobot_ligand(void);
extern int bio_set_num_subrobot_AA(void);
