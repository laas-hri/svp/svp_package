#ifndef __CEXTRACT__

extern void create_file_selector_Form();
extern int do_file_selector_Form(const char* default_directory, file_name_list* file_list, char* name, int nameLength, int* p3d_file_source);

#endif /* __CEXTRACT__ */
