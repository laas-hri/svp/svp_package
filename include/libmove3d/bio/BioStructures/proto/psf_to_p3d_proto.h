/*
 *   This file was automatically generated by version 1.7 of cextract.
 *   Manual editing not recommended.

 */
#ifndef __CEXTRACT__

extern int psf_make_p3d_prot_desc ( psf_protein* proteinPt, int numprot);
extern int psf_make_p3d_lig_desc ( psf_ligand* ligandPt, int numlig);
extern int psf_make_p3d_desc(psf_molecule* molecule);
extern int psf_make_p3d_from_pdb ( char *file);
extern int psf_make_p3d_from_multiple_pdb (file_name_list* file_list, char* name);

#endif /* __CEXTRACT__ */
