/*
 *   This file was automatically generated by version 1.7 of cextract.
 *   Manual editing not recommended.

 */
#ifndef __CEXTRACT__

extern int kcd_gjk_intersect ( kcd_bb *obbox1, kcd_bb *obbox2, kcd_vector3 *v, int with_report, double *min_distance, double gjk_tolerance );

#endif /* __CEXTRACT__ */
