#ifndef TRIANGLES_H
#define TRIANGLES_H

typedef double              tricd_vertex[3];
typedef tricd_vertex       tricd_triangle[3];
typedef tricd_triangle    *tricd_triangulation;

typedef int output_triangle[3];
typedef output_triangle *output_triangle_p;

#endif

