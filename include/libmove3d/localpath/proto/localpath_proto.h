/* 
 *    File generated automatically. Do not edit by hand.
 */ 

#include "libmove3d/localpath/proto/rs_dist_proto.h"
#include "libmove3d/localpath/proto/rs_curve_proto.h"
#include "libmove3d/localpath/proto/p3d_local_proto.h"
#include "libmove3d/localpath/proto/p3d_multiLocalPath_proto.h"
#include "libmove3d/localpath/proto/p3d_linear_proto.h"
#include "libmove3d/localpath/proto/p3d_hermite_proto.h"
#include "libmove3d/localpath/proto/p3d_quadspline_proto.h"
#include "libmove3d/localpath/proto/p3d_reeds_shepp_proto.h"
#include "libmove3d/localpath/proto/p3d_manhattan_proto.h"
#include "libmove3d/localpath/proto/p3d_trailer_proto.h"
#include "libmove3d/localpath/proto/p3d_hilare_flat_proto.h"

#ifdef MULTILOCALPATH
#include "libmove3d/localpath/proto/p3d_softMotion_proto.h"
#endif
