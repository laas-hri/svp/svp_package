/*
 *   This file was automatically generated by version 1.7 of cextract.
 *   Manual editing not recommended.

 */
#ifndef __CEXTRACT__

extern void hilflat_conv_sub_path_fpath ( p3d_rob *robotPt, p3d_sub_hilflat_data *sub_hilflat_dataPt, FLAT_LOCAL_PATH_STR *fpathPt );
extern p3d_localpath * p3d_alloc_hilflat_localpath ( p3d_rob *robotPt, configPt q_init, configPt q_cusp, configPt q_end, double u_start, double u_end, int lp_id, int symmetric );
extern int is_valid_hilflat ( p3d_rob *robotPt, p3d_hilflat_data *hilflat_dataPt, int symmetric );
extern double p3d_sub_hilflat_distance ( p3d_sub_hilflat_data *sub_hilflat_dataPt );
extern double p3d_hilflat_dist ( p3d_rob *robotPt, p3d_localpath *localpathPt );
extern void p3d_destroy_sub_hilflat_data ( p3d_rob* robotPt, p3d_sub_hilflat_data* sub_hilflat_dataPt, int q_init_is_there );
extern void p3d_destroy_hilflat_data ( p3d_rob* robotPt, p3d_hilflat_data* hilflat_dataPt );
extern void p3d_hilflat_destroy ( p3d_rob* robotPt, p3d_localpath* localpathPt );
extern configPt p3d_sub_hilflat_config_at_distance ( p3d_rob *robotPt, p3d_sub_hilflat_data *sub_hilflat_dataPt, double d_lp );
extern configPt p3d_sub_hilflat_config_at_param ( p3d_rob *robotPt, p3d_sub_hilflat_data *sub_hilflat_dataPt, double u_lp );
extern configPt p3d_hilflat_config_at_distance ( p3d_rob *robotPt, p3d_localpath *localpathPt, double d_lp );
extern configPt p3d_hilflat_config_at_param ( p3d_rob *robotPt, p3d_localpath *localpathPt, double u_lp );
extern double p3d_hilflat_stay_within_dist ( p3d_rob* robotPt, p3d_localpath* localpathPt, double u_lp, int dir, double *distances );
extern p3d_localpath *p3d_copy_hilflat_localpath ( p3d_rob* robotPt, p3d_localpath* localpathPt );
extern void p3d_extract_sub_hilflat ( p3d_rob *robotPt, p3d_sub_hilflat_data *sub_hilflat_dataPt, double d_lp1, double d_lp2, double *Tab_new_u );
extern p3d_localpath *p3d_extract_hilflat ( p3d_rob *robotPt, p3d_localpath *localpathPt, double d_lp1, double d_lp2 );
extern p3d_localpath *p3d_extract_hilflat_by_param ( p3d_rob *robotPt, p3d_localpath *localpathPt, double u1, double u2 );
extern double p3d_hilflat_cost ( p3d_rob *robotPt, p3d_localpath *localpathPt );
extern p3d_localpath *p3d_simplify_hilflat ( p3d_rob *robotPt, p3d_localpath *localpathPt, int *need_colcheck );
extern int p3d_write_hilflat ( FILE *file, p3d_rob* robotPt, p3d_localpath* localpathPt );
extern void lm_destroy_hilflat_params ( p3d_rob *robotPt, void *local_method_params );
extern p3d_localpath *p3d_hilflat_localplanner ( p3d_rob *robotPt, double *qi, double *qf, int* ikSol );
extern p3d_localpath *p3d_nocusp_hilflat_localplanner ( p3d_rob *robotPt, double *qi, double *qf, int* ikSol);
extern int p3d_create_hilflat_local_method_for_robot ( p3d_rob *robotPt, double *dtab, int *itab );
extern int p3d_create_hilflat_local_method ( int *itab );
extern philflat_str lm_create_hilflat ( p3d_rob *robotPt, int *itab );
extern philflat_str lm_get_hilflat_lm_param ( p3d_rob *robotPt );
extern p3d_localpath *p3d_read_hilflat_localpath_symmetric ( p3d_rob *robotPt, FILE *file, double version );
extern p3d_localpath *p3d_read_hilflat_localpath_not_symmetric ( p3d_rob *robotPt, FILE *file, double version );

#endif /* __CEXTRACT__ */
