/* 
 *    File generated automatically. Do not edit by hand.
 */ 

#include "libmove3d/graphic/proto/g3d_states_proto.h"
#include "libmove3d/graphic/proto/g3d_window_proto.h"
#include "libmove3d/graphic/proto/uglyfont.h"

#ifdef P3D_COLLISION_CHECKING
#include "libmove3d/graphic/proto/g3d_kcd_draw_proto.h"
#endif
#include "libmove3d/graphic/proto/g3d_draw_proto.h"
#include "libmove3d/graphic/proto/g3d_draw_env_proto.h"
#include "libmove3d/graphic/proto/g3d_draw_graph_proto.h"
#include "libmove3d/graphic/proto/g3d_draw_traj_proto.h"

#ifndef WITH_XFORMS
#include "libmove3d/graphic/proto/g3d_newWindow.hpp"
#endif

#ifdef USE_GLUT
#include "libmove3d/graphic/proto/g3d_glut.hpp"
#endif
