/* 
 *  This file includes the protoypes of the functions defined in the
 *  directory collision and the files defining the structure appearing in 
 *  these prototypes.
 */

#ifndef COLLISION_PKG_H
#define COLLISION_PKG_H

#ifdef UNIX
#endif


/* struct */
#include "libmove3d/p3d_sys.h"
#include "libmove3d/p3d_type.h"
#include "libmove3d/p3d_matrix.h"
#include "libmove3d/polyhedre.h"
#include "libmove3d/p3d_poly.h"
#include "libmove3d/p3d.h"
#include "libmove3d/collision_context.h"

#include "libmove3d/traj.h"
#include "libmove3d/localpath.h"
#include "libmove3d/device.h"

#ifdef VCOLLIDE_ACT
#include "libmove3d/collision/Vcollide/src/VCol.h"
#endif

#include "libmove3d/collision/Kcd/include/kcd_api.h"
#include "libmove3d/collision/Kcd/include/kcd_type.h"
#include "libmove3d/collision/Kcd/include/kcd.h"
#include "libmove3d/collision/Kcd/include/kcd_aabb_tree.h"



/* globals */

extern int COLLISION_BY_OBJECT;

#ifdef VCOLLIDE_ACT
extern void *vc_hand;			/* the VCOLLIDE collision detection engine */
extern VCReportType *vc_colrep; /* the VCOLLIDE collision report */
#endif

#include "libmove3d/collision/Kcd/include/kcd_global.h"  /* Kcd */
#include "libmove3d/collision/Kcd/include/kcd_aabb_tree_global.h"  /* Kcd */


/* proto */

#include "libmove3d/triangles.h"
#include "libmove3d/collision/proto/collision_proto.h"

#include "libmove3d/collision/Vcollide/src/VCol.h"
#include "libmove3d/collision/Kcd/proto/kcd_proto.h"

#ifdef UNIX
#endif

#endif /* #ifndef COLLISION_PKG_H */
