#ifdef MULTIGRAPH
extern void p3d_writeMultiGraph(void * graph, const char* file, xmlNodePtr root);
extern void p3d_writeSuperGraph(p3d_rob* robot, p3d_flatSuperGraph * graph, xmlNodePtr root);
extern void p3d_writeMultiGraphRootNode(void * graph, xmlNodePtr root);
#endif
