extern void p3d_writeGraph(void *graph, const char *file, int graphType);
extern int p3d_readGraph(const char *file, int graphType);
extern configPt readXmlConfig(p3d_rob *robot, xmlNodePtr cur);
