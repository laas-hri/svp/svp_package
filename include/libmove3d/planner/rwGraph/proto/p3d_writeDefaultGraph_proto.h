extern void p3d_writeDefaultGraph(void * graph, const char* file , xmlNodePtr root);
extern void p3d_writeDefaultGraphRootNode(void * graph, xmlNodePtr root);
extern void writeXmlRobotConfig(xmlNodePtr parent, p3d_rob *robot, configPt q_back);
