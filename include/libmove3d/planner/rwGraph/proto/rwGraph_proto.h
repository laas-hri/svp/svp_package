#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include "libmove3d/planner/rwGraph/proto/p3d_readDefaultGraph_proto.h"
#include "libmove3d/planner/rwGraph/proto/p3d_writeDefaultGraph_proto.h"
#include "libmove3d/planner/rwGraph/proto/p3d_readMultiGraph_proto.h"
#include "libmove3d/planner/rwGraph/proto/p3d_writeMultiGraph_proto.h"
#include "libmove3d/planner/rwGraph/proto/p3d_rwXmlGraph_proto.h"
#include "libmove3d/planner/rwGraph/proto/p3d_rw_graph_proto.h"
