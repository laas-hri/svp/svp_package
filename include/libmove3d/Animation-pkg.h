/* 
 *  This file includes the prototypes of the functions defined in the
 *  directory localpath and the files defining the structure appearing in 
 *  these prototypes.
 */


#ifdef UNIX
#endif


/* struct */
#include "libmove3d/animation.h"\

extern struct LPcarac LPChar;

/* proto */

#include "libmove3d/animation/proto/anim_proto.h"

#ifdef UNIX
#endif
