/* 
 *  This file includes the prototypes of the functions defined in the
 *  directory p3d and the files defining the structure appearing in 
 *  these prototypes.
 */

#ifndef P3D_PKG_H
#define P3D_PKG_H

#ifdef UNIX
#endif

#ifdef WITH_XFORMS
#include "forms.h"
#endif

/* struct */
#include "libmove3d/p3d_sys.h"
#include "libmove3d/p3d_type.h"
#include "libmove3d/p3d_matrix.h"
#include "libmove3d/polyhedre.h"
#include "libmove3d/p3d_poly.h"
#include "libmove3d/dyna_list.h"              // modif Juan
#include "libmove3d/p3d.h"

#include "libmove3d/traj.h"
#include "libmove3d/localpath.h"
#include "libmove3d/device.h"
#include "libmove3d/cntrt.h"                  // modif Juan

#include "libmove3d/environment.h"

#include "libmove3d/roadmap.h"

#include "libmove3d/collision_context.h"



/* globals */
extern pp3d_env  XYZ_ENV;
extern pp3d_env  *XYZ_TAB_ENV;
extern int       XYZ_NUM_ENV;
extern int       XYZ_MAX_NUM_ENV;
extern pp3d_rob  XYZ_ROBOT;
extern pp3d_obj  XYZ_OBSTACLES;
extern pp3d_graph XYZ_GRAPH;

/* Random Numbers */
#ifdef USE_GSL
#include <gsl/gsl_randist.h>
extern gsl_rng * _gsl_seed;
#endif
#include "libmove3d/p3d/MTRand.hpp"
extern MTRand mersenne_twister_rng;
  
  /* pointer to function to choose the type of bounding box computation */
typedef void (*p3d_BB_update_BB_fct_type)(p3d_obj *obj, p3d_matrix4 mat);
extern p3d_BB_update_BB_fct_type p3d_BB_update_BB_obj;

  /* pointer to function to choose the type of bounding box computation */
typedef void (*p3d_BB_get_BB_poly_fct_type)(p3d_poly *p,double *x1,double *x2,
			    double *y1,double *y2,double *z1,double *z2);
extern p3d_BB_get_BB_poly_fct_type p3d_BB_get_BB_poly;

#include "libmove3d/p3d/env.hpp"
//#include "libmove3d/p3d/ParametersEnv.hpp"

/* proto */
#include "libmove3d/p3d/proto/p3d_proto.h"

#ifdef UNIX
#endif

#endif /* #ifndef P3D_PKG_H */
