/*
 *   This file was automatically generated by version 1.7 of cextract.
 *   Manual editing not recommended.

 */
#ifndef __CEXTRACT__

extern void basic_alloc_debugon ( void );
extern void basic_alloc_debugoff ( void );
extern void report_whats_left ( void );
extern void* basic_alloc (unsigned  long n,  size_t size );
extern void* basic_realloc ( void *ptr, unsigned long nold, unsigned long nnew, size_t size );
extern void basic_free ( void *ptr, unsigned long n, size_t size);
extern void basic_alloc_set_maxsize ( size_t s );
extern size_t  basic_alloc_get_size ( void );
extern void basic_alloc_info ( const char *str );

#endif /* __CEXTRACT__ */
