
extern atom *get_N(residue *resPt);
extern atom *get_CA(residue *resPt);
extern atom *get_C(residue *resPt);

extern void get_N_pos(residue *resPt, double **apos);
extern void get_CA_pos(residue *resPt, double **apos);
extern void get_C_pos(residue *resPt, double **apos);

