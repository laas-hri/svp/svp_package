#ifndef LIST_H
#define LIST_H

typedef struct s_int_list {
  int n_values;
  int* values;
} int_list;;

#include "libmove3d/BioTools/Translators/Protein/list_proto.h"

#endif
