/* 
 *  This file includes the prototypes of the functions defined in the
 *  directory localpath and the files defining the structure appearing in 
 *  these prototypes.
 */

#ifndef LOCALPATH_PKG_H
#define LOCALPATH_PKG_H

#ifdef UNIX
#endif

#define SOFT_MOTION_PRINT_DATA 0

/* struct */
#include "libmove3d/p3d_sys.h"
#include "libmove3d/p3d_type.h"
#include "libmove3d/p3d_matrix.h"
#include "libmove3d/polyhedre.h"
#include "libmove3d/p3d_poly.h"
#include "libmove3d/p3d.h"

#include "libmove3d/traj.h"
#include "libmove3d/localpath.h"
#include "libmove3d/device.h"

#include "libmove3d/rs.h"
#include "libmove3d/localpath/flat/include/struct_flat.h"


/* Arm models */
#include "libmove3d/arm_models/pa10Const.h"

/* globals */


/* proto */
#include "libmove3d/localpath/flat/include/init_flat_proto.h"
#include "libmove3d/localpath/flat/include/general_flat_proto.h"
#include "libmove3d/localpath/flat/include/kinematic_flat_proto.h"

// gbM  XB
#if defined(USE_GBM)
extern "C"{
#include "gbM/Proto_gb.h"
#include "gbM/Proto_gbModeles.h"
#include "gbM/gbStruct.h"
}
#endif
#if defined(MULTILOCALPATH)
#include "softMotion/softMotionStruct.h"
#include "softMotion/softMotion.h"

#endif

#include "libmove3d/localpath/proto/localpath_proto.h"


#ifdef UNIX
#endif

#endif /* #ifndef LOCALPATH_PKG_H */
