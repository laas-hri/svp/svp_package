/*
 *   This file was automatically generated by version 1.7 of cextract.
 *   Manual editing not recommended.
 *
 *   Created: Wed Jun 23 14:30:09 2004
 */
#ifndef __CEXTRACT__

extern void anim_speed_characterization ( p3d_animation * AnimPt );
extern void anim_process_mcap ( p3d_animation * AnimPt );

#endif /* __CEXTRACT__ */
