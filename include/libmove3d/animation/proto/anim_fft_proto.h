/*
 *   This file was automatically generated by version 1.7 of cextract.
 *   Manual editing not recommended.
 *
 *   Created: Wed Jun 23 14:30:16 2004
 */
#ifndef __CEXTRACT__

extern void CheckPointer ( const void * Pointer, const char * Name );
extern int IsPowerOfTwo ( const unsigned x );
extern unsigned NumberOfBitsNeeded ( unsigned PowerOfTwo );
extern unsigned ReverseBits ( unsigned Index, unsigned NumBits );
extern double Index_to_frequency ( unsigned NumSamples, unsigned Index );
extern void anim_fft_double ( unsigned NofSamples, int InverseTransform, double *RealIn, double *ImagIn, double *RealOut, double *ImagOut );
extern double p3d_fourier_inverse_transform ( int Filter, double NofSamples, double * RealIn, double * ImagIn, double param );
extern double modulo ( double Input, double Mod );
extern void anim_initialize_anim_options_default ( void );
extern void anim_initialize_anim_options ( int a, int b, int c, int d, int e, int f, int g, int h, double i, double j, int k, int l );

#endif /* __CEXTRACT__ */
