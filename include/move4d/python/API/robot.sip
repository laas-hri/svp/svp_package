namespace move4d
{
class Robot {
%TypeHeaderCode
#include <move4d/API/Device/robot.hpp>
%End
private:
    Robot();
%MethodCode

%End
public:
	/**
	 * obtient le nom du Robot
	 * @return le nom du Robot
	 */
	const std::string &getName() const;

    /**
      * return objectRob related to the robot if possible
      */
    //ObjectRob* getObjectRob();

    /**
      * set hri_agent related to the robot if possible
      */
    //void setObjectRob(ObjectRob* ore);
	
	/**
	 * Gets the current trajectory
	 */
	// API::GeometricPath *getCurrentTraj();
    move4d::API::Path *getCurrentPath();
	
	/**
	 * Set the trajectory of this robot
	 * @param trajectory The trajectory
	 */
	void setCurrentTraj(move4d::API::Path *trajectory);

    void setMultiLpGroupToPlan(int mlpId,bool enable=true);
    void setMultiLpGroupToPlan(const std::string &mlpName, bool enable=true);
    std::vector<std::string> getMultiLpGroupNames(bool print=false);
    bool getMultiLpGroupToPlan(int mlpId);

    unsigned int getNumberOfJoints();
    unsigned int getNumberOfActiveDoF();
    unsigned int getNumberOfDoF();
    move4d::Joint* getIthActiveDoFJoint(unsigned int ithActiveDoF , unsigned int& ithDofOnJoint  );
    unsigned int getIthActiveDofIndex(unsigned int ithActiveDof);
    bool isActiveDoF(const move4d::Joint *jnt, unsigned int i_dof_on_jnt);

     void setActiveDofs(const std::vector<unsigned int> &active_dofs);
     void setActiveJoints(const std::vector<int> &active_jnts);
	/**
	 * Gets the ith joint structure
	 * @return ith joint structure
	 */
	move4d::Joint* getJoint(unsigned int i);
  
  /**
	 * Gets joint by name
	 * @return pointer to the joint by name or NULL if not found
	 */
	move4d::Joint* getJoint(std::string name);
  
  /**
   * Returns an vector of all robot joints
   */
  // const std::vector<Joint*>& getAllJoints();

	/**
	 * Returns the Object
	 * Box
	 */
	// std::vector<Eigen::Vector3d> getObjectBox();
	
	/**
	 * Initializes the box in which the FF 
	 * Will be sampled
	 */
	// void initObjectBox();
	
	/**
	 * tire une RobotState aléatoire pour le Robot
	 * @param samplePassive (default = TRUE) indique si l'on tire les joints passif ou non (ie. FALSE dans le cas de ML-RRT)
	 * @return la RobotState tirée
	 */
	//std::shared_ptr<RobotState> shoot(bool samplePassive = false);

    //std::shared_ptr<RobotState> shootDirections(std::shared_ptr<RobotState> from , int nactive, int scale=1);
	
	/**
	 * obtient une RobotState-Direction aléatoire pour le Robot
	 * @param samplePassive (default = true) indique si l'on tire les joints passif ou non (ie. FALSE dans le cas de ML-RRT)
	 * @return la RobotState tirée
	 */ 
	//std::shared_ptr<RobotState> shootDir(bool samplePassive = false);
	
	/**
	 * shoots the active free flyer inside a box
	 */
	//std::shared_ptr<RobotState> shootFreeFlyer(double* box);
  
  /**
	 * set and update the active free flyer
	 */
	//int setAndUpdateFreeFlyer(const Eigen::Vector3d& pos);
	
	/**
	 * place le Robot dans une RobotState
	 * @param q la RobotState dans laquelle le Robot sera placé
	 * @return la RobotState est atteignable cinématiquement
	 */
	virtual  int setAndUpdate(const move4d::RobotState & q, bool withoutFreeFlyers = false);

     bool checkConstraints() const;
	/**
	 * Calcule la dynamique du robot dans l'espace cartésien
	 * @param rs État du robot à partir duquel calculer la dynamique
	 */
	//void setAndUpdateKino(RobotState &rs);

	/**
	 * Get velocity orientation between two robots
	 * param other robot
	 * param idJoint for this joint
	 * return orientation
	 */
	//double getOrientation(Robot *other, unsigned idJoint);

	/**
	 * place le Robot dans une RobotState
	 * @param q la RobotState dans laquelle le Robot sera placé
	 * @return la RobotState est atteignable cinématiquement
	 */
	//bool setAndUpdateMultiSol(RobotState & q);
	
	/**
	 * place le Robot dans une RobotState, without checking the cinematic constraints.
	 * @param q la RobotState dans laquelle le Robot sera placé
	 */
	//void setAndUpdateWithoutConstraints(RobotState & q);
	
	/**
	 * set and update Human Arms
	 */
	//bool setAndUpdateHumanArms(RobotState & q);
	
	/**
	 * obtient la RobotState current du Robot
	 * @return la RobotState current du Robot
	 */
	move4d::statePtr_t getInitialPosition();
	
	/**
	 * Returns true if the robot is 
	 * in colision with obstacles, other robots and self
	 */
	bool isInCollision();

	/**
	 * Returns true if the robot is
	 * in colision with obstacles or other robots
	 */
	bool isInCollisionWithOthersAndEnv();
	
	/**
	 * Sets the Initial Position of the Robot
	 */
	void setInitialPosition(move4d::RobotState & conf);
	
	/**
	 * obtient la RobotState GoTo du Robot
	 * @return la RobotState GoTo du Robot
	 */
	move4d::statePtr_t getGoTo();
	
	/**
	 * Sets the Goto Position of the Robot
	 */
	void setGoTo(move4d::RobotState & conf);

	/**
	 * Sets the Initial velocity of the Robot
	 */
	void setInitialVelocity(move4d::RobotState & conf);

	/**
	 * Sets the Goto velocity of the Robot
	 */
	void setVelocityGoTo(move4d::RobotState & conf);

	/**
	 * Returns the Robot current RobotState
	 */
	 //move4d::RobotState &getCurrentPos();
//%MethodCode
    //sipRes = sipCpp->getCurrentPos().get();
//%End
    //move4d::statePtr_t getCurrentPos();
    move4d::RobotState getCurrentPos();
%MethodCode
    sipRes = new move4d::RobotState(*sipCpp->getCurrentPos());
%End

	/**
	 * Returns a new configuration
	 */
	// std::shared_ptr<RobotState> getNewConfig();
	
	/**
	 * Get the Robot joint AbsPos
	 */
    // Eigen::Matrix4d getJointAbsPos(int id);
	
	/**
	 * Get the Robot joint Position
     * @warning  based on _Robot (p3d_rob) joints array, with different indices,
     * has one more freeflyer joint in front, usually set all at 0.
	 */
	// Eigen::Vector3d getJointPos(int id);
	
	/**
	 * @brief Get joint of dof ith (and the index on this joint)
	 * @param idof Dof to test (active or not)
	 * @param ithDofOnJoint index into the joint
	 * @return joint
	 */
	// Joint* getJointAt(int idof, unsigned int &ithDofOnJoint);
    //std::pair<double,double> getDofLimit(int idof,uint derivative=0);

	// void getListOfPassiveJoints(std::list<Joint *> & passiveJoints);

	unsigned getNbConf() const;

	// statePtr_t getConf(unsigned i);

	/**
	 * @brief get minimum distance between two robot
	 * @details is fast computation : use only the robot bases (2D)
	 * @param other robot
	 * @return distance (m)
	 */
	double getMinDist(move4d::Robot *other);

// %If (LIGHT_PLANNER)
// 	/**
// 	 * Returns the Virtual object dof
// 	 */
// 	int getObjectDof();
// 	
// 	/**
// 	 * Returns Wether the closed chain constraint
// 	 * is active
// 	 */ 
// 	bool isActiveCcConstraint();
// 	
// 	/**
// 	 * Activate Constraint
// 	 */
// 	void activateCcConstraint();
// 	
// 	/**
// 	 * Deactivate Constraint
// 	 */
// 	void deactivateCcConstraint();
// 	
// 	/**
// 	 * Returns the base Joint
//  	 */
// 	// Joint* getBaseJnt();
// 
//     unsigned int getBaseJointIndex();
// 
// 	/**
// 	 * Shoots a random direction
// 	 */
// 	// std::shared_ptr<RobotState> shootRandomDirection();
// 	
// 	/**
// 	 * Shoots the base Joint of the robot
// 	 */
// 	// std::shared_ptr<RobotState> shootBase();
// 	
// 	/**
// 	 *
// 	 */
// 	// std::shared_ptr<RobotState> shootBaseWithoutCC();
// 	
// 	/**
// 	 * Shoots the base Joint of the robot
// 	 */
// 	// std::shared_ptr<RobotState> shootAllExceptBase();
// 	
// 	/**
// 	 * Update but not base
// 	 */ 
// 	// bool setAndUpdateAllExceptBase(RobotState & Conf);
// 	
// 	/**
// 	 * shoots the robot by shooting the 
// 	 * FreeFlyer inside the accecibility box
// 	 */
// 	// void shootObjectJoint(RobotState & Conf);
// 
//     /**
//       *return if an object is carryed or not in the arm
//       */
//     bool isCarryingObject(int armId);
// %End

    bool isAgent();
    void isAgent(bool isAg);

    void attachObj(move4d::Robot* obj, int jointId);
    void dettachObj(move4d::Robot* obj);
    void dettachObj(int jointId);
    bool isObjectAttached();
    //bool isObjectAttached(Robot* obj, int jointId);
    //Joint* whereObjectAttached(Robot* obj);

    const move4d::HriAgent *getHriAgent() const;
    //HriAgent *getHriAgent();
    //void setHriAgent(const HriAgent &hriAgent);
};

namespace helpers{
bool setRobotPlanningPart(move4d::Robot *r,const std::string &planning_part);
};
}; //namespace move4d
