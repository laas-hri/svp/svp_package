namespace move4d {
namespace API {

class Parameter
{
%TypeHeaderCode
#include "move4d/API/Parameter.hpp"
%End
public:
    typedef boost::unique_lock<boost::mutex> lock_t;
    typedef std::map<std::string,Parameter> ObjectContainer;
    enum ValueType{
        NullValue,
        IntValue,
        BoolValue,
        RealValue,
        StringValue,
        ObjectValue,
        ArrayValue
    };

    static move4d::API::Parameter &root(UniqueLockMutex &);

    Parameter();
%MethodCode
    sipCpp = new move4d::API::Parameter(move4d::API::Parameter::NullValue);
%End
    //Parameter(int v);
    Parameter(double v);
    Parameter(const std::string &v);
    //Parameter(const std::map<std::string,Parameter> &obj);

    static bool setParameter(const std::string &path, SIP_PYOBJECT val);
%MethodCode
    //detect type of second parameter:
    if(PyFloat_Check(a1)){
        double x=PyFloat_AsDouble(a1);
        sipRes = move4d::API::Parameter::setParameter(*a0,move4d::API::Parameter(x));
    }else if(PyLong_Check(a1)){
        int x=PyLong_AsLong(a1);
        sipRes = move4d::API::Parameter::setParameter(*a0,move4d::API::Parameter(x));
    }else if(PyUnicode_Check(a1)){
        std::string x=PyUnicode_AsUTF8(a1);
        sipRes = move4d::API::Parameter::setParameter(*a0,move4d::API::Parameter(x));
    }else{
        PyErr_SetString(PyExc_RuntimeError, std::string("move4d.API.Parameter.param expects a float, integer, or string as value, got ").append(Py_TYPE(a1)->tp_name).c_str());
        sipIsErr=1;
    }
%End
    static SIP_PYOBJECT param(const std::string &path,SIP_PYOBJECT default_v);
%MethodCode
    //detect type of second parameter:
    if(PyFloat_Check(a1)){
        double x=PyFloat_AsDouble(a1);
        sipRes = PyFloat_FromDouble(move4d::API::Parameter::param<double>(*a0,x));
    }else if(PyLong_Check(a1)){
        int x=PyLong_AsLong(a1);
        sipRes = PyLong_FromLong((long) move4d::API::Parameter::param<int>(*a0,x));
    }else if(PyUnicode_Check(a1)){
        std::string x=PyUnicode_AsUTF8(a1);
        sipRes = PyUnicode_FromString(move4d::API::Parameter::param<std::string>(*a0,x).c_str());
    }else{
        PyErr_SetString(PyExc_RuntimeError, std::string("move4d.API.Parameter.param expects a float, integer, or string as value, got ").append(Py_TYPE(a1)->tp_name).c_str());
        sipIsErr=1;
    }
%End

    bool asBool();
    int asInt();
    double asDouble();
    std::string &asString();

    unsigned int size() const;
    move4d::API::Parameter &operator[](unsigned int index);
    move4d::API::Parameter &at(unsigned int i);
    void append(const move4d::API::Parameter &p);

    move4d::API::Parameter &operator[](const std::string &k);

    ValueType type() const;

    void __setitem__(SIP_PYOBJECT, SIP_PYOBJECT);
%MethodCode
    move4d::API::Parameter x;
    //detect type of second parameter:
    if(PyBool_Check(a1)){
        x=bool(PyLong_AsLong(a1));
    }else if(PyFloat_Check(a1)){
        x=PyFloat_AsDouble(a1);
    }else if(PyLong_Check(a1)){
        x=(int)PyLong_AsLong(a1);
    }else if(PyUnicode_Check(a1)){
        x=PyUnicode_AsUTF8(a1);
    }else if(sipCanConvertToType(a1,sipFindType("move4d::API::Parameter"),0)){
        x=*(move4d::API::Parameter*)sipConvertToType(a1,sipFindType("move4d::API::Parameter"),NULL,0,NULL, &sipIsErr);
    }else{
        PyErr_SetString(PyExc_RuntimeError, std::string("move4d.API.Parameter.__setitem__ expects a float, integer, or string as value, got ").append(Py_TYPE(a1)->tp_name).c_str());
        sipIsErr=1;
    }
    if(!sipIsErr){
        //detect type of index and set
        if(PyLong_CheckExact(a0)){
            (*sipCpp)[PyLong_AsLong(a0)]=x;
        }else if (PyUnicode_Check(a0)){
            (*sipCpp)[PyUnicode_AsUTF8(a0)] = x;
        }else{
            PyErr_SetString(PyExc_RuntimeError,"move4d.API.Parameter.__setitem__ expects a string or an integer as index value");
            sipIsErr=1;
        }
    }
%End
};

}; // namespace API
}; // namespace move4d

