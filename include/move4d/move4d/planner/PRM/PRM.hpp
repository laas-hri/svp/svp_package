#ifndef PRM_HPP
#define PRM_HPP

#include "move4d/planner/planner.hpp"
#include <iostream>

namespace move4d
{
/**
 @ingroup PRM
 @brief Classe représentant l'algorithme PRM
 @author Florian Pilardeau,B90,6349 <fpilarde@jolimont>
 */
class PRM : public Planner
{
public:
	//! Constructor.
	PRM() = default;
	PRM(Robot * robot, Graph * graph) : Planner(robot, graph), m_nbConscutiveFailures(0) {
        std::cout << "Construct the PRM" << std::endl;
    }

	//! Destructor.
	~PRM() {};

	int getNumberOfExpansion() const { return m_nbExpansions; }

    /**
     * Initialize the PRM.
     * @return the number of nodes added to the graph
     */
	virtual unsigned init();

	/**
	 * Check the stopping conditions of PRM.
	 * @return TRUE if one of the conditions is satisfied, and FALSE otherwise
	 */
	bool checkStopConditions();

	/**
	 * Check the pre-conditions of PRM.
	 * @return TRUE if one of the conditions is satisfied, and FALSE otherwise
	 */
	bool preConditions();

	//! Perform a single expansion step of PRM.
	virtual void expandOneStep();

	/**
	 * Main function of the PRM.
	 * @return the number of nodes added to the graph
	 */
	unsigned int run();

	/**
	 * Check whether PRM has found a trajectory.
	 * @return TRUE if the criteria required for a trajectory to be found are met, and FALSE otherwise
	 */
	bool trajFound();

    //! Extract a trajectory going through all the waypoints.
    API::GeometricPath * extractTrajectory();

private:
    /**
     * Solve the Traveling Salesman Problem by exhaustive search.
     * @param distances: pairwise distance matrix given as input
     * @param solution: shortest path returned as output
     * @return length of the shortest path
     */
    double TPSbyExhaustiveSearch(std::vector<std::vector<double> > & distances,
			std::vector<unsigned> & solution);

    //! Load the user-defined waypoints (start, goal, and potential intermediate waypoints).
    void loadWaypoints();

protected:
	unsigned m_nbAddedNode;
	int m_nbConscutiveFailures;
	int m_nbExpansions;

    //! waypoints through which the solution path should go
    std::vector<statePtr_t> waypoints;

    //! nodes containing the waypoints
    std::vector<Node *> waypointNodes;
};

} //namespace move4d
#endif
