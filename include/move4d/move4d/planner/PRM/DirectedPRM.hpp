#ifndef DIRECTEDPRM_HPP
#define DIRECTEDPRM_HPP

#include "move4d/planner/planner.hpp"

namespace move4d
{
class DirectedPRM : public Planner {

public:

    DirectedPRM(Robot *robot,Graph *graph) : Planner(robot, graph){
        std::cout << "Construct the DirectedPRM" << std::endl;
        m_nbAddedNode=0;
        m_nbExpansions=0;
        m_nbConscutiveFailures=0;
        m_nbFailures=0;
        m_nbShoots=0;
        m_nbConnectibleNodes=0;
    }

    ~DirectedPRM(){}

    unsigned int getNumberOfAddedNode() const { return m_nbAddedNode; }
    unsigned int getNumberOfExpansions() const { return m_nbExpansions; }
    unsigned int getNumberOfFailures() const { return m_nbFailures; }
    double getShootingFailureRate() const { return 100*(1-(double)m_nbExpansions/(double)m_nbShoots); }
    double getConnectibleRate() const { return 100*(double)m_nbConnectibleNodes/(double)m_nbAddedNode; }

    void pause();

    bool initialize();
    bool setStartNode(std::shared_ptr<RobotState> Cs);
    bool setGoalNode(std::shared_ptr<RobotState> Cg);

	bool checkStopConditions();
	bool preConditions();
	virtual void expandOneStep();
    unsigned int run();
    bool trajFound();
    bool linkNode(Node* N1);
    void extractSmoothAndReplaceTraj();
    void updateConnections();

protected:
    unsigned int m_nbAddedNode;
    unsigned int m_nbConscutiveFailures;
    unsigned int m_nbExpansions;
    unsigned int m_nbFailures;
    unsigned int m_nbShoots;
    unsigned int m_nbConnectibleNodes;

};

} //namespace move4d
#endif
