//
//  RRM.hpp
//  libmove3d-motion
//
//  Created by Jim Mainprice on 21/02/12.
//  Copyright (c) 2012 LAAS-CNRS. All rights reserved.
//

#ifndef RRM_HPP_
#define RRM_HPP_

#include "move4d/planner/PRM/PRM.hpp"

#include "move4d/API/Trajectory/trajectory.hpp"

namespace move4d
{
class RRM : public PRM
{
public:
	/**
	 * Creates a perturbation roadmap from a given robot 
   * and a given graph
	 * @param Robot
   * @param Graph
	 */
	RRM(Robot* R, Graph* G);
	
	/**
	 * Deletes a perturbation planner
	 */
	~RRM();
  
	/**
	 * Adds nodes to Graph
	 */
	virtual void expandOneStep();
	
protected:
  
  statePtr_t m_qi;
  statePtr_t m_qf;
};

} //namespace move4d
#endif
