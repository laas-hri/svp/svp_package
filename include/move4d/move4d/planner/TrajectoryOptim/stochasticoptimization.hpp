#ifndef API_STOCHASTICOPTIMIZATION_HPP
#define API_STOCHASTICOPTIMIZATION_HPP

#include "move4d/API/Trajectory/trajectory.hpp"
#include "move4d/Logging/Logger.h"
namespace nlopt{
class opt;
}

namespace move4d
{
namespace API {
class Path;

/**
 * @brief The StochasticOptimization class
 * @note parameters used:
 *  * PlanEnv->getBool(PlanParam::withMaxIteration)
 *  * PlanEnv->getInt(PlanParam::smoothMaxIterations)
 *  * PlanEnv->getBool(PlanParam::trajWithTimeLimit)
 *  * PlanEnv->getDouble(PlanParam::timeLimitSmoothing)
 */
class StochasticOptimization
{
    MOVE3D_STATIC_LOGGER;
public:
    StochasticOptimization(Path &traj, double nb_deriv=2, bool clamp_start=true, bool clamp_goal=true);
    virtual ~StochasticOptimization();

    bool run(bool fix_pos=false);
    bool computeSpeeds();
    Path *getTrajetory() const;
    static GeometricPath make_traj(Robot *r, const std::vector<std::pair<double, RobotState> > &states);
    /// for nlopt
    GeometricPath make_traj_estimate(const std::vector<double> &x);
    GeometricPath make_traj(const std::vector<double> &x);
    static double cost(const std::vector<double> &x, std::vector<double> &grad, void *data);
    double getCost(const std::vector<double> &x);
    static double constraint(const std::vector<double> &x, std::vector<double> &grad, void *data);
    double getConstraint(const std::vector<double> &x);

    static void durationConstraint(unsigned m, double *result, unsigned n, const double* x, double* grad, void* f_data);
    void getDurationConstraint(std::vector<double> &result, const std::vector<double> &x, std::vector<double> &grad);

    static void speedConstraint(unsigned m, double *result, unsigned n, const double* x, double* grad, void* f_data);
    void getSpeedConstraint(std::vector<double> &result, const std::vector<double> &x, std::vector<double> &grad);


    bool testNlOPT(bool fix_pos=false);

    unsigned int getNbDerivatives() const;
    void setNbDerivatives(unsigned int nb_derivatives);

    std::string nloptCodeToString(int code);
    static std::string nloptAlgoIdToString(int code);


    double getFtol_rel() const;
    void setFtol_rel(double ftol_rel);

    double getXtol_rel() const;
    void setXtol_rel(double xtol_rel);

    int getAlgoId() const;
    void setAlgoId(const int &algoId);

    bool isKinoEstimation() const;
    void setKinoEstimation(bool kinoEstimation);

    void kinoConstraint(std::vector<double>::iterator result_from, std::vector<double>::iterator result_end, Path *traj);
    static void kinoConstraint(std::vector<double>::iterator result_from, std::vector<double>::iterator result_end, Path *traj, uint nb_derivatives);
protected:
    std::vector<double> trajToSTL(uint nb_deriv, bool fix_pos, std::vector<double> &lower_bounds, std::vector<double> &upper_bounds) const;

    void computeSpeed(RobotState &pos_prev, RobotState &pos, RobotState &pos_next) const;

    bool checkStopCondition(uint iteration);

    /// delete pointers and set them to NULL
    void reset();

private:
    std::vector<std::pair<double,RobotState> > _states; ///< duration from previous + state
    move4d::API::Path *_traj;
    nlopt::opt *_nlopt;
    unsigned long _eval_count;
    unsigned int _nb_derivatives;
    double _xtol_rel,_ftol_rel;
    double _cost;
    long unsigned int _costTests; ///< nb of cost tests done per run
    Robot *_robot;
    bool _clampStart,_clampGoal;
    bool _fix_pos;
    double _time;
    int _algoId;
    bool _kinoEstimation; ///< if true, estimates speed from time parameter instead of optimizing speed. add a constraint cost to respect acceleration bounds
    class NloptData;
    NloptData *_nlopt_data;
    struct TimeProfiles{
        double descr_to_traj;
        double cost;
    } _time_profiles;
};

} // namespace API

} //namespace move4d
#endif // API_STOCHASTICOPTIMIZATION_HPP
