#ifndef COST_SPACE_HPP_INCLUDED
#define COST_SPACE_HPP_INCLUDED

#include <boost/function.hpp>
#include <map>
#include <string>
#include <boost/thread/mutex.hpp>

#include "move4d/API/ConfigSpace/RobotState.hpp"
//#include "move4d/planner/planEnvironment.hpp"
//#include "move4d/API/Roadmap/graph.hpp"
#include "move4d/Logging/Logger.h"

#include "move4d/API/forward_declarations.hpp"

struct graph;
struct node;
class doubleContainer;
namespace move4d
{
class Node;
/**
 * FlyCrane
 */
typedef Eigen::Matrix<double, 6, 1> Vector6d;
typedef Eigen::Matrix<double, 6, 6> Matrix6d;
typedef Eigen::Matrix<double, 5, 6> Matrix56d;


/*!
 * Delta step cost method enum
 */
enum CostSpaceDeltaStepMethod 
{
	cs_integral,
	cs_mechanical_work,
	cs_visibility,
	cs_average,
	cs_config_cost_and_dist,
	cs_boltzman_cost,
  cs_max,
    cs_average_and_max
};

/*!
 * Delta step cost method enum
 */
enum CostSpaceResolutionMethod
{
  cs_classic,
  cs_pr2_manip
};

/*!
 * Class thats holding the CostSpace
 */
class CostSpace
{
    MOVE3D_STATIC_LOGGER;
public:
    typedef boost::function<double (RobotState&)> StateCostFunc;
    typedef boost::function<double(API::Path&)> PathCostFunc;
  CostSpace();
  
  // Get selected cost function name
  std::string getSelectedCostName();
  std::string getSelectedCostTrajName();
  
  // Get All Cost Functions
  std::vector<std::string> getAllCost();
  std::vector<std::string> getAllCostTraj();

  StateCostFunc getCostFunction(std::string name);
  PathCostFunc getCostFunctionTrajectories(std::string name);
  
  // Select the cost function with the given name in the map
  bool setCost(std::string name);
  bool setCostTraj(std::string name);
  
  // Register a new cost function.
  void addCost(std::string name, 
               StateCostFunc f);
  void addCostTrajectories(std::string name,
               PathCostFunc f);

  // Delete a cost function
  void deleteCost(std::string name);
  void deleteCostTraj(std::string name);

  // Compute the cost of the configuration conf.
  double cost(RobotState & conf);

  /**
   * Compute the cost over localpath
   * @param path the path
   * @param kino compute cost along a kinodynamic path
   */
  double cost(API::LocalPathBase * path, bool kino = false);

  double cost(std::vector<API::LocalPathBase*>::iterator first, std::vector<API::LocalPathBase*>::iterator last) __attribute_deprecated__;
  double cost(std::vector<API::LocalPathBase*> &path) __attribute_deprecated__;
  double cost(API::Path& path);

  /**
   * compute the cost over the trajectory
   * @param path for this trajectory
   * @param kino compute cost along a kinodynamic path
   * @return the cost
   */
  double costAggregationOverTraj(API::Path &path, bool kino = false);
  double costAggregationOverTraj(std::vector<API::LocalPathBase*> &path, bool kino = false) __attribute_deprecated__;
  /**
   * compute the cost over the trajectory
   * @param first form this localpath
   * @param last to this localpath
   * @param kino compute cost along a kinodynamic path
   * @return the cost
   */
  double costAggregationOverTraj(std::vector<API::LocalPathBase*>::iterator first, std::vector<API::LocalPathBase*>::iterator last, bool kino = false) __attribute_deprecated__;

  // Compute the cost of
  double cost(API::LocalPathBase& path, int& nb_test);

	// Set node cost
	void setNodeCost(Node* node, Node* parent);
	
  // Initializes the Cost space motion planning problem
  void initMotionPlanning(graph* graph, node* start, node* goal);
  
  // Set DeltaStepCost
  void setDistanceMethod(CostSpaceResolutionMethod method) { m_resolution = method; }
	
  // Set DeltaStepCost
  void setDeltaStepMethod(CostSpaceDeltaStepMethod method) { m_deltaMethod = method; }
  
  // Get DeltaStepCost
  CostSpaceDeltaStepMethod getDeltaStepMethod() { return m_deltaMethod; }
	
	// Compute the delta step cost
  double deltaStepCost(double cost1, double cost2, double length);
  double edgeCost(statePtr_t from, statePtr_t to);
  double edgeCost(API::localPathBasePtr_t lp);

  //Composite cost
  double compositeCost(RobotState &conf);
  void setCompositeCostFactor(std::string func, double factor);
  double getCompositeCostFactor(std::string func);
  doubleContainer *getCompositeCostFactorObject(std::string func);

  void initCompositeCostFactors();
  void setCompositeSumFunction(std::string f);
  std::vector<std::string> getCompositeSumFunctions(void);
  std::string getCompositeSumFunction();

  // distance start goal
  double startGoalDist_ratio() const;
  void setStartGoalDist_ratio(double startGoalDist_ratio);
  double startGoalDist_power() const;
  void setStartGoalDist_power(double startGoalDist_power);




  
protected:
  std::string mSelectedCostName;
  StateCostFunc mSelectedCost;
  std::map<std::string, StateCostFunc > mFunctions;

  boost::mutex mMutex;
  std::map<std::string,double> mCostDetails;


  std::string mSelectedCostTrajectoriesName;
  PathCostFunc  mSelectedFunctionsTrajectories;
  std::map<std::string, PathCostFunc > mFunctionsTrajectories;
  
  void getPr2ArmConfiguration( Eigen::VectorXd& x, statePtr_t q );
	double getPr2ArmDistance( Robot* robot, Eigen::VectorXd& q_i, Eigen::VectorXd& q_f );
  /**
   * Compute kinodynamic cost over localpath
   * @param path the local path
   * @return the cost
   */
  double kinoCost(API::LocalPath * path);

private:
	
  // Delta
  enum CostSpaceDeltaStepMethod m_deltaMethod;
  enum CostSpaceResolutionMethod m_resolution;
  
  // Composite
  std::map<std::string , doubleContainer* > mCompositionFactors;
  enum{ COST_COMPOSITE_PLUS, COST_COMPOSITE_MAX, COST_COMPOSITE_MIN} mCompostionFunction;

  // distance start goal
  double mStartGoalDist_ratio, mStartGoalDist_power;

  double m_dmax;

    /*************************************************************************************
     ****************        FlyCrane      ***********************************************
            NOTATION:
                                     (quadrotor 1)
                                         A21
                                        .   .
                                       .     .
                                      .       .
                                     .         .
                                    B2---------B1
                                  //             \\
                                 B3               B0
                                 .\               / .
                                .  \  Platform   /   .
                               .    \           /     .
                              .      \         /       .
                             .        \       /         .
           (quadrotor 2)   A43 . . . . \     / . . . . A05  (quadrotor 0)
                                        B4==B5
     *************************************************************************************/

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    //! Initialize the FlyCrane cost space.
    void initializeFlyCraneCostSpace();

    /**
     * Compute the rotation matrix of a given vector of angles.
     * (This function is useful mainly to keep track of the Euler convention used here.)
     */
    Eigen::Matrix3d RotationMatrix(double orientation[]);

    /**
     * Compute the coordinates (in the global reference frame) of the anchor points, Aij, of the quadrotors
     * and fill the corresponding attributes in the given configuration.
     */
    void computeQuadAnchorPoints(RobotState * conf);

    //! Cost function involving the tensions exerted on the cables and the quadrotor thrusts.
    double flyCraneCost(RobotState * conf);

    std::map<std::string, double> getCostDetails();
    void setCostDetails(std::map<std::string, double> value);

private:
    //! position of each anchor point of the platform in the local reference frame
    Eigen::Vector3d B[6];

    //! length of each cable (in meters)
    double cableLength[6];

    //! interval of admissible tension of each cable: [ admissibleTension[k][0], admissibleTension[k][1] ]
    double admissibleTension[6][2];

    //! wrench exerted on the platform
    Vector6d wrench;

    //! coordinates (in the local frame) of the point of the platform on which the wrench is exerted
    Eigen::Vector3d wrenchPoint;

    //! ellipsoid defining the uncertainty region around the given wrench
    Matrix6d Ellipsoid;

    //! mass of each quadrotor
    double quadMass[3];

    //! maximum thrust that each quadrotor can exert
    double quadThrust[3];

	//! offset of the cable anchor points with respect to the center of mass of each quadrotor (in meters)
	double offsetAnchorCM[3];

	//! distance from the center of mass to the center of the motors, for each quadrotor (in meters)
	double dCMmotor[3];

    //! vectors B5-B0, B1-B2 and B3-B4 after normalization
    Eigen::Vector3d vB[3];

    //! angles formed by vector vB05 and cable 0, by vector vB21 and cable 2, and by vector vB43 and cable 4
    double beta[3];

    //! weight of each quadrotor
    Eigen::Vector3d quadWeight[3];
};

namespace GlobalCostSpace 
{
	void initialize();
};

double computeIntersectionWithGround(RobotState & conf);
double computeFlatCost(RobotState & conf);
double computeClearance(RobotState & conf);
double computeOBBClearance(RobotState & conf);
double computeClearanceAll(RobotState & conf);
double computeDistanceToObstacles(RobotState & conf);
double computeARCAScost(RobotState & conf);
double computeInCollisionCost(RobotState & conf);
//double computeLocalpathKinematicCost(p3d_rob* rob, p3d_localpath* LP);
double computeHotCost(RobotState &conF) __attribute_deprecated__;
double computeCompositeCost(RobotState &conf);
double computeHandoverCost(RobotState &conf);
double computeStartGoalMin(RobotState &conf);
double computeStraightPenalty(RobotState &conf);
double computeSaphariCost(RobotState &conf);

/**
 * FlyCrane cost function.
 */
double computeFlyCranecost(RobotState & conf);


extern CostSpace* global_costSpace;

} //namespace move4d
#endif
