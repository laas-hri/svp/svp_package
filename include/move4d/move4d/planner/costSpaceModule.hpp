#ifndef COSTSPACEMODULE_H
#define COSTSPACEMODULE_H

#include "move4d/Logging/Logger.h"
#include "move4d/API/moduleBase.hpp"

namespace move4d
{
class CostSpaceModule : public ModuleBase
{
    MOVE3D_STATIC_LOGGER;
protected:
    CostSpaceModule();
public:
    static CostSpaceModule *getInstance();
    ~CostSpaceModule();
    void initialize();
    std::string getName();
    static std::string name();

    void enable(bool enable=true);
    bool isEnabled();

private:
    static CostSpaceModule *_instance;

};

} //namespace move4d
#endif // COSTSPACEMODULE_H
