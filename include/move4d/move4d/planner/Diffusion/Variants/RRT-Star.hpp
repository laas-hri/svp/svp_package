/*
 * RRT-Star.hpp
 *
 *  Created on: Nov 04, 2013
 *      Author: ddevaurs
 */

#ifndef _RRT_STAR_HPP_
#define _RRT_STAR_HPP_

#include "move4d/planner/Diffusion/RRT.hpp"


namespace move4d
{
/**
 * Expansion procedure of the RRT* algorithm.
 */
class StarExpansion: public RRTExpansion
{
public:
	//! Constructor.
	StarExpansion(Graph * graph);

	//! Destructor.
	virtual ~StarExpansion() {};

private:
	/**
	 * Check whether the given path is a valid connection.
	 * @return TRUE if the connection is valid, and FALSE otherwise
	 */
	bool isValidConnection(localPathBasePtr_t path);

	/**
	 * Add a node to the graph, after an expansion that has produced the given path, starting from fromNode.
	 * @param directionNode: node toward which the expansion was attempted.
	 * @return the added node
	 */
	Node * addNode(localPathBasePtr_t path, Node * fromNode, Node * directionNode);

private:
	//! dimension of the configuration space
	unsigned dimension;

	//! constant used to compute the radius of the ball within which connections are attempted
	double gamma;

	//! minimal value that the 'gamma' constant should be allow to take
	double gamma_star;
};


/**
 * The RRT* algorithm.
 */
class StarRRT: public RRT
{
public:
	//! Constructor.
	StarRRT() = default;
	StarRRT(Robot * robot, Graph * graph) : RRT(robot, graph) {
		std::cout << "Construct the RRT*" << std::endl;
	}

	//! Destructor.
	virtual ~StarRRT() {};

	/**
	 * Initialize RRT*.
	 * @return the number of nodes added to the graph
	 */
	unsigned init() {
		expansion = new StarExpansion(_Graph);
		return TreePlanner::init();
	}

	/**
	 * Check the stopping conditions of RRT*.
	 * @return TRUE if one of the conditions is satisfied, and FALSE otherwise
	 */
	bool checkStopConditions() {
		return TreePlanner::checkStopConditions();
	}
};

} //namespace move4d
#endif
