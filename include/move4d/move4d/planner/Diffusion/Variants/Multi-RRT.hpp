/*
 * Multi-RRT.hpp
 *
 *  Created on: Nov 29, 2012
 *      Author: ddevaurs
 */

#ifndef _MULTI_RRT_HPP_
#define _MULTI_RRT_HPP_

#include "move4d/planner/Diffusion/RRT.hpp"

namespace move4d
{

/**
 * A Multiple-tree variant of the RRT algorithm: Multi-RRT.
 */
class MultiRRT: public RRT
{
public:
    //! Constructor.
	MultiRRT() = default;

	MultiRRT(Robot * robot, Graph * graph) : RRT(robot, graph) {
		std::cout << "Construct the Multi-RRT" << std::endl;
	}

	//! Destructor.
	virtual ~MultiRRT() {};

	/**
	 * Initialize the Multi-RRT.
	 * @return the number of nodes added to the graph
	 */
	virtual unsigned init() {
		this->loadWaypoints();
		return RRT::init();
	}

    /**
	 * Main function of the Multi-RRT.
	 * @return the number of nodes added to the graph
	 */
	unsigned run();

	/**
	 * Check whether the Multi-RRT has found a trajectory.
	 * @return TRUE if the criteria required for a trajectory to be found are met, and FALSE otherwise
	 */
	bool trajFound();
	
	API::GeometricPath * extractTrajectory(statePtr_t q_init, statePtr_t q_goal);

protected:
	//! Load the user-defined waypoints (start, goal, and potential intermediate waypoints).
	void loadWaypoints();

private:
	//! Extract a trajectory going through all the waypoints.
	API::GeometricPath * extractTrajectory();

	/**
	 * Solve the Traveling Salesman Problem by exhaustive search.
	 * @param distances: pairwise distance matrix given as input
	 * @param solution: shortest path returned as output
	 * @return the length of the shortest path
	 */
	double TPSbyExhaustiveSearch(std::vector<std::vector<double> > & distances,
			std::vector<unsigned> & solution);

	/**
	 * Solve the Traveling Salesman Problem using the Nearest Neighbor heuristic.
	 * @param distances: pairwise distance matrix given as input
	 * @param solution: shortest path returned as output
	 * @return the length of the shortest path
	 */
	double TPSwithNearestNeighbor(std::vector<std::vector<double> > & distances,
			std::vector<unsigned> & solution);

	/**
	 * Solve the Traveling Salesman Problem using the Multi-Fragment heuristic.
	 * @param distances: pairwise distance matrix given as input
	 * @param solution: shortest path returned as output
	 * @return the length of the shortest path
	 */
	double TPSwithMultiFragments(std::vector<std::vector<double> > & distances,
			std::vector<unsigned> & solution);

protected:
	//! waypoints through which the solution path should go
	std::vector<statePtr_t> waypoints;

	//! nodes containing the waypoints
	std::vector<Node *> waypointNodes;
};

} //namespace move4d
#endif
