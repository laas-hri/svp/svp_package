/**
 * @file TransitionSST.hpp
 *
 * @copyright  2016 CNRS/LAAS. All rights reserved.
 * @author Jean-François Erdelyi
 *
 */
#ifndef LIBMOVE3D_PLANNERS_TRANSITIONSST_HPP
#define LIBMOVE3D_PLANNERS_TRANSITIONSST_HPP

#include "move4d/API/ConfigSpace/localpath.hpp"
#include "move4d/planner/Diffusion/SST.hpp"

namespace move4d
{
class TransitionSST : public SST {
public:
	TransitionSST() = default;
	/**
	 * @brief Constructor
	 * @param robot T-SST for this robot
	 * @param graph the graph
	 * @param start from this node
	 * @param goal to this node
	 */
	TransitionSST(Robot *robot, Graph *graph, statePtr_t start, statePtr_t goal);

	/**
	 * @brief Perform an iteration of a motion planning algorithm.
	 * @details Perform an iteration of a motion planning algorithm.
	 */
	virtual void step();

	/**
	 * @brief Get temperature
	 * @return temperature
	 */
	double getTemperature();

protected:
	/**
	 * @brief cost of the highest-cost node in the graph
	 */
	double _highestCost;

	/**
	 * @brief cost of the lowest-cost node in the graph
	 */
	double _lowestCost;

	/**
	 * @brief temperature parameter of T-SST
	 */
	double _temperature;
    double _temperature_tmp;

	/**
	 * @brief weight for cost, used to compute the cost of the trajectory
	 */
	double _weightCost;

	/**
	 * @brief weight for duration, used to compute the cost of the trajectory
	 */
	double _weightDuration;

	/**
	 * @brief costs over the current trajectory
	 */
	std::vector<double> costs;

protected:
	/**
	 * Check whether the given path satisfies the cost constraints, and compute its cost.
	 * @param temperatureTuning: with or without temperature tuning in the transition test
	 * @return TRUE if this the case, and FALSE otherwise
	 */
	bool isCostValid(API::localPathPtr_t path, bool temperatureTuning = true);

	/**
	 * Transition test based on the cost difference between the two given configurations.
	 * @param temperatureTuning: with or without temperature tuning
	 * @return TRUE if the transition is accepted, and FALSE otherwise
	 */
	bool transitionTest(double from, double to, bool temperatureTuning = true);

	/**
	 * Compare the given cost to the current lowest and highest costs, and update them if necessary.
	 * @param cost the cost to test
	 */
	void updateCostRange(double cost);

	/**
	 * @brief Check if the currently created state is close to a witness.
	 * @details Check if the currently created state is close to a witness.
	 * @param costNew the cost of the new trajectory
	 */
	virtual bool isNodeLocallyTheBestSST(double costNew);

};

} //namespace move4d
#endif //LIBMOVE3D_PLANNERS_TRANSITIONSST_HPP
