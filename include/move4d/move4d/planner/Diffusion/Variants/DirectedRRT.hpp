/*
 * DirectedRRT.hpp
 *
 *  Created on: Jan 2015
 *      Author: aboeuf
 */

#ifndef DIRECTED_RRT_HPP
#define DIRECTED_RRT_HPP

#include "move4d/planner/Diffusion/RRT.hpp"
namespace move4d
{

class DirectedExpansion : public RRTExpansion {

public:

    DirectedExpansion(Graph * graph) : RRTExpansion(graph){
        _expandGoalTree=false;
    }

    virtual ~DirectedExpansion(){}

    bool expandGoalTree();
    void switchTrees();
    unsigned extend(Node * fromNode, statePtr_t directionConf, Node * directionNode);
    unsigned connect(Node * fromNode, statePtr_t directionConf, Node * directionNode);
    Node* addNode(localPathBasePtr_t path, Node * fromNode, Node * directionNode);
    bool addCycles();

private:
    bool _expandGoalTree;

};

class DirectedRRT : public RRT {

public:
    DirectedRRT() = default;
    DirectedRRT(Robot *robot,Graph *graph) : RRT(robot, graph) {
        std::cout<<"Construct the DirectedRRT"<<std::endl;
    }
    virtual ~DirectedRRT(){}

    unsigned init();
    unsigned run();
    bool trajFound();
    unsigned expandOneStep(ConnectedComponent * fromComp);
    bool checkStopConditions();

private:

    void updateConnections();
    void pause();

};



} //namespace move4d
#endif
