/*
 * Threshold-RRT.hpp
 *
 *  Created on: Feb 05, 2013
 *      Author: ddevaurs
 */

#ifndef _THRESHOLD_RRT_HPP_
#define _THRESHOLD_RRT_HPP_

#include "move4d/planner/Diffusion/RRT.hpp"
#include "move4d/planner/Diffusion/Variants/Transition-RRT.hpp"
#include "move4d/API/plannerBase.hpp"


namespace move4d
{
/**
 * Expansion procedure of the obstacleness-aware RRT (RRT_obst) algorithm,
 * which involves an adaptive cost threshold.
 * The threshold value can be defined by the user in the GUI (if left at 0, it means 'not user-defined').
 * If no value is defined by the user, the threshold value is automatically set to be
 * the minimal cost of all the nodes present in the graph at initialization time.
 */
class ThresholdExpansion: public TransitionExpansion
{
public:
	//! Constructor.
	ThresholdExpansion(Graph * graph) : TransitionExpansion(graph)
	{
		if (ENV.getDouble(Env::costThreshold) > 0.)
			// if the threshold has been defined by the user, use that value
			threshold = ENV.getDouble(Env::costThreshold);
		else
			// otherwise, use the minimal cost of nodes present in the graph
			threshold = lowestCost;

		// set the threshold increment
		increment = ENV.getDouble(Env::thresholdIncrement);
	}

	//! Destructor.
	virtual ~ThresholdExpansion() {};

    /**
	 * RRT_obst Extend.
	 * @param fromNode: node from which the expansion is performed
	 * @param directionConf: configuration toward which the expansion is going
	 * @param directionNode: node toward which the expansion is going (when relevant)
	 * @return the number of created nodes
	 */
	unsigned extend(Node * fromNode, statePtr_t directionConf, Node * directionNode) {
		threshold += increment;
		return TransitionExpansion::extend(fromNode, directionConf, directionNode);
	}

	/**
	 * RRT_obst Connect.
	 * @param fromNode: node from which the expansion is performed
	 * @param directionConf: configuration toward which the expansion is going
	 * @param directionNode: node toward which the expansion is going (when relevant)
	 * @return the number of created nodes
	 */
	unsigned connect(Node * fromNode, statePtr_t directionConf, Node * directionNode) {
		threshold += increment;
		return TransitionExpansion::connect(fromNode, directionConf, directionNode);
	}

private:
	/**
	 * Transition test based on the cost threshold.
	 * @param fromConf: useless parameter inherited from the TransitionExpansion
	 * @param toConf: configuration whose cost is compared to the threshold
	 * @param temperatureTuning: useless parameter inherited from the TransitionExpansion
	 * @return TRUE if the transition is accepted, and FALSE otherwise
	 */
	bool transitionTest(statePtr_t fromConf, statePtr_t toConf, bool temperatureTuning = true) {
		return (toConf->cost() <= threshold);
	}

private:
	//! value of the cost threshold
	double threshold;

	//! increment for the cost threshold
	double increment;
};


/**
 * The obstacleness-aware RRT (RRT_obst) algorithm, which involves an adaptive cost threshold.
 */
class ThresholdRRT: public RRT
{
public:
	//! Constructor.
	ThresholdRRT() = default;
	ThresholdRRT(Robot * robot, Graph * graph) : RRT(robot, graph) {
		std::cout << "Construct the Threshold-RRT" << std::endl;
	}

	//! Destructor.
	virtual ~ThresholdRRT() {};

	/**
	 * Initialize RRT_obst.
	 * @return the number of nodes added to the graph
	 */
	unsigned init() {
		expansion = new ThresholdExpansion(_Graph);
		return TreePlanner::init();
	}
};
} //namespace move4d
#endif
