/*
 * BaseExpansion.hpp
 *
 *  Created on: Jun 11, 2009
 *      Author: jmainpri
 */
#ifndef P3D_BASE_EXPANSION_HPP
#define P3D_BASE_EXPANSION_HPP

#include "move4d/API/ConfigSpace/RobotState.hpp"
#include "move4d/API/ConfigSpace/localpath_factory.hpp"

#include "libmove3d/p3d/env.hpp"

#include "move4d/API/forward_declarations.hpp"

namespace move4d
{
#ifndef GRAPH_HPP
class Graph;
#endif

#ifndef NODE_HPP
class Node;
#endif

/**
 * @ingroup Diffusion
 * 
 * The expansion class holds the method and local variables
 * allowing to expand a Tree in a given configuration space
 * methods such as chose a direction, check the validity in that direction and 
 * add nodes to the tree
 */
class BaseExpansion 
{
public:
	
	/**
	 * Constructor
	 */
	BaseExpansion();
	BaseExpansion(Graph* prtGraph);
	
	/**
	 * Destructor
	 */
	~BaseExpansion();
	
	/**
	 * Set the graph that is beeing expanded
	 */
	void setGraph(Graph* G) { m_Graph = G; }
	
	/**
	 * Get the graph that is beeing expanded
	 */
	Graph* getGraph() { return m_Graph; }
	
	/**
	 * Get Node Expansion Method
	 */
	int getDirectionMethod() { return m_ExpansionDirectionMethod; }
	
	/**
	 * Set Expansion node Method
	 */
	void setDirectionMethod(int directionExpansion) { m_ExpansionDirectionMethod = directionExpansion; }
	
	/**
	 * Get Node Expansion Method
	 */
	int getNodeMethod() { return m_ExpansionNodeMethod; }
	
	/**
	 * Set Expansion node Method
	 */
	void setNodeMethod(int nodeExpansion) { m_ExpansionNodeMethod = nodeExpansion; }
	
	/**
	 * Set the From Connected Component
	 */
	void setFromComp(Node* fromComp) { m_fromComp = fromComp; }  
	
	/**
	 * Get the From Connected Component
	 */
	Node* getFromComp() { return m_fromComp; }
	
	/**
	 * Set the To Connected Component
	 */
	void setToComp(Node* toComp) { m_toComp = toComp; }  
	
	/**
	 * Get the To Connected Component
	 */
	Node* getToComp() { return m_toComp; }
	
	/**
	 * Expansion Step (Delta)
	 */
	double step();

	double getDMax();

	//! Compute the interpolation factor along the path, based on the extension step.
	double computeInterpolationFactor(double pathLength) {
		return (pathLength == 0.) ? 1. : std::min(1., this->step() / pathLength );
	}

	//! Interpolation along the given path.
	statePtr_t interpolate(statePtr_t conf, API::LocalPathBase * path, double factor);

	/**
	 * Position on localpath in [0,1]
	 */
	double positionAlongPath(API::LocalPathBase& path, double param);
	
	/**
	 * Return the path parameter that is 
	 * the closest to step()
	 */
	double pathDelta(API::LocalPathBase& path);
	
	/**
	 * Computes a localpath parameter used for expansion
	 * of at max step() of length
	 */
	API::localPathBasePtr_t getExtensiontPath(std::shared_ptr<RobotState> qi,
															std::shared_ptr<RobotState> qf );
	
	/**
	 * Function called when a node can not be connected
	 * @param the node which has not been connected
	 */
	void expansionFailed(Node& node);
	
	/**
	 * Adds a node to a connected component
	 */
	virtual Node* addNode(Node* currentNode, API::LocalPathBase& path, double pathDelta,
												Node* directionNode, unsigned& nbCreatedNodes);
	
	/**
	 * Function that balances the ratio of exploration towards refinement
	 */
	bool refinementControl(API::LocalPathBase& path, double rho, Node& compNode);

	/**
	 * Returns a valid configuration on the local path
	 */
	bool nextStep(API::LocalPathBase& path,
								std::shared_ptr<RobotState>& directionConfig,
								double& pathDelta,
								std::shared_ptr<API::LocalPathBase>& newPath,
								Env::expansionMethod method);
	
	/**
	 * Returns a valid configuration on the local path
	 */
	bool nextStep(API::LocalPathBase& path,
								Node* directionNode,
								double& pathDelta,
								std::shared_ptr<API::LocalPathBase>& newPath,
								Env::expansionMethod method);
	
	/**
	 * Gets the nearest node in the graph
	 *
	 * @param compNode Expanding component
	 * @param direction Direction of expansion
	 * @param Sampling passive mode
	 */
	virtual Node* getExpansionNode(Node* compNode, statePtr_t direction, int distance);

	std::shared_ptr<RobotState> getExpansionDirection(Node* expandComp, Node* goalComp,
															bool samplePassive, Node*& directionNode);

	//! Return last added node
    Node* getLasAddedNode() {
        return m_last_added_node;
    }

    void addConnectibleNode(){
        _nbConnectibleNodes++;
    }

    unsigned int getNbConnectibleNodes(){
        return _nbConnectibleNodes;
    }

protected:
	
	int m_ExpansionNodeMethod;
	int m_MaxExpandNodeFailure;
	int m_kNearestPercent;
	
	int m_ExpansionDirectionMethod; // = GLOBAL_CS_EXP;
	bool m_IsDirSampleWithRlg; //= FALSE;
	
	Graph* m_Graph;

	Node* m_last_added_node;

	Node* m_fromComp;
	Node* m_toComp;

    unsigned int _nbConnectibleNodes;
};

} //namespace move4d
#endif
