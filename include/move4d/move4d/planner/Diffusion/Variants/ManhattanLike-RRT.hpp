/*
 * ManhattanLike-RRT.hpp
 *
 *  Created on: Aug 31, 2009
 *      Author: jmainpri
 */

#ifndef _MANHATTAN_LIKE_RRT_HPP_
#define _MANHATTAN_LIKE_RRT_HPP_

#include "move4d/planner/Diffusion/RRT.hpp"

namespace move4d
{

/**
 * The Manhattan-Like RRT (ML-RRT) algorithm.
 */
class ManhattanLikeRRT: public RRT
{
public:
	//! Constructor.
	ManhattanLikeRRT() = default;
	ManhattanLikeRRT(Robot * robot, Graph * graph) : RRT(robot, graph) {
		std::cout << "Construct the ML-RRT" << std::endl;
	}

	//! Destructor.
	virtual ~ManhattanLikeRRT() {};

	/**
	 * Perform a single expansion step of ML-RRT, growing the given connected component.
	 * @return the number of created nodes
	 */
	unsigned expandOneStep(ConnectedComponent * fromComp);

private:
	/**
	 * expansion des joints passifs dans le cas ML_RRT
	 * @param expansionNode
	 * @param directionNode la direction de l'expansion
	 * @return le nombre de Node Créés
	 */
	unsigned passiveExpandProcess(Node * expansionNode, Node * directionNode);

	bool getCollidingPassiveJntList(Robot * R, RobotState & qinv, std::vector<p3d_jnt *> & joints);

	bool selectNewJntInList(std::vector<p3d_jnt *> & joints, std::vector<p3d_jnt *> & oldJoints,
			std::vector<p3d_jnt *> & newJoints);

	void shoot_jnt_list_and_copy_into_conf(RobotState & qrand, std::vector<p3d_jnt *> & joints);
};

} //namespace move4d
#endif
