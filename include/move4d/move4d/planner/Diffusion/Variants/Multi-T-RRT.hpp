/*
 * Multi-T-RRT.hpp
 *
 *  Created on: Nov 29, 2012
 *      Author: ddevaurs
 */

#ifndef _MULTI_T_RRT_HPP_
#define _MULTI_T_RRT_HPP_

#include <move4d/API/plannerBase.hpp>
#include "move4d/planner/Diffusion/Variants/Multi-RRT.hpp"
#include "move4d/planner/Diffusion/Variants/Transition-RRT.hpp"


namespace move4d
{
/**
 * The Multi-T-RRT algorithm.
 */
class MultiTRRT: public MultiRRT
{
public:
	//! Constructor.
	MultiTRRT() = default;
	MultiTRRT(Robot * robot, Graph * graph) : MultiRRT(robot, graph) {
		std::cout << "Construct the Multi-T-RRT" << std::endl;
	}

	//! Destructor.
	virtual ~MultiTRRT() {};

	/**
	 * Initialize the Multi-T-RRT.
	 * @return the number of nodes added to the graph
	 */
	unsigned init() {
		this->loadWaypoints();
		expansion = new TransitionExpansion(_Graph);
		return TreePlanner::init();
	}
};
} //namespace move4d

#endif
