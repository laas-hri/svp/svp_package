/*
 * Multi-Threshold-RRT.hpp
 *
 *  Created on: Feb 28, 2013
 *      Author: ddevaurs
 */

#ifndef _MULTI_THRESHOLD_RRT_HPP_
#define _MULTI_THRESHOLD_RRT_HPP_

#include <move4d/API/plannerBase.hpp>
#include "move4d/planner/Diffusion/Variants/Multi-RRT.hpp"
#include "move4d/planner/Diffusion/Variants/Threshold-RRT.hpp"


namespace move4d
{

/**
 * The Multi-Threshold-RRT algorithm.
 */
class MultiThresholdRRT: public MultiRRT
{
public:
	//! Constructor.
	MultiThresholdRRT() = default;
	MultiThresholdRRT(Robot * robot, Graph * graph) : MultiRRT(robot, graph) {
		std::cout << "Construct the Multi-Threshold-RRT" << std::endl;
	}

	//! Destructor.
	virtual ~MultiThresholdRRT() {};

	/**
	 * Initialize the Multi-Threshold-RRT.
	 * @return the number of nodes added to the graph
	 */
	unsigned init() {
		this->loadWaypoints();
		expansion = new ThresholdExpansion(_Graph);
		return TreePlanner::init();
	}
};
PLANNER_REGISTER_FACTORY(MultiThresholdRRT)
} //namespace move4d

#endif
