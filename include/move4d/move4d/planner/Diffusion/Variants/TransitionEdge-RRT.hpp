/*
 * TransitionEdge-RRT.hpp
 *
 *  Created on: Sep 17, 2013
 *      Author: bvadant
 */

#ifndef _TRANSITION_EDGE_RRT_HPP_
#define _TRANSITION_EDGE_RRT_HPP_

#include "move4d/planner/Diffusion/RRT.hpp"

namespace move4d
{

/**
 * Expansion procedure of the TransitionEdge-based RRT (TE-RRT) algorithm.
 */
class TransitionEdgeExpansion: public RRTExpansion
{
public:
    //! Constructor.
    TransitionEdgeExpansion(Graph * graph, Node * goal);

    //! Destructor.
    virtual ~TransitionEdgeExpansion() {};

	/**
	 * Sample a direction for the expansion process (taking into account the goal bias, if relevant).
	 * @param directionNode: contains the goal node when relevant
	 * @return the configuration toward which the tree will be expanded
	 */
    virtual statePtr_t sampleExpansionDirection(Node * directionNode, Node * fromNode,
                                                int nConsecutiveFail = 0);

    /**
     * Attempt to link two given nodes (i.e. two components, when relevant).
     * @return TRUE if the junction is successful.
     */
    bool attemptJunction(Node * fromNode, Node * toNode);

private:
    /**
     * TE-RRT Extend.
     * @param fromNode: node from which the expansion is performed
     * @param directionConf: configuration toward which the expansion is going
     * @param directionNode: node toward which the expansion is going (when relevant)
     * @return the number of created nodes
     */
    unsigned extend(Node * fromNode, statePtr_t directionConf, Node * directionNode);

    /**
     * TE-RRT Connect.
     * @param fromNode: node from which the expansion is performed
     * @param directionConf: configuration toward which the expansion is going
     * @param directionNode: node toward which the expansion is going (when relevant)
     * @return the number of created nodes
     */
    unsigned connect(Node * fromNode, statePtr_t directionConf, Node * directionNode);


    bool transitionTest(statePtr_t fromConf, statePtr_t toConf);
    /**
     * Transition test between fromConf and toConf, with temperature tuning.
     * @return TRUE if the transition is accepted
     */
    bool transitionTestConf(statePtr_t fromConf, statePtr_t toConf);
    /**
     * Transition test of the edge between fromConf and toConf, with temperature tuning.
     * @return TRUE if the transition is accepted
     */
    bool transitionTestEdge(statePtr_t fromConf, statePtr_t toConf);

    //! Compare the given cost to the current lowest and highest costs, and update them if necessary.
    void updateCostRange(double cost) {
        if (cost > highestCostNode) highestCostNode = cost;
        if (cost < lowestCostNode) lowestCostNode = cost;
    }

    void updateCostRangeEdge(double cost) {
        if (cost > highestCostEdge) highestCostEdge = cost;
        if (cost < lowestCostEdge) lowestCostEdge = cost;
    }

private:
    Node * goalNode;

    //! temperature parameter of the TE-RRT
    double temperatureNode;
    double temperatureEdge;

    //! cost of the highest-cost node/edge in the tree
    double highestCostNode;
    double highestCostEdge;

    //! cost of the lowest-cost node/edge in the tree
    double lowestCostNode;
    double lowestCostEdge;
};


/**
 * The TransitionEdge-based RRT (TE-RRT) algorithm.
 */
class TransitionEdgeRRT: public RRT
{
public:
    //! Constructor.
    TransitionEdgeRRT() = default;
    TransitionEdgeRRT(Robot * robot, Graph * graph) : RRT(robot, graph) {
        std::cout << "Construct the TE-RRT" << std::endl;
    }

    //! Destructor.
    virtual ~TransitionEdgeRRT() {};

    /**
     * Initialize the TE-RRT.
     * @return the number of nodes added to the graph
     */
    unsigned init();

protected:
    /**
     * Perform a single expansion step of RRT, growing the connected component containing fromNode.
     * @return the number of created nodes
     */
    unsigned expandOneStep(Node * fromNode);

};

} //namespace move4d
#endif
