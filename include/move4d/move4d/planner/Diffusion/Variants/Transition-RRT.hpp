/*
 * Transition-RRT.hpp
 *
 *  Created on: Nov 20, 2012
 *      Author: ddevaurs
 */

#ifndef _TRANSITION_RRT_HPP_
#define _TRANSITION_RRT_HPP_

#include "move4d/planner/Diffusion/RRT.hpp"

namespace move4d
{

/**
 * Expansion procedure of the Transition-based RRT (T-RRT) algorithm.
 */
class TransitionExpansion: public RRTExpansion
{
public:
	//! Constructor.
	TransitionExpansion(Graph * graph);

	//! Destructor.
	virtual ~TransitionExpansion() {};

    /**
	 * T-RRT Extend.
	 * @param fromNode: node from which the expansion is performed
	 * @param directionConf: configuration toward which the expansion is going
	 * @param directionNode: node toward which the expansion is going (when relevant)
	 * @return the number of created nodes
	 */
	virtual unsigned extend(Node * fromNode, statePtr_t directionConf, Node * directionNode);

	/**
	 * T-RRT Connect.
	 * @param fromNode: node from which the expansion is performed
	 * @param directionConf: configuration toward which the expansion is going
	 * @param directionNode: node toward which the expansion is going (when relevant)
	 * @return the number of created nodes
	 */
	virtual unsigned connect(Node * fromNode, statePtr_t directionConf, Node * directionNode);

	/**
	 * Check whether the given path is a valid connection, and compute its cost.
	 * @return TRUE if the connection is valid, and FALSE otherwise
	 */
	bool isValidConnection(localPathBasePtr_t path);

	/**
	 * Try to add cycles to the graph.
	 * @return TRUE if at least one cycle has been added, and FALSE otherwise
	 */
	bool addCycles();
    bool addCycles1();
    bool addCycles2();
    bool addCycles3();

    double getTemperature(){return temperature;}

private:
	/**
	 * Check whether the given path satisfies the cost constraints, and compute its cost.
	 * @param temperatureTuning: with or without temperature tuning in the transition test
	 * @return TRUE if this the case, and FALSE otherwise
	 */
	bool isCostValid(localPathBasePtr_t path, bool temperatureTuning = true);

	/**
	 * Transition test based on the cost difference between the two given configurations.
	 * @param temperatureTuning: with or without temperature tuning
	 * @return TRUE if the transition is accepted, and FALSE otherwise
	 */
	virtual bool transitionTest(statePtr_t fromConf, statePtr_t toConf, bool temperatureTuning = true);

	//! Compare the given cost to the current lowest and highest costs, and update them if necessary.
	void updateCostRange(double cost) {
		if (cost > highestCost) highestCost = cost;
		if (cost < lowestCost) lowestCost = cost;
	}

protected:
	//! cost of the highest-cost node in the graph
	double highestCost;

	//! cost of the lowest-cost node in the graph
	double lowestCost;

private:
	//! temperature parameter of T-RRT
	double temperature;
};


/**
 * The Transition-based RRT (T-RRT) algorithm.
 */
class TransitionRRT: public RRT
{
public:
	TransitionRRT() = default;
	//! Constructor.
	TransitionRRT(Robot * robot, Graph * graph);

	//! Destructor.
	virtual ~TransitionRRT() {};

	/**
	 * Initialize T-RRT.
	 * @return the number of nodes added to the graph
	 */
	unsigned init() {
		expansion = new TransitionExpansion(_Graph);
		return TreePlanner::init();
	}

    double getTemperature(){TransitionExpansion *te = dynamic_cast<TransitionExpansion*>(expansion); return (te ?  te->getTemperature() : -1);}
};

} //namespace move4d
#endif
