/*
 * TreePlanner.hpp
 *
 *  Created on: Aug 31, 2009
 *      Author: jmainpri
 */

#ifndef TREEPLANNER_HPP_
#define TREEPLANNER_HPP_

#include "move4d/planner/planner.hpp"
#include "move4d/planner/Diffusion/Variants/BaseExpansion.hpp"

namespace move4d
{

/**
 * Expansion procedure of the tree planner.
 */
class TreeExpansion: public BaseExpansion
{
public:
	//! Constructor.
	TreeExpansion(Graph * graph) : BaseExpansion(graph) {};

	//! Destructor.
	virtual ~TreeExpansion() {};

	//! Actions to be done after the expansion attempted from the given node, in case of success.
	void expansionSucceeded(Node * node);

	//! Actions to be done after the expansion attempted from the given node, in case of failure.
	void expansionFailed(Node * node);
};


/**
  @ingroup Diffusion
  */
class TreePlanner : public Planner {

public:
	/**
	 * Constructor
	 */
	TreePlanner() = default;
	TreePlanner(Robot* R, Graph* G);

	/**
	 * Destructor
	 */
	~TreePlanner();

	/**
	 * Initializes Planner
	 */
	virtual unsigned init();

	/**
	 * Check the stopping conditions of the Tree Planner.
	 * @return TRUE if one of the conditions is satisfied, and FALSE otherwise
	 */
	virtual bool checkStopConditions();

	/**
	 * Checks out the Pre-conditions
	 * - start and goal are not in collision
	 * - start and goal are different configurations
	 */
	virtual bool preConditions();

	/**
	 * Tries to connect a node to the other
	 * connected component of the graph
	 *
	 * @param currentNode The node that will be connected
	 * @param ComNode The Connected Component
	 */
	virtual bool connectNodeToComp(Node* N, Node* CompNode);

	/**
	 * Main function to connect to the other Connected Component
	 */
	virtual bool connectionToTheOtherCompco(Node* toNode);

	/**
	 * Expands tree from component fromComp
	 * @param fromComp the starting connex component
	 * @return the number of node created
	 */
    virtual unsigned int expandOneStep(Node * fromComp) {return 0;}

	/**
	 * Main function of the Tree process
	 * @return the number of Nodes added to the Graph
	 */
	virtual unsigned int run();

	/**
	 * Decide whether the goal bias should be applied, based on a random trial.
	 * @return TRUE if the goal bias is applied, and FALSE otherwise
	 */
	bool goalBias() const;

	//! Return time in algorithm: this function must be called after ChronoTimeOfDayOn()
	double getTime();

	//! Get the run Id.
    int getRunId() {
        return m_runId;
    }

    //! Set the run Id.
    void setRunId(int id) {
        m_runId = id;
    }

	//! Return the number of consecutive failures observed during planification.
	unsigned int getNumberOfConsecutiveFail() {
		return m_nbConscutiveFailures;
	}

	//! Return the number of expansions performed during planification.
	unsigned int getNumberOfExpansion()	{
		return m_nbExpansion;
	}

	//! Return the number of expansion that failed during planification.
	unsigned int getNumberOfFailedExpansion() {
		return m_nbFailedExpansion;
	}

	//! Return the initial number of nodes.
	unsigned int getNumberOfInitialNodes() 	{
		return m_nbInitNodes;
	}

	//! Return the last node added to the graph.
    Node* getLastNode() {
        return m_last_node;
    }

    double getDistanceGap(){ return m_DistanceGap;}

protected:
	int m_runId;

	unsigned int m_nbConscutiveFailures;
	unsigned int m_nbExpansion;
	unsigned int m_nbFailedExpansion;
	unsigned int m_nbInitNodes;
    double m_DistanceGap;

    Node* m_last_node;
};

} //namespace move4d
#endif /* TREEPLANNER_HPP_ */
