/*
 * RRT.hpp
 *
 *  Created on: Nov 20, 2012
 *      Author: ddevaurs
 */

#ifndef _RRT_HPP_
#define _RRT_HPP_

#include <iostream>
#include "move4d/API/ConfigSpace/localpath.hpp"

#include "move4d/planner/Diffusion/TreePlanner.hpp"

namespace move4d
{
using namespace API;
class ConnectedComponent;


/**
 * Expansion procedure of the Rapidly-exploring Random Tree (RRT) algorithm.
 */
class RRTExpansion: public TreeExpansion
{
public:
	//! Constructor.
    RRTExpansion(Graph * graph) : TreeExpansion(graph) {};

	//! Destructor.
	virtual ~RRTExpansion() {};

	/**
	 * RRT Extend.
	 * @param fromNode: node from which the expansion is performed
	 * @param directionConf: configuration toward which the expansion is going
	 * @param directionNode: node toward which the expansion is going (when relevant)
	 * @return the number of created nodes
	 */
	virtual unsigned extend(Node * fromNode, statePtr_t directionConf, Node * directionNode);

	/**
	 * RRT Connect.
	 * @param fromNode: node from which the expansion is performed
	 * @param directionConf: configuration toward which the expansion is going
	 * @param directionNode: node toward which the expansion is going (when relevant)
	 * @return the number of created nodes
	 */
	virtual unsigned connect(Node * fromNode, statePtr_t directionConf, Node * directionNode);

	/**
	 * Check whether the given path is a valid connection.
	 * @return TRUE if the connection is valid, and FALSE otherwise
	 */
	virtual bool isValidConnection(localPathBasePtr_t path) {
		// check whether the path is collision-free
		return path->isValid();
	}

	/**
	 * Try to add cycles to the graph.
	 * @return TRUE if at least one cycle has been added, and FALSE otherwise
	 */
	virtual bool addCycles();
    virtual bool addCycles1();
    virtual bool addCycles2();
    virtual bool addCycles3();

protected:
	/**
	 * Add a node to the graph, after an expansion that has produced the given path, starting from fromNode.
	 * If the expansion has successfully reached the already existing node directionNode,
	 * directionNode is simply linked to fromNode.
	 * Otherwise, a new node containing the end configuration of the path is created and linked to fromNode.
	 * @return the added node
	 */
	virtual Node * addNode(localPathBasePtr_t path, Node * fromNode, Node * directionNode);
};


/**
 * The Rapidly-exploring Random Tree (RRT) algorithm.
 */
class RRT: public TreePlanner
{
    MOVE3D_STATIC_LOGGER;
public:
	RRT() = default;
	//! Constructor.
    RRT(Robot * robot, Graph * graph);

	//! Destructor.
	virtual ~RRT() { delete expansion; }

	/**
	 * Initialize RRT.
	 * @return the number of nodes added to the graph
	 */
	virtual unsigned init() {
        expansion = new RRTExpansion(_Graph);
		return TreePlanner::init();
	}

	/**
	 * Main function of RRT.
	 * @return the number of nodes added to the graph
	 */
	virtual unsigned run();

	/**
	 * Check whether RRT has found a trajectory in the graph it is building.
	 * @return TRUE if the criteria required for a trajectory to be found are met, and FALSE otherwise
	 */
	virtual bool trajFound();

    void addConnectibleNode(){
        expansion->addConnectibleNode();
    }

    unsigned int getNbConnectibleNodes(){
        return expansion->getNbConnectibleNodes();
    }

protected:
	/**
	 * Perform a single expansion step of RRT, growing the given connected component.
	 * @return the number of created nodes
	 */
	virtual unsigned expandOneStep(ConnectedComponent * fromComp);

	/**
	 * Check the stopping conditions of RRT.
	 * @return TRUE if one of the conditions is satisfied, and FALSE otherwise
	 */
	bool checkStopConditions();

    //! Extract a trajectory from Start to Goal.
    virtual API::GeometricPath * extractTrajectory();

    //! access point to the expansion procedure of RRT
    RRTExpansion * expansion;

    std::vector<double> trajCosts;

};

} //namespace move4d
#endif
