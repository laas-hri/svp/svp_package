/*
 * EST.hpp
 *
 *  Created on: Sep 16, 2009
 *      Author: jmainpri
 */

#ifndef _EST_HPP_
#define _EST_HPP_

#include "move4d/planner/Diffusion/TreePlanner.hpp"
#include <iostream>


namespace move4d
{
/**
 * Expansion procedure of the Expansive Space Tree (EST) algorithm.
 */
class ESTExpansion: public TreeExpansion
{
public:
	//! Constructor.
	ESTExpansion(Graph * graph) : TreeExpansion(graph) {};

	//! Destructor.
	virtual ~ESTExpansion() {};

	/**
	 * EST Special case
	 */
	Node* getExpansionNode(std::vector<Node*>& nodes);

	/**
	 * EST Special case
	 */
	std::shared_ptr<RobotState> getExpansionDirection(Node* expansionNode, Node* toComp);

	/**
	 * EST Special case
	 */
	Node* expandProcessEST(Node* expansionNode, std::shared_ptr<RobotState> directionConfig,
			unsigned& nbOfNodesAdded);

	/**
	 * EST
	 */
	void printAllNodes(std::vector<Node*>& nodes);
};


/**
 * The Expansive Space Tree (EST) algorithm.
 */
class EST: public TreePlanner
{
public:
	EST() = default;
	//! Constructor.
	EST(Robot * robot, Graph * graph) : TreePlanner(robot, graph) {
		std::cout << "Construct the EST" << std::endl;
	}

	//! Destructor.
	virtual ~EST() { delete expansion; }

	/**
	 * Initialize EST.
	 * @return the number of nodes added to the graph
	 */
	virtual unsigned init();

	/**
	 * Perform a single expansion step of EST, growing the connected component to which the given node belongs.
	 * @return the number of created nodes
	 */
	unsigned expandOneStep(Node * fromComp);

private:
	/**
	 * Check the stopping conditions of EST.
	 * @return TRUE if one of the conditions is satisfied, and FALSE otherwise
	 */
	bool checkStopConditions();

	/**
	 * Check the preconditions.
	 * @return TRUE if all conditions are satisfied, and FALSE otherwise
	 */
	bool preConditions();

	/**
	 * Sample a direction for the expansion process (taking into account the goal bias, if relevant).
	 * @param directionNode: contains the goal node when relevant
	 * @return the configuration toward which the tree will be expanded
	 */
	statePtr_t sampleExpansionDirection(Node * directionNode);

	/**
	 * Try to connect the given node to the given component taking into account that the space is a cost space.
	 * @return TRUE if the connection is successful, and FALSE otherwise
	 */
	bool connectNodeToComp(Node * node, Node * compNode);

	//! Add the given node to the sorted set.
	void addNodeToSet(Node * node);

private:
	//! access point to the expansion procedure of EST
	ESTExpansion * expansion;

	std::vector<Node *> sortedNodes;
};

} //namespace move4d
#endif
