/**
 * @file SST.hpp
 *
 * @copyright  2016 CNRS/LAAS. All rights reserved.
 * @author Jean-François Erdelyi
 *
 */

#ifndef LIBMOVE3D_PLANNERS_SST_HPP
#define LIBMOVE3D_PLANNERS_SST_HPP

#include "move4d/planner/Diffusion/TreePlanner.hpp"
#include "move4d/Logging/Logger.h"
#include "move4d/API/forward_declarations.hpp"

#define SSTALGORITHM 0

namespace move4d
{
class Node;
class Witness;
class Graph;

/**
 * @brief The motion planning algorithm SST (Stable Sparse-RRT).
 * @details The motion planning algorithm SST (Stable Sparse-RRT).
 */
class SST : public TreePlanner {
public:
    typedef API::LocalPath KinoLp;
    typedef API::localPathPtr_t kinoLpPtr_t;

	SST() = default;
	/**
	* @brief Planner Constructor.
	* @details Planner Constructor.
	* @param robot The system this planner will plan for.
	* @param graph a graph belonging to that robot.
	* @param start from this node
	* @param goal to this node
	*/
	SST(Robot *robot, Graph *graph, statePtr_t start, statePtr_t goal);

	/**
	* @brief Perform any initialization tasks required before calling step().
	* @details Perform any initialization tasks required before calling step().
	*/
	unsigned int init();

	/**
	 * @brief Get the solution path.
	 * @param duration the duration of the trajectory (out)
	 * @return the trajectory
	 */
	API::GeometricPath *extractTrajectory(double& duration);

	/**
	 * @brief Get the solution path.
	 * @param duration the duration of the trajectory (out)
	 * @return the trajectory
	 */
	API::GeometricPath *extractSmoothTrajectory(double &duration);

	/**
	 * @brief Perform an iteration of a motion planning algorithm.
	 * @details Perform an iteration of a motion planning algorithm.
	 */
	virtual void step();

	/**
	 * @brief Start SST.
	 * @return the number of node created.
	 */
	unsigned int run();

	/**
	 * @brief Get number total of shoot
	 * @return number total of shoot.
	 */
	unsigned int getTotalShoot() const {
		return _totalShoot;
	}

	/**
	 * @brief Get number total of accepted nodes
	 * @return number total of accepted nodes.
	 */
	unsigned int getAccepted() const {
		return _accepted;
	}

	/**
	 * @brief Get number of reached node
	 * @return number number of reached node
	 */
	unsigned int getNbReached() const {
		return _nbReached;
	}

	/**
	 * @brief Get time of each reached nodes
	 * @return time of each reached nodes
	 */
	const std::vector<double> &getTimesReached() const {
		return _timesReached;
	}

	/**
	 * @brief Get begin time
	 * @return begin time
	 */
	clock_t getBegin() const {
		return _begin;
	}

	/**
	 * @brief Get if the goal is reached
	 * @return true if the goal is reached
	 */
	bool isGoalReached() const {
		return _goalReached;
	}

	/**
	 * @brief number of valid nodes
	 * @return number of valid nodes
	 */
	unsigned getValidNodes() const {
		return _validNode;
	}

	/**
	 * @brief number of witnesse nodes
	 * @return number of witnesse nodes
	 */
	unsigned long getNbWitnesses() const {
		return _witnesses.size ();
	}

	/**
 	 * @brief number of witnesse nodes
 	 * @return number of witnesse nodes
 	 */
	std::vector<statePtr_t> getSampleStates() const {
		return _sampleStates;
	}

protected:
	/**
	 * @brief A special storage node for SST
	 */
	std::vector<Witness *> _witnesses;

	/**
	* @brief A randomly sampled state.
	*/
	statePtr_t _sampleState;

	/**
	* @brief List of random shoot.
	*/
	std::vector<statePtr_t> _sampleStates;

	/**
	 * @brief A randomly sampled control.
	 */
	std::vector<double> _sampleControl;

	/**
	 * @brief A resulting duration of a propagation step.
	 */
	double _duration;

	/**
	* @brief A set of nodes used to get sets of nodes from the nearest neighbor structure.
	*/
	std::vector<Node *> _closeNodes;

	/**
	 * @brief A set of distances used to get sets of nodes from the nearest neighbor structure.
	 */
	std::vector<double> _distances;

	/**
	 * @brief The best goal node found so far.
	 */
	Node *_bestGoal;

	/**
	 * @brief The result of a query in the nearest neighbor structure.
	 */
	Node *_xSelected;

	/**
	 * @brief The result.
	 */
	Node *_xNew;

	/**
	 * @brief A temporary storage for quering for close witnesses.
	 */
	Witness *_sNew;

	/**
	 * @brief Nb of valid node
	 */
	unsigned _validNode;

	/**
	 * @brief Nb of total shoot
	 */
	unsigned _totalShoot;

	/**
	 * @brief Nb of accepted node (valid is not neceseraly accepted)
	 */
	unsigned _accepted;

	/**
	 * @brief Used to get execution time
	 */
	clock_t _begin;

	/**
	 * @brief logger
	 */
     logm3d::LoggerPtr _log;
	
	/**
	 * @brief True if the goal is reached
	 */
	bool _goalReached;

	/**
	 * @brief True if the shoot is goal directed
	 */
	bool _goalDirected;

	/**
	 * @brief number of reached node
	 */
	unsigned _nbReached;

	/**
	 * @brief time of each reached nodes
	 */
	std::vector<double> _timesReached;

	/**
	 * @brief SST anytime
	 */
	bool _anyTime;

	/**
	 * @brief time for anytime SST
	 */
	int _execTime;

	/**
	 * @brief integration time
	 */
	double _inteStep;

	/**
	 * @brief minimum integration step
	 */
	int _minStep;

	/**
	 * @brief maximum integration step
	 */
	int _maxStep;

	/**
	 * @brief The radius for BestNear (delta_n in the WAFR paper SST)
	 */
	double _deltaNear;

	/**
 	* @brief The radius for sparsification (delta_s in the WAFR paper SST)
 	*/
	double _drainRadius;

	/**
 	* @brief The goal tolerance. (This is needed because in general, a dynamic system cannot reach a target state exactly.)
 	*/
	double _goalRadius;

	/**
	 * @brief goal selection euclidean (if false => weighted distance)
	 */
	bool _gsEucl;

	/**
	 * @brief near witness selection euclidean (if false => weighted distance)
	 */
	bool _eucl;

	/**
	 * @brief use cost space
	 */
	bool _isCostSpace;

	/**
	 * @brief The best goal cost (store for speed)
	 */
	double _bestGoalCost;

protected:
	/**
	 * @brief Finds a node to forwardPropagation from.
	 * @details Finds a node to forwardPropagation from. It does this through a procedure called BestNear w
	 * which examines a neighborhood around a randomly sampled point and returns the lowest cost one.
	 */
	Node *nearestVertex();

	/**
	 * @brief Get the nearest witness from the query node.
	 * @param distance Distance between the witness and the query node (out)
	 * @return If the trajectory is collision free or not.
	 */
	Witness *nearestWitness(double &distance);

	/**
	 * @brief Check if the currently created state is close to a witness.
	 * @details Check if the currently created state is close to a witness.
	 * @param costNew the cost of the new trajectory
	 */
	virtual bool isNodeLocallyTheBestSST(double costNew);

	/**
	 * @brief Checks if this node is on the solution path.
	 * @details Checks if this node is on the solution path.
	 * @param v The node to check
	 * @return True if on the solution path, false if not.
	 */
	bool isBestGoal(Node *v);

	/**
	 * @brief Removes a leaf node from the tree.
	 * @details Removes a leaf node from the tree.
	 * @param node The node to remove.
	 */
	void removeLeaf(Node *node);

	/**
	 * @brief Implementation of branch and bound algorithm
	 * @details Graph and tree search algorithm
	 * @param node start node
	 * @param bsCost best goal cost
	 */
	void branchAndBound(Node *node, double bsCost);

	/**
	 * @brief Test if the new node is the best in the area
	 * @param sNew The center of the area (Witness)
	 */
	void pruneDominatedSST();

	/**
	 * @brief Create new node and get the nearest in the graph
	 * @detail if the query in deltaNear return nothing, get the nearest in the graph
	 */
	Node *bestFirstSelection();

	/**
	 * @brief Get random control and propagate from the node
	 * @return new node after propagation
	 */
	Node *monteCarloProp();

};

} //namespace move4d
#endif //LIBMOVE3D_PLANNERS_SST_HPP
