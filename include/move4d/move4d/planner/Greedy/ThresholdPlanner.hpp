/*
 *  ThresholdPlanner.h
 *  BioMove3D
 *
 *  Created by Jim Mainprice on 05/07/10.
 *  Copyright 2010 LAAS/CNRS. All rights reserved.
 *
 */

#ifndef THRESHOLD_PLANNER_H_
#define THRESHOLD_PLANNER_H_

#include "move4d/API/Trajectory/trajectory.hpp"

#include "move4d/planner/Diffusion/TreePlanner.hpp"

namespace move4d
{
class ThresholdPlanner : public TreePlanner
{
public:
	ThresholdPlanner(Robot* R, Graph* G);
	~ThresholdPlanner();
	
	/**
	 * Initializes the planner
	 */
	unsigned init();
	
	/**
	 * stop Conditions
	 */
	bool checkStopConditions();
	
	/**
	 * Tries to extract a trajectory and erase the old one
	 */ 	
	bool newTrajectoryExtracted(API::GeometricPath* trajPt);
	
	/**
	 * Expand One Step
	 */
	unsigned expandOneStep(Node* fromComp, Node* toComp);
	
	/**
	 * Main function of the planner
	 */
	unsigned int run();
	
};

} //namespace move4d
#endif
