/*
 *  Seeding.h
 *  BioMove3D
 *
 *  Created by Jim Mainprice on 01/06/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SEEDING_H
#define SEEDING_H

namespace move4d
{
class Seeding {
	
	Seeding();
	~Seeding();
	
	void SampleCostMap();
	
};

} //namespace move4d
#endif
