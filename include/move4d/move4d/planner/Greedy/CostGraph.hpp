/*
 *  CostGraph.h
 *  BioMove3D
 *
 *  Created by Jim Mainprice on 06/07/10.
 *  Copyright 2010 LAAS/CNRS. All rights reserved.
 *
 */

#ifndef COST_GRAPH_H_
#define COST_GRAPH_H_

#include "move4d/API/Device/robot.hpp"
#include "move4d/API/ConfigSpace/localpath.hpp"
#include "move4d/API/Roadmap/Graph.hpp"

namespace move4d
{
class CostGraph : class Graph 
{
public:
	CostGraph(Robot* R);
	CostGraph(Robot* R, p3d_graph* G);
	CostGraph(const Graph& G);
	~CostGraph();
	
	void setThreshold(const double& thresh) { m_thresh = thresh; }
	double getThreshold() { return m_thresh; }
	
	Graph* getGraphUnderThreshold();
	vector<LocalPathBase*> getLocalPathsUnderThreshold(const LocalPathBase& LP);
	
private:
	double m_thresh;
};

} //namespace move4d
#endif
