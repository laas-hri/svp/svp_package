#ifndef TRAJEVAL_H
#define TRAJEVAL_H

#include <boost/function.hpp>
#include "move4d/API/Trajectory/trajectory.hpp"
#include "move4d/Logging/Logger.h"


namespace move4d
{
class TrajEval
{
    MOVE3D_STATIC_LOGGER;
public:
    TrajEval();
    double evaluate(API::GeometricPath & traj);

    static double maximum(API::GeometricPath &t, std::vector<boost::function<double(API::GeometricPath&)> > &funcs);
    static double sum(API::GeometricPath &t, std::vector<boost::function<double(API::GeometricPath&)> > &funcs);
    static double alpha(API::GeometricPath &t,double a,boost::function<double(API::GeometricPath &)> &f);

    /// see example
    static boost::function<double(API::GeometricPath&)> getTrajFunction(std::string name);
    static double traj_cost(API::GeometricPath &t, std::string &fx_name);

    static double example(API::GeometricPath &t);
    static void run_example();


private:
    std::vector<boost::function<double(API::GeometricPath&)> > _min_operands_f, _max_operands_f, _sum_operands_f; ///< lists of functions given as operands to min, max and sum in the evaluation

};

} //namespace move4d
#endif // TRAJEVAL_H
