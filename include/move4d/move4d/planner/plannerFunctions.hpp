#ifndef PLANNERFUNCTIONS
#define PLANNERFUNCTIONS

#ifdef LIST_OF_PLANNERS
#define Global
#else
#define Global extern
#endif

#include "P3d-pkg.h"
#include "move4d/planner/Diffusion/SST.hpp"

/**
	@author Florian Pilardeau,B90,6349 <fpilarde@jolimont>
*/
/*object global permettant d'acceder aux planner dans tous les fichiers de Move3d*/

#include "move4d/planner/planner.hpp"
#include "move4d/API/Trajectory/trajectory.hpp"

#define PLAN_RIGHT_ARM  0
#define PLAN_LEFT_ARM  1
#define PLAN_BASE  2
#define PLAN_FULL  3
#define PLAN_BOTH_ARMS 4

#define PLAN_STATUS_FOUND  0
#define PLAN_STATUS_START_EQUALS_GOAL  1
#define PLAN_STATUS_START_INVALID  2
#define PLAN_STATUS_GOAL_INVALID  3
#define PLAN_STATUS_NOT_FOUND  4


namespace move4d
{
Global std::vector<Planner*> plannerlist;

/**
 * RRT statistics
 */
struct RRTStatistics
{
  int runId;
  bool succeeded;
  double time;
  double cost;
  int nbNodes;
  int nbExpansions;
};

/**
 * Return the last trajectory
 */
API::GeometricPath *p3d_get_last_trajectory();

/**
 * Get last RRT statistics
 */
void p3d_get_rrt_statistics( RRTStatistics& stat );

/**
 * Get last Trajectory statistics
 */
void p3d_get_traj_statistics( TrajectoryStatistics& stat );

/**
 * @ingroup NEW_CPP_MODULE
 * Funtion to set multirun Ids
 */
void p3d_planner_functions_set_run_id( unsigned int idRun );

/**
 * @ingroup NEW_CPP_MODULE
 * Funtion to get multi-run Ids
 */
unsigned int p3d_planner_functions_get_run_id();


p3d_traj* p3d_extract_traj(bool is_traj_found, int nb_added_nodes, Graph* graph, statePtr_t q_source, statePtr_t q_target);

/**
 * @ingroup NEW_CPP_MODULE
 * Funtion for planning (Manip. Module)
 */
p3d_traj* p3d_planner_function( p3d_rob* robotPt, configPt qs, configPt qg);

/**
 * @ingroup NEW_CPP_MODULE
 * Funtion for smoothing a trajectory (Manip. Module)
 */
void p3d_smoothing_function( p3d_rob* robotPt, p3d_traj* traj, int nbSteps, double maxTime );

/**
 * @ingroup NEW_CPP_MODULE
 * Funtion for smoothing a trajectory (Manip. Module)
 */
void p3d_smoothing_function_dummy( p3d_rob* robotPt, p3d_traj* traj, int nbSteps, double maxTime );

/**
 * Set a function that generate IK solutions
 */ 
void p3d_set_goal_solution_function( configPt (*fct)() );

/**
 * Generate Goal RobotState Function
 * This function can be set outside the library
 */ 
bool p3d_generate_goal_configuration( RobotState & q );

/**
  @ingroup NEW_CPP_MODULE
 * \fn int p3d_run_prm(p3d_graph* Graph_Pt, int* fail, int (*fct_stop)(void), void (*fct_draw)(void))
 * \brief fonction de lancement de l'algorithme PRM
 * @param robotPt the robot
 * @return le nombre de Node ajoutés au Graph
 */
int p3d_run_prm(p3d_rob* robotPt);


int p3d_run_directed_prm(p3d_rob* robotPt);

/**
  @ingroup NEW_CPP_MODULE
 * \fn int p3d_run_acr(p3d_graph* Graph_Pt, int* fail, int (*fct_stop)(void), void (*fct_draw)(void))
 * \brief fonction de lancement de l'algorithme ACR
 * @param robotPt the robot
 * @return le nombre de Node ajoutés au Graph
 */
int p3d_run_acr(p3d_rob* robotPt);

/**
  @ingroup NEW_CPP_MODULE
 * \fn int p3d_run_vis_prm(p3d_graph* Graph_Pt, int* fail, int (*fct_stop)(void), void (*fct_draw)(void))
 * \brief fonction de lancement de l'algorithme Vis_PRM
 * @param robotPt the robot
 * @return le nombre de Node ajoutés au Graph
 */
int p3d_run_vis_prm(p3d_rob* robotPt);

/**
 @ingroup NEW_CPP_MODULE
 * \fn int p3d_run_perturb_prm
 * \brief function that starts the perturbation algorithm
 * @param robotPt the robot
 * @return le nombre de Node ajoutés au Graph
 */
int p3d_run_perturb_prm(p3d_rob* robotPt);

/**
  @ingroup NEW_CPP_MODULE
 * \fn bool p3d_run_rrt(p3d_graph* GraphPt,int (*fct_stop)(void), void (*fct_draw)(void));
 * \brief fonction de lancement de l'algorithme RRT
 * @param robotPt the robot
 * @return la trajectoire peut être générée
 */
int p3d_run_rrt(p3d_rob* robotPt);



/**
  @ingroup NEW_CPP_MODULE
 * \fn bool p3d_plan(p3d_rob* robotPt, configPt from, configPt to, int robotPart, bool cartesian, Graph* fromGraph); 
 * \brief Simplified planning function. Warning! This function uses Env variables such as Env::isCostSpace,Env::useTRRT
 * in order to choose the planner.
 * @param robotPt the robot
 * @param configPt from the starting configuration
 * @param configPt to the ending configuration
 * @param int robotPart the kinematic group (see the top of the file)
 * @param bool cartesian planning in cartesian world or configuration
 * @param Graph* fromGraph not used yet
 * @return status code (see the top of the file)
 */
int p3d_plan(p3d_rob* robotPt, configPt from, configPt to, int robotPart, bool cartesian);//, Graph* fromGraph); 
int p3d_plan(p3d_rob* robotPt, configPt from, configPt to, const std::vector<int> &joints, bool cartesian);
/**
  @ingroup NEW_CPP_MODULE
 * \fn bool p3d_run_rrt(p3d_graph* GraphPt,int (*fct_stop)(void), void (*fct_draw)(void));
 * \brief function running the EST algorithm
 * @param robotPt the robot
 * @return if there's a trajectory
 */
bool p3d_run_est(p3d_rob* robotPt);

/**
  @ingroup NEW_CPP_MODULE
 * \fn bool p3d_run_sst(p3d_graph* GraphPt,int (*fct_stop)(void), void (*fct_draw)(void));
 * \brief function running the SST algorithm
 * @param robotPt the robot
 * @param SST variant (0 = SST, 1 = T-SST)
 * @return if there's a trajectory
 */
SST* p3d_run_sst(p3d_rob* robotPt, int variant);

/**
 * @ingroup NEW_CPP_MODULE
 * @brief LEARN FUNCTION to use with C++ Planner API
 */
void p3d_learn_cxx(int NMAX, int (*fct_stop)(void), void (*fct_draw)(void));

/**
 * @ingroup NEW_CPP_MODULE
 * @brief SPECIFIC LEARN FUNCTION to use with C++ Planner API
 */
int p3d_specific_learn_cxx(double *qs, double *qg, int *iksols, int *iksolg, int (*fct_stop)(void), void (*fct_draw)(void));

} //namespace move4d
#endif
