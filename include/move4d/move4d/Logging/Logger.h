/**
 * @file Logger.h
 * @brief This file implements macros to help with the logging, in a way similar to ROS, using log4cxx.
 * @author Jules Waldhart
 * @date 22 april 2015
 * @ingroup Logging
 *
 * The macros defined here have to be called to initialize and then use logging in a class/method/file/function...
 * The macros allow to log for different levels, which are (ordered): TRACE < DEBUG < INFO < WARN < ERROR < FATAL
 * see http://wiki.ros.org/Verbosity%20Levels for information about the purpose of each level
 *
 * It is possible to select at execution time the lower log level that will be displayed for each logger (in each cpp file)
 * see http://logging.apache.org/log4cxx/usage.html for a short introduction to log4cxx
 *
 * You can directly use log4cxx API, but here you will find some helpers to make it easier and faster to implement.
 * Also, if for some reason the logging system needs to be changed, using the logm3d interface defined here will help in the
 * changing process.
 *
 * Names And Hierarchy
 * -----
 *
 * The logger names have to be choosen wisely. There is a hierarchy of names, so for instance, planners is an ancestor of planners.hrics.mho and
 * the parent of planners.hrics.
 * By default, a logger inherits its parents property, but it can be overriden. So for debugging all HRICS module, one can disable debug infromation
 * for the root (by default, no debug information anywhere) and set planners.hrics logging level to debug, only the debug (and higher) level information will
 * be print.
 *
 * When naming a logger, you need to keep this in mind, so that an other developer/user can choose to display only what he needs. So do not hesitate to go deep in the hierarchy
 * (e.g. planners.hrics.mho.grid.cell)
 *
 * %Configuration files
 * -------
 *
 * The system will look for different file, until finding one, testing in that order:
 *  - `$LOGM3D_CONFIGURATION`
 *  - `$LOG4CXX_CONFIGURATION`
 *  - `./logm3d.properties`
 *  - `./log4cxx.properties`
 *  - `$HOME_MOVE4D/logm3d.properties`
 *  - `$HOME_MOVE4D/log4cxx.properties`
 *
**/

/**
 * @defgroup Logging Logging module
**/

#ifndef LOGGER_H
#define LOGGER_H

#ifndef MOVE3D_LOG_OFF
#include <log4cxx/logger.h>
#endif
#include <iostream>
#include <mutex>

/**
 * INIT_MOVE3D_LOG macro creates a logger to be used by \ref AutoLogger macros in the scope where it is called.
 * @param name is the name of the logger, according to log4cxx. e.g. "move4d.api.traj" or "move4d.hrics.visibility"
 * @warning mind the spelling and case of the parents of your logger ("planners", "api", "hrics")
 * @ingroup AutoLogger
**/
#if !defined(MOVE3D_LOG_OFF)
#define INIT_MOVE3D_LOG(name) log4cxx::LoggerPtr __move3d_logger__  = move4d::logm3d::getLogger(name)
#else
#define INIT_MOVE3D_LOG(name)
#endif

/**
 * MOVE3D_STATIC_LOGGER macro defines a static logger for the class.
 * It the has to be initialized (defined) using the INIT_MOVE3D_STATIC_LOGGER
 * @ingroup AutoLogger
**/
#if !defined(MOVE3D_LOG_OFF)
#define MOVE3D_STATIC_LOGGER static  log4cxx::LoggerPtr __move3d_logger__
#else
#define MOVE3D_STATIC_LOGGER
#endif
/**
 * INIT_MOVE3D_STATIC_LOGGER() macro is to be called in the class implementation file (.cpp) to instantiate the static logger.
 * The static logger has to be defined first with MOVE3D_STATIC_LOGGER macro in the class definition (.h)
 * @ingroup AutoLogger
**/
#if !defined(MOVE3D_LOG_OFF)
#define INIT_MOVE3D_STATIC_LOGGER(classname,loggername) log4cxx::LoggerPtr classname::__move3d_logger__ = move4d::logm3d::getLogger(loggername)
#else
#define INIT_MOVE3D_STATIC_LOGGER(classname,loggername)
#endif

#define LOGM3D_LOG(level,logger,msg) \
    if(move4d::logm3d::isEnabledFor(level,logger)){\
    std::ostringstream oss; \
    oss << msg; \
    move4d::logm3d::log(level,logger,oss.str());\
    }

#define M3D_IF_DEBUG() M3D_IF_DEBUG_LOGGER_COND(__move3d_logger__,1)
#define M3D_IF_DEBUG_COND(cond) M3D_IF_DEBUG_LOGGER_COND(__move3d_logger__,cond)
#define M3D_IF_DEBUG_LOGGER(_logger_,cond) M3D_IF_DEBUG_LOGGER_COND(_logger_,1)
#define M3D_IF_DEBUG_LOGGER_COND(_logger_,cond) if(__builtin_expect(move4d::logm3d::isEnabledFor(move4d::logm3d::LogLevel::DEBUG,_logger_) && (cond),0))

/**
 * The following macros does not need INIT_MOVE3D_LOG to be called, and can be
 * used to have different loggers in a same cpp file.
 * @param \_logger_ a logm3d::LoggerPtr to use as logger
 * @param msg a std::string, char*, wchar*, the insertion operator (<<) can be used (see log4cxx doc)
 *
 * These loggers are obtained through the various logm3d::getLogger() and similar functions,
 * (or log4cxx::Logger::getLogger() : to be avoided, create a logm3d logger instead)
 **/
#if !defined(MOVE3D_LOG_ERROR_ONLY) && !defined(MOVE3D_LOG_OFF)
#define LOGM3D_TRACE(_logger_,msg) LOGM3D_LOG(move4d::logm3d::LogLevel::TRACE,_logger_,msg)
#define LOGM3D_DEBUG(_logger_,msg) LOGM3D_LOG(move4d::logm3d::LogLevel::DEBUG,_logger_,msg)
#define LOGM3D_INFO(_logger_,msg)  LOGM3D_LOG(move4d::logm3d::LogLevel::INFO,_logger_,msg)
#else
#define LOGM3D_TRACE(_logger_,msg)
#define LOGM3D_DEBUG(_logger_,msg)
#define LOGM3D_INFO(_logger_,msg)
#endif
#if !defined(MOVE3D_LOG_OFF)
#define LOGM3D_WARN(_logger_,msg)  LOGM3D_LOG(move4d::logm3d::LogLevel::WARN,_logger_,msg)
#define LOGM3D_ERROR(_logger_,msg) LOGM3D_LOG(move4d::logm3d::LogLevel::ERROR,_logger_,msg)
#define LOGM3D_FATAL(_logger_,msg) LOGM3D_LOG(move4d::logm3d::LogLevel::FATAL,_logger_,msg)
#define LOGM3D_ASSERT_MSG(_logger_,cond,msg) LOG4CXX_ASSERT(_logger_,cond,msg)
#define __ASSERT_MSG(cond) "Assertion \""\
    cond\
    "\" failed"
#define LOGM3D_ASSERT(_logger_,cond) LOG4CXX_ASSERT(_logger_,cond,__ASSERT_MSG(#cond))
#else
#define LOGM3D_WARN(_logger_,msg)
#define LOGM3D_ERROR(_logger_,msg)
#define LOGM3D_FATAL(_logger_,msg)
#define LOGM3D_ASSERT_MSG(_logger_,cond,msg)
#define LOGM3D_ASSERT(_logger_,cond)
#endif

/**
 * @defgroup AutoLogger
 * @ingroup Logging
 * The macro in this \ref Logging submodule allow simpler management of the loggers.
 *
 * The M3D_* macro will use a logger defined by a previous call to a INIT_MOVE3D_*LOGGER macro in the scope.
 * @{
**/
#if !defined(MOVE3D_LOG_ERROR_ONLY) && !defined(MOVE3D_LOG_OFF)
#define M3D_TRACE(msg) LOGM3D_TRACE(__move3d_logger__,msg)
#define M3D_DEBUG(msg) LOGM3D_DEBUG(__move3d_logger__,msg)
#define M3D_INFO(msg)  LOGM3D_INFO(__move3d_logger__,msg)
#else
#define M3D_TRACE(msg)
#define M3D_DEBUG(msg)
#define M3D_INFO(msg)
#endif
#if !defined(MOVE3D_LOG_OFF)
#define M3D_WARN(msg)  LOGM3D_WARN(__move3d_logger__,msg)
#define M3D_ERROR(msg) LOGM3D_ERROR(__move3d_logger__,msg)
#define M3D_FATAL(msg) LOGM3D_FATAL(__move3d_logger__,msg)
#define M3D_ASSERT_MSG(cond,msg) LOGM3D_ASSERT_MSG(__move3d_logger__,cond,msg)
#define M3D_ASSERT(cond) LOGM3D_ASSERT(__move3d_logger__,cond)
#else
#define M3D_WARN(msg)
#define M3D_ERROR(msg)
#define M3D_FATAL(msg)
#define M3D_ASSERT_MSG(cond,msg)
#define M3D_ASSERT(cond)
#endif
///@}

/**
 * @def M3D_TRACE
 * @def M3D_DEBUG
 * Logs a message with the logger defined in the scope with macro of \ref AutoLogger.
 * @param[in] msg
**/

template<typename T>
std::string array_to_str(const std::vector<T> &v, const std::string &sep){
    std::ostringstream s;
    for (size_t i=0;i<v.size()-1;++i) s << v[i] << sep;
    s<<v.back();
    return s.str();
}
#define APPEND_ARRAY(array,sep) array_to_str(array,sep)

namespace move4d
{
///@ingroup Logging
namespace logm3d {

enum class LogLevel{
    TRACE=0,
    DEBUG,
    INFO,
    WARN,
    ERROR,
    FATAL
};

#ifndef MOVE3D_LOG_OFF
typedef log4cxx::LoggerPtr LoggerPtr ;
#else
typedef int LoggerPtr;
#endif

struct OutputHandler{
public:
    virtual ~OutputHandler()=default;
    virtual void log(LogLevel level,LoggerPtr logger, const std::string &message) = 0;
    virtual bool isEnabledFor(LogLevel level,LoggerPtr logger) = 0;
};
struct OutputHandlerLog4cxx : public OutputHandler
{
public:
    virtual log4cxx::LevelPtr getLevel(LogLevel);
    virtual void log(LogLevel level, LoggerPtr logger, const std::string &message) override;
    virtual bool isEnabledFor(LogLevel level, LoggerPtr logger) override;
};

/// to be called to initialize the logging system to its defaults
/// (once in a session)
void initializePlannerLogger();


LoggerPtr getLogger(std::string name);

LoggerPtr getLoggerDataFile(std::string name, std::string file_path);
LoggerPtr getLoggerConsoleAndFile(std::string name,std::string file_path);

bool isEnabledFor(LogLevel level, LoggerPtr logger);
void log(LogLevel level, LoggerPtr logger, const std::string &message);

void setOutputHandler(OutputHandler *handler);
void noHandler();///< short hand for setOutputHandler(nullptr)
}

} //namespace move4d
#endif // LOGGER_H

