#ifndef PLOTSOCKETAPPENDER_H
#define PLOTSOCKETAPPENDER_H

#ifndef MOVE3D_LOG_OFF

#include <log4cxx/spi/loggingevent.h>
#include <log4cxx/appenderskeleton.h>
#include <log4cxx/net/socketappenderskeleton.h>

#include "move4d/Logging/Logger.h"

namespace log4cxx{
class PlotSocketAppender : public log4cxx::net::SocketAppenderSkeleton
{
    MOVE3D_STATIC_LOGGER;
public:
    /**
    The default port number of remote logging server (4560).
    */
    static int DEFAULT_PORT;

    /**
    The default reconnection delay (30000 milliseconds or 30 seconds).
    */
    static int DEFAULT_RECONNECTION_DELAY;

    DECLARE_LOG4CXX_OBJECT(PlotSocketAppender)

    BEGIN_LOG4CXX_CAST_MAP()
    LOG4CXX_CAST_ENTRY(PlotSocketAppender)
    LOG4CXX_CAST_ENTRY_CHAIN(AppenderSkeleton)
    END_LOG4CXX_CAST_MAP()

    PlotSocketAppender();
    PlotSocketAppender(helpers::InetAddressPtr& address, int port);
    PlotSocketAppender(const LogString& host, int port);

    ~PlotSocketAppender();

protected:
        virtual void setSocket(log4cxx::helpers::SocketPtr& socket, log4cxx::helpers::Pool& p);

        virtual void cleanUp(log4cxx::helpers::Pool& p);

        virtual int getDefaultDelay() const;

        virtual int getDefaultPort() const;


    // This method is called by the AppenderSkeleton#doAppend method
    void append(const log4cxx::spi::LoggingEventPtr& event, log4cxx::helpers::Pool& p);

    //void close();

    //bool isClosed() const { return closed; }

    bool requiresLayout() const { return true; }

private:
        log4cxx::helpers::ObjectOutputStreamPtr oos;

};

} //namespace log4cxx

#endif // MOVE3D_LOG_OFF
#endif // PLOTSOCKETAPPENDER_H
