#ifndef SOCKETPLOT_H
#define SOCKETPLOT_H

//#include <QThread>
#include <Eigen/Eigen>
#include <deque>
#include <vector>

#include "move4d/Logging/Logger.h"

namespace move4d
{
class SocketPlot
{
    MOVE3D_STATIC_LOGGER;
public:
    SocketPlot(std::string name);
    ~SocketPlot();

    void addPoint(double x,double y);
    void addPoint(std::vector<double> p);
    void addIndexedValue(double val);
    void send();
    void sendEmpty();
    unsigned int bufferMaxSize() const;
    void setBufferMaxSize(unsigned int bufferMaxSize);

    std::string vectorToString(std::vector<double> v);
protected:
    //void run();
private:
    static std::string __name_prefix;
    std::deque<std::vector<double> > _buffer;
    logm3d::LoggerPtr _logger;
    unsigned int _index;
    unsigned int _bufferMaxSize;
};

} //namespace move4d
#endif // SOCKETPLOT_H
