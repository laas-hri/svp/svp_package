#ifndef HRICS_COSTMANAGER_H
#define HRICS_COSTMANAGER_H


#include "move4d/API/forward_declarations.hpp"
#include "move4d/Logging/Logger.h"
#include "move4d/database/semantics/Sem_ActionsManager.hpp"
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include "move4d/HRI_costspace/HRICS_CostSettingType.h"
#include <jsoncpp/json/json.h>
#include <libmove3d/p3d/env.hpp>

namespace move4d{
namespace Sem{
class AgentType;
}

namespace API{
class GeometricPath;
class Path;
}

namespace HRICS {

namespace CF{
class TrajectoryCostAggregation;
}

class TrajerctoryCostFuncion
{
public:
    virtual ~TrajerctoryCostFuncion()=default;
    virtual void operator()(RobotState &state)=0;
    virtual double get()=0;
protected:
    boost::function<double(RobotState &q)> f;
};

class CostManager
{
    MOVE3D_STATIC_LOGGER;
public:
    //typedef boost::function<double (std::pair<std::set<CF::TrajectoryCostAggregation>::iterator,std::set<CF::TrajectoryCostAggregation>::iterator>)> FunctionAggregation_t;
    typedef boost::function<double (void*)> FunctionAggregation_t;

    CostManager();
    CostManager(const std::string &global_path, bool proactive=false);
    ~CostManager();
    static CostManager * mainCostManager();

    struct AlgoParams {
        AlgoParams():extStep(0),finalGap(0),extend(0),
            tempRate(0.1),goalBias(5),isGoalBiased(0){}
        AlgoParams(double dmax,double timeStep,double extStep,double finalGap,bool extend,double tempRate,double goalBias,bool isGoalBiased):
            dmax(dmax),timeStep(timeStep),extStep(extStep),finalGap(finalGap),extend(extend),
            tempRate(tempRate),goalBias(goalBias),isGoalBiased(isGoalBiased){}
        double dmax=0.;
        double timeStep=0.;
        double extStep=0.,finalGap=0.;
        bool extend=false;
        double tempRate=0.1,goalBias=5.;
        bool isGoalBiased=false;
    };

    /**
     * @brief set the planner and cost space according to given settings
     */
    void takeControlAndSet();

    void setActionType(Sem::ActionType &act_t);
    void setCostType(CostSettingType cost_t);
    void updatePlannerSettings();

    double getCost(RobotState &q);
    double getCostPath(API::Path &path);
    double getCostPathAggregation(API::Path &path);
    double getCostAggregationNormalized(API::Path &path);

    double getCostTest(RobotState &q);
    double getCostTrajTest(API::Path &path);

    boost::function<double (API::Path &)> getCostPathFunction(CostSettingType type);
    FunctionAggregation_t getCostPathAggregationFunction(CostSettingType type);

    void clear();

    void prepareForSmoothing(API::Path &path);
    void endSmoothing(API::Path *path=0);


    typedef enum{CONF_COST,PATH_COST,PATH_SMOOTHING_COST,PATH_AGGREGATION_COST} CostAppliesWhen_t;

    /**
     * @brief fetchSettings get settings from json files at default emplacement
     * @return
     */
    bool fetchSettings();
    bool readActionsCostMapping(std::string path);
    bool readCostSetting(const CostSettingType &cost, CostAppliesWhen_t when);
    bool readCostSetting(std::string path,const CostSettingType &cost,CostAppliesWhen_t when);

    bool readAlgoParams(const Sem::AgentType &agent_type);
    bool readAlgoParams(const Sem::AgentType &agent_type, const std::string &path);

    void setAlgoParams(const Sem::AgentType &agent_type,const AlgoParams &params);

    /**
     * @brief addFunction
     * @param root
     * @param cost
     * @param when
     * @return
     */
    bool addFunction(const Json::Value &root, const CostSettingType &cost,CostAppliesWhen_t when);
    boost::function<double(RobotState &)> jsonToFunction(const Json::Value &f);
    static boost::function<double(double,double)> elementaryOperation(std::string const &name);
    static boost::function<double(double)> elementaryFunction(std::string const &name, const Json::Value &json);
    boost::function<double(API::Path&)> jsonToFunctionTraj(const Json::Value &f);
    FunctionAggregation_t jsonToFunctionTrajAggreg(const Json::Value &f);

    bool setTestFunction(const std::string &expression);
    bool setTrajTestFunction(const std::string &expression);

    std::string getPath();
    std::string getPath(CostAppliesWhen_t when);


    bool proactive() const;
    void setProactive(bool proactive);

    std::string globalPath() const;
    void setGlobalPath(const std::string &globalPath);

    CostSettingType currentCostType() const;

    bool isSmoothing() const;

    double pathCostNormalizationFactor() const;
    void setPathCostNormalizationFactor(double pathCostNormalizationFactor);

    static double sigmoidFunction(double val, double upper, double lower, double min, double max);
    static double boundFunction(double val,double min,double max);
    static double affineFunction(double val,double a,double b);

    Env::boolParameter planningMethod();

    bool hasControlOnPlanning() const;
    void setHasControlOnPlanning(bool hasControlOnPlanning);

    std::map<std::string, double> const &functionCostStack() const;

    const std::map<Sem::ActionType, CostSettingType> &actionCostMapping() const;
    void setActionCostMapping(const std::map<Sem::ActionType, CostSettingType> &actionCostMapping);

protected:
    double getCostStack(std::string const &name, RobotState &q);
    double getCostStackPath(std::string const &name, move4d::API::Path &path);
    double getCostStackAggregation(std::string const &name, void *dummy=0x0);

private:
    bool _proactive;
    bool _hasControlOnPlanning;

    std::string _globalPath;

    std::map<Sem::ActionType,CostSettingType> _actionCostMapping;
    std::map<CostSettingType,boost::function<double(RobotState &)> > _functions;
    boost::function<double(RobotState &)> _testFunction; ///< defined by setTestFunction(std::string expression), used by CostManagerTest
    FunctionAggregation_t _trajTestFunction;

    std::map<CostSettingType,boost::function<double(API::Path&)> > _functionsTraj;
    std::map<CostSettingType,boost::function<double(API::Path&)> > _functionsTrajSmoothing;
    std::map<CostSettingType,FunctionAggregation_t> _functionsTrajAggreg;
    std::vector<CF::TrajectoryCostAggregation*> _aggregatorSet;
    std::map<std::string,CF::TrajectoryCostAggregation*> _aggregatorMap;

    std::map<std::string,double> _functionCostStack;
    bool _lockFunctionsCostStack;
    std::map<std::string,double> _functionCostStackPath;
    bool _lockFunctionsCostStackPath;

    void updatePlannerSettings(AlgoParams &params);
    std::map<Sem::AgentType,AlgoParams> _algoParams;

    CostSettingType _currentCostType;

    static CostManager* __mainCostManger;

    bool _isSmoothing;

    double _pathCostNormalizationFactor;

};

} // namespace HRICS
} // namespace move4d

#endif // HRICS_COSTMANAGER_H
