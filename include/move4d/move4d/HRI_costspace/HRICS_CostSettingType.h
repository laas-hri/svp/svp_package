#ifndef HRICS_COSTSETTINGTYPE_H
#define HRICS_COSTSETTINGTYPE_H

#include <string>
#include <vector>

namespace move4d{
namespace HRICS {

class CostSettingType
{
public:
    CostSettingType();
    CostSettingType(const std::string &s);
    ~CostSettingType();

    static CostSettingType getDefault();
    std::string toString() const;

    bool isDefined() const;

    friend bool operator==(CostSettingType const &v1 , CostSettingType const &v2) {return v1._value ==v2._value;}
    friend bool operator!=(CostSettingType const &v1 , CostSettingType const &v2) {return v1._value!=v2._value;}
    friend bool operator<(CostSettingType const &v1 , CostSettingType const &v2) {return v1._value<v2._value;}
    friend bool operator>(CostSettingType const &v1 , CostSettingType const &v2) {return v1._value>v2._value;}
private:
    static std::vector<std::string> __stringValues;
    int _value;
};

} // namespace HRICS
} // namespace move4d

#endif // HRICS_COSTSETTINGTYPE_H
