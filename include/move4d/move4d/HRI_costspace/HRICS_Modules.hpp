#ifndef HRICS_MODULES_H
#define HRICS_MODULES_H

#include <string>

#include "move4d/Logging/Logger.h"
#include "move4d/API/moduleBase.hpp"

//implementation is in HRICS_costspace.cpp

namespace move4d{
class Robot;

/**
 * @brief The HriInfoModule class initializes info about agents (Robot::getHriAgent()).
 *
 * Creates HRI agents, link to the Robot, update the HUD
 * @note formerly in HRICS_init()
 */
class HriInfoModule : public ModuleBase
{
    MOVE3D_STATIC_LOGGER;
protected:
    HriInfoModule();
public:
    static HriInfoModule *getInstance();
    ~HriInfoModule();
    void initialize();
    std::string getName();
    static std::string name();
protected:
    void createAgent(Robot *r);
    void createAgents();
    void changeTshirtColor(Robot *r, int color);
private:
    static HriInfoModule *_instance;
};

/**
 * @brief The HriCostGridModule class manages init for the 3D cost grids for HRI
 *
 * @note (April 2016) not used since a while, unknown state
 * @note formerly in HRICS_init()
 */
class HriCostGridModule : public ModuleBase
{
    MOVE3D_STATIC_LOGGER;
protected:
    HriCostGridModule();
public:
    static HriCostGridModule *getInstance();
    ~HriCostGridModule();
    void initialize();
    void enable(bool enable=true);
    bool isEnabled();
    std::string getName();
    static std::string name();
private:
    static HriCostGridModule *_instance;
};

/**
 * @brief The HriCostFunctionModule class manages init for the cost functions for HRI.
 *
 * These cost functions doesn't use the 3D grids (HriCostGridModule)
 *
 * @see HRICS::CF, CostManager, CostFunctionCommon
 *
 * @note formerly in HRICS_init()
 */
class HriCostFunctionModule : public ModuleBase
{
    MOVE3D_STATIC_LOGGER;
protected:
    HriCostFunctionModule();
public:
    static HriCostFunctionModule *getInstance();
    ~HriCostFunctionModule();
    void initialize();
    /**
     * @brief enables or disables the functionalities
     * @param enable
     *
     * This changes the Env variable Env::enableHri, and set the cost function to HRICS::CostManager::getCost()
     * @see CostSpace::setCost() and HRICS::CostManager
     */
    void enable(bool enable=true);
    bool isEnabled();
    std::string getName();
    static std::string name();
private:
    static HriCostFunctionModule *_instance;

};
} // namespace move4d
#endif // HRICS_MODULES_H
