#ifndef HRICS_MULTI_AGENT_GRID_HPP
#define HRICS_MULTI_AGENT_GRID_HPP
//#define DRAW_FRONTIER_HAND_IN_HAND
//#define DRAW_FRONTIER_STEPS // for generating pictures for algo explanation
//#define CARACT_TIME_FIND_CELLS_IN_RANGE
#define DRAW_OPTIMIZE_FRONTIER

#include "move4d/API/planningAPI.hpp"
#include "move4d/API/Grids/gridsAPI.hpp"
#include "move4d/Logging/Logger.h"

namespace HRICS
{

class MetaFrontierCells;
struct FrontierCells;
class MultiAgentCell;

typedef std::shared_ptr<FrontierCells> FrontierPtr;
typedef std::shared_ptr<MetaFrontierCells> MetaFrontierPtr;


/**
     @ingroup HRICS
     @brief Plannar HRI Grid considering multiple agents systems for HRi operations.
     Developped for Multiple Handover problems.
     */
class MultiAgentGrid : public API::TwoDGrid
{
    MOVE3D_STATIC_LOGGER;
public:
    class AgentInGrid;
    /**
     * @brief class representing data that can be common to several agents (reachable space, cylinder, frontiers, etc...)
     */
    class MetaAgentInGrid{
    public:
        typedef std::shared_ptr<MetaAgentInGrid> s_ptr;
        MetaAgentInGrid(unsigned int agent_number);
        MetaAgentInGrid::s_ptr shared_clone();
        std::vector<AgentInGrid*> & getAgentsInGrid(void){return this->_realAgents;}
        void addAgent(AgentInGrid* a,s_ptr s_ptr_this);
    private:
        void removeAgent(AgentInGrid* a);
    public:
        //getter/setters
        Robot* getCylinder(void){return _cylinder;}
        double getCylinderRadius(void){return _cyl_radius;}
        void setCylinder(Robot * c);
        ///the list of cells where the agent can go, reachable space, it's included in accessible.
        std::vector<MultiAgentCell*> & reachable(){return _reachable;}
        ///a subset of reachable where all cells are near an obstacle (have a not accesscible neighbour)
        std::vector<MultiAgentCell*> & boundaries(){return _boundaries;}
        /// the cells that are frontiers for the agent with other agents
        std::vector<MultiAgentCell* > & frontiers(MetaAgentInGrid::s_ptr other){return _frontiers[other.get()];}
        std::vector<MultiAgentCell* > & frontiers_ok(MetaAgentInGrid::s_ptr other){return _frontiers_ok[other.get()];}
        bool frontierFullyChecked(MetaAgentInGrid::s_ptr other){return _frontierFullyChecked[other.get()];}
        void setFrontierFullyChecked(MetaAgentInGrid::s_ptr other,bool b=true){_frontierFullyChecked[other.get()]=b;}
        bool isFrontierComputed(MetaAgentInGrid::s_ptr other){return _frontiersComputed[other.get()];}
        void setFrontierComputed(MetaAgentInGrid::s_ptr other,bool b=true){_frontiersComputed[other.get()]=b;}

        char letter_id;
    private:
        Robot* _cylinder; ///<the cylinder for collision computation.
        double _cyl_radius; ///< the radius of the cylinder, for distance based collision checks
        ///the list of cells where the agent can go, reachable space, it's included in accessible.
        std::vector<MultiAgentCell*> _reachable;
        ///a subset of reachable where all cells are near an obstacle (have a not accesscible neighbour)
        std::vector<MultiAgentCell*> _boundaries;
        /// the cells that are frontiers for the agent with other agents
        std::map<MetaAgentInGrid*, std::vector<MultiAgentCell* > >  _frontiers;
        std::map<MetaAgentInGrid*, std::vector<MultiAgentCell* > >  _frontiers_ok;
        std::map<MetaAgentInGrid*, bool> _frontierFullyChecked;
        std::map<MetaAgentInGrid*, bool> _frontiersComputed;

        std::vector<AgentInGrid*> _realAgents;///<ptrs to real agents (AgentInGrid) that point to this meta agent
        static char _letter_id_cnt;
    };

    /**
     * @brief Holds all the data concerning an agent in the MultiAgentGrid class.
     */
    class AgentInGrid{
    public:
        Robot* getRobot(){return _robot;} ///< @brief pointer to the robot.
                      ///< @see HRICS::MultiAgentGrid::setAgents().
        void setRobot(Robot* r){_robot=r;}
        Robot* getCylinder(){return _metaAgent->getCylinder();} ///<the cylinder for collision computation.
        double getMaxDist(){return _maxDist;} ///<the maximum distance the agent can cover (for display purpose, computed during distance computation).
        void setMaxDist(double d){_maxDist=d;}
        //std::vector<MultiAgentCell*> accessible; ///<the list of cells where the agent can 'fit'. NOT where it actually can travel. @warning obsolete
        MultiAgentCell* getOrigin(){return this->_origin;}
        void setOrigin(MultiAgentCell* c){this->_origin=c;}
        MultiAgentCell* getTarget(){return this->_target;}
        void setTarget(MultiAgentCell* c){this->_target=c;}
        ///the list of cells where the agent can go, reachable space, it's included in accessible.
        std::vector<MultiAgentCell*> & reachable(){return _metaAgent->reachable();}
        ///a subset of reachable where all cells are near an obstacle (have a not accesscible neighbour)
        std::vector<MultiAgentCell*> & boundaries(){return _metaAgent->boundaries();}
        /// the cells that are frontiers for the agent with other agents
        std::vector<MultiAgentCell* > & frontiers(AgentInGrid *other){return getMetaAgent()->frontiers(other->getMetaAgent());}
        bool frontierFullyChecked(AgentInGrid* other){return _metaAgent->frontierFullyChecked(other->getMetaAgent());}
        void setFrontierFullyChecked(AgentInGrid* other,bool b=true){_metaAgent->setFrontierFullyChecked(other->getMetaAgent(),b);}
        bool isHuman();
        double getSpeed(void){return _speed;} ///< for cost computation.
        void setSpeed(double s){_speed=s;}
        bool isDistanceComputed(){return _distanceComputed;} ///< whether the distances (reachability) have been computed or not.
        void setDistanceComputed(bool b=true){_distanceComputed=b;}
        void setMetaAgent(MetaAgentInGrid::s_ptr meta){_metaAgent=meta;}
        MetaAgentInGrid::s_ptr getMetaAgent(void){return _metaAgent;}
    private:
        Robot* _robot;
        double _speed;
        double _maxDist;
        bool _distanceComputed;
        MetaAgentInGrid::s_ptr _metaAgent;
        MultiAgentCell *_origin,*_target;

    };

    struct ComputationStat{
        timespec access_total_time;
        std::vector<timespec> reachability_time;
        timespec reachability_total_time;
        std::vector<timespec> frontiers_time;
        std::vector<unsigned int> frontiers_iterations;
        timespec frontiers_total_time;
        unsigned int frontiers_total_iterations;
        ComputationStat(void){
            access_total_time.tv_nsec = 0;
            access_total_time.tv_sec = 0;
            reachability_time.clear();
            reachability_total_time.tv_nsec=0;
            reachability_total_time.tv_sec=0;
            reset_frontiers(0);
        }
        void reset_frontiers(unsigned int size){
            frontiers_time.assign(size,null_ts());
            frontiers_iterations.assign(size,0);
            frontiers_total_iterations=0;
            frontiers_total_time=null_ts();
        }
        static timespec null_ts(void){timespec nts;nts.tv_nsec=nts.tv_sec=0;return nts;}
    };

    MultiAgentGrid();
    /**
     * @brief constructs a grid with no agents
     * @param pace
     * @param envSize
     * @see API::TwoDGrid
     */
    MultiAgentGrid(double pace, std::vector<double> envSize);
    /**
     * @brief specify agents list
     * @param pace
     * @param envSize
     * @param agents
     */
    MultiAgentGrid(double pace, std::vector<double> envSize, std::vector<Robot *> &agents);
    ~MultiAgentGrid();

    void resetData(void);
    //getters and setters
    /**
     * @brief set the agents list to use in this HRI computation.
     * @param agents pointers to the Robot classes to uses, does not copy.
     * @todo set agentsInGrid structures
     */
    void setAgents(std::vector<Robot*> &agents);
    std::vector<Robot*> & getAgents() { return mAgents; }
    AgentInGrid* getAgentStructAt(unsigned int index){return mAgentsInGrid[index];}

    void setAgentTarget(unsigned int a);

    double getNbCellX() {return _nbCellsX;}
    double getNbCellY() {return _nbCellsY;}

    void setAsNotSorted() {gridIsSorted = false;}

    void dumpVar();

    /**
      * initialisation of the grid (find cylinders corresponding to each agent)
      */
    void init(void);
    /**
     * @brief init agents cylinders for reachability computation.
     *
     * find by name two cylinders (HUMAN... and PR2_CYLINDER) and associate them to agents
     * according to their name (HUMAN or not)
     */
    void initCylinders(void);
    /**
     * @brief compute the Accessibility for each agent.
     * Accessibility test if an agent can fit in a cell, not if it is reachable
     */
    void computeAccessibility(void);
    void computeAccessAndDistCombined(void);

    /**
     * @brief computeOtherTrajectory uses A* algo to find shortest path between 2 cells.
     * @param cell_origin
     * @param cell_dest
     * @param agent_index
     * @param epsilon to apply a weighted A*, 1 for classic A*
     * @param dist_max the maximum distance of the path to find
     * @param max_expansions the maximum number of expansions accepted, used to limit time when nothing found; use with care, kills all A* properties
     * @return the path found or empty vector.
     *
     * When an epsilon and a max distance are provided, the max distance actually considered is epsilon times
     * the distance provided, to stay consistent with the fact that the shortest path found in Weighted A*
     * is at most epsilon times that of the least cost path.
     */
    std::vector<MultiAgentCell*> computeOtherTrajectory(MultiAgentCell * cell_origin, MultiAgentCell *cell_dest, unsigned int agent_index,double epsilon=1,double dist_max=-1,int max_expansions=-1);

    /// check if the agent 'a' can go straight from one cell to another
    bool areCellsInNeighbourhood(MultiAgentCell* c1,MultiAgentCell *c2,unsigned int a,double dist);

    /**
     * \brief function that creates a new Cell.
     */
    API::TwoDCell* createNewCell(unsigned int index,unsigned  int x,unsigned  int y );

    /**
     * using the method of distance propagation to compute distances.
     */
    void computeDistances();
    void computeDistancesForAgent(MultiAgentCell* cell, unsigned int agent_index);
    void computeAltDistancesForAgent(MultiAgentCell* origin, unsigned int agent_index);
    void enableAltDistancesSums(bool b=true){_enable_alt_distances_sums=b;if(b){this->clearAltDistancesInCells();}}
    void clearAltDistancesInCells(void);

    unsigned int getMaxAlternativeDistancesForCells(void){return _max_alternative_distances_for_cells;}
    void getMaxAlternativeDistancesForCells(unsigned int v){_max_alternative_distances_for_cells=v;}


    /**
     * @brief find a neighhbour solution for the simulated annealing algorithm.
     * @param agent_1 the first agent index
     * @param agent_2 the second agent index
     * @param solution the pair of cells that describes the base solution
     * @return a new random solution within a certain range defined by..
     * @todo attribute for neighbourhood size (hardcoded)
     */
    std::pair<MultiAgentCell*,MultiAgentCell*> neighborSolution(unsigned int agent_1,unsigned int agent_2,std::pair<MultiAgentCell*,MultiAgentCell*> solution);
    void searchAllFrontiers();
    /**
     * @brief searchFrontiers
     * @param agent_1
     * @param agent_2
     * @param keep_distance
     * @return
     * Simulated annealing
     */
    std::vector<FrontierPtr > searchFrontiers(unsigned int agent_1,unsigned int agent_2,double keep_distance);
    std::vector<FrontierPtr > simulatedAnnealingFrontierSearch(unsigned int agent_1,unsigned int agent_2,double keep_distance);
    std::vector<MetaFrontierPtr > hillClimberFrontierSearch(unsigned int agent1,unsigned int agent2, double keep_distance);
    std::vector<FrontierPtr > frontierSearch(unsigned int agent1,unsigned int agent2, double keep_distance);
    std::vector<FrontierPtr > exhaustiveFrontierSearch(unsigned int agent1,unsigned int agent2, double keep_distance);
    std::vector<FrontierPtr > handInHandFrontierSearch(unsigned int agent1,unsigned int agent2, double keep_distance);

    void saveFrontiers(std::ofstream & output);
    bool loadFrontiers(std::ifstream & input);

    ///inits the frontier container to the good size.
    /// the container is a vector representing a null diagonal symetric matrix.
    void initVectorFrontiers(void);
    ///computes the index int the frontier vector corresponding to the frontiers between agents at i and j.
    unsigned int getFrontierIndex(unsigned int i,unsigned int j);
    unsigned int getFrontierIndex(std::pair<unsigned int,unsigned int> ind){return getFrontierIndex(ind.first,ind.second);}
    ///the reverse of getFrontierIndex().
    std::pair<unsigned int,unsigned int> getFrontierAgentIndices(unsigned int i);
    //std::vector<std::shared_ptr<FrontierCells> > & getFrontier(unsigned int i,unsigned int j);
    std::vector<MultiAgentCell*> const & getFrontierCells(unsigned int i, unsigned int j);
    //void setFrontier(unsigned int i,unsigned int j,vector<std::shared_ptr<FrontierCells> > & frontier);
    double getArmReach(unsigned int i);
    double getArmReach(AgentInGrid *a);
    /// @brief an approximation of the maximal distance for a handover (touching hands)
    /// The sum of the arm reachs of each agent
    double getHandOverReach(unsigned int i,unsigned int j);

    /** @brief add a frontier cell to the agent 'agent'
     * @param agent the agent index that will be modified
     * @param other the other agent concerned by the frontier (not modified)
     * @param cell the cell which is accessible for 'agent'
     * the cell have to be accessible to agent by definition (not checked)
     */
    void addFrontier(unsigned int agent,unsigned int other, MultiAgentCell * cell);
    FrontierPtr getRandomFrontier(unsigned int a1,unsigned int a2, double timeout_ms=-1);
    std::vector<MultiAgentCell*> & getFrontierCheckedOk(unsigned int a1,unsigned int a2);

    double getFrontierSearchTempDecrease(void){return _frontier_search_temp_decrease;}
    void setFrontierSearchTempDecrease(double v){_frontier_search_temp_decrease=v;}
    double getFrontierSearchStartTemp(void){return _frontier_search_start_temp;}
    void setFrontierSearchStartTemp(double v){_frontier_search_start_temp=v;}
    unsigned int getFrontierSearchMaxIt(void){return _frontier_search_max_it;}
    /**
     * @brief set the computational cost limit in # of iterations for the frontier search algorithm.
     * @param v
     * as the frontier search (simulated annealing algo) may be called several times for each pair of agent,
     * the real worst number of iterations in total is:
     * c * n!/(2!(n-2)!) = c * C(n,k) .
     * where :
     *     - c is the number of searchs for each pair
     *     - n is the total number of agents (note the binomial coefficient C(n,k) (n choose 2).
     */
    void setFrontierSearchMaxIt(unsigned int v){_frontier_search_max_it=v;}
    double getFrontierSearchStopDist(void){return _frontier_search_stop_dist;}
    /**
     * @brief set at which distance the frontier search should stop.
     * @param v the distance, in meters, or whichever measure unit is used.
     * If v==0, the distance is set to the cellsize, so that the search
     * stops when the agents appears to be abble to meet each other
     */
    void setFrontierSearchStopDist(double v){_frontier_search_stop_dist=(v==0 ? std::min(this->_cellSize[0],this->_cellSize[1]): v);}
    ComputationStat & getComputationStats(void){return this->_computation_stat;}

    /** @name Display grid
     * methods for displaying info in openGL
     * and setting display options
     */
    ///@{
    /**
     * drawing the grid
     */
    void draw();
private:
    void _drawCell(MultiAgentCell *cell, double *color);

public:
    void setAgentIndexToDrawGrid(int index){
        this->_agent_index_to_draw_grid=index;
    }
    void setDrawAgentAccessibilityGrid(bool b,bool boundariesOnly=false){this->_drawAgentReachGrid=b; this->_drawBoundariesOnly=boundariesOnly;}
    void setDrawAStar(bool b){_draw_a_star=b;}
    void setDrawFrontiers(bool b,unsigned int i=0,unsigned int j=0){_drawFrontiers=b;_drawFrontierIndices.first=i;_drawFrontierIndices.second=j;}
    void setDrawFrontiersSearchLog(bool b){_drawLog=b;}
    unsigned int getFrontierSearchLogSize(void);
    void setFrontierSearchLogIndex(unsigned int i){_log_draw_window=i;}
    void setFrontierSearchLogWindow(unsigned int i){_log_draw_window_size=i;}

#ifdef DRAW_OPTIMIZE_FRONTIER
    void setDrawGridForOptim(bool b,std::vector<std::pair<MultiAgentCell*,unsigned int> > &cells_agents){_draw_grid_for_optim=b;_draw_grid_for_optim_cells_agents=cells_agents;}
    void unsetDrawGridForOptim(void){_draw_grid_for_optim=false;_draw_grid_for_optim_cells_agents.clear();}
#endif
#if defined(DRAW_FRONTIER_HAND_IN_HAND) || defined(DRAW_OPTIMIZE_FRONTIER)
    void setDrawOptimizeFrontier(MultiAgentCell *c1,MultiAgentCell* c2,bool ok){_draw_frontier_search_cells.first=c1;_draw_frontier_search_cells.second=c2;_draw_frontier_search_is_pair_ok=ok;_draw_frontier_search=true;}
    void unsetDrawOptimizeFrontier(void){_draw_frontier_search=false;}
#endif

    void setDrawExtraCells(std::vector<MultiAgentCell*>::iterator first,std::vector<MultiAgentCell*>::iterator last,double ratio_max){
        _draw_extra=true;
        _draw_extra_cells=std::make_pair(first,last);
        _temp_color_max_ratio=ratio_max;
    }
    void unsetDrawExtraCells(){_draw_extra=false;_draw_extra_cells.first=_draw_extra_cells.second;_temp_color_max_ratio=1;}

    ///@}

    /**
     * call setBlankCost() in each cell
     */
    void setCellsToblankCost();

    /**
     * call resetexplorationstatus() in each cell
     */
    void initAllCellState();

    /**
     * call resetTrajs() in each cell
     */
    void initAllTrajs();

    /**
     * call resetReacheability() in each cell
     */
    void resetAllAccessibility();
    void resetDistances(void);

    /**
     * sort the cells of the grid
     */
//    std::vector<std::pair<double,MultiAgentCell*> > getSortedGrid();

private:
    /**
     * the Agents list
     */
    std::vector<Robot*> mAgents;
    std::vector<AgentInGrid*> mAgentsInGrid;

    Robot* robotCyl; ///<the cylinder for robots
    Robot* humCyl;///<the cylinder for humans

    /**
     * the list of sorted cells
     */
    std::vector<std::pair<double,MultiAgentCell*> > sortedGrid;

    /**
     * if the grid is already sorted
     */
    bool gridIsSorted;

    /// used to limit the number of distances from different cells we can store for each agent
    /// (effective storage is in MultiAgentCell::AgentInCell)
    unsigned int _max_alternative_distances_for_cells;
    bool _enable_alt_distances_sums;
    std::pair<MultiAgentCell*,double> _best_cell_in_alt_dist_sum;

    //frontier searching
    bool _use_reachable_for_frontier_search;
    //std::vector<std::vector<std::shared_ptr<FrontierCells> > > _frontiers;
    double _frontier_search_temp_decrease;
    double _frontier_search_start_temp;
    unsigned int _frontier_search_max_it;
    double _frontier_search_stop_dist;
    std::vector<std::vector<std::pair<double,std::pair<MultiAgentCell*,MultiAgentCell*> > > > _log_frontier_seeking;
    unsigned int _log_draw_window;
    unsigned int _log_draw_window_size;
    bool _save_log_frontier;///<whether or not to save logs of frontier search algorithm (for display and debug purpose only)


    //grid drawing parameters
    bool _drawAgentReachGrid;
    bool _drawBoundariesOnly;
    bool _draw_a_star;
#if defined(DRAW_FRONTIER_HAND_IN_HAND) || defined(DRAW_OPTIMIZE_FRONTIER)
    bool _draw_frontier_search;
    std::pair<MultiAgentCell*,MultiAgentCell*> _draw_frontier_search_cells;
    bool _draw_frontier_search_is_pair_ok;
#endif
#ifdef DRAW_OPTIMIZE_FRONTIER
    bool _draw_grid_for_optim;
    std::vector<std::pair<MultiAgentCell*,unsigned int> >  _draw_grid_for_optim_cells_agents;
#endif
#ifdef DRAW_FRONTIER_STEPS
    bool _draw_frontier_steps;
    std::vector<MultiAgentCell*> _draw_frontier_steps_cells;
#endif
    int _agent_index_to_draw_grid;
    bool _drawFrontiers;
    bool _drawLog; ///<to draw the _log_frontier_seeking instead of the frontier itself
    std::pair<unsigned int, unsigned int> _drawFrontierIndices;
    ComputationStat _computation_stat;
    bool _draw_extra;
    std::pair<std::vector<MultiAgentCell*>::iterator,std::vector<MultiAgentCell*>::iterator > _draw_extra_cells;
    double _temp_color_max_ratio;


    //debug parameters
    bool _showText;

};

/**
     @ingroup HRICS
     @brief Plannar HRI Cell
     */
class MultiAgentCell : public API::TwoDCell
{

    typedef MultiAgentGrid::AgentInGrid AgentInGrid;
public:
    class AgentInCell;
    class CylinderInCell
    {
    public:
        typedef std::shared_ptr<CylinderInCell> s_ptr;
        CylinderInCell (Robot * cyl,bool access_computed=false,bool accessible=false):
            _cylinder(cyl),_accessible(accessible),_access_computed(access_computed){}
        bool isAccessible(){return _accessible;}
        bool isAccessibilityComputed(){return _access_computed;}
        void setAccessible(bool accessible){_accessible=accessible; _access_computed=true;}
        void unsetAccessibilityComputed(){_accessible=_access_computed=false;}
        Robot* getCylinder(){return _cylinder;}

    private:
        Robot* _cylinder;
        bool _accessible;
        bool _access_computed;
    };

    class MetaAgentInCell
    {
    public:
        typedef std::shared_ptr<MetaAgentInCell> s_ptr;
        MetaAgentInCell(MultiAgentGrid::MetaAgentInGrid::s_ptr meta_in_grid,CylinderInCell::s_ptr cyl,bool reach_computed=false,bool reach=false,bool boundary=false):
            _cylInCell(cyl),_reachable(reach),_reachComputed(reach_computed),_boundary(boundary), _metaAgentInGrid(meta_in_grid),letter_id(_letter_cnt++){}
        MetaAgentInCell::s_ptr shared_clone(MultiAgentGrid::MetaAgentInGrid::s_ptr meta_in_grid);
        void addRealAgent(AgentInCell* a, s_ptr s_this);
        void removeAgent(AgentInCell* a);
        bool isAccessible(){return _cylInCell->isAccessible();}
        bool isAccessibilityComputed(){return _cylInCell->isAccessibilityComputed();}
        void setAccessible(bool accessible){this->getCylinderInCell()->setAccessible(accessible);}
        void unsetAccessibilityComputed(){getCylinderInCell()->unsetAccessibilityComputed();}
        bool isReachable(){return _reachable;}
        bool isReachabilityComputed(){return _reachComputed;}
        void setReachability(bool b){_reachable=b;_reachComputed=true;}
        void unsetReachabilityComputed(){_reachable=_reachComputed=false;}
        bool isBoundary(){return _boundary;}
        void setBoundary(bool b){_boundary=b;}

        CylinderInCell::s_ptr getCylinderInCell(void){return _cylInCell;}
        MultiAgentGrid::MetaAgentInGrid::s_ptr getMetaAgentInGrid(){return _metaAgentInGrid;}

        std::vector<MetaFrontierPtr> & getFrontiers(MetaAgentInCell::s_ptr other){return _meta_frontiers[other->getMetaAgentInGrid().get()];}
        std::vector<MetaFrontierPtr> & getFrontiers(MultiAgentGrid::MetaAgentInGrid::s_ptr other){return _meta_frontiers[other.get()];}


    private:
        CylinderInCell::s_ptr _cylInCell;
        bool _reachable;
        bool _reachComputed;
        bool _boundary;
        MultiAgentGrid::MetaAgentInGrid::s_ptr _metaAgentInGrid;
        std::vector<AgentInCell*> _realAgents;
        static char _letter_cnt;
        std::map< MultiAgentGrid::MetaAgentInGrid*,std::vector<MetaFrontierPtr > > _meta_frontiers;
    public:
        char letter_id;
    };

    /**
     * @brief structure defining the data for an agent in a cell
     */
    struct AgentInCell{
        void initFrontiersVector(unsigned int size){_frontiers.assign(size,std::vector<FrontierPtr>(0));}
        AgentInGrid* agentInGrid; ///< pointer to the AgentInGrid structure in the Grid
        // traj //
        std::vector<Eigen::Vector2d,Eigen::aligned_allocator<Eigen::Vector2d> > vect_traj;
        std::vector<MultiAgentCell*> traj; ///< the shortest path to come to this cell from starting position
        // accessibility //
        bool isAccessible(){return getMetaAgent()->isAccessible();} ///< true if the Agent (its cylinder) can fit in this cell, not if it actually can go there
        bool isAccessibilityComputed(){return getMetaAgent()->isAccessibilityComputed();} ///< whether the accessibility have been computed or is unknown
        void setAccessible(bool b){getMetaAgent()->setAccessible(b);}
        void unsetAccessibilityComputed(){getMetaAgent()->unsetAccessibilityComputed();}
        // reachability //
        /// check if the meta agent is correctly set, if it is, return reachability status, false otherwise
        /// meta agent can be baddly set if the cell is not accessible by this meta agent
        bool isReachable(){return  checkMetaAgent() && getMetaAgent()->isReachable();}
        /// check if the meta agent is correctly set, if it is, return reachability computation status, false otherwise
        bool isReachabilityComputed(){assert(checkMetaAgent()); return  getMetaAgent()->isReachabilityComputed();}
        /// set the reachability of the corresponging meta agent, ABORT otherwise
        void setReachability(bool b){assert(this->checkMetaAgent()); getMetaAgent()->setReachability(b);}
        void unsetReachabilityComputed(){assert(checkMetaAgent());getMetaAgent()->unsetReachabilityComputed();}
        // distance //
        bool distanceIsComputed; ///< whether the distance have been computed or is unknown
                                 ///< false either means that it is unreachable (inf distance)
                                 ///< or it is unknown (not computed yet)
        double distance; ///< distance of this cell from initial position of agent (shortest)
        std::deque<double> distance_alternatives; ///< distances of this cell from cell origin_alternatives (others online defined cells)
        std::deque<MultiAgentCell*> origin_alternatives;
        std::deque<MultiAgentCell*> come_from_alternatives;
        // boundaries //
        /** true if this cell is a boundary for this agent,
         * i.e. it has at least one neighbour cell which is not accessible
         * or again, this cell is near an obstacle
         */
        bool isBoundary(void){return getMetaAgent()->isBoundary();}
        void setBoundary(bool b){getMetaAgent()->setBoundary(b);}
        std::vector<std::shared_ptr<FrontierCells> > & frontiers(AgentInCell* other){return _frontiers[other->index];}
        std::vector<MetaFrontierPtr> & getMetaFrontier(AgentInCell* other){return getMetaAgent()->getFrontiers(other->getMetaAgent());}
        std::vector<MetaFrontierPtr> & getMetaFrontier(MultiAgentGrid::MetaAgentInGrid::s_ptr other){return getMetaAgent()->getFrontiers(other);}
        // other //
        Robot * robot(void){return agentInGrid->getRobot();}
        CylinderInCell::s_ptr getCylinderInCell(void){return this->getMetaAgent()->getCylinderInCell();}
        MetaAgentInCell::s_ptr getMetaAgent(){return _metaAgentInCell;}
        void setMetaAgent(MetaAgentInCell::s_ptr meta){_metaAgentInCell=meta;metaLetter=_metaAgentInCell->letter_id;}
        bool checkMetaAgent(){return (this->agentInGrid->getMetaAgent()==this->getMetaAgent()->getMetaAgentInGrid());}
        char metaLetter;
        unsigned int index;
    private:
        MetaAgentInCell::s_ptr _metaAgentInCell;
        std::vector<std::vector<FrontierPtr > > _frontiers;
    };


public:
    MultiAgentCell();
    MultiAgentCell(int i, Eigen::Vector2i coord, Eigen::Vector2d corner, MultiAgentGrid* grid);

    ~MultiAgentCell() { }

    //#######################
    //getters et setters ####
    //#######################

    /**
     * @brief getAgentAt
     * @param i
     * @return
     * @see getAgentNumber()
     */
    AgentInCell * getAgentAt(unsigned int i){return _agentsInCell[i];}
    unsigned int getAgentNumber(){return _agentsInCell.size();}
    MultiAgentGrid::MetaAgentInGrid::s_ptr getMetaFromGrid(unsigned int a){return this->getAgentAt(a)->agentInGrid->getMetaAgent();}

    ///@brief reset data as for a new cell, set agents list according to the grid ones.
    void resetData(void);

    //distance computing
    void setDistance(unsigned int index,double value) {getAgentAt(index)->distance = value; }
    void setAltDistance(unsigned int index, double value, MultiAgentCell* alt_origin, MultiAgentCell *previous_cell=0);
    double getDistance(unsigned int index) {return getAgentAt(index)->distance; }
    double getAltDistance(unsigned int index,MultiAgentCell* alt_origin);
    void setDistanceIsComputed(unsigned int index) {getAgentAt(index)->distanceIsComputed = true;}
    void unsetDistanceIsComputed(unsigned int index) {getAgentAt(index)->distanceIsComputed = false;}
    bool isDistanceComputed(unsigned int index) {return getAgentAt(index)->distanceIsComputed;}

    /// get distance either using getDistance or getAltDistance, on this or other
    /// according to the origins set
    /// does not read tempDist
    /// returns inf if not found
    double getDistanceAny(unsigned int agent,MultiAgentCell* other);
    std::vector<MultiAgentCell*> getTrajAny(unsigned int agent, MultiAgentCell *origin);
    /// returns the shortest path FROM alt_origin TO this
    /// except if reverse is set to true, its the other way around
    std::vector<MultiAgentCell*> getAltTraj(unsigned int agent, MultiAgentCell *alt_origin, bool reverse=false);

    // distance propagation algorithm
    bool getOpen() { return _Open; }
    void setOpen() { _Open = true; }

    bool getClosed() { return _Closed; }
    void setClosed() { _Closed = true; }

    double getTempColor(){return _tmp_color;}
    void setTempColor(double c){_tmp_color=c;}

    void resetExplorationStatus() { _Open = false; _Closed = false; }

    // Trajectories

    std::vector<Eigen::Vector2d,Eigen::aligned_allocator<Eigen::Vector2d> > & getVectorTraj(unsigned int index) {return getAgentAt(index)->vect_traj;}
    void setVectorTraj(unsigned int index,std::vector<Eigen::Vector2d,Eigen::aligned_allocator<Eigen::Vector2d> > & vec_traj) {getAgentAt(index)->vect_traj = vec_traj; }

    std::vector<MultiAgentCell*> & getTraj(unsigned int index) {return getAgentAt(index)->traj;}
    //std::vector<Eigen::Vector2d> getTempVectorTraj(void);
    void setTraj(unsigned int index, std::vector<MultiAgentCell*> & traj) {this->getAgentAt(index)->traj = traj; }
    double getTempDist(void){return _temp_dist;}
    void setTempDist(double v){_temp_dist=v;}
    double getTempEstim(void){return _temp_estim;}
    void setTempEstim(double v){_temp_estim=v;}
    MultiAgentCell* getTempCameFrom(void) {return _temp_came_from;}
    void setTempCameFrom(MultiAgentCell * f) {_temp_came_from=f;}

    //void setAltCameFrom(unsigned int index,MultiAgentCell* origin,MultiAgentCell* previous);
    MultiAgentCell* getAltCameFrom(unsigned int index, MultiAgentCell* alt_origin);

    //accessibility
    bool isAccessible(unsigned int index){return getAgentAt(index)->getCylinderInCell()->isAccessible();}
    void setAccessible(unsigned int index,bool v){getAgentAt(index)->getCylinderInCell()->setAccessible(v);}
    bool isAccessibilityComputed(unsigned int index){return getAgentAt(index)->getCylinderInCell()->isAccessibilityComputed();}
    //void setAccessibilityComputed(unsigned int index,bool v){}//getAgentAt(index)->getCylinderInCell()->setAccessibilityComputed(v);}
    void setReachable(unsigned int index,bool b){getAgentAt(index)->setReachability(b);}
    bool isReachable(unsigned int index){return getAgentAt(index)->isReachable();}

    bool isBoundary(unsigned int index){return getAgentAt(index)->isBoundary();}
    bool isFrontier(unsigned int a1,unsigned int a2){return (getAgentAt(a1)->frontiers(getAgentAt(a2)).size() > 0);}

    bool checkIsInCollision(unsigned int agent_this,MultiAgentCell* other,unsigned int agent_other);

    /// calls createRealFrontierFor(a1,a2,meta) for each meta frontier in which a1 and a2 are concerned
    std::vector<std::shared_ptr<FrontierCells> > createAllRealFrontiersFor(unsigned int a1,unsigned int a2);
    /// creates and set a real frontier as a specialization of the given MetaFrontier, for agents a1 and a2
    /// returns this frontier
    FrontierPtr createRealFrontierFor(unsigned int a1,unsigned int a2, MetaFrontierPtr mfrontier);
    std::vector<MetaFrontierPtr> & getMetaFrontier(unsigned int a1,unsigned int a2){
        assert (isReachable(a1));
        //checkMetaAgent(a2);
        return getAgentAt(a1)->getMetaFrontier(getMetaFromGrid(a2));
    }
    std::vector<MetaFrontierPtr> & getMetaFrontier(unsigned int a1,MultiAgentGrid::MetaAgentInGrid::s_ptr m2){
        assert (isReachable(a1));
        return getAgentAt(a1)->getMetaFrontier(m2);
    }
    /// returns the real Frontier that is a specification of the MetaFrontier mfrontier
    /// returns empty ptr if not set
    /// @see createRealFrontierFor()
    FrontierPtr getRealFrontier(unsigned int a1,unsigned int a2,MetaFrontierPtr mfrontier);

    //other
    void setBlankCost() { _costIsComputed = false;}
    //void setCost(double value) {getAgentAt(index)->cost = value; }
    //double getCost() {return mCost; }
    //double getMoveCost(unsigned int index){return getAgentAt(index)->moveCost;}

    Eigen::Vector2i getCoord() { return _Coord; }



    //#######################
    // others ###############
    //#######################

    /**
      * test if there is a collision with a BB (the cylinders) for the agent referenced in the the MultiAgentGrid as at given index (must not change)
      */
    void computeAccessibility(unsigned int index_agent);

    // /**
    // * @brief compute move cost for an agent, according to it's speed.
    // * @param index_agent
    // * @return
    // * @todo use different weights for travel distance and duration
    // */
    //double computeMoveCost(unsigned int index_agent);
    /**
     * compute partial cost
     */
    double computeCost();

    /**
     * compute best robot pos
     */
    bool computeBestRobotPos();

    /** return true if meta agent is OK, false if it have been changed
     */
    bool checkMetaAgent(unsigned int index);

    /**
     * reset the reacheability computing for all agents
     */
    void resetAccessibility();
    /// @brief reset reachability for agent at index
    void resetAccessibility(unsigned int index);
    void resetDistances(unsigned int index);
    void resetAllDistances(void);

    /**
     * reset the Trajectories computing
     */
    void resetTraj();
    /// reset trajectories computing related data for agent at index
    void resetTraj(unsigned int index);

    /**
     * find the direct neighbors of this cell (used when computing distance propagation)
     */
    std::vector<MultiAgentCell*> getNeighbors(unsigned int index);

    std::vector<MultiAgentCell*> findNeighborsInRange(unsigned int agent,double range);

    /**
     * compute distance between two cells
     */
    double computeDist(MultiAgentCell* neighCell);

    /**
     * add a point to the random vector in order to draw it (the red arrows)
     */
    void addPoint(double Rz);

    /**
     * return the crown arround the cell taking the min and max value
     */
    std::vector<MultiAgentCell*> getCrown(double min, double max);


    std::shared_ptr<MetaFrontierCells> addMetaFrontier(MultiAgentCell *other, bool over_obstacle, unsigned int agent_this,unsigned int agent_other,double dist,bool check_exists=false);
    std::shared_ptr<MetaFrontierCells> addMetaFrontier(MultiAgentCell *other, bool over_obstacle, MetaAgentInCell::s_ptr agent_this, MetaAgentInCell::s_ptr agent_other, double dist, bool check_exists=false);
    std::shared_ptr<FrontierCells> addFrontierComplete(MultiAgentCell *other, bool over_obstacle, unsigned int agent_this,unsigned int agent_other,double dist,bool check_exists=false);

private:

    std::vector<AgentInCell*> _agentsInCell;
    std::vector<CylinderInCell::s_ptr> _cylinders;
    std::vector<MetaAgentInCell::s_ptr> _metaAgents;

    /**
     * the x y coord of the cell
     */
    Eigen::Vector2i _Coord;

    /**
     * the status of the cell
     */
    bool _Open;

    /**
     * the status of the cell
     */
    bool _Closed;

    bool _costIsComputed;

    double _tmp_color;

    /// trajectory to come to this cell, in a temporary computation
    /// is generally set to empty
    /// @see MultiAgentGrid::computeOtherTrajectory()
    MultiAgentCell * _temp_came_from;
    /// cost associated
    double _temp_dist;
    double _temp_estim;///< for A*

    double _alt_distance_sum;

};

class MetaFrontierCells{
public:
    MetaFrontierCells(MultiAgentGrid::MetaAgentInGrid::s_ptr agentA,MultiAgentCell* cellA,MultiAgentGrid::MetaAgentInGrid::s_ptr agentB, MultiAgentCell* cellB,double distance,bool over_obstacle) :
        _metaA(agentA),_metaB(agentB),_cellA(cellA),_cellB(cellB), _dist(distance), _checked_ok(0), _over_obstacle(over_obstacle) {}
    MultiAgentCell * cellOf(MultiAgentGrid::MetaAgentInGrid::s_ptr meta){
        if(meta==_metaA){return _cellA;}
        if(meta==_metaB){return _cellB;}
        return 0x0;
    }
    FrontierPtr getRealFrontier (MultiAgentGrid::AgentInGrid* a1, MultiAgentGrid::AgentInGrid* a2) {
        //if (a1>a2){std::swap(a1,a2);}
        return _implementations_of_this[std::make_pair(a1,a2)];
    }
    void setRealFrontier (MultiAgentGrid::AgentInGrid* a1, MultiAgentGrid::AgentInGrid* a2,FrontierPtr frontier) {
        //if (a1>a2){std::swap(a1,a2);}
        _implementations_of_this[std::make_pair(a1,a2)] = frontier;
    }
    MultiAgentCell* getCellA(){return _cellA;}
    MultiAgentCell* getCellB(){return _cellB;}
    double getDistance(){return _dist;}
    struct CheckCellsAre{
        CheckCellsAre(MultiAgentCell* cell1,MultiAgentGrid::MetaAgentInGrid::s_ptr a1,MultiAgentCell* cell2,MultiAgentGrid::MetaAgentInGrid::s_ptr a2) :
            cell1(cell1),cell2(cell2),a1(a1),a2(a2){}
        CheckCellsAre(MetaFrontierPtr & val){cell1=val->_cellA;cell2=val->_cellB;a1=val->_metaA;a2=val->_metaB;}
        bool operator()(MetaFrontierPtr other){
            if(other->cellOf(a1) == cell1 && other->cellOf(a2) == cell2){
                return true;
            }
            return false;
        }
        MultiAgentCell* cell1,*cell2;
        MultiAgentGrid::MetaAgentInGrid::s_ptr a1,a2;
    };

    bool getCheckedOk(){return _checked_ok;}
    void setCheckedOk(bool b=true){_checked_ok=b;}
    statePtr_t getObjectPosition(){return object_position;}
    void setObjectPosition(statePtr_t q_obj){ this->object_position = q_obj;}
    bool isOverObstacle(){return _over_obstacle;}

private:
    std::map<std::pair<MultiAgentGrid::AgentInGrid*,MultiAgentGrid::AgentInGrid*>, FrontierPtr> _implementations_of_this;
    MultiAgentGrid::MetaAgentInGrid::s_ptr _metaA;
    MultiAgentGrid::MetaAgentInGrid::s_ptr _metaB;
    MultiAgentCell * _cellA;
    MultiAgentCell * _cellB;
    double _dist;
    bool _checked_ok;
    statePtr_t object_position; ///< a position of the object where it may be reachable by the two agents
    bool _over_obstacle;
};

struct FrontierCells{
    enum FrontierCheckStatus {FCS_NONE=0, FCS_OBJECT, FCS_GRAB_CONFS, FCS_HANDOVER,FCS_TRAJECTORIES,FCS_ENUM_SIZE};
    FrontierCells() : agentA(0),agentB(0),agentA_ind(0),agentB_ind(0),
        confA(),confB(),check_status(FCS_NONE),status_ok(FCS_ENUM_SIZE,false),//object_ok(0),grab_confs_ok(0),handover_ok(0),
        object_position(),grabA(),grabB(),trajA(0),trajB(0),confChecked(0),_symetric(true),
        cell(0), meta_frontier() {status_ok[FCS_NONE]=true;}
    virtual ~FrontierCells();
    MultiAgentCell * cellA(){return meta_frontier->getCellA();}
    MultiAgentCell * cellB(){return meta_frontier->getCellB();}
    MultiAgentCell::AgentInCell * agentA;
    MultiAgentCell::AgentInCell * agentB;
    unsigned int agentA_ind;
    unsigned int agentB_ind;
    statePtr_t confA;
    statePtr_t confB;
    statePtr_t r_confA,r_confB;
    double dist(){return meta_frontier->getDistance();}
    FrontierCells::FrontierCheckStatus check_status,r_check_status; ///< the results of the tests are in object_ok, grab_confs_ok, handover_ok
    //bool object_ok;
    //bool grab_confs_ok;
    //bool handover_ok;
    std::vector<bool> status_ok;
    std::vector<bool> r_status_ok;
    std::shared_ptr<RobotState> object_position; ///< a position of the object where it may be reachable by the two agents (when from A to B or symetric)
    std::shared_ptr<RobotState> r_object_position; ///< a position of the object where it may be reachable by the two agents (when from B to A)
    Eigen::Vector3d grabA;
    Eigen::Vector3d grabB;
    Eigen::Vector3d r_grabA,r_grabB;
    API::GeometricPath *trajA,*trajB; ///< owned by this, deleted when this is deleted
    API::GeometricPath *r_trajA,*r_trajB; ///< idem, in the handover from B to A
    bool confChecked;
private:
    bool _symetric; ///< true if the handover form A to B has same attributes than from B to A
public:
    bool select_reverse; ///< used as flag/argument for functions, to use symetric/direct direction or reverse direction
    bool isSymetric(){return _symetric;}

    /// use this when the frontiers diverge according to its direction
    /// it duplicates data to their "reverse" counterpart, and unset the "is symetric" flag
    void deSymetrize();

    MultiAgentCell* cellOf(unsigned int agent){
        if(agent==agentA_ind){return cellA();}
        if(agent==agentB_ind){return cellB();}
        return 0x0;
    }
    statePtr_t confOf(Robot *r){
        if(agentA->robot()==r){return confA;}
        else if(agentB->robot()==r){return confB;}
        else { statePtr_t p; return p;}
    }
    /**
     * @brief create or find a similar frontier, changing positions of agents
     * @return the randomly selected/created frontier
     */
    FrontierPtr neighbourFrontier(double keep_distance, double min_distance=0);
    std::vector<FrontierPtr> findNeighbourFrontiers(double keep_distance, double min_distance=0);
    std::vector<FrontierPtr> findNeighbourFrontiersJumping(double keep_distance,double min_distance,unsigned int number);
    MultiAgentCell *cell;
    MetaFrontierPtr meta_frontier;
    bool isOverObstacle(){return meta_frontier->isOverObstacle();}
};
}



#endif // HRICS_TWODGRID_HPP
