#ifndef HRICS_CF_PATHLENGTHCOST_H
#define HRICS_CF_PATHLENGTHCOST_H

//#include "move4d/HRI_costspace/HRICS_costspace.hpp"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"
#include "move4d/API/Trajectory/trajectory.hpp"

namespace move4d
{
namespace HRICS {
namespace CF {

class PathLengthCost : public VirtualCostFunction
{
public:
    using VirtualCostFunction::getCost;

    PathLengthCost(CostFunctionCommon *commonCF, double min, double max, double low, double up, double maxToMeanRatio, FunctionShape shape);
    ~PathLengthCost();

    virtual double getCost(){return 0;}

    static std::string getName(){return "PathLength";}

    double getCost(std::vector<API::LocalPathBase *> &pathes);
    double getCost(API::Path &path);

    ///get length run by all specified joints (sum)
    double getTotalLength(API::Path &path);

    /// get length run by a joint
    double getLength(API::Path &path, std::vector<int> &jointIndices);

    /// set max length from path length
    void setMaxLength(API::Path &path);

private:
};

class TrajectoryDurationCost : public VirtualCostFunction
{
public:
    using VirtualCostFunction::getCost;

    TrajectoryDurationCost(CostFunctionCommon *commonCF,double min, double max,double low,double up,double maxToMeanRatio, FunctionShape shape);
    virtual ~TrajectoryDurationCost(){}

    virtual double getCost(){return 0;}
    static std::string getName(){return "Duration";}

    double getCost(std::vector<API::LocalPathBase*> &pathes);
    double getCost(API::Path &path);
};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_PATHLENGTHCOST_H
