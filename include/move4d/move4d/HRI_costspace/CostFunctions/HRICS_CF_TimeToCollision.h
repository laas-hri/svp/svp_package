#ifndef HRICS_CF_TIMETOCOLLISION_H
#define HRICS_CF_TIMETOCOLLISION_H

#include "move4d/HRI_costspace/CostFunctions/HRICS_costfunction.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"
#include "move4d/Logging/Logger.h"
#include "move4d/API/forward_declarations.hpp"


namespace move4d
{
namespace HRICS {
namespace CF {

class TimeToCollision : public HRICS::CF::VirtualCostFunction
{
    MOVE3D_STATIC_LOGGER;
public:
    using VirtualCostFunction::getCost;

    TimeToCollision(CostFunctionCommon *commonCF,double min, double max, double low,double up,double maxToMeanRatio, FunctionShape shape);
    virtual ~TimeToCollision();

    static std::string getName(){return "TimeToCollision";}

    double getCost();
    /// rate the last configuration of confs according to its history
    double getCost(std::vector<statePtr_t> &confs, double dt);
    double getCost(const API::Path &traj);
    double getTimeToCollision(statePtr_t cprev, statePtr_t ccurrent, double dt, double maxt);
    statePtr_t projectConfiguration(statePtr_t cprev, statePtr_t ccurrent, double dt, double t);

    virtual bool isDynamicCost(){return true;}

    static int test(CostFunctionCommon* commonCF);
private:
};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_TIMETOCOLLISION_H
