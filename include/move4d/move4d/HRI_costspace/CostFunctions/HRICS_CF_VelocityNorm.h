/**
 * @file HRICS_CF_VelocityNorm.h
 *
 * @copyright  2016 CNRS/LAAS. All rights reserved.
 * @author Jean-François Erdelyi
 *
 */

#ifndef LIBMOVE3D_PLANNERS_HRICS_CF_VELOCITYNORM_H
#define LIBMOVE3D_PLANNERS_HRICS_CF_VELOCITYNORM_H

#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"
#include "move4d/API/forward_declarations.hpp"

namespace move4d
{
namespace HRICS {
    namespace CF {
        class VelocityNorm : public VirtualCostFunction {
        public:
            using VirtualCostFunction::getCost;

            VelocityNorm(CostFunctionCommon *commonCF, double min, double max, double low, double up, double maxToMeanRatio, FunctionShape shape);

            virtual ~VelocityNorm();

            double getCost();

            static std::string getName();
        };
    } // namespace CF
} // namespace HRICS

} //namespace move4d
#endif //LIBMOVE3D_PLANNERS_HRICS_CF_VELOCITYNORM_H
