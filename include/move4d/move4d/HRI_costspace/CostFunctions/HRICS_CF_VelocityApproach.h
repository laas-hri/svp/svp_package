/**
 * @file HRICS_CF_ApproachVelocity.h
 *
 * @copyright  2016 CNRS/LAAS. All rights reserved.
 * @author Jean-François Erdelyi
 *
 */

#ifndef LIBMOVE3D_PLANNERS_HRICS_CF_APPROACHVELOCITY_H
#define LIBMOVE3D_PLANNERS_HRICS_CF_APPROACHVELOCITY_H

#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"
#include "move4d/API/forward_declarations.hpp"

namespace move4d
{
namespace HRICS {
    namespace CF {

    class VelocityNorm;
    class VelocityOrientation;
    class Distance2D;

        class VelocityApproach : public VirtualCostFunction {
        public:
            using VirtualCostFunction::getCost;

            VelocityApproach(CostFunctionCommon *commonCF, double min, double max, double low, double up,
                             double maxToMeanRatio, FunctionShape shape, double distH);

            virtual ~VelocityApproach();

            double getValSigmoid(double x, double min, double max, double lower, double upper);
            double getCost();

            static std::string getName();
        protected:
            double _distH;
            VelocityNorm *_vNormCF;
            VelocityOrientation *_vOrienCF;
            Distance2D *_velBoundCF;

        };

    } // namespace CF
} // namespace HRICS

} //namespace move4d
#endif //LIBMOVE3D_PLANNERS_HRICS_CF_APPROACHVELOCITY_H
