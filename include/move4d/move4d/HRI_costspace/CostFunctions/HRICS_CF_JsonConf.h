#ifndef HRICS_CF_JSONCONF_H
#define HRICS_CF_JSONCONF_H

#include <jsoncpp/json/json.h>
#include "move4d/HRI_costspace/CostFunctions/HRICS_costfunction.h"
#include "move4d/database/semantics/Sem_AgentType.hpp"
#include "move4d/Logging/Logger.h"


namespace move4d
{
namespace HRICS {
namespace CF {

class JsonConf
{
    MOVE3D_STATIC_LOGGER;
public:
    JsonConf(CostFunctionCommon *common=NULL,std::string path="");
    ~JsonConf();

    std::string getPathBodyWeights(Sem::AgentType const &agent_type);
    std::string getPathJointWeights(Sem::AgentType const &agent_type);

    bool writeBodiesWeights();
    bool readBodiesWeights();
    bool readBodiesWeights(const Sem::AgentType &agent_type);

    bool writeJointWeights();
    bool readJointWeights();
    bool readJointWeights(const Sem::AgentType &agent_type);


private:
    CostFunctionCommon *_common;
    std::string _path;
};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_JSONCONF_H
