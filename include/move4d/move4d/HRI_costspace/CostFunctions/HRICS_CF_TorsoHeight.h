#ifndef HRICS_CF_TORSOHEIGHT_H
#define HRICS_CF_TORSOHEIGHT_H

#include "move4d/Logging/Logger.h"
#include "move4d/API/forward_declarations.hpp"

#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_KeepItLow.h"

namespace move4d
{
namespace HRICS {
namespace CF {

class TorsoHeight : public VirtualCostFunction
{
    MOVE3D_STATIC_LOGGER;
public:
    using VirtualCostFunction::getCost;

    TorsoHeight(CostFunctionCommon *commonCF,double min, double max, double low,double up,double maxToMeanRatio, FunctionShape shape);
    virtual ~TorsoHeight();

    double getCost();

    void findJoints();
    void computeLawParam();

    void onHumanMove();

    void setLowerBound(double lowerBound);
    double loweBound() const;

    void setUpperBound(double upperBound);
    double upperBound() const;

    static std::string getName();

private:

    KeepItLow * _keepItLow;

    std::vector<Joint*> _shoulders,_pelvises;
};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_TORSOHEIGHT_H
