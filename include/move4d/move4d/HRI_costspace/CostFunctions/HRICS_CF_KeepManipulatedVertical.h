#ifndef HRICS_CF_KEEPMANIPULATEDVERTICAL_H
#define HRICS_CF_KEEPMANIPULATEDVERTICAL_H

#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"
#include "move4d/API/forward_declarations.hpp"

namespace move4d
{
namespace HRICS {
namespace CF {

/**
 * @brief The KeepManipulatedVertical class provide a cost function that leads to keep an object vertical during it is being moved by a Robot.
 *
 * The class uses data stored into the ObjectRob of each manipulated object to keep the axis defined there vertical, with the tolerance given.
 * The lowerBound and upperBound should be kept normalized (0 and 1 resp.), as the class manage the angle tolerance. They can be changed to
 * reinforce or relax the constraint. maxCost should be set to a very high value, if we want the planner to really stay in the valid inclination.
 * minCost is the cost returned when no object is in hand, so it should be set to 0. It is the cost when the inclination is in
 * [0,ObjectRob::maxVerticalTolerance()] (when using LINEAR shape, and (low,up) is (0,1)).
 */
class KeepManipulatedVertical : public VirtualCostFunction
{
public:
    using VirtualCostFunction::getCost;

    KeepManipulatedVertical(CostFunctionCommon *commonCF, double min, double max, double low, double up, double maxToMeanRatio, FunctionShape shape);
    virtual ~KeepManipulatedVertical();

    virtual double getCost();

    static std::string getName(){return "KeepManipVertical";}


    std::vector<Robot *> getObjects();

private:

};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_KEEPMANIPULATEDVERTICAL_H
