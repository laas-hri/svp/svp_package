#ifndef HRICS_CF_COSTFUNCTION_H
#define HRICS_CF_COSTFUNCTION_H

//#include "move4d/API/planningAPI.hpp"
#include "move4d/API/forward_declarations.hpp"
#include "move4d/Logging/Logger.h"
#include "move4d/HRI_costspace/HRICS_CostManager.h"
#include "move4d/HRI_costspace/HRICS_CostSettingType.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"
#include <Eigen/Geometry>

typedef struct obj p3d_obj;
namespace move4d
{
namespace HRICS {
namespace CF {

class PathLengthCost;
class TrajectoryDurationCost;

class CostFunctionCommon
{
    MOVE3D_STATIC_LOGGER;
public:
    CostFunctionCommon(Robot* rob, std::vector<Robot*> humans, std::vector<Joint*> costJoints,CostManager *costManger=0);
    CostFunctionCommon(bool guess_settings=false);
    ~CostFunctionCommon();

    void readExternalConf();
    void saveExternalConf();
protected:
    /// fill _bodyWeights from _bodyWeightsByName
    void extractBodyWeightsForComputation();

public:

    void createCosts();
    void deleteCosts();
    void addCostsToCostSpace();

    double getCost(std::string name, RobotState &q);
    double getCost(VirtualCostFunction* cf, RobotState &q);

    std::vector<std::string> listCosts();
    VirtualCostFunction* getCostFunction(const std::string &name);

    void prepareForSmoothing();
    void prepareForSmoothing(API::GeometricPath &path);
    void endSmoothing();
    void endSmoothing(API::GeometricPath &path);


    /// the active robot
    Robot * const & getRobot() const;
    /// the joints used to compute the cost
    std::vector<Joint*> const & getCostJoints() const;
    /// the humans to consider
    std::vector<Robot*> const & getHumans() const;
    /// the 2d positions of the humans
    std::vector<Eigen::Vector2d> const & getHumanPos() const;

    /**
     * @brief setHumanTargets
     * @param humans
     * human targets is a specific group of humans to consider differently for some costs.
     * E.g. some cost will not try to avoid them
     * @see HRICS::CF::VelocityApproach
     */
    void setHumanTargets(std::vector<Robot*> &humans);
    const std::vector<Robot *> &getHumanTargets(void) const;

    void setRobot(Robot *rob);
    void setCostJoints(const std::vector<Joint *> &cost_joints);
    void setHumans(const std::vector<Robot *> &humans);
    /// read the list of humans form global scene
    void updateHumans();
    /// read the active robot form global scene
    void updateRobot();
    void updateRobotBodies();

    /// update the vector of 2d position vector of the humans from the scene (current config)
    void updateHumanPos();

    void updateHumanGaze();

    void updateHumanBodies();
    void addBodyWeights(Robot *r);
    /// removes from this class containers the objects(bodies) that are pure graphical
    /// @see isBody()
    void cleanBodiesAll();
    void cleanBodyWeightsByName();
    void cleanBodiesHuman(unsigned int i);
    void cleanBodiesHumanAll();
    void cleanBodiesRobot();
    void cleanBodies(std::vector<p3d_obj*> & bodies);
    /// test if the given object is a physical object
    /// return false if the object is pure graphical
    static bool isBody(p3d_obj * body);
    static Robot* getRobotHavingType(Sem::AgentType type);

    std::vector<std::vector<p3d_obj *> > humanBodies() const;
    void setHumanBodies(const std::vector<std::vector<p3d_obj *> > &humanBodies);

    const std::map<std::string,double> &bodyWeightsByNameCurrentCost(Sem::AgentType type) const;
    const std::map<std::string,double> &bodyWeightsByName(Sem::AgentType type, CostSettingType cost) const;
    const std::map<Sem::AgentType, std::map<CostSettingType, std::map<std::string, double> > > &bodyWeightsByName() const;
    void addBodyWeightsByName(Sem::AgentType agent_type, const std::map<CostSettingType, std::map<std::string, double> > &bodyWeights);
    void setBodyWeightsByName(const std::map<Sem::AgentType, std::map<CostSettingType, std::map<std::string, double> > > &bodyWeightsByName);

    const std::vector<p3d_obj *> & robotBodies() const;
    void setRobotBodies(const std::vector<p3d_obj *> &robotBodies);


    const std::vector<std::pair<int, double> > &bodyWeightsCurrentCost(Sem::AgentType type) const;
    const std::vector<std::pair<int, double> > &bodyWeights(Sem::AgentType type, CostSettingType cost) const;
    const std::map<Sem::AgentType, std::map<CostSettingType,std::vector<std::pair<int, double> > > > &bodyWeights() const;
    void setBodyWeights(const std::map<Sem::AgentType, std::map<CostSettingType, std::vector<std::pair<int, double> > > > &bodyWeights);

    CostManager *costManager() const;
    void setCostManager(CostManager *costManager);

    /**
     * @brief compute joints to use for cost-computation and weights.
     * @param agent_type
     * @return
     *
     * Joints are only those that can move (active for planner, or having a predecessor in kinematic chain that is active).
     * i.e. joints that may have their absolute position changing.
     * @see isJointMoveable()
     */
    std::map<int,double> jointWeightsMovingCurrentCost(Sem::AgentType agent_type) const;
    const std::map<int,double> &jointWeightsCurrentCost(Sem::AgentType agent_type) const;
    const std::map<int, double> &jointWeights(Sem::AgentType agent_type, CostSettingType cost_type) const;
    std::map<Sem::AgentType, std::map<CostSettingType, std::map<int, double> > > jointWeights() const;
    void addJointWeights(Sem::AgentType agent_type, const std::map<CostSettingType, std::map<int,double> > &jointWeights);
    void setJointWeights(const std::map<Sem::AgentType, std::map<CostSettingType, std::map<int, double> > > &jointWeights);

    /// return true if the joint absolute position can change according to current joints settings (is_user_dof) of all the kinematic chain
    static bool isJointMoveable(Joint *j);
    static bool isJointActive(Joint *j);

    void populateJointWeights(Robot *r);

    PathLengthCost *pathLengthCost() const;

    void drawJointWeightsBalls() const;
    void drawBodyWeightColor(bool enable) const;

private:
    CostManager *_costManager;

    ///@name Cost function instances
    std::map<std::string,VirtualCostFunction*> _costFunctions;

    PathLengthCost *_pathLengthCost;
    TrajectoryDurationCost *_durationCost;


    /// the active robot
    Robot *_rob;
    /// the joints used to compute the cost
    std::vector<Joint*> _costJoints;

    /// the humans to consider
    std::vector<Robot *> _humans;
    std::vector<Robot *> _humanTargets;
    std::vector<Eigen::Vector2d> _humanPos;
    std::vector<Joint*> _humanGaze;
    std::vector<std::vector<p3d_obj *> > _humanBodies;
    std::vector<p3d_obj *> _robotBodies;
    /// for each agent type, store the weight of each body, by order in the p3d_object.o array
    std::map<Sem::AgentType,std::map<CostSettingType,std::map<std::string,double> > > _bodyWeightsByName;
    std::map<Sem::AgentType, std::map<CostSettingType, std::vector<std::pair<int,double> > > > _bodyWeights;

    std::map<Sem::AgentType, std::map<CostSettingType, std::map<int,double> > > _jointWeights;
};

double HRICS_CF_computeCost(CostFunctionCommon* cfc, std::string name, RobotState &q);


} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_COSTFUNCTION_H
