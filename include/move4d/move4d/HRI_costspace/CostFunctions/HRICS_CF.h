#ifndef HRICS_CF_H
#define HRICS_CF_H

#include "move4d/HRI_costspace/CostFunctions/HRICS_costfunction.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_BodiesDistance.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_distance2d.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_FaceHumans.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_TimeToCollision.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_KeepItLow.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_TorsoHeight.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_PathLengthCost.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_KeepManipulatedVertical.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VelocityOrientation.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VelocityNorm.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VelocityApproach.h"

#endif // HRICS_CF_H

