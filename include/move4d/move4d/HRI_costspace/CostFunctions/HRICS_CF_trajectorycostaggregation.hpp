#ifndef HRICS_CF_TRAJECTORYCOSTAGGREGATION_HPP
#define HRICS_CF_TRAJECTORYCOSTAGGREGATION_HPP

#include <boost/function.hpp>
#include "move4d/API/ConfigSpace/localpathbase.hpp"

namespace move4d
{
class RobotState;
namespace API{
class Path;
}

namespace HRICS {
namespace CF {

class TrajectoryCostAggregation
{
protected:
    TrajectoryCostAggregation(boost::function<double (RobotState &)> &costFunction);
    TrajectoryCostAggregation();
public:
    virtual void operator()(RobotState &state, double stepSize)=0;
    virtual double get()=0;
    virtual std::string name()=0;
protected:
    boost::function<double (RobotState &)> f;
};

class TrajectoryCostAggregationFactory
{
public:
    static TrajectoryCostAggregation *create(const std::string &type, boost::function<double (RobotState &)> costFunction);
    static TrajectoryCostAggregation *createPathCost(const std::string &type, boost::function<double (API::Path &)> costFunctionLp);
};

class TrajectoryCostFunction : public TrajectoryCostAggregation
{
public:
    TrajectoryCostFunction(boost::function<double (API::Path&)> path_cost):
        TrajectoryCostAggregation()
    {
        _path_cost=path_cost;
    }
    void operator ()(RobotState &, double){assert(false);throw std::runtime_error("can't call operator() on TrajectoryCostFunction");}
    void set(API::Path& path){_value=_path_cost(path);}
    double get(){return _value;}
    std::string name(){return "FUNCTION";}
protected:
    double _value;
    boost::function<double (API::Path&)> _path_cost;
};

class TrajectoryCostMax : public TrajectoryCostAggregation
{
public:
    TrajectoryCostMax(boost::function<double (RobotState &)> &costFunction);
    void operator ()(RobotState &state, double);
    double get();
    std::string name(){return "MAX";}
protected:
    double _max;
};

class TrajectoryCostIntegral : public TrajectoryCostAggregation
{
public:
    TrajectoryCostIntegral(boost::function<double (RobotState &)> &costFunction);
    void operator ()(RobotState &state, double stepSize);
    double get();
    std::string name(){return "INTEGRAL";}
protected:
    double _integral;
    double _previous_conf_cost;
};

class TrajectoryCostMechanicalWork :public TrajectoryCostAggregation
{
public:
    TrajectoryCostMechanicalWork(boost::function<double (RobotState &)> &costFunction);
    void operator ()(RobotState &state, double stepSize);
    double get();
    std::string name(){return "MECHANICALWORK";}
protected:
    double _mecha;
    double _previous_conf_cost;
};

class TrajectoryCostAverage : public TrajectoryCostAggregation
{
public:
    TrajectoryCostAverage(boost::function<double (RobotState &)> &costFunction);
    void operator ()(RobotState &state, double stepSize);
    double get();
    std::string name(){return "AVERAGE";}
protected:
    unsigned int _nb_samples;
    double _sum;
};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_TRAJECTORYCOSTAGGREGATION_HPP
