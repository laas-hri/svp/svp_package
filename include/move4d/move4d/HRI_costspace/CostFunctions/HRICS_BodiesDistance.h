#ifndef HRICS_CF_BODIESDISTANCE_H
#define HRICS_CF_BODIESDISTANCE_H

#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"
#include "move4d/API/forward_declarations.hpp"

namespace move4d
{
namespace HRICS {
namespace CF {

class BodiesDistance :public VirtualCostFunction
{
public:
    using VirtualCostFunction::getCost;

    BodiesDistance(CostFunctionCommon *commonCF, double min,double max, double low,double up,double maxToMeanRatio,FunctionShape shape,bool useBoundingBoxes=false);
    virtual ~BodiesDistance();

    static std::string getName(){return "BodiesDistance";}

    double getCost();
    ///returns the cost between 2 robots
    double getCost(Robot *r, Robot *human);
    /// returns the cost and the weight (weight is NOT applied to the cost)
    std::pair<double,double> getCost(Robot *r1, unsigned int o1, Robot *r2, unsigned int o2);

    bool useBoundingBoxes() const;
    void setUseBoundingBoxes(bool useBoundingBoxes);

private:
    bool _useBoundingBoxes; ///< is set to true, use only distance between bounding boxes instead of real bodies

};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_BODIESDISTANCE_H
