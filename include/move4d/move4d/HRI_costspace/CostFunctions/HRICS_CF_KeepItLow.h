#ifndef HRICS_CF_KEEPITLOW_H
#define HRICS_CF_KEEPITLOW_H

#include "move4d/Logging/Logger.h"

#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"
#include <jsoncpp/json/json.h>

namespace move4d
{
namespace HRICS {
namespace CF {

class KeepItLow : public VirtualCostFunction
{
    MOVE3D_STATIC_LOGGER;
public:
    using VirtualCostFunction::getCost;

    KeepItLow(CostFunctionCommon *commonCF, double min,double max,double low,double up, double maxToMeanRatio, FunctionShape shape);
    virtual ~KeepItLow();

    double getCost();

    static std::string getName(){return "KeepItLow";}

    Json::Value getJson();
    void readJson(Json::Value const &jv);

private:
};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_KEEPITLOW_H
