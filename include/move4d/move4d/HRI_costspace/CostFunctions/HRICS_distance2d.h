#ifndef HRICS_DISTANCE2D_H
#define HRICS_DISTANCE2D_H

#include "move4d/API/forward_declarations.hpp"
#include "move4d/Logging/Logger.h"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"

namespace move4d
{
namespace HRICS {
namespace CF{

class Distance2D :public VirtualCostFunction
{
    MOVE3D_STATIC_LOGGER;
public:
    using VirtualCostFunction::getCost;

    Distance2D(CostFunctionCommon *commonCF,double min, double max, double low,double up,double maxToMeanRatio, FunctionShape shape);
    virtual ~Distance2D();

    static std::string getName(){return "Distance2D";}

    double getCost();

};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_DISTANCE2D_H
