#ifndef HRICS_CF_FACEHUMANS_H
#define HRICS_CF_FACEHUMANS_H

#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_VirtualCostFunction.h"

namespace move4d
{
namespace HRICS {
namespace CF {

class FaceHumans :public VirtualCostFunction
{
public:
    using VirtualCostFunction::getCost;

    FaceHumans(CostFunctionCommon *commonCF,double min, double max, double low,double up,double maxToMeanRatio, FunctionShape shape);
    virtual ~FaceHumans();

    static std::string getName(){return "FaceHumans";}

    double getCost();

private:
};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_FACEHUMANS_H
