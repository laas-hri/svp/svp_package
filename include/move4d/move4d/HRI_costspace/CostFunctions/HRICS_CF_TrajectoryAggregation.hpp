#ifndef HRICS_CF_TRAJECTORYAGGREGATION_HPP
#define HRICS_CF_TRAJECTORYAGGREGATION_HPP

#include <boost/function.hpp>
#include "move4d/API/ConfigSpace/localpathbase.hpp"
#include "move4d/HRI_costspace/CostFunctions/HRICS_CF_trajectorycostaggregation.hpp"
#include "move4d/API/Trajectory/Path.hpp"
#include "move4d/API/planningAPI.hpp"

namespace move4d
{
namespace HRICS{
namespace CF{

class TrajectoryAggregation
{
public:
    TrajectoryAggregation(double step): _step(step)
    {

    }

    template<class iterator_type>
    double computeLp(iterator_type begin,iterator_type end,boost::function<double ()> func, API::Path &path,boost::function<void()> after_each_step){
        double pathLength = path.length();
        double stepSize = std::min(_step, pathLength);
        double param = stepSize;
        uint cost_test_nb(0);
        bool do_sample=false;
        for(auto it=begin;it!=end;++it){
            if ((*it)->name() != "FUNCTION"){
                do_sample=true;
                break;
            }
        }
        if(do_sample){
            while (param <= pathLength)
            {
                statePtr_t x=path.getState(param);
                for(iterator_type it=begin;it!=end;++it){
                    if((*it)->name() != "FUNCTION"){
                        (**it)(*x,stepSize);
                    }
                }
                if (param == pathLength)
                    param = pathLength + 1.;
                else if (pathLength - param > stepSize + 1e-3)
                    param += stepSize;
                else {
                    stepSize = pathLength - param;
                    param = pathLength;
                }
                if(!after_each_step.empty())
                    after_each_step();
                ++cost_test_nb;
            }
        }
        //TODO:move4d
        //path.setNbCostTest(cost_test_nb);
        double cost = (func.empty() ? 0. : func());
        //TODO:move4d
        //path.setCost(cost);
        return cost;
    }

    template<class iterator_aggreg>
    double compute(iterator_aggreg begin,iterator_aggreg end,boost::function<double ()> func, API::Path &path,boost::function<void()> after_each_step){

        //for(std::vector<LocalPathBase*>::iterator it = path.begin();it!=path.end();++it){
            computeLp<iterator_aggreg>(begin,end,func,path,after_each_step);
        //}
        for(iterator_aggreg it = begin; it!=end; ++it){
            if((*it)->name() == "FUNCTION"){
                dynamic_cast<TrajectoryCostFunction*>((*it))->set(path);
            }
        }
        return (func.empty() ? 0. : func());
    }
private:
    double _step;
};
} // namespace CF
} // namespace HRICS
} //namespace move4d
#endif // HRICS_CF_TRAJECTORYAGGREGATION_HPP
