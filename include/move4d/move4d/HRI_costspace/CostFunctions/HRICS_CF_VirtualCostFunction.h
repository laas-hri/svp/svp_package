#ifndef HRICS_CF_VIRTUALCOSTFUNCTION_H
#define HRICS_CF_VIRTUALCOSTFUNCTION_H

#include <string>
#include "move4d/Logging/Logger.h"


namespace move4d
{
namespace HRICS {
namespace CF {

class CostFunctionCommon;

class VirtualCostFunction
{
public:
    typedef enum {LINEAR,EXP_INV,EXP,SIGMOID,CENTERED_GAUSS_INV} FunctionShape;

    VirtualCostFunction(CostFunctionCommon *common, double min,double max, double low,double up, double maxToMeanRatio,FunctionShape shape);
    virtual ~VirtualCostFunction(){}
    virtual double getCost()=0;
    static std::string getName();
    virtual bool isDynamicCost(){return false;}
    CostFunctionCommon *commonCF() const;


    double getCost(double x);
    double getCostExpInv(double x);
    double getCostLin(double x);
    double getCostExp(double x);
    double getCostSigmoid(double x);
    /**
     * @brief min at x=(up+low)/2, max at +/-inf, ~0.99*max at x=up or x=low
     * @param x
     * @return
     */
    double getCostCenteredGaussInv(double x);

    bool checkConstraintOk(double x);

    /**
     * @brief getCostMaxMean
     * @param max
     * @param mean
     * @return maxToMeanRatio() * max + (1-maxToMeanRatio())*mean
     */
    double getCostMaxMean(double max,double mean);

    /// upper bound is the value of the studied variable which as a cost of maxCost
    double upperBound() const;
    void setUpperBound(double upperBound);
    /// upper bound is the value of the studied variable which as a cost of minCost
    double lowerBound() const;
    void setLowerBound(double lowerBound);
    double minCost() const;
    void setMinCost(double minCost);
    double maxCost() const;
    void setMaxCost(double maxCost);
    /// when the cost is a compostion of several elements (e.g. BodiesDistance, KeepItLow,...), we can either use the max cost of the elements (maxToMeanRatio() = 1),
    /// or their mean (maxToMeanRatio = 0) or any weighted sum
    /// @see getCostMaxMean(double,double)
    double maxToMeanRatio() const;
    void setMaxToMeanRatio(double maxToMeanRatio);
    FunctionShape functionShape() const;
    void setFunctionShape(const FunctionShape &functionShape);

    bool isConstraint() const;
    void setConstraint(bool constraint);

protected:
    void setCommonCF(CostFunctionCommon *commonCF);
    CostFunctionCommon *_common;

    double _upperBound;
    double _lowerBound;
    double _minCost; /// the cost to apply where the robot is at more than _ignore_threshold from humans
    double _maxCost; /// the cost when the robot is on the human
    double _maxToMeanRatio; ///< the ratio "a" when using getCost, computes the cost as c = a*cmax + (1-a)*cmean. needs to be in [0,1]
    FunctionShape _functionShape; ///< the shape of the function
    bool _constraint; ///< if true, getCost returns +inf if x>_upperBound
};

} // namespace CF
} // namespace HRICS

} //namespace move4d
#endif // HRICS_CF_VIRTUALCOSTFUNCTION_H
