#ifndef HRICS_RRTEXPANSION_H
#define HRICS_RRTEXPANSION_H

#include "move4d/API/planningAPI.hpp"
#include "move4d/planner/Diffusion/Variants/Transition-RRT.hpp"
#include "move4d/HRI_costspace/Grid/HRICS_Grid.hpp"

/**
  @ingroup HRICS
  @brief Special RRT Expansion method for the HRICS
  */
class HRICS_rrtExpansion : public TransitionExpansion
{
public:
    HRICS_rrtExpansion(Graph * ptrGraph, Node * goal);

    /**
      * Initializes some variables for the expansion
      * method
      */
    void init();

     /**
      * Sets the grid
      */
    void setGrid(API::ThreeDGrid* grid) { _3DGrid = dynamic_cast<HRICS::Grid*>(grid); }

     /**
      * Sets the cell path
      */
    void setCellPath(std::vector<API::ThreeDCell*> cellPath);

    /**
      * Direction used in RRT one step
      */
    std::shared_ptr<RobotState> getExpansionDirection(
            Node* expandComp, Node* goalComp, bool samplePassive, Node*& directionNode);

    /**
      * RobotState from the next cell along the 3dPath
      */
    std::shared_ptr<RobotState> getConfigurationInNextCell(Node* CompcoNode, Node* goalComp);

    /**
      * Adds a node to a conected component
      */
    Node* addNode(Node* currentNode, LocalPathBase& path, double pathDelta,
                  Node* directionNode, unsigned& nbCreatedNodes);

    /**
      * Checks it the cell is after the given cell on the
      * 3D path
      */
    bool on3DPathAndAfter(API::ThreeDCell* cell);

private:
    HRICS::Grid*             _3DGrid;
    std::vector<API::ThreeDCell*>  _3DCellPath;

    API::ThreeDCell*               _LastForward;
    API::ThreeDCell*               _LastBackward;

    int         mIndexObjectDof;

    bool                     _forward;
    bool                     _biasing;

    double*                  _Box;

};

#endif // HRICS_RRTEXPANSION_H
