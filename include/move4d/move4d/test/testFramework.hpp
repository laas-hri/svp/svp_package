#ifndef TESTFRAMEWORK_HPP
#define TESTFRAMEWORK_HPP
#define MOVE4D_TEST

#ifdef MOVE4D_TEST
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#endif

#endif // TESTFRAMEWORK_HPP
