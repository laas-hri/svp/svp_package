#ifndef ENVIRONNEMENT_HPP
#define ENVIRONNEMENT_HPP

#include <boost/function.hpp>
#include "move4d/API/Device/robot.hpp"

#ifndef _ENVIRONMENT_H
struct env;
#endif

extern std::string global_ActiveRobotName;

struct rob;
typedef rob p3d_rob;
namespace move4d
{

//class PropertyManager;
//class FactManager;
/**
 @ingroup CPP_API
 
 @brief Class that represents a Scene,
 Described by a p3d file
 
 @author Florian Pilardeau,B90,6349 <fpilarde@jolimont>
 */
class Scene
{	
public:
    static Scene *instance();
	/**
	 * Constructeur de la classe
	 * @param name le nom de l'Scene
	 */
    Scene(env *environnement, boost::function<Robot *(rob *)> robotCreator);

	/**
	 * Destructeur de la classe
	 */
	~Scene();
	
    /**
     * @brief dmax get the maximal discretization step
     */
    double dmax();

	/**
	 * obtient le nom de l'Scene
	 * @return le nom de l'Scene
	 */
    std::string getName();

    void saveScenario(const std::string &filename);
    void loadScenario(const std::string &filename);

    /**
     * @return the facts manager
     */
    //FactManager* getFactManager() {return m_fact_manager;}

    /**
     * @return the properties manager
     */
    //PropertyManager* getPropertyManager() {return m_property_manager;}
	
	/**
	 * modifie le Robot actif
	 * @param name le nom du nouveau Robot actif
	 */
	void setActiveRobot(std::string name);
	
	/**
	 * obtient le Robot actif
	 * @return le Robot actif; NULL si le Robot ne peux pas être créé
	 */
	Robot* getActiveRobot();
	
	/**
	 * Returns the robot by id
	 */
	Robot* getRobot(unsigned int i) { return m_Robot[i]; }
	
	/**
	 * Returns the robot ID
	 */
	unsigned int getRobotId(std::string str);
	unsigned int getRobotId(Robot *robot);

	/**
	 * Get robot by name
	 */
	Robot* getRobotByName(std::string name);
	
	/**
	 * Get robot by name containing
	 */
	Robot* getRobotByNameContaining(std::string name);

    /**
     * Get all robot with name containing
     */
    std::vector<Robot*> getAllRobotWithNameContaining(std::string str);

    Robot* getRobot(p3d_rob *rob);
	
	/**
	 * insert un nouveau Robot au vecteur des Robot
	 * @param R le nouveau Robot
	 */
	void insertRobot(Robot* R);
	
	/**
	 * Returns the number of Robots in the
	 * Scene
	 */
	unsigned int getNumberOfRobots() { return m_Robot.size(); }
	
	/**
	 * Returns the scene resolution step DMax
	 */
	double getDMax();
    void setDMax(double dmax);
  
  /**
   * Returns boundries of scene
   */
  std::vector<double> getBounds();


  /**
   * Draw surfaces of all the objects
   */
  void drawSurfaces();
  double getTimeStep() const;
  void setTimeStep(double timeStep);

private:
  std::vector<Robot*> m_Robot;/*!< All Robots in the scene */
	std::string m_Name;/*!< The environnement name */
    double m_timeStep=0.01; ///< for sampling on time
	
	env* m_Scene; 

    //FactManager * m_fact_manager;
    //PropertyManager * m_property_manager;
    bool _agentsOK; ///< set to true once hri agents have been created
	
};

} //namespace move4d
#endif
