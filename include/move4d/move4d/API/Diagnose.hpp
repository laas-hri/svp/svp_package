#ifndef DIAGNOSE_HPP
#define DIAGNOSE_HPP

#include "move4d/API/forward_declarations.hpp"
#include "move4d/Logging/Logger.h"

namespace move4d {
namespace API {

class Diagnose
{
    MOVE3D_STATIC_LOGGER;
public:
    Diagnose();

    int diagnose();

    int checkDmax();
    int checkMultiLocalPath();
    int checkDofs();

    int checkRobotCollisionsSettings();

    int checkPathFactories();
private:

    Project *proj;
    Scene *sce;
    Robot *rob;
};

} // namespace API
} // namespace move4d

#endif // DIAGNOSE_HPP
