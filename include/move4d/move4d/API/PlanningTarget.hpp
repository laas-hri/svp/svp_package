#ifndef MOVE4D_PLANNINGTARGET_HPP
#define MOVE4D_PLANNINGTARGET_HPP

#include "move4d/API/ConfigSpace/RobotState.hpp"

namespace move4d {
namespace API {

class Target
{
public:
    virtual ~Target()=default;
    virtual Target* clone() const =0;

    virtual double distance(const RobotState &state) const =0;
    virtual bool isIn(const RobotState &state) const =0;
};

class TargetRegion : public Target
{
public:
    TargetRegion(const RobotState &state, double radius);

    virtual Target *clone() const;

    virtual double distance(const RobotState &state) const;
    virtual bool isIn(const RobotState &state) const;

protected:
    RobotState mCenter;
    double mRadius;
};

class TargetDiscrete : public Target
{
public:
    TargetDiscrete(std::vector<RobotState> *states, double radius);
    virtual Target *clone() const override;
    virtual double distance(const RobotState &state) const override;
    virtual bool isIn(const RobotState &state) const override;
protected:
    std::vector<RobotState> mCenters;
    double mRadius;
};
} // ns API
} // namespace move4d

#endif // MOVE4D_PLANNINGTARGET_HPP
