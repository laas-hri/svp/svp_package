#ifndef API_TRAJECTORYREADER_HPP
#define API_TRAJECTORYREADER_HPP

#include <sstream>
#include <ostream>
#include <memory>

#include "move4d/API/forward_declarations.hpp"
namespace Json{class Value;}
namespace move4d
{
namespace API {

class TrajectoryReader
{
public:
    TrajectoryReader();
    virtual ~TrajectoryReader();

    Path *fromPath(const std::string &file_path);
    Path *fromString(const std::string &str);
    Path *fromStream(std::istream &stream);

    Path *parse(Json::Value &root);

    bool writeToStream(std::ostream &stream, Path *traj);
    std::string writeToString(Path *traj);
    bool writeToPath(const std::string &path, Path *traj);
    Json::Value stateToJson(RobotState &q);

protected:
    bool _writePathWp(Json::Value &points, API::PathWaypointInterface *path);
    bool _writePathLp(Json::Value &points, API::PathLocalPathInterface *path);
};

} // namespace API

} //namespace move4d
#endif // API_TRAJECTORYREADER_HPP
