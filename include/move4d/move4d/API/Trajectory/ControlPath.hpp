#ifndef API_CONTROLPATH_HPP
#define API_CONTROLPATH_HPP

#include "move4d/API/Trajectory/trajectory.hpp"

namespace move4d
{
namespace API {

class ControlPath : public API::GeometricPath
{
public:
    ControlPath(Robot *r);
    statePtr_t getState(double p) const;
    double length() const;
    double cost();
    bool check();
};

} // namespace API

} //namespace move4d
#endif // API_CONTROLPATH_HPP
