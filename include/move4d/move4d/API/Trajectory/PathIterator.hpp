#ifndef MOVE4D_API_PATHITERATOR_HPP
#define MOVE4D_API_PATHITERATOR_HPP

#include <iterator>
#include "move4d/API/ConfigSpace/RobotState.hpp"

namespace move4d {
namespace API {
class Path;

class PathIterator : public std::iterator<std::random_access_iterator_tag,move4d::RobotState>
{
public:
    typedef RobotState Type;
    using difference_type = typename std::iterator<std::random_access_iterator_tag, Type>::difference_type;

    PathIterator();
    PathIterator(const Path *path, double param, double step);
    PathIterator(const PathIterator &it);

    PathIterator& operator+=(difference_type rhs);
    PathIterator& operator-=(difference_type rhs);

    Type operator*() const;
    Type* operator->() const;
    Type operator[](difference_type rhs) const;

    PathIterator& operator++();
    PathIterator& operator--();
    PathIterator operator++(int);
    PathIterator operator--(int);
    difference_type operator-(const PathIterator& rhs) const;
    PathIterator operator+(difference_type rhs) const;
    PathIterator operator-(difference_type rhs) const;
    friend PathIterator operator+(difference_type lhs, const PathIterator& rhs);
    friend PathIterator operator-(difference_type lhs, const PathIterator& rhs);

    bool operator==(const PathIterator& rhs) const;
    bool operator!=(const PathIterator& rhs) const;
    bool operator>(const PathIterator& rhs) const;
    bool operator<(const PathIterator& rhs) const;
    bool operator>=(const PathIterator& rhs) const;
    bool operator<=(const PathIterator& rhs) const;

private:
    const Path *mPath;
    double mParam;
    double mStep;
};

} // namespace API
} // namespace move4d

#endif // MOVE4D_API_PATHITERATOR_HPP
