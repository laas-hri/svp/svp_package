#ifndef API_TRAJECTORYDATAPLOT_HPP
#define API_TRAJECTORYDATAPLOT_HPP

#include <vector>
#include <map>
#include <string>

class LocalPathBase;

namespace move4d
{
namespace API {
class GeometricPath;

class TrajectoryDataPlot
{
public:
    TrajectoryDataPlot(GeometricPath *traj);
    virtual ~TrajectoryDataPlot();

    std::map<std::string,std::vector<double> > &getData();

    int compute();

    std::string printToScilab();

private:
    GeometricPath *_traj;
    std::map<std::string,std::vector<double> > _data;

};

} // namespace API

} //namespace move4d
#endif // API_TRAJECTORYDATAPLOT_HPP
