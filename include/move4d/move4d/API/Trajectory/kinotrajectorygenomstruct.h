#ifndef KINOGENSTRUCT
#define KINOGENSTRUCT

#define NBDOFMAX 100    //Maximum number of degree of freedom
#define NBINTPOINTS 300 //Maximum number of points in a trajectory

namespace move4d
{
typedef struct KinoGenomPoint{
 	double configuration[NBDOFMAX];
 }KinoGenomPoint;

typedef struct KinoGenomTrajectory{
	int nb_points;
	int nb_dofs;
	double timeStep;//Precision of computation of the KinoTrajectory
	double maxAcceleration[NBDOFMAX]; // Assuming that the characteristics of the degrees of freedom are symetric
	double maxVelocity[NBDOFMAX];
	KinoGenomPoint waypoints[NBINTPOINTS];
}KinoGenomTrajectory;


} //namespace move4d
 #endif
