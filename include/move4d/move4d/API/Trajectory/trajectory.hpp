/*
 * trajectory.hpp
 *
 *  Created on: Jun 17, 2009
 *      Author: jmainpri
 */

#ifndef TRAJECTORY_HPP_
#define TRAJECTORY_HPP_

#include "move4d/API/Trajectory/Path.hpp"
#include <memory>
#include "move4d/Logging/Logger.h"

#include "move4d/API/forward_declarations.hpp"

#ifndef _TRAJ_H
struct traj;
#endif
typedef struct traj p3d_traj;
namespace move4d
{

/**
 * @ingroup CPP_API
 * @defgroup Trajectory
 */

struct TrajectoryStatistics 
{
    double length;
    double max;
    double sum;
    double average;
    double integral;
    double mecha_work;
    bool is_valid;
};

/**
 * @ingroup Trajectory
 * @brief Trajectory witch is a vector of local paths
 */
namespace API 
{
class GeometricPath : public Path, public PathLocalPathInterface, public PathWaypointInterface
{
    MOVE3D_STATIC_LOGGER;
public:
    //---------------------------------------------------------
    // Constructors
    GeometricPath();
    GeometricPath(Robot* R);
    GeometricPath(Robot* R,traj* t) __attribute_deprecated__;
    GeometricPath(Robot* R,traj* t, std::vector<unsigned int> ignoreJoints) __attribute_deprecated__;
    GeometricPath(const std::vector<statePtr_t>& C);
    GeometricPath(const GeometricPath& T);
    GeometricPath(std::vector<LocalPathBase*> &lp);
    GeometricPath(Robot *r, const std::vector<double> &descrVector);
    ~GeometricPath();

    virtual GeometricPath *clone() const;
    GeometricPath& operator= (const GeometricPath& t);

    bool operator == ( const GeometricPath& t ) const;
    bool operator != ( const GeometricPath& t ) const;

    //---------------------------------------------------------
    // Operations
    void copyPaths(std::vector<LocalPathBase *> &vect );

    std::vector< std::shared_ptr<RobotState> > getTowConfigurationAtParam(double param1, double param2,
                                                                          uint& lp1, uint& lp2 );

    std::pair<bool,std::vector<LocalPathBase*> > extractSubPortion(double param1,double param2,unsigned int& first,unsigned int& last, bool check_for_coll = true);
    GeometricPath extractSubTrajectoryOfLocalPaths(unsigned int id_start, unsigned int id_end);
    GeometricPath extractSubTrajectory(double param1,double param2, bool check_for_coll = true);
    virtual GeometricPath *subPath(double from_t,double to_t) const override;

    bool concat(const GeometricPath& traj);

    bool replacePortionOfLocalPaths(unsigned int id1,unsigned int id2,std::vector<LocalPathBase*> paths, bool freeMemory = true );
    bool replacePortion(double param1,double param2,std::vector<LocalPathBase*> paths , bool freeMemory = true );

    bool replaceBegin(double param, const std::vector<LocalPathBase*>& paths );
    bool replaceEnd(double param, const std::vector<LocalPathBase*>& paths );

    bool cutTrajInSmallLP(unsigned int nLP);
    bool cutTrajInSmallLPSimple(unsigned int nLP);
    uint cutPortionInSmallLP(std::vector<LocalPathBase*>& portion, uint nLP);

    virtual void push_back(const RobotState &state) override;
    void push_back(const RobotState &state, double time_from_start);
    void push_back(statePtr_t q,double time_from_start=0.);
    void push_back(statePtr_t q, std::vector<unsigned int> ignoreList);
    bool push_back(std::shared_ptr<LocalPathBase> path);
    virtual void push_back(const LocalPathBase &lp) override;

    //---------------------------------------------------------
    // Cost
    double collisionCost();

    std::vector< std::pair<double,double > > getCostProfile();
    double computeSubPortionCost(std::vector<LocalPathBase*>& portion);
    double computeSubPortionCost(std::vector<LocalPathBase*>::iterator first, std::vector<LocalPathBase*>::iterator last);
    double computeSubPortionMaxCost(std::vector<LocalPathBase*>& portion);
    double reComputeSubPortionCost(std::vector<LocalPathBase*>& portion, int& nb_cost_tests);

    virtual double cost() override;
    double computeCostComplete();
    double costComplete();
    double costNoRecompute();
    double costRecomputed();
    double costStatistics( TrajectoryStatistics& stat );
    double costDeltaAlongTraj();
    double costNPoints(const int n_points);
    double costSum();

    std::vector<double> getCostAlongTrajectory(int nbSample);
    void resetCostComputed();

    //---------------------------------------------------------
    // Basic
    bool isEmpty();

    void clear();

    statePtr_t operator [] (const int &i ) const;
    statePtr_t configAtParam(double param, unsigned int* id_localpath=NULL) const;
    virtual statePtr_t getState(double p) const override {return configAtParam(p);}
    double paramAtConfig(statePtr_t config, double step = -1) const;
    LocalPathBase* getLocalPathPtrOf(statePtr_t conf) const;
    double distanceToTraj(statePtr_t config, double step = -1);

    virtual const RobotState &waypoint(unsigned int i) const override;
    virtual std::vector<statePtr_t> waypoints() const override;
    virtual unsigned int waypointNumber() const override;

    virtual LocalPathBase &localPath(unsigned int id) const override;
    int						getNbOfPaths() const;
    int						getNbOfViaPoints() const;
    virtual unsigned int localPathNumber() const override;

    using Path::begin;
    using Path::end;
    virtual PathIterator begin() const;
    virtual PathIterator end() const;

    virtual bool check() override {return isValid();}
    virtual void resetCheckStatus() override {this->resetIsValid();}
    bool isValid();
    void resetIsValid();

    void 	updateRange();
    double computeSubPortionRange(const std::vector<LocalPathBase*>& portion) const;

    bool        replaceP3dTraj();
    p3d_traj* 	replaceP3dTraj(p3d_traj* trajPt);
    p3d_traj* 	replaceHumanP3dTraj(Robot*rob, p3d_traj* trajPt);
    double      computeDuration() const;

    void print();

    int meanCollTest();

    /**
         * Draw kino trajectory
         * @param rob The Robot
         */
    void drawKinoPath(Robot* rob);

    //---------------------------------------------------------
    // B-Spline
    bool makeBSplineTrajectory();

    std::vector<double> getDescriptionVector() const;
    std::pair<std::vector<double>, std::vector<double> > getDescriptionVectorLimits(bool clampStart, bool clampGoal) const;
    virtual GeometricPath *fromDescriptionVector(const std::vector<double> &descrVector);


    //---------------------------------------------------------
    // Getters & Setters

    void setColor(int col) {mColor=col;}

    unsigned int getHighestCostId() const
    {
        return HighestCostId;
    }

    Robot* getRobot() const
    {
        return mRobot;
    }

    int size() const
    {
        return m_Courbe.size();
    }

    statePtr_t getBegin() const
    {
        return m_Source;
    }

    statePtr_t getEnd() const
    {
        return m_Target;
    }

    virtual std::vector<LocalPathBase*> localPathes() const override
    {
        return m_Courbe;
    }
    std::vector<LocalPathBase*> &localPathes()
    {
        return m_Courbe;
    }

    virtual double length() const override{
        return getRangeMax();
    }
    double getRangeMax() const {
        return computeSubPortionRange(m_Courbe);
    }

    //---------------------------------------------------------
    // Members
protected:
    unsigned int HighestCostId;
    bool isHighestCostIdSet;

private:
    std::vector<LocalPathBase*> m_Courbe;

    /* name of trajectory */
    std::string name;

    /* Name of the file */
    std::string file;

    int mColor;

    /* Start and Goal (should never change) */
    statePtr_t m_Source;
    statePtr_t m_Target;

};
}

extern std::vector<API::GeometricPath> trajToDraw;
void draw_traj_debug();

} //namespace move4d
#endif /* TRAJECTORY_HPP_ */
