#ifndef MOVE4D_API_PATHFACTORY_HPP
#define MOVE4D_API_PATHFACTORY_HPP

#include <type_traits>
#include <boost/static_assert.hpp>
#include <map>
#include <vector>
#include <iostream>

#include "move4d/API/Trajectory/Path.hpp"

namespace move4d {
namespace API {

class PathCreatorInterface
{
public:
    virtual Path *create(Robot *r)=0;
    virtual Path *create(Robot *r,const std::vector<double> &descrVector)=0;
    virtual Path *create(const std::vector<statePtr_t> &waypoints)=0;
};

template<class T>
class PathBaseCreator : public PathCreatorInterface
{
    BOOST_STATIC_ASSERT(std::is_base_of<Path,T>::value);
public:
    typedef T PathClass;
    virtual Path *create(Robot *r){return new PathClass(r);}
    virtual Path *create(Robot *r,const std::vector<double> &descrVector){
        return new PathClass(r,descrVector);
    }
    virtual Path *create(const std::vector<statePtr_t> &waypoints){
        return new PathClass(waypoints);
    }
};

class PathFactory
{
public:
    static PathFactory *instance();

    virtual ~PathFactory()=default;

    void registerCreator(const std::string &name, PathCreatorInterface *creator);

    Path *create(Robot *r);
    Path *create(Robot *r,const std::vector<double> &descrVector);
    Path *create(const std::vector<statePtr_t> &waypoints);
    Path *create(const std::string &type,Robot *r);
    Path *create(const std::string &type,Robot *r,const std::vector<double> &descrVector);
    Path *create(const std::string &type,const std::vector<statePtr_t> &waypoints);

    bool setType(const std::string &type);

    std::vector<std::string> getTypes() const;

    const std::string &getCurrentType() const;


protected:
    std::pair<std::string,PathCreatorInterface*> _default;
    std::map<std::string,PathCreatorInterface*> _creators;
};

//credits to github.com/ros/class_loader for the macro
#define PATH_REGISTER_CREATOR_INTERNAL(Derived, UniqueID) \
namespace \
{\
  struct ProxyExec##UniqueID \
  {\
    typedef  Derived _derived; \
    ProxyExec##UniqueID() \
    { \
      move4d::API::PathFactory::instance()->registerCreator(#Derived, new move4d::API::PathBaseCreator<_derived>()); \
    }\
  };\
  static ProxyExec##UniqueID g_register_plugin_##UniqueID;\
}

#define PATH_REGISTER_CREATOR_INTERNAL_HOP1(Derived,UniqueID) \
    PATH_REGISTER_CREATOR_INTERNAL(Derived,UniqueID)
#define PATH_REGISTER_FACTORY(path_class)\
    PATH_REGISTER_CREATOR_INTERNAL_HOP1(path_class,__COUNTER__)
} // namespace API
} // namespace move4d

#endif // MOVE4D_API_PATHFACTORY_HPP
