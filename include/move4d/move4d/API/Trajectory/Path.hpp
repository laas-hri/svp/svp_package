#ifndef MOVE_API_PATH_HPP
#define MOVE_API_PATH_HPP

#include "move4d/API/ConfigSpace/RobotState.hpp"
#include "move4d/API/Trajectory/PathIterator.hpp"

#include <memory>

namespace move4d
{
namespace API {
class LocalPathBase;

typedef std::shared_ptr<Path> PathPtr;

/**
 * @brief base class for motion representation
 */
class Path
{
public:
    Path(Robot *r):
        mRobot(r)
    { }

    Path(Robot *r,const std::vector<double> &descrVector, uint nb_states, uint nb_derivatives=1) = delete;
    Path(const std::vector<statePtr_t> &waypoints)=delete;

    Path(const Path&)=delete;
    virtual ~Path() =default;

    virtual Path *clone() const =0;
    virtual Path *subPath(double from_t, double to_t) const =0;

    Path & operator=(const Path &)=delete;

    virtual statePtr_t getState(double p) const=0;
    virtual PathIterator begin() const=0;
    virtual PathIterator end() const=0;
    virtual PathIterator begin(double step) const
    {
        return PathIterator(this,0,step);
    }
    virtual PathIterator end(double step) const
    {
        return PathIterator(this,length()+step,step);
    }

    /**
     * @brief set this to correspond to the given description vector
     * @param descrVector
     * @return this
     */
    virtual Path *fromDescriptionVector(const std::vector<double> &descrVector) =0;


    virtual double length() const =0;

    virtual double cost() =0;

    virtual bool check() =0;
    virtual void resetCheckStatus() = 0;

    template <class T>
    const T *as() const
    {
        return dynamic_cast<const T *>(this);
    }

    template <class T>
    T *as()
    {
        return dynamic_cast<T *>(this);
    }

    Robot *getRobot() const {return mRobot;}

    //for optimization:
    virtual std::vector<double> getDescriptionVector() const = 0;
    virtual std::pair<std::vector<double>,std::vector<double> > getDescriptionVectorLimits(bool clampStart, bool clampGoal) const = 0;


protected:
    Robot *mRobot;

};

class PathWaypointInterface
{
public:
    PathWaypointInterface() = default;
    PathWaypointInterface(const PathWaypointInterface&)=delete;
    virtual ~PathWaypointInterface()=default;
    PathWaypointInterface & operator=(const PathWaypointInterface&)=delete;

    virtual const RobotState &waypoint(unsigned int i) const =0;
    virtual std::vector<statePtr_t> waypoints() const =0;
    virtual unsigned int waypointNumber() const =0;
    virtual void push_back(const RobotState &s) = 0;
};

class PathLocalPathInterface
{
public:
    PathLocalPathInterface() = default;
    PathLocalPathInterface(const PathLocalPathInterface&)=delete;
    virtual ~PathLocalPathInterface()=default;
    PathLocalPathInterface & operator=(const PathLocalPathInterface&)=delete;

    virtual LocalPathBase &localPath(unsigned int i) const =0;
    virtual std::vector<LocalPathBase*> localPathes() const =0;
    virtual unsigned int localPathNumber() const =0;
    virtual void push_back(const LocalPathBase &lp) =0;
};



} // namespace API

} //namespace move4d
#endif // MOVE_API_PATH_HPP
