/*
 * Adaptation of the code of Tobias Kunz for use in Move3d  
 *
 *
 * Copyright (c) 2012, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * Author: Tobias Kunz <tobias@gatech.edu>
 * Date: 05/2012
 *
 * Humanoid Robotics Lab      Georgia Institute of Technology
 * Director: Mike Stilman     http://www.golems.org
 *
 * Algorithm details and publications:
 * http://www.golems.org/node/1570
 *
 * This file is provided under the following "BSD-style" License:
 *   Redistribution and use in source and binary forms, with or
 *   without modification, are permitted provided that the following
 *   conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 *   CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 *   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 *   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 *   AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *   LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *   ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *   POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once
#include <Eigen/Core>
#include "move4d/API/Trajectory/kinopath.hpp"

#include "move4d/API/Device/robot.hpp"
#include "move4d/API/Trajectory/trajectory.hpp"
#include "move4d/API/ConfigSpace/RobotState.hpp"
#include <vector>

#include "move4d/API/Trajectory/kinotrajectorygenomstruct.h"


namespace move4d
{
struct KinoROSPoint{
    double time_from_start;
    std::vector<double> positions;
    std::vector<double> velocities;
    //vector<double> accelerations;
};

namespace API{
    class KinoTrajectory
    {
    public:
        // Generates a time-optimal trajectory
        KinoTrajectory(const KinoPath &path, const Eigen::VectorXd &maxVelocity, const Eigen::VectorXd &maxAcceleration, double timeStep = 0.001, double initialVel = 0);
        
        ~KinoTrajectory(void);

        // Call this method after constructing the object to make sure the trajectory generation succeeded without errors.
        // If this method returns false, all other methods have undefined behavior.
        bool isValid() const;

        // Returns the optimal duration of the trajectory
        double getDuration() const;

        // Return the position/configuration or velocity vector of the robot for a given point in time within the trajectory.
        Eigen::VectorXd getPosition(double time) const;
        Eigen::VectorXd getVelocity(double time) const;
        std::list<Eigen::VectorXd> getWaypointsAfter(double time) const;

        KinoPath getPath() const { return path;};
        Eigen::VectorXd getMaxVelocity() const { return maxVelocity;};
        Eigen::VectorXd getMaxAcceleration() const { return maxAcceleration;};
        double getTimeStep() const { return timeStep;};
        double getPathVelocity(double time) const;

        double getFactor() const { return factor;};
        void setFactor(double factor) { this->factor = factor;};


        //Creation of KinoTrajectory from libmove3d and libmove3d-planners trajectories
        static KinoTrajectory* createKinoTrajectory(API::GeometricPath* move3dTrajectory, std::vector<unsigned int> dofs, double timeStep = 0.001);
        static KinoTrajectory* createKinoTrajectory(p3d_traj* p3dTrajectory, std::vector<unsigned int> dofs, double timeStep = 0.001);

        //Creation of genom struct and of KinoTrajectory from libmove3d trajectory
        static KinoGenomTrajectory createKinoGenomTrajectory(p3d_traj* p3dTrajectory, std::vector<unsigned int> dofs, double timeStep = 0.001);
        static KinoTrajectory* createKinoTrajectory(KinoGenomTrajectory genomTrajectory);

        //Modification of an existing KinoTrajectory
        static KinoTrajectory* factorKinoTrajectory(KinoTrajectory* originalTrajectory, double factor = 1.0); //Changing the whole trajectory speed
        static KinoTrajectory* changeKinoTrajectory(KinoTrajectory* originalTrajectory, API::GeometricPath* move3dTrajectory, std::vector<unsigned int> dofs, double timeChange); //Switching from a trajectory to another at a given time
        static KinoTrajectory* modulateKinoTrajectory(KinoTrajectory* originalTrajectory, double timeChange, double factor = 1.0);//Changing the speed of the trajectory at a given time


        //Exports
        std::vector<KinoROSPoint> toROS(unsigned int intSteps=0) const; //the whole trajectory ready to be use in a Joint Trajectory Action goal
        KinoROSPoint toROSAt(double timeStamp) const; //the configuration, velocity and acceleration of the robot at a given time
        API::GeometricPath * toP3DTraj(Robot* robot, unsigned int intSteps = 0) const;//the whole trajectory ready to be use in libmove3d-planners, not very useful due to the absence of time handling in move3d

        void toGazebo(unsigned int intSteps = 0) const; //A cpp text output to be use in the ROS tutorial http://wiki.ros.org/pr2_controllers/Tutorials/Moving the arm using the Joint Trajectory Action
        void print( unsigned int intSteps = 0) const; //A csv text output

        // Outputs the phase trajectory and the velocity limit curve in 2 files for debugging purposes.
        void outputPhasePlaneKinoTrajectory() const;

    private:
        struct KinoTrajectoryStep {
            KinoTrajectoryStep() {}
            KinoTrajectoryStep(double pathPos, double pathVel) :
                pathPos(pathPos),
                pathVel(pathVel)
            {}
            double pathPos;
            double pathVel;
            double time;
        };

        void compute(double initialVel = 0.0);
        bool getNextSwitchingPoint(double pathPos, KinoTrajectoryStep &nextSwitchingPoint, double &beforeAcceleration, double &afterAcceleration);
        bool getNextAccelerationSwitchingPoint(double pathPos, KinoTrajectoryStep &nextSwitchingPoint, double &beforeAcceleration, double &afterAcceleration);
        bool getNextVelocitySwitchingPoint(double pathPos, KinoTrajectoryStep &nextSwitchingPoint, double &beforeAcceleration, double &afterAcceleration);
        bool integrateForward(std::list<KinoTrajectoryStep> &trajectory, double acceleration);
        void integrateBackward(std::list<KinoTrajectoryStep> &startKinoTrajectory, double pathPos, double pathVel, double acceleration);
        double getMinMaxPathAcceleration(double pathPosition, double pathVelocity, bool max);
        double getMinMaxPhaseSlope(double pathPosition, double pathVelocity, bool max);
        double getAccelerationMaxPathVelocity(double pathPos) const;
        double getVelocityMaxPathVelocity(double pathPos) const;
        double getAccelerationMaxPathVelocityDeriv(double pathPos);
        double getVelocityMaxPathVelocityDeriv(double pathPos);
        
        std::list<KinoTrajectoryStep>::const_iterator getKinoTrajectorySegment(double time) const;
        
        static double deltaValue(double q1, double q2);

        KinoPath path;
        Eigen::VectorXd maxVelocity;
        Eigen::VectorXd maxAcceleration;
        unsigned int n;
        bool valid;
        std::list<KinoTrajectoryStep> trajectory;
        std::list<KinoTrajectoryStep> endTrajectory; // non-empty only if the trajectory generation failed.

        static const double eps;
        const double timeStep;

        mutable double cachedTime;
        mutable std::list<KinoTrajectoryStep>::const_iterator cachedTrajectorySegment;

        double factor;
    };
}
} //namespace move4d
