#ifndef UTILS_QUADSPLINE_H
#define UTILS_QUADSPLINE_H

#define EPSI 1e-4

namespace move4d
{
typedef enum {
    A1,
    A2,
    B,
    C1,
    C2
} durations_index;

typedef enum {
    A,
    C
} signes_index;

typedef struct data_struct{
  double v0;
  double a0;
  double amax;
  double jmax;
  double smax;
  double signs[2];
  double durations[5];
  double int_v[8];
  double int_a[8];
  int cases[8];
} quad_data;

double x_bound(double v0,double a0,double xmin,double xmax,double vmax,double amax,double jmax,double smax,bool backward);
double get_a_root(double a0,double aB,double smax,double t1,double t2);
double get_velocity(double t,double v0,double a0,double aB,double smax,double t1,double t2);

} //namespace move4d
#endif
