#ifndef HRIAGENT_HPP
#define HRIAGENT_HPP

#include "move4d/database/semantics/Sem_AgentType.hpp"

namespace move4d
{
class Joint;

class HriAgent
{
public:
    HriAgent();

    bool isHuman() const;
    void setIsHuman(bool isHuman);

    const Sem::AgentType &type() const;
    void setType(const Sem::AgentType &type);
    void setType(const std::string &type);

    //TODO:move4d
    int head_nb;
    std::vector<Joint*> head;

    //TODO:move4d
    Joint* perspective;
    double fov;

    operator bool() const;



private:
    bool _isHuman;
    Sem::AgentType _type;
};

} //namespace move4d
#endif // HRIAGENT_HPP
