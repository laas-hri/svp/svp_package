#ifndef VIMANTAGS_H
#define VIMANTAGS_H

#include "move4d/API/Device/robot.hpp"
#include <Eigen/Geometry>

namespace move4d
{
class VimanTags
{
public:
    static unsigned int readTag(Robot* rob);
    static unsigned int readTag(Robot* rob,ObjectRob* obj_rob);

    static void setSilent(bool silent=true){VimanTags::_showText = !silent;}
    static bool getSilent(){return !_showText;}

    /**
     * @brief getRectangleFromTransform
     * @param tf a transform as given by getTransforms()
     * @return 4 Vectors describing the corners of the tag in the frame of the object
     *
     * order of the vertices is (with the object frame represented for a transform without rotation) :
     *  3---2
     *  |   |
     *  0---1
     *
     * ^ y
     * |
     * o--> x
     * z
     *
     *  with the normal and z pointing out of the screen toward the reader.
     */
    static std::vector<Eigen::Vector3d> getRectangleFromTransform(const Eigen::Affine3d &t, Eigen::Vector3d &norm);
    /**
     * @brief getTagNormalScaled
     * @param t a ref to the transform
     * @return a vector normal to the tag surface, pointing outside, with a norm equal to the side length of the tag
     */
    static Eigen::Vector3d getTagNormalScaled(Eigen::Affine3d const &t);
    /**
     * as getTagNormalScaled() but with norm = 1
     */
    static Eigen::Vector3d getTagNormal(Eigen::Affine3d const &t);

private:
    static const std::string &getVimanPath();
    static std::string getCfgPath(std::string const & viman_path, std::string const & robot_name);
    static std::vector<Eigen::Affine3d> getTransforms(std::string const & cfg_path);

    static std::string _viman_path;

    static bool _showText;
};

} //namespace move4d
#endif // VIMANTAGS_H
