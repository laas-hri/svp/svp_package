#ifndef STABILITY_HPP
#define STABILITY_HPP

#include "move4d/API/forward_declarations.hpp"
#include "move4d/Logging/Logger.h"
#include <Eigen/Geometry>

namespace move4d {
namespace API {

class Stability
{
    MOVE3D_STATIC_LOGGER;
public:
    Stability(Robot *robot);

    bool check();
    double cost();
    double cost(RobotState &q);
    ///version to use as inequality constraint: <0 means stable
    double constraint();

    static double globalCost();
    static double globalCost(RobotState &q);

protected:
    bool isInPolygon(const std::vector<Eigen::Vector2d> &pol, const Eigen::Vector2d &point);
    double squareDistanceToPolygon(const std::vector<Eigen::Vector2d> &pol2d, const Eigen::Vector2d &center2d);
    /// distance between segment [a,b] and point p
    double squareDistanceToSegment(const Eigen::Vector2d &a, const Eigen::Vector2d &b, const Eigen::Vector2d &p);
private:
    Robot *_robot;

};

} // namespace API
} // namespace move4d

#endif // STABILITY_HPP
