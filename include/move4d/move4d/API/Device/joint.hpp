/*
 *  joint.h
 *  BioMove3D
 *
 *  Created by Jim Mainprice on 12/05/10.
 *  Copyright 2010 LAAS/CNRS. All rights reserved.
 *
 */
#ifndef JOINT_HPP
#define JOINT_HPP

#include "move4d/API/ConfigSpace/RobotState.hpp"
#include <eigen3/Eigen/Geometry>
//#include "p3d_matrix.h"


#ifndef _DEVICE_H
struct jnt;
#endif
namespace move4d
{
class Robot;

/*!
 @ingroup ROBOT
 @brief This class holds a Joint and is associated with a Body (Link)
 It's the basic element of a kinematic chain
 */
class Joint 
{	
public:
    enum JointType{
        /*! Joint rotate (1 dof). Its default axis is z. */
        ROTATE = 7,
        /*! Joint translate (1 dof). Its default axis is z. */
        TRANSLATE = 8,
        /*! Joint base. Only use as the first joint. It gives compatibility with
    previous version of Move3D. Furthermore, it could be use as placement
    joint. It owns 6 dof (3 translations x, y, z, 3 rotations rx, ry, rz) */
        BASE = 9,
        /*! Joint plan (3 dof). Its default use is 2 translation x, y and
    then a rotation rz. */
        PLAN = 10,
        /*! This joint looks like the base joint but it could be place at any
    level in the kinematic. */
        FREEFLYER = 11,
        /*! This joint looks like the freeflyer joint but it contains speed and acceleration vectors components. */
        QUADROTOR = 12,
        /*! This joint is fixed, there is no degree of freedom. */
        FIXED = 13,
        /*! Knee joint (3 dof). It owns 3 dof (3 rotations rx, ry, rz) */
        KNEE = 15
    };
	/**
   * Constructor
   * @param The p3d_jnt that is used
   */
  Joint(Robot* R, jnt* jntPt , int id = -1, bool copy = false );
	
  /**
   * Destructor of the class
   */
  //~Joint();
  
  /**
   * Returns the name of the joint
   */
  std::string getName() const { return m_Name; }

  std::string getDofName(uint i) const;
  JointType getType();

  Robot* getRobot() const {return m_Robot;}
	
	/**
	 * Get the Joint structue
	 */
	jnt* getJointStruct() const { return m_Joint; }
	
	/**
	 * Get the Matrix abs_pos of the Joint 
	 */
    Eigen::Affine3d	getMatrixPos() const;
	
	/**
	 * Get the Vector abs_pos of the Joint 
	 */
	Eigen::Vector3d		getVectorPos() const;

	/**
	 * Get the Vector abs_vel of the Joint
	 */
	Eigen::Vector3d		getVectorVel() const;

	/**
	 * Get the Vector abs_vel of the Joint
	 */
	void				setVectorVel(Eigen::Vector3d vel);

    /**
     * Get the p3d abs vect
     */
    //void getAbsVector(p3d_vector3 r);
	
	/**
	 * Get the p3d abs pos
	 */
	//p3d_matrix4*		getAbsPos();
	
	/**
	 * Random shoot the joint
	 */
	void            shoot(RobotState & q, bool sample_passive=false);
	
	/**
	 * Returns the Joint Dof
	 */
	double          getJointDof(int ithDoF) const;
	
	/**
	 * Set the Joint Dof
	 */
	void            setJointDof(int ithDoF, double value);
  
  /** 
   * Set ff from Eigen::Transform
   */
  bool            setFreeFlyerFromMatrix( const Eigen::Affine3d& T );
  
  double distanceSq(RobotState &s1, RobotState &s2) const;
  /**
	 * True if Joint Dof is user
	 */
	bool            isJointDofUser(int ithDoF) const;
    bool isJointDofActiveForPlanner(int ithDoF) const;
    void setDofActive(int ithDof, bool active);
    void setActive(bool active);
	
    bool isDofAngular(int ithDof) const;
    bool isDofCircular(int ithDof) const;
	/**
	 * Get Min Max dof
	 */
	void            getDofBounds(int ithDoF, double& vmin, double& vmax, unsigned int derivative=0) const;

	/**
	 * @brief Get velocity bounds of ithDof dof
	 * @param ithDoF Dof to test
	 * @param velmin min velocity
	 * @param velmax max velocity
	 */
	void getDofVelocityBounds(int ithDoF, double& velmin, double& velmax) const;

	/**
	 * @brief Get acceleration bounds of ithDof dof
	 * @param ithDoF Dof to test
	 * @param velmin min acceleration
	 * @param velmax max acceleration
	 */
	void getDofAccelerationBounds(int ithDoF, double& amin, double& amax) const;

	/**
	 * @brief Get jerk bounds of ithDof dof
	 * @param ithDoF Dof to test
	 * @param velmin min jerk
	 * @param velmax max jerk
	 */
	void getDofJerkBounds(int ithDoF, double& jmin, double& jmax) const;
	
	/**
	 * Get Number of DoF
	 */
	unsigned int		getNumberOfDof() const;
	
	/**
	 * Get Dof Pos in RobotState
	 */
	unsigned int		getIndexOfFirstDof() const;
  
  /**
   * Get the id in the joint structure of the robot
   */
  int getId() const { return m_id; }
	
	/**
	 * Set the config from the DoF values
	 */
	void            setConfigFromDofValues(RobotState & q);
  
  /**
   * Returns the previous joint
   */
  Joint*          getPreviousJoint();
  
  /**
   * Returns the array of previous joints
   */
  std::vector<Joint*> getAllPrevJoints();

    //! Compute the dimension of the space swept by the joint.
  	unsigned computeDimension();

    //! Compute the volume of the space swept by the joint.
  	double computeSpaceVolume();
	
private:
	Robot*				m_Robot;
	jnt*          m_Joint; /*!< The p3d structure for the Joint*/
  std::string		m_Name; /*!< The Joint's Name */
	bool          m_copy; /*!< Is true if the p3d_jnt copies and not only points to the structure */
  int           m_id;   /*!< id with which it was initilized */
	Eigen::Vector3d           m_vel;   /*!< Velocity vector */

};

} //namespace move4d
#endif
