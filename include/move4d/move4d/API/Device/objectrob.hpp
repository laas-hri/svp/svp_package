#ifndef OBJECTROB_HPP
#define OBJECTROB_HPP

#define OPEN_VALUE -100


#include <Eigen/Geometry>
#include "move4d/API/IK/ikTypes.hpp"
#include "move4d/Logging/Logger.h"
#include "libmove3d/p3d.h"

namespace move4d
{
class Robot;
class Joint;
class GeometricForm;


class ObjectRob
{
    MOVE3D_STATIC_LOGGER;
public:
    ObjectRob();
    ObjectRob(Robot* rob);

    ~ObjectRob();

    //return the closest distance to a support from the object
    // for now implemented for rectangle only
    double distToPoint(p3d_point p);

    double distToPointZ(p3d_point p);

    void setManipulable (bool val) {_manipulable = val;}
    bool isManipulable() {return _manipulable;}

    void setSupport (bool val) {_support = val;}
    bool isSupport() {return _support;}

    void setContainer (bool val) {_container = val;}
    bool isContainer() {return _container;}

    void setIsObject (bool val) {_isObject = val;}
    bool isObject() {return _isObject;}

    void addStableRotation(const Eigen::Quaterniond &q) {_stableRotations.push_back(q);_manipulable=true;}
    std::vector<Eigen::Quaterniond> getStableRotations() {return _stableRotations;}
    Eigen::Quaterniond getStableRotation(unsigned int id, double rot);

    void addSupports(GeometricForm* gf) { _supportList.push_back(gf);_support = true;}
    std::vector<GeometricForm*> getSupports();

    /**
     * @brief compute and return absolute position of the support polygon
     * @return
     */
    std::vector<Eigen::Vector3d> getSupportPolygonAbs();
    /**
     * @brief set the support polygon of the robot for stability computation
     * @param jnt the joint to which the polygon is attached
     * @param poly vector of vertices of the polygon
     */
    void setSupportPolygon(Joint *jnt,const std::vector<Eigen::Vector3d> &poly);

    /// true if the object has viman tags attributed
    bool hasTags() {return _tags.size();}
    /// add a viman tag in the form of a transform
    void addTag(Eigen::Affine3d tf) {_tags.push_back(tf);}
    void clearTags(){_tags.clear();}
    std::vector<Eigen::Affine3d> const & getTags(){return _tags;}

    bool isInStableRotation();

    bool isVirtual() {return _isVirtual;}
    void getIsVirtual(bool iv) {_isVirtual = iv;}

    void setToVirtual(bool val);

    bool hasVerticalityConstraint() const;
    void setHasVerticalityConstraint(bool hasVerticalityConstraint);

    Eigen::Vector3d keepVerticalAxis() const;
    void setKeepVerticalAxis(const Eigen::Vector3d &keepVerticalAxis);

    double maxVerticalTolerance() const;
    void setMaxVerticalTolerance(double angle);

    void setCylinder(Robot* cyl) { _cylinder = cyl;}
    Robot* getCylinder() {return _cylinder;}


    void setDefaultIk(IK::IkEnumTypes d) {defaultGTPIk = d;}
    IK::IkEnumTypes getDefaultIk(){return defaultGTPIk;}

private:
    bool _manipulable;
    bool _support;
    bool _container;
    bool _isObject;
    bool _isVirtual;

    std::vector<Eigen::Quaterniond> _stableRotations;

    std::vector<GeometricForm*> _supportList;
    std::vector<GeometricForm*> _currentSupportList;

    Joint *_supportPolygonJoint = nullptr; // joint to which the support polygon is attached
    std::vector<Eigen::Vector3d> _supportPolygon; // support polygon for stability computation

    bool _hasVerticalityConstraint;
    Eigen::Vector3d _keepVerticalAxis;
    double _maxVerticalTolerance;

    std::vector<Eigen::Affine3d> _tags; ///< list of viman tags. Applyng this tranform to a unity square in the x,y plane of the object frame gives the square representing the tag.



    Robot* _robot;
    Robot* _cylinder;

    /**
      * @brief default ik to use for GTP with this robot
      */
    IK::IkEnumTypes defaultGTPIk;

};

} //namespace move4d
#endif // OBJECTROB_HPP
