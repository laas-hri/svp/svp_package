#ifndef ROBOT_HPP
#define ROBOT_HPP

#include <Eigen/Core>
#define EIGEN_USE_NEW_STDVECTOR
#include <Eigen/StdVector>
#include <Eigen/Geometry> 

#include <list>

#include "move4d/API/Device/joint.hpp"
#include "move4d/API/ConfigSpace/RobotState.hpp"
#include "move4d/API/Device/hriAgent.hpp"
#include "move4d/API/IK/TracIK/forwardkynematicsolver.h"

#include "move4d/Logging/Logger.h"

#ifndef _DEVICE_H
struct rob;
struct jnt;
#endif

#ifndef _TRAJ_H
struct traj;
#endif

namespace move4d
{
class Scene;
class ObjectRob;

class HriAgent;

namespace API {
class Path;
class GeometricPath;
}

/**
 * @ingroup CPP_API
 * @defgroup ROBOT Device
 * @brief The robot class
 */

/*!
 @ingroup ROBOT
 @brief This class holds a the robot 
 represented by a kinematic chain
 */
class Robot {
	MOVE3D_STATIC_LOGGER;
public:
    static Robot *create(rob *R, bool copy);
    static Robot *create(rob *R);
  //constructor and destructor
	/**
	 * Constructeur de la classe
	 * @param R le p3d_rob pour lequel l'objet Robot est créé
     * @warning the indices of the joints in this class has an offset of 1: the first joint of p3d_rob is not copied here.
	 */
	Robot(rob* R , bool copy = false );
	
	/**
	 * Destructeur de la classe
	 */
	virtual ~Robot();
	
	rob* copyRobotStruct(rob* robotPt);
	
  //Accessor
	/**
	 * obtient la structure p3d_rob de la classe
	 * @return la structure p3d_rob
	 */
	rob* getRobotStruct() const;
	
	/**
	 * obtient le nom du Robot
	 * @return le nom du Robot
	 */
	const std::string &getName() const;

    /// unique index of the robot, can be used to get the robot through Scene::getRobot(int)
    int getIndex() const;

    /**
      * return objectRob related to the robot if possible
      */
    ObjectRob* getObjectRob();

    /**
      * set hri_agent related to the robot if possible
      */
    void setObjectRob(ObjectRob* ore);
	
	/**
	 * Associate an active scene to the robot
	 */
	void setActiveScene(Scene* sc) { m_ActiveScene = sc; }
	
	/**
	 * Returns the active scene involving the robot
	 */
	Scene* getActiveScene() { return m_ActiveScene; }
	
	/**
	 * Gets traj associated with Robot
	 * @return pointer to structure p3d_traj
	 */
	traj* getTrajStruct() __attribute_deprecated__;
	
	/**
	 * Gets the current trajectory
	 */
	API::GeometricPath *getCurrentTraj();
    API::Path *getCurrentPath();
	
	/**
	 * Set the trajectory of this robot
	 * @param trajectory The trajectory
	 */
	void setCurrentTraj(API::Path *trajectory);

	/**
	 * Get the number of Joints
	 * @return the Number of Joints
	 */
    unsigned int getNumberOfJoints() const
    {
        return m_Joints.size();
    }
	
	/**
	 * Gets the ith joint structure
	 * @return ith joint structure
	 */
	Joint* getJoint(unsigned int i) const;
  
  /**
	 * Gets joint by name
	 * @return pointer to the joint by name or NULL if not found
	 */
	Joint* getJoint(const std::string &name) const;
  
  /**
   * Returns an vector of all robot joints
   */
  const std::vector<Joint*>& getAllJoints();

	/**
	 * Returns the Object
	 * Box
	 */
	std::vector<Eigen::Vector3d> getObjectBox();
	
	/**
	 * Initializes the box in which the FF 
	 * Will be sampled
	 */
	void initObjectBox();
	
    /**
     * Sampling method for quadrotor configurations
     * @return Sampled quadrotor configuration
     */
    std::shared_ptr<RobotState> shootQuadConf();

	/**
	 * tire une RobotState aléatoire pour le Robot
	 * @param samplePassive (default = TRUE) indique si l'on tire les joints passif ou non (ie. FALSE dans le cas de ML-RRT)
	 * @return la RobotState tirée
	 */
	std::shared_ptr<RobotState> shoot(bool samplePassive = false);

    std::shared_ptr<RobotState> shootDirections(std::shared_ptr<RobotState> from , int nactive, int scale=1);
	
	/**
	 * obtient une RobotState-Direction aléatoire pour le Robot
	 * @param samplePassive (default = true) indique si l'on tire les joints passif ou non (ie. FALSE dans le cas de ML-RRT)
	 * @return la RobotState tirée
	 */ 
	std::shared_ptr<RobotState> shootDir(bool samplePassive = false);
	
	/**
	 * shoots the active free flyer inside a box
	 */
	std::shared_ptr<RobotState> shootFreeFlyer(double* box);
  
  /**
	 * set and update the active free flyer
	 */
	int setAndUpdateFreeFlyer(const Eigen::Vector3d& pos);
	
	/**
	 * place le Robot dans une RobotState
	 * @param q la RobotState dans laquelle le Robot sera placé
	 * @return la RobotState est atteignable cinématiquement
	 */
	virtual  int setAndUpdate(const RobotState & q, bool withoutFreeFlyers = false);
    int setAndUpdate(RobotState &q, bool withoutFreeFlyers=false);

	/**
	 * Calcule la dynamique du robot dans l'espace cartésien
	 * @param rs État du robot à partir duquel calculer la dynamique
	 */
	void setAndUpdateKino(const RobotState &rs);

	/**
	 * Get velocity orientation between two robots
	 * param other robot
	 * param idJoint for this joint
	 * return orientation
	 */
	double getOrientation(Robot *other, unsigned idJoint);

	/**
	 * place le Robot dans une RobotState
	 * @param q la RobotState dans laquelle le Robot sera placé
	 * @return la RobotState est atteignable cinématiquement
	 */
	bool setAndUpdateMultiSol(RobotState & q);
	
	/**
	 * place le Robot dans une RobotState, without checking the cinematic constraints.
	 * @param q la RobotState dans laquelle le Robot sera placé
	 */
	void setAndUpdateWithoutConstraints(RobotState & q);
	
	/**
	 * set and update Human Arms
	 */
	bool setAndUpdateHumanArms(RobotState & q);
	
	/**
	 * obtient la RobotState current du Robot
	 * @return la RobotState current du Robot
	 */
	std::shared_ptr<RobotState> getInitialPosition();
	
	/**
	 * Returns true if the robot is 
	 * in colision with obstacles, other robots and self
	 */
	bool isInCollision();

	/**
	 * Returns true if the robot is
	 * in colision with obstacles or other robots
	 */
	bool isInCollisionWithOthersAndEnv();
	
	/**
	 * Sets the Initial Position of the Robot
	 */
	void setInitialPosition(RobotState & conf);
	
	/**
	 * obtient la RobotState GoTo du Robot
	 * @return la RobotState GoTo du Robot
	 */
	std::shared_ptr<RobotState> getGoTo();
	
	/**
	 * Sets the Goto Position of the Robot
	 */
	void setGoTo(RobotState & conf);

	/**
	 * Sets the Initial velocity of the Robot
	 */
	void setInitialVelocity(RobotState & conf);

	/**
	 * Sets the Goto velocity of the Robot
	 */
	void setVelocityGoTo(RobotState & conf);

	/**
	 * Returns the Robot current RobotState
	 */
	std::shared_ptr<RobotState> getCurrentPos();

	/**
	 * Returns a new configuration
	 */
	std::shared_ptr<RobotState> getNewConfig();
	
	/**
	 * Get the Robot joint AbsPos
	 */
    Eigen::Matrix4d getJointAbsPos(int id);
	
	/**
	 * Get the Robot joint Position
     * @warning  based on _Robot (p3d_rob) joints array, with different indices,
     * has one more freeflyer joint in front, usually set all at 0.
	 */
	Eigen::Vector3d getJointPos(int id);
	

    void setMultiLpGroupToPlan(int mlpId,bool enable=true);
    void setMultiLpGroupToPlan(const std::string &mlpName, bool enable=true);
    std::vector<std::string> getMultiLpGroupNames(bool print=false);
    bool getMultiLpGroupToPlan(int mlpId);
	/**
	 * Get Number of active DoFs
	 */
	 unsigned int getNumberOfActiveDoF();

     unsigned int getNumberOfDoF();

     void setActiveDofs(const std::vector<uint> &active_dofs);
     void setActiveJoints(const std::vector<int> &active_jnts);
	
	/**
	 * Get Number of active DoFs
	 */
	Joint* getIthActiveDoFJoint(unsigned int ithActiveDoF , unsigned int& ithDofOnJoint  );
    unsigned int getIthActiveDofIndex(unsigned int ithActiveDof);

    bool isActiveDoF(const Joint *jnt, unsigned int i_dof_on_jnt);

	/**
	 * @brief Get joint of dof ith (and the index on this joint)
	 * @param idof Dof to test (active or not)
	 * @param ithDofOnJoint index into the joint
	 * @return joint
	 */
	Joint* getJointAt(int idof, unsigned int &ithDofOnJoint);
    std::pair<double,double> getDofLimit(int idof,uint derivative=0);

	void getListOfPassiveJoints(std::list<Joint *> & passiveJoints);

	unsigned getNbConf() const;

	statePtr_t getConf(unsigned i);

	/**
	 * @brief get minimum distance between two robot
	 * @details is fast computation : use only the robot bases (2D)
	 * @param other robot
	 * @return distance (m)
	 */
	double getMinDist(Robot *other);

    Eigen::Vector3d getCenterOfMass();

    bool checkConstraints() const;
#ifdef LIGHT_PLANNER
	/**
	 * Returns the Virtual object dof
	 */
	int getObjectDof();
	
	/**
	 * Returns Wether the closed chain constraint
	 * is active
	 */ 
	bool isActiveCcConstraint();
	
	/**
	 * Activate Constraint
	 */
	void activateCcConstraint();
	
	/**
	 * Deactivate Constraint
	 */
	void deactivateCcConstraint();
	
	/**
	 * Returns the base Joint
 	 */
	Joint* getBaseJnt();

    unsigned int getBaseJointIndex();

	/**
	 * Shoots a random direction
	 */
	std::shared_ptr<RobotState> shootRandomDirection();
	
	/**
	 * Shoots the base Joint of the robot
	 */
	std::shared_ptr<RobotState> shootBase();
	
	/**
	 *
	 */
	std::shared_ptr<RobotState> shootBaseWithoutCC();
	
	/**
	 * Shoots the base Joint of the robot
	 */
	std::shared_ptr<RobotState> shootAllExceptBase();
	
	/**
	 * Update but not base
	 */ 
	bool setAndUpdateAllExceptBase(const RobotState & Conf);
	
	/**
	 * shoots the robot by shooting the 
	 * FreeFlyer inside the accecibility box
	 */
	void shootObjectJoint(RobotState & Conf);

    /**
      *return if an object is carryed or not in the arm
      */
    bool isCarryingObject(int armId);
#endif

    bool isAgent(){return isAnAgent;}
    void isAgent(bool isAg){isAnAgent = isAg;}

    void attachObj(Robot* obj, int jointId);
    void dettachObj(Robot* obj);
    void dettachObj(int jointId);
    bool isObjectAttached();
    bool isObjectAttached(Robot* obj, int jointId);
    Joint* whereObjectAttached(Robot* obj);

    const HriAgent *getHriAgent() const;
    HriAgent *getHriAgent();
    void setHriAgent(HriAgent *hriAgent);

    bool hasMassInfo() const;

private:
    rob* _Robot; /*!< une structure de p3d_rob contenant les données sur le Robot*/
    statePtr_t _State; ///< full state of the robot (kino dyn)
    bool isAnAgent;
    HriAgent *_hriAgent;
    ObjectRob* _objectRob;
    bool _hasMassInfo;
    double _totalMass = 0;
	std::string _Name; /*!< le nom du Robot*/
	Scene* m_ActiveScene;
	bool _copy; /*!< Is true if the p3d_jnt copies and not only points to the structure */
	
	std::vector<Joint*> m_Joints;
	
	Eigen::Vector3d m_ObjectBoxCenter;
	Eigen::Vector3d m_ObjectBoxDimentions;

	/**
	 * @brief trajectory
	 */
	API::Path *trajectory;

    std::map<int,forwardKynematicSolver> m_forwardKinSolvers;
};


namespace helpers{
bool setRobotPlanningPart(Robot *r, const std::string &planning_part);
}



extern Robot* API_activeRobot;

} //namespace move4d
#endif
