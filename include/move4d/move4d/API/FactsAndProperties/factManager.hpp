#ifndef FACTMANAGER_HPP
#define FACTMANAGER_HPP

#include <string>
#include <map>
#include "move4d/API/FactsAndProperties/Facts/facts.hpp"
#include "move4d/API/FactsAndProperties/Facts/factType.hpp"
#include "move4d/API/FactsAndProperties/Facts/virtualFact.hpp"

#include "move4d/Logging/Logger.h"


namespace move4d
{
class FactManager
{
    MOVE3D_STATIC_LOGGER;
public:
    FactManager();

    /**
      * @brief creates an object of the correct type
      * @param fType the wanted type
      * @return an object from the right class
      */
    VirtualFact* createObjectFromFactType(Facts::FactType fType);

    /**
      * @return all available facts
      */
    std::vector<Facts::FactType> getAllPossibleFacts();

    /**
      * @return all available sub facts for a specific fact
      */
    std::vector<Facts::FactSubType> getAllPossiblesubFacts(Facts::FactType fType);

    /**
      * @brief read the Json file located in Biomove3d/data/Facts/enabledFacts.json and retrive the enabled facts
      * @note A fact is by default disabled
      * @return the list of enabled facts
      */
    std::vector<Facts::FactType> parseJsonEnabledFacts();

    /**
      * @brief read the Json file located in Biomove3d/data/Facts/disabledEntities.json and retrive the disabled entoties
      * @note An entity is by default enabled
      * @return the list of enabled entities
      */
    std::vector<Robot*> parseJsonDisabledEntities();



    /**
      * @brief compute one fact
      * @param fType: the type of fact to compute
      * @param entity1: first entity involved in the fact
      * @param entity2: second entity involved in the fact
      * @param ws: the worldstate where the fact should be computed
      */
    VirtualFact* computeFact(Facts::FactType fType, Robot* entity1, Robot* entity2, WorldState* ws);


    /**
      * @brief compute all fTypes facts
      * @param fType: the type of fact to compute
      * @param ws: the worldstate where the facts should be computed
      */
    std::vector<VirtualFact*> computeAllOneTypeFacts(Facts::FactType fType, WorldState* ws);

    /**
      * @brief compute the fact for the given entity as _entity1
      * @param fType: the type of fact to compute
      * @param entity: the entity on which to compute the facts
      * @param ws: the worldstate where the facts should be computed
      */
    std::vector<VirtualFact*> computeFactFor(Facts::FactType fType, Robot* entity, WorldState* ws);


    /**
      * @brief compute all the available facts
      * @param ws: the worldstate where the facts should be computed
      * @return all the computed facts
      */
    std::vector<VirtualFact*> computeAllEnabledFacts(WorldState* ws);

    /**
      * @brief Call computeAllEnabledFacts(worldState) with the current state of the world
      */
    std::vector<VirtualFact*> computeAllEnabledFacts();

    /**
      * @brief compute all enabled fact for the entity input
      * @param entity: _entity1
      */
    std::vector<VirtualFact*> computeAllEnabledFactsForEntity1(Robot* entity, WorldState* ws);

    /**
      * @brief Call computeAllEnabledFactsForEntity1(Robot, WorldState) with the current state of the world
      */
    std::vector<VirtualFact*> computeAllEnabledFactsForEntity1(Robot* entity);



private:


};

} //namespace move4d
#endif // FACTMANAGER_HPP
