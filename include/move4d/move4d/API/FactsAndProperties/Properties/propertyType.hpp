#ifndef PROPERTYTYPE_HPP
#define PROPERTYTYPE_HPP

#include<string>
#include<map>

#include "move4d/Logging/Logger.h"

namespace move4d
{
namespace Properties {
typedef enum PropertyType{
    sizeType,
    clutternessType,
    unknownProperty
} PropertyType;
}


class PropertyTypeTranslation
{
    MOVE3D_STATIC_LOGGER;
public:
    static PropertyTypeTranslation* getInstance();
    ~PropertyTypeTranslation(){}

    /**
     * @param pType: the type of the PropertyType as a string
     * @return the enum value of the corresponding PropertyType
     */
    Properties::PropertyType getPropertyTypeFromString(std::string pType);

    /**
     * @param pType: the enum value of a PropertyType
     * @return a string that describes the PropertyType
     */
    std::string getPropertyTypeAsString(Properties::PropertyType pType);


    //getters
    std::map<std::string,Properties::PropertyType> getPropertyTypeMap() { return _stringPropertyMap;}


private:
    PropertyTypeTranslation();
    static PropertyTypeTranslation* _instance;


    std::map<std::string,Properties::PropertyType> _stringPropertyMap;
};

} //namespace move4d
#endif // PROPERTYTYPE_HPP
