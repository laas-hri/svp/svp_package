#ifndef CLUTTERNESS_HPP
#define CLUTTERNESS_HPP

#include "move4d/API/FactsAndProperties/Properties/virtualProperty.hpp"

namespace move4d
{
class Clutterness : public VirtualProperty
{
    MOVE3D_STATIC_LOGGER;
public:
    Clutterness();
    Clutterness(Robot* e);
    Clutterness(Robot* e, WorldState* ws);
    void resetAndFetchPropertyParameters();

    /**
      * To compute the clutterness we consider a hexadron over the surfaces of
      * the considered objects (regtangles only, it is not done for the rest of
      * the surface types) and sample a number of point in it randomly. Then
      * we test if this points are in any other entity.
      * the clutterness is defined as the free space over the surfaces: The closer
      * it is to 1, the emptyer is the surfaces.
      */
    bool compute();
private:
    double _clutterUpLimit;
    int _sampleSize;
};

} //namespace move4d
#endif // CLUTTERNESS_HPP
