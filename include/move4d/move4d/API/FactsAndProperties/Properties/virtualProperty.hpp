#ifndef VIRTUALPROPERTY_HPP
#define VIRTUALPROPERTY_HPP

#include "move4d/Logging/Logger.h"
#include "move4d/API/Device/robot.hpp"
#include "move4d/API/FactsAndProperties/Properties/propertyType.hpp"

namespace move4d
{
class WorldState;

namespace Json {
class Value;
}

class VirtualProperty
{
    MOVE3D_STATIC_LOGGER;
public:
    VirtualProperty();
    VirtualProperty(Robot* e);
    VirtualProperty(Robot* e, WorldState* ws);
    virtual ~VirtualProperty();

    /**
      * @brief getter for _propertyType
      */
    Properties::PropertyType getPropertyType(){ return _propertyType;}

    /**
      * @brief setting the specific property parameter, either setting the default ones, or retriving
      * them from the json file at: Facts/enabledProperties.json (more info in src/database/DatabaseReader.hpp)
      */
    virtual void resetAndFetchPropertyParameters(){}

    /**
      * @brief computes the related property
      * @return returns the value of the property in the current context
      */
    virtual bool compute() = 0;

    /**
      * @brief check if the property has already been computed
      * @return true if it exists
      */
    bool checkPropertyExistance();

    /**
      * @brief print all property related info
      */
    virtual std::string getReadableProperty();

    /**
      * @brief get a Json Value
      */
    Json::Value getPropertyAsJson();

    /**
      * @return return the numeric value of the property
      */
    double getValue(){return _value;}

    /**
      * @brief test if this property is equal to p
      * @param p: the property to compare to
      * @return return true if properties are equals
      */
    bool equalsTo(VirtualProperty* p);

    //getters and setters
    Robot* getEntity() {return _entity;}
    void setEntity(Robot* e) {_entity = e;}

    std::map<std::string,Robot*> getOtherEntities() {return _otherEntities;}
    virtual void setOtherEntities(std::map<std::string,Robot*> e) {_otherEntities = e;}
    void addOtherEntity(std::string key, Robot* e) {_otherEntities[key] = e;}

    WorldState* getWS() {return _WS;}
    void setWS(WorldState* ws) {_WS = ws;}

    bool isOneEntiTyOnly() {return _isOneEntityOnly;}

protected:
    Properties::PropertyType _propertyType;

    Robot* _entity;
    std::map<std::string,Robot*> _otherEntities;

    WorldState* _WS;

    double _value;

    bool _isOneEntityOnly;
};

} //namespace move4d
#endif // VIRTUALPROPERTY_HPP
