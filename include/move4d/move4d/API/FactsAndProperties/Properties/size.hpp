#ifndef SIZE_HPP
#define SIZE_HPP

#include "move4d/API/FactsAndProperties/Properties/virtualProperty.hpp"

namespace move4d
{
class Size : public VirtualProperty
{
    MOVE3D_STATIC_LOGGER;
public:
    Size();
    Size(Robot* e);
    Size(Robot* e, WorldState* ws);
    void resetAndFetchPropertyParameters();

    /**
      * The dimension of the bounding box.
      */
    bool compute();
};

} //namespace move4d
#endif // SIZE_HPP
