#ifndef PROPERTYMANAGER_HPP
#define PROPERTYMANAGER_HPP

#include <string>
#include <map>
#include "move4d/API/FactsAndProperties/Properties/properties.hpp"
#include "move4d/API/FactsAndProperties/Properties/propertyType.hpp"
#include "move4d/API/FactsAndProperties/Properties/virtualProperty.hpp"

#include "move4d/Logging/Logger.h"


namespace move4d
{
class PropertyManager
{
    MOVE3D_STATIC_LOGGER;
public:
    PropertyManager();

    /**
      * @brief creates an object of the correct type
      * @param pType the wanted type
      * @return an object from the right class
      */
    VirtualProperty* createObjectFromPropertyType(Properties::PropertyType pType);

    /**
      * @return all available propertiess
      */
    std::vector<Properties::PropertyType> getAllPossibleProperties();

    /**
      * @brief read the Json file located in Biomove3d/data/Facts/enabledPropertiess.json and retrive the enabled properties
      * @note A property is by default disabled
      * @return the list of enabled properties
      */
    std::vector<Properties::PropertyType> parseJsonEnabledProperties();

    /**
      * @brief read the Json file located in Biomove3d/data/Facts/disabledEntities.json and retrive the disabled entoties
      * @note An entity is by default enabled
      * @return the list of enabled entities
      */
    std::vector<Robot*> parseJsonDisabledEntities();

    /**
      * @brief compute one property
      * @param pType: the type of property to compute
      * @param entity: entity involved in the property
      * @param otherEntities: the list of additional entities
      * @param ws: the worldstate where the property should be computed
      */
    VirtualProperty* computeProperty(Properties::PropertyType pType, Robot* entity, std::map<std::string,Robot*> otherEntities, WorldState* ws);


    /**
      * @brief compute property for all robots
      * @param pType: the type of property to compute
      * @param ws: the worldstate where the properties should be computed
      */
    std::vector<VirtualProperty*> computeAllOneTypeProperties(Properties::PropertyType pType, WorldState* ws);

    /**
      * @brief compute one property for one entity
      * @param pType: the type of property to compute
      * @param ws: the worldstate where the properties should be computed
      * @param entity: the entity for which to compute the property
      * @note: this function differs from computeProperty as it tests if the property is computable or not.
      */
    std::vector<VirtualProperty*> computePropertyFor(Properties::PropertyType pType, Robot* entity, WorldState* ws);


    /**
      * @brief compute all the available properties
      * @param ws: the worldstate where the properties should be computed
      * @return all the computed properties
      */
    std::vector<VirtualProperty*> computeAllEnabledProperties(WorldState* ws);

    /**
      * @brief Call computeAllEnabledProperties(worldState) with the current state of the world
      */
    std::vector<VirtualProperty*> computeAllEnabledProperties();

    /**
      * @brief compute all enabled properties for the entity input
      * @param entity: _entity
      */
    std::vector<VirtualProperty*> computeAllEnabledpropertiesForEntity(Robot* entity, WorldState* ws);

    /**
      * @brief Call computeAllEnabledFactsForEntity1(Robot, WorldState) with the current state of the world
      */
    std::vector<VirtualProperty*> computeAllEnabledpropertiesForEntity(Robot* entity);
};

} //namespace move4d
#endif // PROPERTYMANAGER_HPP
