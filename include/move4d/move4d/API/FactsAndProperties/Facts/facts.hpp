#ifndef FACTS_HPP
#define FACTS_HPP

#include "move4d/API/FactsAndProperties/Facts/isIn.hpp"
#include "move4d/API/FactsAndProperties/Facts/isOn.hpp"
#include "move4d/API/FactsAndProperties/Facts/isNextTo.hpp"
#include "move4d/API/FactsAndProperties/Facts/isReachable.hpp"
#include "move4d/API/FactsAndProperties/Facts/isVisible.hpp"
#include "move4d/API/FactsAndProperties/Facts/isPickable.hpp"

#endif // FACTS_HPP
