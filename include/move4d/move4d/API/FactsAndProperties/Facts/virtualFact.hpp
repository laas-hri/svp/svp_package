#ifndef VIRTUALFACT_HPP
#define VIRTUALFACT_HPP

#include "move4d/Logging/Logger.h"
#include "move4d/API/Device/robot.hpp"
#include "move4d/API/FactsAndProperties/Facts/factType.hpp"

namespace move4d
{
class WorldState;

namespace Json {
class Value;
}

class VirtualFact
{
    MOVE3D_STATIC_LOGGER;
public:
    VirtualFact();
    VirtualFact(Robot* e1, Robot* e2);
    VirtualFact(Robot* e1, Robot* e2, WorldState* ws);

    virtual ~VirtualFact(){}

    /**
      * @brief getter for _factType
      */
    Facts::FactType getFactType(){ return _factType;}

    /**
      * @brief setting the specific fact parameter.
      * Either setting the default ones, or retriving
      * them from the json file at: Facts/enabledFacts.json (more info in DatabaseReader)
      * @note for each class, a list of optional parameters are provided in the coments.
      */
    virtual void resetAndFetchFactParameters(){}

    /**
      * @return return the list of all possible factSubTypes
      */
    virtual std::vector<Facts::FactSubType> getSubTypes() = 0;

    /**
      * @brief computes the related fact
      * @return returns if the fact is true or not in the current context
      */
    virtual bool compute() = 0;

    /**
      * @brief check if the fact has already been computed
      * @return true if it exists
      */
    bool checkFactExistence();

    /**
      * @brief when relevant, check if the fact where the two entities are
      * exchanged exists
      * @return true if it exists
      */
    bool checkInverseFactExistence();

    /**
      * @brief print all fact related info
      */
    virtual std::string getReadableFacts();

    /**
      * @brief get a Json Value
      */
    Json::Value getFactAsJson();

    /**
      * @return return the boolean value of the fact with the default sub fact
      */
    bool getBoolValue();

    /**
      * @return return the numeric value of the fact with the default sub fact
      */
    double getNumericValue();

    /**
      * @return return the boolean value of the fact with the specified sub fact
      */
    bool getBoolValueOfSubType(Facts::FactSubType fSType);

    /**
      * @return return the numeric value of the fact with the specified sub fact
      */
    double getNumericValueOfSubType(Facts::FactSubType fSType);

    /**
      * @brief test if this fact contains the fact f.
      * @param f: with specified subfacts
      * @return return true if all the subfacts contained by f exists in this virtualfact.
      */
    bool contains(VirtualFact* f);

    //getters and setters
    Robot* getEntity1() {return _entity1;}
    void setEntity1(Robot* e1) {_entity1 = e1;}

    Robot* getEntity2() {return _entity2;}
    void setEntity2(Robot* e2) {_entity2 = e2;}

    WorldState* getWS() {return _WS;}
    void setWS(WorldState* ws) {_WS = ws;}


    std::map<Facts::FactSubType,std::pair<bool,double> > getValues() {return _values;}
    void addValues(Facts::FactSubType fType ,std::pair<bool,double> df) {_values[fType] = df;}

    Facts::FactSubType getDefaultSubType(){ return _defaultSubType;}

    double getComputationTime() {return _computationTime;}

protected:
    Facts::FactType _factType;

    Robot* _entity1;
    Robot* _entity2;

    WorldState* _WS;

    std::map<Facts::FactSubType,std::pair<bool,double> > _values;
    Facts::FactSubType _defaultSubType;

    double _computationTime;

};

} //namespace move4d
#endif // VIRTUALFACT_HPP

