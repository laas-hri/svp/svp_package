#ifndef ISON_HPP
#define ISON_HPP

#include "move4d/API/FactsAndProperties/Facts/virtualFact.hpp"


namespace move4d
{
class IsOn : public VirtualFact
{
    MOVE3D_STATIC_LOGGER;
public:
    IsOn();
    IsOn(Robot* e1, Robot* e2);
    IsOn(Robot* e1, Robot* e2, WorldState* ws);

    /**
      * positiveThreshold: double (the max dist between hovering object and support to consider it isOn)
      * negativeThreshold: double (the max dist between surface and bottom of object inside the support isOn)
      * defaultSubType: string (default sub type to use)
      */
    void resetAndFetchFactParameters();
    std::vector<Facts::FactSubType> getSubTypes();

    /**
      * computation of is On fact
      * an object A is On another one B in the next conditions:
      * The center of A is within the boundaries of a surface of B
      * B is not too tilted
      * TODO: Recursivity
      * is above is a special case where there is no contact
      */
    bool compute();

private:
    double _positiveThreshold;
    double _negativeThreshold;
};

} //namespace move4d
#endif // ISON_HPP
