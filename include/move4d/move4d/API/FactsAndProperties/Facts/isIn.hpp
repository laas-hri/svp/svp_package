#ifndef ISIN_HPP
#define ISIN_HPP

#include "move4d/API/FactsAndProperties/Facts/virtualFact.hpp"

namespace move4d
{
class IsIn : public VirtualFact
{
    MOVE3D_STATIC_LOGGER;
public:
    IsIn();
    IsIn(Robot* e1, Robot* e2);
    IsIn(Robot* e1, Robot* e2, WorldState* ws);

    /**
      * ignoreEpsilon: double (minimum size of objects to ignore)
      * stepdiv: double (the steps to take to test the colisions when needed)
      * defaultSubType: string (default sub type to use)
      */
    void resetAndFetchFactParameters();
    std::vector<Facts::FactSubType> getSubTypes();
    bool compute();

private:
    double _ignoreEpsilon;
    double _stepdiv;
};

} //namespace move4d
#endif // ISIN_HPP
