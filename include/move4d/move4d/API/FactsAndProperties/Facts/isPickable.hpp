#ifndef ISPICKABLE_HPP
#define ISPICKABLE_HPP

#include "move4d/API/FactsAndProperties/Facts/virtualFact.hpp"

namespace move4d
{
class gpGrasp;

class IsPickable : public VirtualFact
{
    MOVE3D_STATIC_LOGGER;
public:
    IsPickable();
    IsPickable(Robot* e1, Robot* e2);
    IsPickable(Robot* e1, Robot* e2, WorldState* ws);


    /**
      */
    void resetAndFetchFactParameters();
    std::vector<Facts::FactSubType> getSubTypes();

    /**
      * @brief Compute if the object entity1 can be picked by the agent entity2 it tries grasps until one is found
      */
    bool compute();

    /**
      * @brief return a grasp id and the coresponding configuration.
      */
    void getGrasp(int &armId, int &graspId, statePtr_t &q);

    /**
      * @brief Computes a graspConf based on the graspId
      */
    bool baseComputation(int armId, int graspId);

    /**
      * @brief read the grasps files and keep them. This is
      */
    bool fetchGraspsFromFile();


    /**
      * @brief test if the pick with armId and graspId have already been tested
      */
    bool alreadyTested(int armId, int graspId);

    void setArmId(int id);
    int getArmId() {return _armId;}

//    void addUsedId(int id) {_usedIds.push_back(id);}
//    void clearUsedId() {_usedIds.clear();}
//    std::vector<int> getUsedid() {return _usedIds;}

private:
    int _armId;
    bool _isArmSpecified;

    /**
      * keep the ids used (either successfully or not) depending on robots and arms
      */
    std::map<int,std::vector<int> > _usedIds;

    /**
      * Keep the already read grasps (doesnot change regarding the object) the first Robot
      * is the object the second the robot
      */
    static std::map<std::pair<Robot*,Robot*>, std::map<int,std::vector<gpGrasp> > >  _robotGraspLists;

    /**
      * Turn the boolean to true when getGrasp already returns the coresponding grasp
      */
    std::map<bool, std::pair<int,statePtr_t> > _possibleGraspConf;
};


} //namespace move4d
#endif // ISPICKABLE_HPP
