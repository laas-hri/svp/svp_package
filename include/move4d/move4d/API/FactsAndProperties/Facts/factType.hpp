#ifndef FACTTYPE_HPP
#define FACTTYPE_HPP

#include<string>
#include<map>

#include "move4d/Logging/Logger.h"

namespace move4d
{
namespace Facts {
typedef enum FactType{
    isOnType,
    isInType,
    isNextToType,
    isReachableType,
    isPickableType,
    isVisibleType,
    unknownFact
} FactType;

typedef enum FactSubType{
    noSubType,
    isAbove,
    isAboveOnly,
    isCovered,
    isInside,
    isReachableByDistance,
    isReachableByIK,
    isReachableByIKNoCol,
    isVisibleAngleBy,
    isVisibleObstructBy,
    unkonwnSubFact
} FactSubType;
}

class FactTypeTranslation
{
    MOVE3D_STATIC_LOGGER;
public:
    static FactTypeTranslation* getInstance();
    ~FactTypeTranslation(){}

    /**
     * @param fType: the type of the fact as a string
     * @return the enum value of the corresponding factType
     */
    Facts::FactType getFactTypeFromString(std::string fType);

    /**
     * @param fType: the enum value of a factType
     * @return a string that describes the fact
     */
    std::string getFactTypeAsString(Facts::FactType fType);

    /**
      * @param fSType: the enum value of a factSubType
      * @return a string that describes the sub fact
      */
    std::string getFactSubTypeAsString(Facts::FactSubType fSType);

    /**
      * @param fSType: the type of the sub fact as a string
      * @return the enum value of the corresponding factSubType
      */
    Facts::FactSubType getFactSubTypeFromString(std::string fSType);

    //getters
    std::map<std::string,Facts::FactType> getFactTypeMap() { return _stringFactMap;}
    std::map<std::string,Facts::FactSubType> getSubFactTypeMap() { return _stringSubFactMap;}


private:
    FactTypeTranslation();
    static FactTypeTranslation* _instance;

    std::map<std::string,Facts::FactType> _stringFactMap;
    std::map<std::string,Facts::FactSubType> _stringSubFactMap;


};

} //namespace move4d
#endif // FACTTYPE_HPP
