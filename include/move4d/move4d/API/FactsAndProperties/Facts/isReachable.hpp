#ifndef ISREACHABLE_H
#define ISREACHABLE_H

#include "move4d/API/FactsAndProperties/Facts/virtualFact.hpp"


namespace move4d
{
class IsReachable : public VirtualFact
{
    MOVE3D_STATIC_LOGGER;
public:
    IsReachable();
    IsReachable(Robot* e1, Robot* e2);
    IsReachable(Robot* e1, Robot* e2, WorldState* ws);

    /**
      * distTolerance: double (only for the human: max error permited)
      * defaultSubType: string (default sub type to use)
      */
    void resetAndFetchFactParameters();
    std::vector<Facts::FactSubType> getSubTypes();

    /**
      * Compute based on computing first the distance
      * between the agent shoulder and the closest corner of the BB of the target object
      * then use an IK computation (with the same point) to test the reachability
      */
    bool compute();


    /**
      * @brief: deactivate collision between r and the rest of the agents if deativate is true
      * @param ativate: false to deativate, true to activate
      */
    void deactivateAgentCollision(bool activate, Robot* r);

    /**
      * @return best stored conf according to costs
      */
    statePtr_t getBestConf();


    double getCost() {return _cost;}
    void setCost(double c) {_cost = c;}



private:
    double _distTolerance;
    int _nbRotations;
    int _nbIkToTest;
    std::map<int,configPt> _confmap;

    double _cost;
    std::map<int,std::pair<statePtr_t, double> > _posPerArm;
};

} //namespace move4d
#endif // ISREACHABLE_H
