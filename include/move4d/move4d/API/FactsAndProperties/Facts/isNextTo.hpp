#ifndef ISNEXTTO_H
#define ISNEXTTO_H

#include "move4d/API/FactsAndProperties/Facts/virtualFact.hpp"

namespace move4d
{
class IsNextTo : public VirtualFact
{
    MOVE3D_STATIC_LOGGER;
public:
    IsNextTo();
    IsNextTo(Robot* e1, Robot* e2);
    IsNextTo(Robot* e1, Robot* e2, WorldState* ws);

    /**
      * distance: double (max distance to consider 2 object next to each others)
      * defaultSubType: string (default sub type to use)
      */
    void resetAndFetchFactParameters();
    std::vector<Facts::FactSubType> getSubTypes();

    /**
      *  computation of is next to fact
      * an object is next to another if the closest distance between those two is smaller than isNextToDist (0.2m)
      * NOTE: if an object is next to another, the inverse is also true!
      */
    bool compute();

private:
    double _distance;
};

} //namespace move4d
#endif // ISNEXTTO_H
