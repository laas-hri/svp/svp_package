#ifndef ISVISIBLE_H
#define ISVISIBLE_H

#include "move4d/API/FactsAndProperties/Facts/virtualFact.hpp"


namespace move4d
{
/**
 * @brief The IsVisible class computes and stores data for perspective taking tests.
 * The computation is made by compute(), and can involve an OpenGL rendering of the scene as viewed by the agent whom we test the perspective.
 *
 * The fact tests if the entity1 is visible by the entity2, which must be an agent with a perspective information (HRI_PERSP)
 *
 * @todo store the perspective data (camera joint,...) in Semantics
 * @ingroup Facts
 */
class IsVisible : public VirtualFact
{
    MOVE3D_STATIC_LOGGER;
public:
    IsVisible();
    IsVisible(Robot* e1, Robot* e2);
    IsVisible(Robot* e1, Robot* e2, WorldState* ws);

    /**
      * percentOfSeenObj: double (minimum percentage to trigger if an object is seen or not)
      * defaultSubType: string (default sub type to use)
      */
    void resetAndFetchFactParameters();
    std::vector<Facts::FactSubType> getSubTypes();

    /** @brief computing visibility fact.
     * @return true if the object is visible
     * for now, it is done roughly
     * a first loop check if the center of mass of the target object is in the field of view,
     * a second checks the occlusions with a rendering of the scene with g3d (openGl).
     *
     * ### Reuse of the rendering
     * The result of the rendering is saved in the WorldState of the fact ( @ref VirtualFact::getWS() ),
     * The result is saved in terms of pixel counts (@ref WorldState::visiblePixelsCount() ).
     *
     * If a pixel count is present in the WorldState for that agent, the pixel count is reused instead.
     *
     * If the scene have more than 360 robots (agents and objects), undefined behaviour.
     *
     * @see setWS()
     * @see WorldState::setVisiblePixelCount()
     *
     * @warning limit of 360 robots for testing.
     *
     * @todo improve the angle computation heuristic (for now based on center of the bounding box)
     * @todo consider several FoV (head motion, focus)
     * @todo manage the limit of multipleObjecVisibTest() (360 objects)
     */
    bool compute();

    /**
     * @brief isVisibleG3d computes visibility of entity1 by entity2.
     * @return true if visible
     *
     * Uses multipleObjecVisibTest() for computation, giving only one object as in `targets`.
     * @see multipleObjecVisibTest()
     */
    bool isVisibleG3d();
    /**
     * @brief isVisibleG3dAll compute visibility of all robots (agents and movables).
     * @return
     * Uses multipleObjecVisibTest() for computation.
     *
     * Saves the result in the WorldState if any (@ref getWS()).
     * @see compute()
     * @see multipleObjecVisibTest()
     */
    bool isVisibleG3dAll();
    /**
     * @brief visibTest is an internal function
     *
     * Used as a callback for counting the number of pixels in the rendered scene.
     * Result is saved int __visible_pixels_test
     */
    static void visibTest();

    /**
     * @brief multipleObjecVisibTest computation of visibility
     * @param agent the agent whose perspective we want to test
     * @param targets the objects we want to use
     * @return the pixel counts for each object
     *
     * The returned array is a count of pixels in the same order as the input targets.
     * It is the number of pixels belonging to the robot that appears in the field of view
     * of the agent, reflecting the place it occupies in the field of view.
     *
     * @note it can test only 360 objects (size of the targets array)
     */
    static std::vector<uint> multipleObjecVisibTest(Robot *agent, std::vector<Robot*> &targets);

    /**
     * @brief getPixelCounts get the pixel counts computed by isVisibleG3dAll()
     * @return a vector of pixel count
     *
     * The order is supposed to be the same as in the Scene object (global_Project->getActiveScene->getRobot(i))
     *
     * @see Scene::getRobot(unsigned int)
     * @see global_Project
     */
    std::vector<uint> getPixelCounts() const;

private:
    double _percentOfSeenObj;
    uint _visiblePixels;
    uint _threshold;
    static std::vector<uint> __visible_pixels_test;
    std::vector<uint> _res;
};

} //namespace move4d
#endif // ISVISIBLE_H
