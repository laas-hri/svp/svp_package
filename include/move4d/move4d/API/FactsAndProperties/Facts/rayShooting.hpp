#ifndef RAYSHOOTING_HPP
#define RAYSHOOTING_HPP

#include <utility>
#include <map>
#include "p3d.h"

namespace move4d
{
class Robot;

class RayShooting
{
public:

    RayShooting();
    void computeVisibilities(Robot *agent);

    double getVisibility(std::pair<Robot*,Robot*> id);

    static RayShooting* getInstance(double wsId);
private:


    bool setVisBall();
    void placeVisball(p3d_point p);
    Robot* isVisballColliding();


    std::map<std::pair<Robot*,Robot*>,double> _visibilitymaps;
    std::map<Robot*,bool> _visibilityComputations;
    Robot* _visball;
    int _nbTests;

    static std::map<double,RayShooting*> _instances;

};

} //namespace move4d
#endif // RAYSHOOTING_HPP
