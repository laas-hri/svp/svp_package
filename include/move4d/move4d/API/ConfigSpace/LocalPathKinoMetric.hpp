#ifndef MOVE4D_API_LOCALPATHKINOMETRIC_HPP
#define MOVE4D_API_LOCALPATHKINOMETRIC_HPP

#include "move4d/API/ConfigSpace/localpathbase.hpp"
#include "move4d/Logging/Logger.h"

namespace move4d {
class LocalPath;
namespace API {

class LocalPathKinoMetric : public move4d::API::LocalPathBase
{
    MOVE3D_STATIC_LOGGER;
public:
    LocalPathKinoMetric(Robot *r);
    LocalPathKinoMetric(statePtr_t B, statePtr_t E, double duration=0, double dynamics_ratio=1);
    LocalPathKinoMetric(const LocalPathKinoMetric &lp);
    LocalPathKinoMetric(LocalPathKinoMetric &path, double &pathDelta, bool lastValidConfig=true);
    LocalPathKinoMetric(LocalPathKinoMetric &path, double begin_offset, double length);
    virtual ~LocalPathKinoMetric();

    virtual LocalPathBase *clone();
    virtual LocalPathBase *buildSmaller(double &pathDelta, bool lastValidConfig);
    virtual LocalPathBase *subLocalPath(double begin_offset, double length);
    virtual bool isValid();
    virtual bool classicTest();
    virtual double length();
    virtual double getParamMax();
    virtual std::shared_ptr<RobotState> configAtDist(double dist);
    virtual std::shared_ptr<RobotState> configAtParam(double param);
    virtual double stayWithInDistance(double u, bool goForward, double *distance);
    virtual std::shared_ptr<LocalPathBase> buildReversePath();

private:

    double t;
    double desired_t;
    LocalPath *lp;
};

} // namespace API
} // namespace move4d

#endif // MOVE4D_API_LOCALPATHKINOMETRIC_HPP
