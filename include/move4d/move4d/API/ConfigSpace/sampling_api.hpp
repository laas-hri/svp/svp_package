#ifndef SAMPLING_API_HPP
#define SAMPLING_API_HPP

#ifndef CONFIGURATION_HPP
class RobotState;
#endif
#ifndef LOCALPATH_HPP
class LocalPath;
#endif

#include <memory>

namespace move4d
{
class Robot;

/**
    @ingroup CONFIG_SPACE
    @brief The sampling API
    */
class SamplingAPI
{
protected:
	Robot* mR;

public:
	SamplingAPI(Robot* r) : mR(r) {}

	virtual ~SamplingAPI();

	virtual std::shared_ptr<RobotState> sample(bool samplePassive = true);

	std::shared_ptr<RobotState> shootCollisionFree();
};

} //namespace move4d
#endif
