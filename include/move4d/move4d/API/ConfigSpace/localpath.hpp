#ifndef LOCALPATH_HPP
#define LOCALPATH_HPP

#include "move4d/API/ConfigSpace/RobotState.hpp"
#include "move4d/API/ConfigSpace/localpathbase.hpp"
#include "move4d/Logging/Logger.h"

#include <libmove3d/Localpath-pkg.h>

namespace move4d
{
class Robot;
namespace API{

/**
 @ingroup CONFIG_SPACE
 \brief Classe représentant un chemin local
 @author Florian Pilardeau,B90,6349 <fpilarde@jolimont>
 */
class LocalPath : public LocalPathBase
{
    MOVE3D_STATIC_LOGGER;
public:
	/**
	 * constructors and destructors
	 */

	/**
	 * Class Constructor
	 * The type is linear by default
	 * @param B la RobotState initiale du LocalPath
	 * @param E la RobotState finale du LocalPath
	 */
	LocalPath(std::shared_ptr<RobotState> B, std::shared_ptr<RobotState> E, double duration = 0.0);

	/**
	 * smaller local path (for the extend method)
	 */
	LocalPath(LocalPath& path, double& pathDelta, bool lastValidConfig = true, double duration = 0.0);
    LocalPath(LocalPath &path, double begin_offset,double length);

	/**
	 * Copy constructor
	 * @param path a LocalPath
	 */
	LocalPath(const LocalPath& path);

	/**
	 * Constructor from a struct
	 * @param p3d struct
	 */
	LocalPath(Robot* R, p3d_localpath* lpPtr, double duration = 0.0);

    LocalPath(Robot *r);

    static LocalPath *cast(LocalPathBase *path);
    static std::shared_ptr<LocalPath> cast(localPathBasePtr_t path);
    virtual LocalPathBase *clone() const;

    virtual LocalPathBase *buildSmaller(double &pathDelta, bool lastValidConfig);
    virtual LocalPathBase *subLocalPath(double begin_offset, double length);
	/**
	 * Destructor
	 */
	virtual ~LocalPath();

    virtual std::shared_ptr<LocalPathBase> buildReversePath();

	//Accessor
	/**
	 * obtient la structure p3d_localpath stockée
	 * @return la structure p3d_localpath stockée
	 */
	p3d_localpath* getLocalpathStruct() const;

	/**
	 * obtient le type de LocalPath
	 * @return le type de LocalPath
	 */
	p3d_localpath_type getType();

	/**
	 * teste si le LocalPath est valide
	 * @return le LocalPath est valide
	 */
	virtual bool isValid();

	/**
	 * test si le LocalPath est valide
	 * @param R le Robot pour lequel le LocalPath est testé
	 * @param ntest in/out le nombre de tests
	 * @return ! le LocalPath n'est pas valide
	 */
	bool unvalidLocalpathTest(Robot* R, int* ntest);

	/**
	 * Test the localpath using the classic method as opposed to dichotomic test
	 * @return true if the localpath is valid
	 */
	virtual bool classicTest();

	/**
	 * obtient la longueur du LocaPath
	 * @return la longueur du LocalPath
	 */
	virtual double length() const override;

	/**
	 * Equivalent to the length when using linear interpolation
	 */
	virtual double getParamMax();

	/**
	 * obtient une RobotState se trouvant à une distance donnée du début du LocalPath
	 * @param R le Robot pour lequel le LocalPath est créé
	 * @param dist la distance par rapport au début
	 * @return la RobotState
	 */
	virtual std::shared_ptr<RobotState> configAtDist(double dist);

	/**
	 * obtient une RobotState se trouvant sur le LocalPath à un paramètre donnée
	 * @param R le Robot pour lequel le LocalPath est créé
	 * @param param le paramètre
	 * @return la RobotState
	 */
	virtual std::shared_ptr<RobotState> configAtParam(double param);

	/**
	 * Stay within dist
	 * From a parameter along the LocalPath and distance a vector of distance in WorkSapce
	 * Stay within dist computes the maximum parameter that the robot can move
	 * in free space
	 */
	virtual double stayWithInDistance(double u, bool goForward, double* distance);

	/**
	 * @brief Set duration.
	 * @param The duration
	 */
	void setDuration(double duration) {
		_duration = duration;
	}

	/**
	 * @brief Get duration.
	 * @return The duration
	 */
	double getDuration() const {
		return _duration;
	}

	/**
	 * @brief Get controls.
	 * @return vector of nb dof jerk
	 */
	const std::vector<double> &getControl() const {
		return control;
	}

	/**
	 * @brief Set controls
	 * @param control vector of nb dof jerk
	 */
	void setControl(const std::vector<double> &control) {
		this->control = control;
	}

    statePtr_t getLastValidConfig(double &p);
protected:
	p3d_localpath* createLocalpathStruct(bool multi_sol = false);
private:
	/**
	 * @brief The control for this edge.
	 */
	std::vector<double> control;

	p3d_localpath* _LocalPath;

	//! type du local path(mahantan, linear ...)
	p3d_localpath_type _Type;

	/**
	 * @brief The duration to execute control.
	 */
	double _duration;

};

typedef std::shared_ptr<LocalPath> localPathPtr_t;

}
} //namespace move4d
#endif
