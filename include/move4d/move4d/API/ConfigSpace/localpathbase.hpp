#ifndef LOCALPATHBASE_HPP
#define LOCALPATHBASE_HPP

#include "move4d/API/ConfigSpace/RobotState.hpp"
#include "move4d/API/Trajectory/Path.hpp"
#include <boost/static_assert.hpp>

typedef struct localpath p3d_localpath;

namespace move4d
{
class Robot;

namespace API{

/**
 @ingroup CONFIG_SPACE
 \brief Classe représentant un chemin local
 @author Florian Pilardeau,B90,6349 <fpilarde@jolimont>
 */
class LocalPathBase //: public Path
{
public:
    /**
     * constructors and destructors
     */

    LocalPathBase();

    /**
     * Class Constructor
     * The type is linear by default
     * @param B la RobotState initiale du LocalPathBase
     * @param E la RobotState finale du LocalPathBase
     */
    LocalPathBase(std::shared_ptr<RobotState> B, std::shared_ptr<RobotState> E);

    /**
     * smaller local path (for the extend method)
     */
    LocalPathBase(LocalPathBase& path, double& pathDelta, bool lastValidConfig = true);
    /**
     * @brief sub localpath, according to parameter
     * @param path
     * @param begin_offset
     * @param length
     * @throw out_of_range if begin_offset or begin_offset+length are greater than the length of the path
     */
    LocalPathBase(LocalPathBase &path, double begin_offset, double length);

    /**
     * Copy constructor
     * @param path a LocalPathBase
     */
    LocalPathBase(const LocalPathBase& path);

    LocalPathBase(Robot *r);

    /**
     * Destructor
     */
    virtual ~LocalPathBase();

    virtual LocalPathBase *clone() const=0;
    virtual LocalPathBase *buildSmaller(double &pathDelta, bool lastValidConfig = true) =0;
    virtual LocalPathBase *subLocalPath(double begin_offset, double length) =0;

    template<class T>
    static std::shared_ptr<LocalPathBase> castPtr(std::shared_ptr<T> &lp_ptr){return std::dynamic_pointer_cast<LocalPathBase>(lp_ptr);}
    /**
     * obtient la configuration initiale
     * @return la configuration initiale
     */
    std::shared_ptr<RobotState> getBegin() const;
    /**
     * obtient la configuration finale
     * @return la configuration finale
     */
    std::shared_ptr<RobotState> getEnd() const;

    /**
     * obtient le Robot pour lequel le LocalPathBase est créé
     * @return le Robot pour lequel le LocalPathBase est créé
     */
    Robot* getRobot();

    /**
     * Returns the number of
     * Colision test done to test the local path
     */
    int getNbColTest();

    /**
     * Returns the number of
     * Colision test done to test the local path
     */
    int getNbCostTest();
    void setNbCostTest(double nbCostTest){_NbCostTest = nbCostTest;}

    /**
     * teste si le LocalPathBase à été évalué
     * @return le LocalPathBase à été évalué
     */
    bool getEvaluated();

    /**
     * obtient la dernière RobotState valide le long du LocalPathBase
     * @param p in/out le paramètre correspondant à la dernière Configauration valide
     * @return la dernière RobotState valide le long du LocalPathBase
     */
    std::shared_ptr<RobotState> getLastValidConfig(double& p);

    /**
     * Set the localpath as untested
     */
    void setLocalpathAsNotTested() {
        _Evaluated = false;
    }

    /**
     * teste si le LocalPathBase est valide
     * @return le LocalPathBase est valide
     */
    virtual bool isValid()=0;

    /**
     * test si le LocalPath est valide
     * @param R le Robot pour lequel le LocalPathBase est testé
     * @param ntest in/out le nombre de tests
     * @return ! le LocalPathBase n'est pas valide
     */
    bool unvalidLocalpathTest(Robot* R, int* ntest);

    /**
     * Test the localpath using the classic method as opposed to dichotomic test
     * @return true if the localpath is valid
     */
    virtual bool classicTest() =0;

    /**
     * obtient la longueur du LocaPath
     * @return la longueur du LocalPathBase
     */
    virtual double length() const = 0;

    /**
     * Equivalent to the length when using linear interpolation
     */
    virtual double getParamMax() =0;

    /**
     * obtient une RobotState se trouvant à une distance donnée du début du LocalPathBase
     * @param R le Robot pour lequel le LocalPathBase est créé
     * @param dist la distance par rapport au début
     * @return la RobotState
     */
    virtual std::shared_ptr<RobotState> configAtDist(double dist) =0;

    /**
     * obtient une RobotState se trouvant sur le LocalPathBase à un paramètre donnée
     * @param R le Robot pour lequel le LocalPathBase est créé
     * @param param le paramètre
     * @return la RobotState
     */
    virtual std::shared_ptr<RobotState> configAtParam(double param) =0;

    virtual std::vector<double> maxima(uint derivative);
    virtual double maxAsRatioOfLimits(uint derivative);

    /**
     * Stay within dist
     * From a parameter along the LocalPathBase and distance a vector of distance in WorkSapce
     * Stay within dist computes the maximum parameter that the robot can move
     * in free space
     */
    virtual double stayWithInDistance(double u, bool goForward, double* distance) =0;

    /**
     * Cost resolution for
     * integral and work along LocalPathBase
     */
    double getResolution(double step = 0.0);

    /**
     * Get number of cost segments
     */
    unsigned int getNumberOfCostSegments();

    /**
     * Returns the cost profile of the localPath
     */
    std::vector<std::pair<double, double> > getCostProfile();

    /**
     * Return param at which integral of cost
     * is higher than the input (going from begin to end)
     */
    double whenCostIntegralPasses(double thresh);

    //! Compute the total cost of the path.
    double cost();

    double getAverageCost();
    void setAverageCost(double averageCost){_AverageCost=averageCost;}

    //! Set the cost of the local path.
    void setCost(double cost) { _Cost = cost; _costEvaluated = true; }
    //! Set the maximal cost found along the local path
    void setMaxCost(double cost){_MaxCost = cost;}
    double getMaxCost() const;

    //! get the cost of the reverse local path.
    double getReverseCost() const { return reverseCost; }

    //! Set the cost of the reverse local path.
    void setReverseCost(double cost) { reverseCost = cost; }

    //! When reset the next cost query will compute it.
    void resetCostComputed() { _costEvaluated = false; }

    //! Set the ik sol to be used in the local planner.
    void setIkSol(int* iksol) { _ikSol = iksol; }

    //! Return the reverse path and update its cost.
    virtual std::shared_ptr<LocalPathBase> buildReversePath() =0;

    //! Print the variables inside the local path.
    void print();
protected:
    Robot* _Robot;

    std::shared_ptr<RobotState> _Begin;
    std::shared_ptr<RobotState> _End;

    bool _Valid;
    bool _Evaluated;
    double _lastValidParam;
    std::shared_ptr<RobotState> _lastValidConfig;
    bool _lastValidEvaluated;
    int _NbColTest;

    int* _ikSol;


    bool _ResolEvaluated;
    double _Resolution;

    bool _costEvaluated;
    double _Cost;
    double _AverageCost;
    double _MaxCost;
    int _NbCostTest;

    //! cost of the path when followed in the opposite direction
    double reverseCost;

    // Path interface
//public:
//    //virtual Path *clone() const override;
//    virtual Path *subPath(double from_t, double to_t) const override;
//    virtual statePtr_t getState(double p) const override;
//    virtual PathIterator begin() const override;
//    virtual PathIterator end() const override;
//    virtual bool check() override;
//    virtual Path *fromDescriptionVector(const std::vector<double> &descrVector) override =0;
//    virtual std::vector<double> getDescriptionVector() const override =0;
//    virtual std::pair<std::vector<double>, std::vector<double> > getDescriptionVectorLimits(bool clampStart, bool clampGoal) const override =0;
};

class LocalPathBaseCreatorInterface
{
public:
    virtual LocalPathBase *create(Robot *r) = 0;
    virtual LocalPathBase *create(statePtr_t B,statePtr_t E) = 0;
    virtual LocalPathBase *create(statePtr_t B,statePtr_t E,double length) = 0;

    virtual LocalPathBase *clone(const LocalPathBase &lp) = 0;
    virtual LocalPathBase *subLocalPath(LocalPathBase &lp, double &pathDelta, bool lastValidConfig=true) = 0;
    //virtual LocalPathBase *create(Robot *r,p3d_localpath *p3d_lp)  = 0;
};

template <class C>
class LocalPathBaseCreator : public LocalPathBaseCreatorInterface
{
    BOOST_STATIC_ASSERT(std::is_base_of<LocalPathBase,C>::value);
public:
    typedef C LocalPathType;
    LocalPathBase *create(Robot *r){return new C(r);}
    LocalPathBase *create(statePtr_t B,statePtr_t E){return new C(B,E);}
    LocalPathBase *create(statePtr_t B,statePtr_t E,double length){return new C(B,E,length);}

    LocalPathBase *clone(const LocalPathBase &lp){const C &_lp=dynamic_cast<const C&>(lp);return new C(_lp);}
    LocalPathBase *subLocalPath(LocalPathBase &lp, double &pathDelta, bool lastValidConfig=true)
    {
        C &_lp=dynamic_cast<C&>(lp);
        return new C(_lp,pathDelta,lastValidConfig);
    }
    //LocalPathBase *create(Robot *r,p3d_localpath *p3d_lp) {return new C(r,p3d_lp);}
};

typedef std::shared_ptr<LocalPathBase> localPathBasePtr_t;

}
} //namespace move4d
#endif // LOCALPATHBASE_HPP
