#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP


#include <Eigen/Core>
//#define EIGEN_USE_NEW_STDVECTOR
#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include <memory>

namespace move4d
{
class Robot;

class RobotState;

typedef std::shared_ptr<RobotState> statePtr_t;
/**
 * @ingroup CPP_API
 * @defgroup CONFIG_SPACE RobotState space
 * @brief C-Space make generic motion planners possible
 */

/**
 @ingroup CONFIG_SPACE
 @brief Classe représentant une RobotState d'un Robot
 @author Florian Pilardeau,B90,6349 <fpilarde@jolimont>
 */
class RobotState {
protected:
	bool _flagInitQuaternions;
	bool _CollisionTested;
	bool _InCollision;
	bool _CostTested;
	double _Cost;
	Robot *_Robot;
	double _temperature;

	// State dof * nbStateValues
	std::vector<std::vector<double> > state;

	// Nb dof
	unsigned int dof;

	// Nb values by dof
	unsigned int nbStateValues;

	// Integration step
	double _inteStep;

public:
	//##### CONSTRUCTORS AND DESTRUCTOR #####
	//    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
	/**
	 * Constructeur de la classe
	 * @param R le Robot pour lequel la RobotState est créée
	 */
	RobotState(Robot *R=NULL, unsigned int nbStateValues = 1, double inteStep = 0.0);

	/**
	 * Constructeur de la classe
	 * @param R le Robot pour lequel la RobotState est créée
	 * @param C la structure de RobotState qui sera stockée
	 * @param noCopy if set to true, _State is set to C, DEPRECATED
	 * otherwise a copy of C is made.
	 */
	RobotState(Robot *R, double *C, bool noCopy = false, unsigned int nbStateValues = 1, double inteStep = 0.0);

	/**
	 * Copy constructor of the class
	 * @param confguration
	 **/
	RobotState(const RobotState &conf);

	/**
	 * Destructeur de la classe
	 */
	~RobotState();


	//##### ASIGNATION ####
    double& operator [] (size_t i){
        return this->state[0][i];
    }
    double operator [] (size_t i) const
    {
        return this->state[0][i];
    }

    double& operator () (const size_t &dof, const size_t &derivative = 0){
        return state[derivative][dof];
    }
    double operator () (const size_t &dof, const size_t &derivative = 0) const {
        return state[derivative][dof];
    }

	double& at(const int &dof, const int &derivative = 0);
    double  at(const int &dof, const int &derivative = 0) const;

	bool operator==(RobotState &Conf);

	bool operator!=(RobotState &Conf);

	statePtr_t operator-(RobotState &Conf);

	statePtr_t operator+(RobotState &Conf);

	RobotState &operator*(const double coeff);

	RobotState &operator=(const RobotState &other);

	statePtr_t sub(RobotState &C);

	statePtr_t add(RobotState &C);

	RobotState &mult(double coeff);

	bool equal(const RobotState &Conf, bool print = false) const;

	bool equalNoJnts(RobotState &Conf, std::vector<unsigned int> ignoreJoints);

	//##### ACCESSORS #####
    bool getFlagInitQuaternions(void) const { return this->_flagInitQuaternions; }

    bool getCollisionTested(void) const { return this->_CollisionTested; }

    bool getInCollision(void) const { return this->_InCollision; }
    void setCollision(bool isInCollision){this->_CollisionTested=true; this->_InCollision=isInCollision;}

    bool getCostTested(void) const { return this->_CostTested; }

    double getCost(void) const { return this->_Cost; }

    void setCost(double cost) { this->_Cost=cost; this->_CostTested=true;}

    Robot *getRobot(void) const { return this->_Robot; }

	const std::vector<std::vector<double> > &getState() const;
	std::vector<std::vector<double> > &getState();

	/**
	 * obtient le pointeur sur la ConfigPt
	 * @return le pointeur sur la ConfigPt
	 */
	double* getConfigStruct();
	const double* getConfigStruct() const;
	double* getSpeedStruct();

	/**
	* obtient le pointeur sur la ConfigPt
	* @return la pointeur sur la ConfigPt
	*/
	double* getConfigStructCopy();

	/**
	 * Get the Ith Active Dof
	 * @return the ith active DoF
	 */
	double getActiveDoF(unsigned int ith, unsigned int stateVal = 0);

	/**
	 * Get the Eigen Vector of the configuration
	 */
	Eigen::VectorXd getEigenVector(unsigned int derivative = 0, bool active_dofs_only=false) const;

	/**
	 * Get Config in degrees
	 */
	statePtr_t getConfigInDegree();

	/**
 	* @brief Get value of [dof, stateValue].
	* @param dof Index of degree of freedom.
	* @param nbStateValues Index of state values (0 is configuration).
	* @return The value of [dof, stateValue].
	*/
	double getValue(unsigned int dof, unsigned int stateValue) const;

	/**
 	* @brief Getter : number of state values.
	* @return Number of state values.
	*/
	unsigned int getNbStateValues() const;

	/**
 	* @brief Getter : number of degree of freedom
	* @return number of degree of freedom.
	*/
	unsigned int getNbDof() const;

	double getInteStep() const;

	double getTemperature() const {
		return _temperature;
	}

	void setTemperature(double temp) {
		_temperature = temp;
	}

#ifdef LIGHT_PLANNER

	/**
	 * Sets the config constraints
	 * and returns the task 3d pos (Virtual Object)
	 */
	Eigen::Vector3d getTaskPos();

#endif

	/**
	 * Set the configuration as not tested
	 */
	void setAsNotTested();

	/**
	 * modifie la structure configPt stockée
	 * @param C la nouvelle structure configPt
	 */
	void setConfiguration(double *C);

	/**
	 * Set the configuration as not tested
	 */
	void setCostAsNotTested();

	/**
	 * Sets the configuration to respect robot constraints
	 * Leaves the robot in the last configuration
	 */
	bool setConstraintsWithSideEffect();

	/**
	 * Sets the configuration to respect robot constraints
	 */
	bool setConstraints();

	/**
	 * Set the ith active DoF
	 */
	void setActiveDoF(unsigned int ith, double value);

	/**
 	* @brief Set the value at dof, nbStateValues.
	* @param dof Index of degree of freedom.
	* @param nbStateValues Index of state values (0 is configuration).
	* @param value Value of this derivative.
	*/
	void setValue(unsigned int dof, unsigned int nbStateValues, double value);
    void setValues(RobotState &other);

	/**
 	* @brief Change size of state.
	* @param nbStateValues New size of state values.
	*/
	void setNbStateValues(unsigned int nbStateValues);

	/**
	* Set Integration step for kinodynamic planning
	* @brief Setter : integration step
	*/
	void setInteStep(double intestep);

	/**
	 * indique si la RobotState est en collision
	 * @return la RobotState est en collision
	 */
	bool isInCollision();

	/**
	 * True is the configuration respects
	 * DoFs bounds
	 */
	bool isOutOfBounds(bool print = false);

	Eigen::VectorXd getEigenVector(const int& startIndex, const int& endIndex) const;

	/**
	 * Gets the quaternion
	 * @return le vecteur des Quaternions
	 */
	Eigen::Quaterniond getQuaternion();

    void setFromQuaternion(Eigen::Quaterniond &q);

	/**
	 * Sets EulersAngles
	 */
	void setQuaternionsToEuler();

	/**
	 * modifie la structure configPt stockée
	 * @param C la RobotState contentant la nouvelle structure
	 */
	void setConfiguration(RobotState& C);

	/**
	 * set the Eigen Vector of the configuration
	 */
	void setFromEigenVector(const Eigen::VectorXd& conf, uint derivative=0);

	void setFromEigenVector(const Eigen::VectorXd& conf, const int& startIndex, const int& endIndex);

	/**
	 * indique si le vecteur de Quaternions est initialisé
	 * @return le vecteur de Quaternions est initialisé
	 */
	bool isQuatInit();



	//##### DISTANCE #####
	/**
	 * calcule la distance à une RobotState
	 * @param Conf la RobotState entrée
	 * @return la distance
	 */
	double dist(RobotState &Conf, bool print = false, bool kino = false);

	/**
	 * calcule la distance à une RobotState
	 * @param q la RobotState entrée
	 * @param distChoice le type de calcul de distance
	 * @return la distance
	 */
	double dist(RobotState &q, int distChoice, bool kino = false);

	//! Compute the distance between this configuration and the given one.
	double distance(const RobotState *q, bool kino = false) const;

	//! Compute the square of the distance between this state and the given one.
	double squareDistance(RobotState *q, bool kino = false);

	/**
	 * weighted distance
	 * @param node the seconde node
	 * @param w1 ponderation of distance
	 * @param w2 ponderation of velocity
	 * @param w3 ponderation of acceleration
	 * @return distance between to nodes
	 */
	double weightedSquareDistance(RobotState *q, float w1 = 0.25, float w2 = 0.5, float w3 = 1.0);

	/**
	 * Get the maximum integration duration for each DOF to reach the node
	 * @param q node to reach
	 * return the maximum time to reach
	 */
	double kinoMetric(RobotState *q);

	/**
	 * Compute the min distance to a CSpace obstacle
	 * @return min dist
	 */
	double distEnv();



	//##### METHODES #####
	/**
	 * détruie la configPt stockée
	 */
	void clear();

	/**
	 * Convert RobotState in radian
	 */
	void convertToRadian();

	/**
	 * True is the configuration respects 
	 * DoFs bounds
	 */
	void adaptCircularJointsLimits();

	/**
	 * copie une RobotState
	 * @return une copie de la RobotState
	 */
	statePtr_t copy();

	/**
	 * copie les joints passifs de la RobotState courante dans la RobotState entrée
	 * @param C in/out la RobotState à modifier
	 */
	void copyPassive(RobotState &C);

	/**
	 * obtient le cout de la RobotState suivant l'espace des fonctions de cout
	 * @return le cout de la RobotState
	 */
	double cost();

	void print(bool withPassive = false);

	/**
	* @brief Return state into string
	* @param withPassive print passive joint or not 
	* @return string
	*/
	std::string toString(bool withPassive = false);

	/**
 	* @brief Init state dof * nbStateValues.
	* @param dof Degree of freedom.
	* @param nbStateValues Number of state values (0 is configuration).
	*/
	void initState(unsigned int dof, unsigned int nbStateValues);

	/**
 	* @brief Init void state.
	*/
	void initState();

	/**
 	* @brief Init state dof * nbStateValues and set configuration.
	* @param dof Degree of freedom.
	* @param nbStateValues Number of state values (0 is configuration).
	* @param configuration Configuration to apply.
	*/
	void initState(unsigned int dof, unsigned int nbStateValues, double* configuration);

	/**
	 * @brief Test if the new state is valid
	 * @return true if is valid, false otherwise
	 */
	bool checkBounds();

    double boundsConstraint(uint derivative);

	/**
	 * @brief Create a new valid random state
	 * @param robot is pointer of robot
	 * @return new state
	 */
	static statePtr_t shootRandomState(Robot *robot);

	/**
	 * @brief Create random vector of valid jerk
	 * @param robot is pointer of robot
	 * @return valid vector of jerk
	 */
	static std::vector<double> shootRandomControl(Robot *robot);

	/**
	 * @brief Propagate vector of jerk with random duration between [min - max] step times integration step (PlanParam)
	 * @param control vector of jerk
	 * @param minStep min integration step
	 * @param maxStep max integration step
	 * @param duration duration of this propagation (out)
	 * @param valid true if path is valid (if testValidity = false then valid = true)
	 * @param testValidity test the validity of the path
	 * @return new state, if is not valid return the last valid test
	 */
	statePtr_t forwardPropagation(std::vector<double> control, double minStep, double maxStep, double& duration, bool& valid, bool testValidity = true);
	statePtr_t forwardPropagation(std::vector<double> control, double minStep, double maxStep, double& duration);

	/**
	 * @brief Propagate vector of jerk with explicite duration
	 * @param control vector of jerk
	 * @param duration duration of this propagation
	 * @param valid true if path is valid (if testValidity = false then valid = true)
	 * @param testValidity test the validity of the path
	 * @return new state, if is not valid return the last valid test
	 */
	statePtr_t forwardPropagation(std::vector<double> control, double duration, bool &valid, bool testValidity = true);
	statePtr_t forwardPropagation(std::vector<double> control, double duration);
    // controlOrder: 1 if control is speed, 2 if control is acceleration...
    statePtr_t forwardPropagation(std::vector<double> control, double duration, int controlOrder);
    statePtr_t backwardPropagation(std::vector<double> control, double duration, int controlOrder);

	/**
	 * @brief Propagate vector of jerk with explicite duration and return the list of intermediary state
	 * @param control vector of jerk
	 * @param duration duration of this propagation
	 * @param valid true if path is valid (if testValidity = false then valid = true)
	 * @param testValidity test the validity of the path
	 * @return list of intermediary state, if valid equals false return the list of valid state
	 */
	std::vector<statePtr_t> forwardPropagationSample(std::vector<double> control, double duration, bool &valid, bool testValidity = true);
	std::vector<statePtr_t> forwardPropagationSample(std::vector<double> control, double duration);

	/**
	 * @brief Propagate vector of jerk with explicite duration and return the cost list of intermediary state
	 * @param control vector of jerk
	 * @param duration duration of this propagation
	 * @param valid true if path is valid (if testValidity = false then valid = true)
	 * @param testValidity test the validity of the path
	 * @return list of intermediary state, if valid equals false return the list of valid state
	 */
	std::vector<double> forwardPropagationCost(std::vector<double> control, double duration, bool &valid, bool testValidity = true);
	std::vector<double> forwardPropagationCost(std::vector<double> control, double duration);

	/**
	 * Is kinodynamic goal directed function
	 * @param robot the robot
	 * @param sampleControl vector of jerk (in/out)
	 * @param from this state
	 * @param to this one
	 * @return pair conposed by minimum time and maximum time for integration
	 */
	static std::pair<double, double> directedControl(Robot* robot, std::vector<double> &sampleControl, statePtr_t from, statePtr_t to);

	/**
	  * print a comparison between the 2 confs
	  */
	void printCompare(RobotState *conf,bool withPassive = false);

	/**
	 * initialise le vecteur de Quaternions
	 */
	void initQuaternions();

	/**
	 * Set Quaternions
	 */
	void initQuaternions(int quatDof,Eigen::Quaternion<double> quat);

protected:
	/**
	 * Propagate vector of jerk and check is new node is valid (one int step)
	 * if the distance between tmp and the new state is greater than dmax :
	 * - test the validity of the new node;
	 * - the tmp node become the new node.
	 * @brief Propagate control
	 * @param control vector of jerk
	 * @param tmp used for validity test (in/out)
	 * @param valid new node is valid (out)
	 */
	void integrate(std::vector<double> control, statePtr_t &tmp, bool &valid, bool testValidity);

	/*************************************************************************************
	 ****************        FlyCrane      ***********************************************
	 *************************************************************************************/

public:
	//! Initialize the FlyCrane attributes.
	void initializeFlyCraneConfiguration();

	//! Get the position (in translation) of the reference point of the platform.
	Eigen::Vector3d getPosition() const { return position; }

	//! Get the position (in rotation) of the reference point of the platform.
	double *getOrientation() { return orientation; }

	//! Get the orientations of the pairs of ropes with respect to the platform.
	double *getDihedralAngles() { return dihedralAngles; }

	//! Get the coordinates of the center of mass of each quadrotor (expressed in the global reference frame).
	Eigen::Vector3d *getQuadCMs() { return CM; }

	//! Get the coordinates of the anchor point of each quadrotor (expressed in the global reference frame).
	Eigen::Vector3d *getQuadAnchors() { return A; }

	//! Have the coordinates of the anchor points on the quadrotors been assigned?
	bool getAssignedQuadAnchors() const { return assignedQuadAnchors; }

	//! Get the upper bounds on the estimations of the tensions exerted on the cables.
	double *getMaximalTensions() { return maximalTensions; }

	//! Get the lower bounds on the estimations of the tensions exerted on the cables.
	double *getMinimalTensions() { return minimalTensions; }

	//! Get the norms of the forces exerted by the quadrotors
	double *getQuadForces() { return quadForces; }

	//! Set the coordinates of the center of mass of each quadrotor (in the global reference frame).
	void setCentersOfMass(Eigen::Vector3d *centersOfMass);

	//! Set the coordinates of the anchor points of the quadrotors (expressed in the global reference frame).
	void setQuadAnchors(Eigen::Vector3d *anchors);

	//! Set the tension bounds and force norms.
	void setTensionsAndForces(double maxTensions[], double minTensions[], double forces[]);

	bool isConnectible();
private:
	//! position of the reference frame of the platform with respect to the global reference frame
	Eigen::Vector3d position;

	//! orientation of the reference frame of the platform with respect to the global reference frame
	double orientation[3];

	//! dihedral angles of the pairs of ropes with respect to the platform
	double dihedralAngles[3];

	//! position of the center of mass of each quadrotor (expressed in the global reference frame)
	Eigen::Vector3d CM[3];

	//! position of the anchor point of each quadrotor (expressed in the global reference frame)
	Eigen::Vector3d A[3];

	//! boolean used to check whether the anchor points have been assigned
	bool assignedQuadAnchors;

	//! upper bound for the estimation of the tension exerted on each cable
	double maximalTensions[6];

	//! lower bound for the estimation of the tension exerted on each rope
	double minimalTensions[6];

	//! norm of the force exerted by each quadrotor
	double quadForces[3];
};


} //namespace move4d
#endif
