#ifndef LOCALPATHDISCRETE_HPP
#define LOCALPATHDISCRETE_HPP

#include "move4d/API/ConfigSpace/localpathbase.hpp"

/**
 * @brief The LocalPathDiscrete class
 * it is a dummy local path that always return the same state object when querried within its bounds
 */
namespace move4d
{
namespace API{
class LocalPathDiscrete : public LocalPathBase
{
public:
    LocalPathDiscrete(Robot *r);
    LocalPathDiscrete(statePtr_t begin, statePtr_t end, double paramMax=0);
    LocalPathDiscrete(LocalPathDiscrete &lp, double &pathDelta, bool lastValidConfig);
    LocalPathBase *clone() const;
    LocalPathBase *buildSmaller(double &pathDelta, bool lastValidConfig);
    LocalPathBase *subLocalPath(double begin_offset, double length);
    bool isValid();
    bool classicTest();
    double length() const override;
    double getParamMax();
    std::shared_ptr<RobotState> configAtDist(double dist);
    std::shared_ptr<RobotState> configAtParam(double param);
    double stayWithInDistance(double u, bool goForward, double *distance);
    std::shared_ptr<LocalPathBase> buildReversePath();

protected:
    double _Length;

};

}
} //namespace move4d
#endif // LOCALPATHDISCRETE_HPP
