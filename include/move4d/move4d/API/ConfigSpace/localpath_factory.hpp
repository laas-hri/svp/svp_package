#ifndef LOCALPATH_FACTORY_HPP
#define LOCALPATH_FACTORY_HPP


#include "move4d/API/Device/robot.hpp"
#include "move4d/API/ConfigSpace/localpathbase.hpp"

//class LocalPathBase;
//template <class C> class LocalPathBaseCreator;

/**
    @ingroup CONFIG_SPACE
    @brief Creares local paths
    */
namespace move4d
{
namespace API{
class LocalpathFactory
{
    LocalpathFactory();
    MOVE3D_STATIC_LOGGER;
public:
    static LocalpathFactory *instance();

    virtual ~LocalpathFactory();

    virtual LocalPathBase* create(
            std::shared_ptr<RobotState> q1,
            std::shared_ptr<RobotState> q2);
    virtual LocalPathBase* create(
            std::shared_ptr<RobotState> q1,
            std::shared_ptr<RobotState> q2,
            double length);
    /**
     * @brief clone creator
     * @param lp
     * @return
     */
    virtual LocalPathBase *create(LocalPathBase *lp);
    virtual LocalPathBase *create(LocalPathBase *lp, double &pathDelta, bool lastValidConfig=true);
    virtual LocalPathBase *create(Robot *r, p3d_localpath *p3d_lp);

    virtual bool setType(const std::string &name);

    void registerFactory(const std::string &name,LocalPathBaseCreatorInterface *creator);

    std::vector<std::string> getTypes() const;
    std::string getCurrentType() const;
private:
    std::map<std::string,LocalPathBaseCreatorInterface* > _creators;
    LocalPathBaseCreatorInterface *_defaultCreator;
    std::string _defaultCreatorName;
};

//credits to github.com/ros/class_loader for the macro
#define LOCALPATH_REGISTER_CREATOR_INTERNAL(Derived, UniqueID) \
namespace \
{\
  struct ProxyExec ## UniqueID \
  {\
    typedef  Derived _derived; \
    ProxyExec ## UniqueID() \
    { \
      ::move4d::API::LocalpathFactory::instance()->registerFactory(#Derived, new ::move4d::API::LocalPathBaseCreator<_derived>()); \
    }\
  };\
  static ProxyExec ## UniqueID g_register_plugin_ ## UniqueID;\
}

//this one needed to have the needed supplementary level of macro expansion
#define LOCALPATH_REGISTER_CREATOR_INTERNAL_HOP1(Derived,UniqueID) \
    LOCALPATH_REGISTER_CREATOR_INTERNAL(Derived,UniqueID)
#define LOCALPATH_REGISTER_FACTORY(localpath_class)\
    LOCALPATH_REGISTER_CREATOR_INTERNAL_HOP1(localpath_class,__COUNTER__)


}
} //namespace move4d
#endif
