#ifndef MOVE4D_API_PARAMETER_HPP
#define MOVE4D_API_PARAMETER_HPP

#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>
#include <boost/thread.hpp>
#include <string>
#include <vector>
#include <map>
#include <stdexcept>

namespace move4d {
namespace API {

class bad_get : public std::logic_error
{
public:
    bad_get(const std::string &what=""):std::logic_error(what){}
};


/**
 * @brief The Parameter class holds data of different types to be accessed globally in the code.
 *
 * The Parameter class actually allows to build a tree of values (int, bool, double, string, array, maps).
 * The root of the tree is accessed via the static function root(lock_t&), branches can be either maps (recommended) or arrays,
 * and leaves are integers, reals, strings or bools.
 *
 * A Parameter can also have a NullValue, when built with the default constructor or Parameter(NullValue). In this case *only*
 * its type can be changed. It will automatically be set to the type corresponding to the method called. e.g.
 *
 * ~~~~~~~~~.cpp
 * Parameter p;
 * assert(p.type()==NullValue); //ok
 * p.asString();
 * assert(p.type()==StringValue); // ok, type set to string
 * p.asInt(); // throw a move4d::API::bad_get exception
 * ~~~~~~~~~
 */
class Parameter
{
public:
    typedef boost::unique_lock<boost::mutex> lock_t;

    typedef std::map<std::string,Parameter> ObjectContainer;
    using Key = ObjectContainer::key_type;
    enum ValueType{
        NullValue,
        IntValue,
        BoolValue,
        RealValue,
        StringValue,
        ObjectValue,
        ArrayValue
    };

    /**
     * @brief this version give access to the root parameter with
     * @param lock an unlocked unique_lock or locked by a previous call to this method in the same thread.
     * @return a reference to the root paremeter
     *
     * @note: the lock parameter is locked inside the function if it is unlocked. It must be deleted to
     * allow other threads to access the root. Keep it alive until you don't need it anymore.
     */
    static Parameter &root(lock_t &lock);

    Parameter(ValueType type=NullValue);
    Parameter(int v);
    Parameter(double v);
    Parameter(const char *v);
    Parameter(const std::string &v);
    Parameter(const std::map<std::string,Parameter> &obj);
    Parameter(const std::vector<Parameter> &array);
    Parameter(bool v);

    /**
     * @brief get the value in a parameter tree
     * @param path path in the Parameter tree, separator is "/"
     * @param default_v (the parameter is created and set to default_v if doesn't exist)
     * @return a copy of the (newly created) value
     *
     * Recursively creates all intermediate branches in the tree structure
     */
    template <typename T>
    T get(const std::string &path, const T &default_v){
        if (path.size()){
            std::string::size_type p = path.find("/");
            if(p==std::string::npos){
                return (*this)[path].get(default_v);
            }
            return (*this)[path.substr(0,p)].get(path.substr(p+1,std::string::npos),default_v);
        }else{
            return get<T>(default_v);
        }
    }
    /**
     * @brief get the value (and evenutally set it)
     * @param default_v (the parameter is created and set to default_v if it doesn't exist)
     * @return a copy of the (newly created) value
     */
    template <typename T>
    T get(const T &default_v){
        static_assert(!std::is_same<T,std::vector<Parameter> >::value,"cannot get an ArrayValue");
        if(type()==NullValue){
            *this=Parameter(default_v);
            return default_v;
        }else{
            return _get<T>();
        }
    }

    template <typename T>
    bool getParam(const std::string &path, T &v) const {
        if(path.size()){
            if(type()!=ObjectValue){return false;}
            std::string::size_type p0=path.find_first_not_of("/");
            std::string path_stripped=path.substr(p0);
            std::string::size_type p = path_stripped.find("/");
            if(p==std::string::npos){
                auto it=find(path_stripped);
                if(it!=end())
                    return it->second.getParam(v);
                else
                    return false;
            }
            auto it=find(path_stripped.substr(0,p));
            if(it!=end())
                return it->second.getParam(path_stripped.substr(p+1,std::string::npos),v);
            else
                return false;
        }else{
            return getParam(v);
        }
    }
    template <typename T>
    bool getParam(T &v) const {
        static_assert(!std::is_same<T,std::vector<Parameter> >::value,"cannot get an ArrayValue");
        if(type()==ArrayValue) return false;
        try{
            v=_get<T>();
            return true;
        }catch(bad_get e){
            return false;
        }
    }

    template <typename T>
    static T param(const std::string &path,const T &default_v){
        lock_t lock;
        return root(lock).get<T>(path,default_v);
    }
    template <typename T>
    static bool getParameter(const std::string &path, T& v){
        lock_t lock;
        return root(lock).getParam(path,v);
    }

    static bool setParameter(const std::string &path, const Parameter& v);
    bool setParam(const std::string &path, const Parameter& v);
    bool setParam(const Parameter &v);

    const bool &asBool() const;
    const int &asInt() const;
    const double &asDouble() const;
    const std::string &asString() const;

    bool &asBool();
    int &asInt();
    double &asDouble();
    std::string &asString();

    /** length of the contained array
     * @throw bad_get if not an array
     */
    size_t size() const;
    Parameter &operator[](size_t index);
    const Parameter &operator[](size_t index) const;
    /**
     * @brief at get a Parameter from sequence container
     * @param i index
     * @return reference to Parameter in vector at index i
     * @throw bad_get if not an array
     * @throw same as std::vector<Parameter>::at
     */
    Parameter &at(size_t i);
    const Parameter &at(size_t i) const;
    void append(const Parameter &p);

    Parameter &operator[](const Key &k);
    //const Parameter &operator[](const Key &k)const; // not defined, same as std::map::operator[]
    ObjectContainer::iterator begin();
    ObjectContainer::iterator end();
    ObjectContainer::const_iterator begin() const;
    ObjectContainer::const_iterator end() const;
    ObjectContainer::iterator find(const Key &k);
    ObjectContainer::const_iterator find(const Key &k) const;
    bool hasKey(const Key &k) const;

    ValueType type() const;

    Parameter &operator=(bool v);
    Parameter &operator=(double v);
    Parameter &operator=(int v);
    Parameter &operator=(const std::string &v);
    Parameter &operator=(const char *v);
    Parameter &operator=(const API::Parameter &v)=default;

    bool operator==(const Parameter &other) const;

    friend std::ostream &operator <<(std::ostream &os, const Parameter &p);
private:
    /**
     * @throw move4d::API::bad_get
     */
    template<typename T>
    const T &_get() const{
        try{
        return boost::get<T>(_v);
        }catch (boost::bad_get &e){
            throw bad_get(std::string(e.what()).append(" -- got: ").append(typeid(T).name()).append(" instead of ").append(_v.type().name()));
        }
    }
    template<typename T>
    T &_get(){
        try{
        return boost::get<T>(_v);
        }catch (boost::bad_get &e){
            throw bad_get(std::string(e.what()).append(" -- got: ").append(typeid(T).name()).append(" instead of ").append(_v.type().name()));
        }
    }

    void setType(ValueType t);
    void checkType(ValueType t) const;

    ValueType _type;
    struct NullValueType{
        bool operator ==(const NullValueType &) const {return true;}
    };
    typedef boost::variant<NullValueType,bool, int, double, std::string,ObjectContainer> Value;
    Value _v;
};

} // namespace API
} // namespace move4d

#endif // MOVE4D_API_PARAMETER_HPP
