#ifndef MODULEBASE_HPP
#define MODULEBASE_HPP

/**
 * @file moduleBase.hpp
 * @brief Defines base tools for managing modules.
 * @ingroup Modular
 * @author Jules Waldhart
 * @see \ref Modular for details on modules
 */

/**
 * @defgroup Modular
 * @brief Tools for managing features of move3d having dependencies between them and/or needing initialization.

 * ModuleBase class provides standard interface for a module,
 * and ModuleRegister is the module manager, that can initialize all required modules,
 * including their dependencies.
 *
 * What's a module (and what's not)
 * ----
 *
 * A module is not a -- just -- a C++ object, it's a higher level concept. A module is a group of functionalities that have a consistency
 * all together, and are optionals in at least some configurations of the software. A module needs to be initialized
 * to have its functionalities working, and/or have dependencies as other modules that needs to be initialized.
 *
 * So, do not create a module if its functionalities:
 * - needs no initialization (just call it when you need it) **AND** isn't dependent on other modules.
 * - or are not optional (initialize them on startup unconditionally)
 *
 * In the specific case of move3d, consider the very basic usages, as simple RRT call on a 2D freeflyer, as references to know if a module is optional or not.
 *
 * Hint: your module is probably optional.
 *
 * ### Discussion
 *
 * Maybe there are cases where it would be pertinent to use modules on non-optional functionalities. These are just guidelines I think about.
 *
 * Creating a module
 * -----
 * You have to create a class derived from ModuleBase. It's a good idea to have it as a singleton (strongly recommended, actually I can't think about other
 * good option).
 * Put the code needed for initialization in the initialize() method.
 * Don't forget to register it to the ModuleRegister with the ModuleBase::addToRegister() method. I recommend doing it in the constructor.
 * Also, if you opt for singleton, pre-initialize it, i.e. put something like this in a cpp file, outside any function/method:
 * ~~~{.cpp}
 * MyModule *MyModule::_instance = new MyModule();
 * ~~~
 * with `MyModule::_instance` being a static member of `MyModule`.
 * You can refer to the code of already existing modules, like GtpModule, HriInfoModule...
 *
 * Access to module functionalities
 * -----
 * Modules are designed to be just a way of representing dependencies and manage initializations. You can have the real work done in another class(es),
 * have the working class being derived from ModuleBase.
 * Also, think about what could happen if you call a functionality of an uninitialized module. You can use the ModuleBase::isInitialized() accessor.
 */

#include <string>
#include <vector>
#include <map>
#include "move4d/Logging/Logger.h"

namespace move4d
{
/**
 * @brief The ModuleBase class
 * @ingroup Modular
 * @see \ref Modular
 */
class ModuleBase
{
public:
    /**
     * @brief ModuleBase default constructor
     * @param name the name of the module, with which it is registered in the ModuleRegister
     * @param dependencies the names of the modules that needs to be initialized prior to this one
     */
    ModuleBase(const std::string &name, const std::vector<std::string> &dependencies);
    virtual ~ModuleBase() {}

    /**
     * @brief Default initializer.
     * re-implement for derived classes.
     * @warning doesn't manage dependencies, use one of the ModuleRegister::init() overloads for it, they use this function.
     */
    virtual void initialize();
    /**
     * @brief is module initialized
     * @return true if the initialize() method have been called successfully
     */
    virtual bool isInitialized(){return _isInit;}
    /**
     * @brief whether or not the module is enabled.
     * @return enable status
     */
    virtual bool isEnabled();
    /**
     * @brief enables or disables the module
     * @param enable true to enable, false to disable, default=true
     * @note some modules can be impossible to disable, thus causing this function to have no effect
     * @warning this does not manage dependencies, use ModuleRegister::enable(bool), ModuleRegister::enable(string,bool),...
     */
    virtual void enable(bool enable=true);

    virtual void run();

    /**
     * @brief get the name of the module.
     * @note recommend to add a static name() method in derived classes
     */
    virtual std::string getName(){return _name;}
    /**
     * @brief get the list of dependencies as module names.
     * @return
     */
    virtual std::vector<std::string> getModuleDependencies(){return _deps;}
protected:
    /**
     * @brief function to be called when a module is created.
     *
     * This allows the module to be accessible via the ModuleRegister singleton.
     */
    void addToRegister();
    /**
     * @brief empty constructor for inheritance
     */
    ModuleBase():_isInit(0){}
    bool _isInit;
    std::string _name;
    std::vector<std::string> _deps;
};

/**
 * @brief The ModuleRegister class
 * @ingroup Modular
 * @see \ref Modular
 */
class ModuleRegister
{
    MOVE3D_STATIC_LOGGER;
protected:
    /**
     * @brief only public access is by singleton.
     * @see getInstance()
     */
    ModuleRegister(){}
public:
    ~ModuleRegister(){}
    /**
     * @brief access to the singleton.
     * @return
     *
     * is created on first call
     */
    static ModuleRegister *getInstance();
    /**
     * @brief add a module to the register
     * @param name
     * @param instance a pointer to the module
     */
    void add(std::string &name, ModuleBase *instance);
    /**
     * @brief mark a module as needed, to be initialized on \ref init()
     * @param name
     */
    void addToInitList(const std::string &name);
    /**
     * @brief initializes all modules added with addToInitList() and their dependencies.
     * @return true if all modules where initialized correctly
     */
    bool init(bool enable=true);
    /**
     * @brief initializes a module and its dependencies.
     * @param module pointer to an instance of the module
     * @return true if all modules where initialized correctly
     * @note it will check that the module is already registered, to enforce the registration of all modules.
     */
    bool init(ModuleBase *module, bool enable=true);
    /**
     * @brief initializes a module and its dependencies.
     * @param moduleName the name of the module.
     * @return true if all modules where initialized correctly
     */
    bool init(const std::string &moduleName, bool enable=true);
    /**
     * @brief enables/disables all modules marked with addToInitList.
     * @param enable true to enable, false to disable
     * @return true if all is in correct status (may fail on initialization)
     * It manages dependencies, so also initializes required modules.
     */
    bool enable(bool enable=true);
    /**
     * @brief enable a module and its dependencies.
     * @param module
     * @param enable true to enable, false to disable
     * @return true if all is in correct status (may fail on initialization)
     * It manages dependencies, so also initializes required modules.
     */
    bool enable(ModuleBase *module,bool enable=true);
    /**
     * @brief enable a module and its dependencies.
     * @param name
     * @param enable true to enable, false to disable
     * @return true if all is in correct status (may fail on initialization)
     * It manages dependencies, so also initializes required modules.
     */
    bool enable(const std::string &name,bool enable=true);
    /**
     * @brief modules list of modules registered.
     * @return
     */
    const std::map<std::string,ModuleBase *> &modules() const;
    /**
     * @brief get a module by name
     * @param name
     * @return a pointer to the instance given to add() or NULL.
     */
    ModuleBase *module(const std::string &name) const;

    /**
     * @brief print list of registered modules into a string.
     * @return a list of module names
     * Modules marked to be initialized are preceded by a star (*)
     * Modules initialized as dependencies are preceded by a plus (+)
     */
    std::string printModuleList() const;
protected:
    /**
     * @brief initRecursive initializes a module dependencies recursively, then the module itself
     * @param mod
     * @return true if all modules where initialized correctly
     */
    bool initRecursive(ModuleBase *mod,bool init=true, bool enable=false);
    /**
     * @brief disable a module and the modules depending on it, recursively
     * @param mod
     * @return
     */
    bool disableRecursive(ModuleBase *mod);

private:
    static ModuleRegister *_instance;
    std::map<std::string,ModuleBase*> _modules;
    std::vector<std::string> _toInit;
    std::map<std::string,std::vector<ModuleBase*> > _dependents;///< list of Modules depending on each module (for disabling)

};

} //namespace move4d
#endif // MODULEBASE_HPP
