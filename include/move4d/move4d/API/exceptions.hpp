#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <stdexcept>
#include <exception>

#include "move4d/API/exceptions/unknown_robot.hpp"

namespace move4d{
    class invalid_property_access_error :public std::logic_error
    {
    public:
        explicit invalid_property_access_error(const std::string &property):
            std::logic_error("attempt to access to a non-existing property: "+property)
        {}
    };
}

#endif // EXCEPTIONS_HPP
