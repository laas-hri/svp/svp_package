#ifndef API_COLLISIONINTERFACE_HPP
#define API_COLLISIONINTERFACE_HPP

#include <vector>
#include <memory>


namespace move4d
{
class Robot;
class RobotState;
class PointCloud;
typedef std::shared_ptr<PointCloud> PointCloudPtr;

namespace API {

class CollisionInterface
{
public:
    /**
     * @brief CollisionChecks defines which type of elements to test for collision.
     * Values can be cumulated using the bit-to-bit "```|```" operator (e.g ```CollisionChecks c=COL_ENV | COL_AGENTS;```)
     * * COL_ENV: collisions with static elements
     * * COL_HUMANS: collisions with humans (defined by Robot::getHriAgent())
     * * COL_ROBOTS: collisions with robots (defined by Robot::getHriAgent())
     * * COL_AGENTS: collisions with agents, i.e. robots or humans
     * * COL_OBJECTS: collisions with movable objects (i.e. not agents) defined by Robot::isAgent()==false
     * * COL_MOVABLE: everything except environment, i.e. agents and objects
     * * COL_ALL: everything that is defined as a collision object
     *
     * @note: Some collision space may define different types of collision. If that is the case, they may accept only COL_ALL (think about a point-cloud based collision space).
     */
    /*enum CollisionChecks {
        COL_NONE        =0b00000,
        COL_ENV         =0b00001,
        COL_SELF        =0b10000,
        COL_HUMANS      =0b00100,
        COL_ROBOTS      =0b00010,
        COL_AGENTS      =0b00110,
        COL_OBJECTS     =0b01000,
        COL_MOVABLE     =0b01110,
        COL_ALL_NOSELF  =0b01111,
        COL_ALL         =0b11111
    };*/
    enum CollisionChecks {
        COL_NONE        =0,
        COL_ENV         =1,
        COL_SELF        =16,
        COL_HUMANS      =4,
        COL_ROBOTS      =2,
        COL_AGENTS      =6,
        COL_OBJECTS     =8,
        COL_MOVABLE     =14,
        COL_ALL_NOSELF  =15,
        COL_ALL         =31
    };


    CollisionInterface(){}
    virtual ~CollisionInterface(){}

    /**
     * @brief check the current world state for collision, return true if no collision
     * @return false if there are collisions in the environment
     */
    virtual bool check(CollisionChecks type=COL_ALL);

    /**
     * @brief check if the given state collides
     * @param s a new state to apply to the environment and test
     * @return
     * update the world state with the given robot state, check
     * if the corresponding robot is in collision and return true if no collisions
     */
    virtual bool check(RobotState &s, CollisionChecks type=COL_ALL);

    /**
     * @brief as check(RobotState&) but limit collision test to the given robots
     * @param s
     * @param others list of robot to test collision against
     * @return
     */
    virtual bool check(RobotState &s, std::vector<Robot*> &others);

    /**
     * @brief shorthand for check(RobotState&,std::vector<Robot*>&) with one robot
     * @param s
     * @param other
     * @return
     */
    virtual bool check(RobotState &s, Robot *other);

    /**
     * @brief like check(RobotState&,CollisionChecks) but doesn't change the world state
     * @param r
     * @param type
     * @return
     */
    virtual bool check(Robot *r, CollisionChecks type=COL_ALL) = 0;
    virtual bool check(Robot *r, std::vector<Robot*> &others);
    virtual bool check(Robot *r, Robot *other) = 0;
    virtual bool check(Robot *r,int type){return this->check(r,static_cast<CollisionChecks>(type));}

    /**
     * @brief compute collisions and return list of colliding robot pairs
     * @param[out] collision_pairs the list of robot colliding
     * @param type
     * @return
     *
     * If a robot is colliding with the environment, there is a pair with it and a NULL Robot ptr.
     *
     * It is guaranteed that the first element of each pair is a valid robot pointer
     */
    virtual bool collisionList(std::vector<std::pair<Robot*,Robot*> > &collision_pairs, CollisionChecks type=COL_ALL) = 0;

    virtual bool collisionList(RobotState &s, std::vector<Robot*> &collisions, CollisionChecks type=COL_ALL);
    virtual bool collisionList(RobotState &s, std::vector<Robot*> &collisions, std::vector<Robot*> &others);
    /**
     * @brief collisionList
     * @param r
     * @param[out] collisions
     * @param type
     * @return
     */
    virtual bool collisionList(Robot *r, std::vector<Robot*> &collisions, CollisionChecks type=COL_ALL) = 0;
    /**
     * @brief collisionList
     * @param r
     * @param[out] collisions
     * @param others
     * @return
     */
    virtual bool collisionList(Robot *r, std::vector<Robot*> &collisions, std::vector<Robot*> &others);

    static bool isToCheck(CollisionInterface::CollisionChecks type, Robot *r);

    void addPointCloud(PointCloud &pc, CollisionInterface::CollisionChecks type);

protected:
    std::vector<std::pair<CollisionChecks,PointCloudPtr> > _pointClouds;
};

} // namespace API

} //namespace move4d
#endif // API_COLLISIONINTERFACE_HPP
