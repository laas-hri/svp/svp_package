#ifndef CYLINDERCOLLISION_HPP
#define CYLINDERCOLLISION_HPP

#include "move4d/API/Collision/collisionInterface.hpp"
#include <Eigen/Geometry>

namespace move4d {
namespace API {

/**
 * @brief The CylinderCollision class checks collision with attached cylinders when available.
 * The robot collision bodies are used if no cylinder is set.
 * This class is a wrapper around a "real" collision checker.
 */
class CylinderCollision : public CollisionInterface
{
public:
    /**
     * @brief CylinderCollision
     * @param collision_checker is the collision checker that will be used to actually perform the collision test
     */
    CylinderCollision(CollisionInterface *collision_checker);

    using CollisionInterface::check;
    using CollisionInterface::collisionList;
    virtual bool check(Robot *r, CollisionChecks type) override;
    /// @brief check but move the cylinder or the robot to the specified pos before
    virtual bool moveCheck(Robot *r,const Eigen::Vector3d &pos, CollisionChecks type);
    /// @brief check but move the cylinder or the robot to the specified pos before
    virtual bool moveCheck(Robot *r, const Eigen::Vector3d &pos, Robot *other, const Eigen::Vector3d &pos_other);
    /// @brief check but move the cylinder or the robot to the specified pos before
    virtual bool moveCheck(Robot *r, const Eigen::Vector3d &pos, Robot *other);
    /// @brief check but leave the cylinder of r in place (use it when the cylinder is already in place, the robot doesn't need to be)
    virtual bool checkCylinderInPlace(Robot *r,Robot *other);
    virtual bool check(Robot *r, Robot *other) override;
    virtual bool collisionList(std::vector<std::pair<Robot *, Robot *> > &collision_pairs, CollisionChecks type) override;
    virtual bool collisionList(Robot *r, std::vector<Robot *> &collisions, CollisionChecks type) override;

private:
    CollisionInterface *mInnerCollisionChecker;
};

} // namespace API
} // namespace move4d

#endif // CYLINDERCOLLISION_HPP
