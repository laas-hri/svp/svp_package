#ifndef API_P3DCOLLISION_HPP
#define API_P3DCOLLISION_HPP

#include "move4d/API/Collision/collisionInterface.hpp"

namespace move4d
{
namespace API {

class PqpCollision : public CollisionInterface
{
public:
    PqpCollision();

    using CollisionInterface::check;
    using CollisionInterface::collisionList;
    bool check(Robot *r, CollisionChecks type);
    bool check(Robot *r, Robot *other);
    bool collisionList(std::vector<std::pair<Robot *, Robot *> > &collision_pairs, CollisionChecks type);
    bool collisionList(Robot *r, std::vector<Robot *> &collisions, CollisionChecks type);

    /**
     * @brief add a PointCloud obstacle
     * @param pc the point cloud
     * @param type the type of the obstacle, used by other functions to know if the PointCloud has to be considered
     */
    void addPointCloud(PointCloud &pc, CollisionChecks type);

protected:
    bool checkPointClouds(Robot *r,CollisionChecks type);

};

} // namespace API

} //namespace move4d
#endif // API_P3DCOLLISION_HPP
