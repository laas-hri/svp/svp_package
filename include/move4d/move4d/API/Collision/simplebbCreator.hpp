#ifndef API_SIMPLEBBCREATOR_HPP
#define API_SIMPLEBBCREATOR_HPP

#include <move4d/API/moduleBase.hpp>
#include <Eigen/Geometry>
#include <vector>
#include "move4d/Logging/Logger.h"
#include "move4d/database/DatabaseInterface.hpp"

struct obj;
typedef struct obj p3d_obj;
struct p3d_poly;

namespace move4d
{
class Robot;

namespace API {

struct BoundingVolumes;

class SimpleBbCreator : public ModuleBase
{
    MOVE3D_STATIC_LOGGER;
public:
    struct Settings{
        Settings():nb_balls(0),nb_threads(0){}
        struct AlgoSettings{
            AlgoSettings():algo(0),maxeval(0),maxtime(0),ftol(0),xtol(0){}
            int algo;
            int maxeval;
            double maxtime;
            double ftol;
            double xtol;
        };
        std::vector<AlgoSettings> algorithms;
        int nb_balls;
        int nb_threads;
        AlgoSettings mainsettings;
    };

    static SimpleBbCreator *getInstance();
    SimpleBbCreator();
    virtual ~SimpleBbCreator();

    bool create(Robot *r);
    bool create(p3d_obj *o,bool isRobot, double &cost);
    bool create(p3d_poly *poly);

    void run();
    std::string getName();
    static std::string name() {return "SimpleBbCreator";}
    std::vector<std::string> getModuleDependencies();

    static void draw();
private:
    static SimpleBbCreator *_instance;

    BoundingVolumes *_bv;
    Settings _settings;
};

class SimpleBbReader : public DatabaseInterface
{
    MOVE3D_STATIC_LOGGER;
public:
    SimpleBbReader(BoundingVolumes *bv);
    virtual ~SimpleBbReader();

    using DatabaseInterface::fetchData;
    bool fetchData();
    bool parseJson(const Json::Value &root, const std::string &name);
    bool write(Robot *r, const std::string &path, double cost);
    SimpleBbCreator::Settings getSettings();

private:
    BoundingVolumes *_bv;
    SimpleBbCreator::Settings *settings;
    Robot *_current_robot;
};

} // namespace API

} //namespace move4d
#endif // API_SIMPLEBBCREATOR_HPP
