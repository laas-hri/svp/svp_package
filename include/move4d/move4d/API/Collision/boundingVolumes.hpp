#ifndef API_BOUNDINGVOLUMES_HPP
#define API_BOUNDINGVOLUMES_HPP

#include <Eigen/Geometry>
#include <map>
#include <vector>
#include <stdexcept>

struct obj;
typedef struct obj p3d_obj;

namespace move4d
{
namespace API{

typedef Eigen::Vector3d Point;
struct Ball{
    Ball():r(0){c.setZero();}
    Ball(const Point &center,double radius):
        c(center),r(radius)
    { }
    Ball(std::vector<double>::const_iterator &begin, std::vector<double>::const_iterator &end)
    {
        if(end-begin != 4){
            throw std::invalid_argument("must be a list of 4 doubles to create a Ball");
        }
        c[0]=*begin;
        c[1]=*(begin+1);
        c[2]=*(begin+2);
        r=*(begin+3);
    }
    Point c; double r;
};
struct AxisAlignedBB
{
    AxisAlignedBB() = default;
    AxisAlignedBB(const Point &minpoint,const Point &maxpoint):minpoint(minpoint),maxpoint(maxpoint){}
    explicit AxisAlignedBB(const p3d_obj *o);
    Point minpoint,maxpoint;
};

struct BoundingVolumes
{
public:
    typedef std::pair<p3d_obj*,std::vector<Ball> > ObjectBalls;
    const std::vector<Ball> &get(p3d_obj* o) const;
    bool has(const p3d_obj * o) const;
    void set(const p3d_obj *o,const std::vector<Ball> &balls);
    void add(const p3d_obj *o,const Ball &ball);
    const std::vector<std::vector<std::vector<Ball> > > &getAll() const;
protected:
    std::vector<std::vector<std::vector<Ball> > > _robot_object_balls;
};

}



} //namespace move4d
#endif // API_BOUNDINGVOLUMES_HPP

