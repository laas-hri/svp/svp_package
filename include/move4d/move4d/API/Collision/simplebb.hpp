#ifndef API_SIMPLEBB_HPP
#define API_SIMPLEBB_HPP

#include "move4d/API/Collision/collisionInterface.hpp"
#include <Eigen/Geometry>

struct obj;
struct BB;

namespace move4d
{
namespace API {
struct BoundingVolumes;
struct Ball;
struct AxisAlignedBB;

class SimpleBB : public API::CollisionInterface
{
public:
    SimpleBB();
    static SimpleBB *instance();

    bool check(Robot *r, CollisionChecks type);
    bool check(Robot *r, Robot *other);
    bool collisionList(std::vector<std::pair<Robot *, Robot *> > &collision_pairs, CollisionChecks type);
    bool collisionList(Robot *r, std::vector<Robot *> &collisions, CollisionChecks type);

    bool check(obj *o1, obj *o2);
    bool checkEnv(Robot *r);

    /**
     * @brief distance
     * @param r
     * @param other
     * @return [0,+inf]
     * all distance methods return a non-negative value
     */
    double distance(Robot *r, Robot *other);
    double distance(Robot *r, CollisionChecks type);///< returns the distance to the closest object matching type
    double distance(obj *o1,obj *o2);
    double distance(const std::vector<Ball> &balls1, const Eigen::Affine3d &pos1, const std::vector<Ball> &balls2, const Eigen::Affine3d &pos2);
    double distance(const Ball &ball1, const Eigen::Affine3d &pos1, const Ball &ball2, const Eigen::Affine3d &pos2);


    /**
     * @brief signedDistance
     * @param r
     * @param other
     * @return minimal signed distance between robot, i.e. minimal distance if not in collision, or maximal penetration if in collision (negative value)
     */
    double signedDistance(Robot *r, Robot *other);
    double signedDistance(obj *o1,obj *o2);
    double signedDistance(const std::vector<Ball> &balls1, const Eigen::Affine3d &pos1, const std::vector<Ball> &balls2, const Eigen::Affine3d &pos2);
    double signedDistance(const Ball &ball1, const Eigen::Affine3d &pos1, const Ball &ball2, const Eigen::Affine3d &pos2);

    double sqDistance(const Eigen::Vector3d &point, const AxisAlignedBB &aabb) const;
    double signedDistance(const Ball &ball, const Eigen::Affine3d &ballPos, const AxisAlignedBB &aabb) const;
    double signedDistance(const std::vector<Ball> &balls, const Eigen::Affine3d &ballPos, const AxisAlignedBB &aabb) const;
    double signedDistanceAABB(const BB &bb1, const BB &bb2) const;

    BoundingVolumes const * getBoundingVolumes() const {return _bv;}

private:
    BoundingVolumes *_bv;
};

} // namespace API

} //namespace move4d
#endif // API_SIMPLEBB_HPP
