/*
 *  compco.h
 *  BioMove3D
 *
 *  Created by Jim Mainprice on 27/04/10.
 *  Copyright 2010 CNRS/LAAS. All rights reserved.
 *
 */
#ifndef COMPCO_HPP
#define COMPCO_HPP

#include "move4d/API/ConfigSpace/RobotState.hpp"

#include <vector>

#ifndef _ROADMAP_H
struct compco;
#endif

namespace move4d
{
class Node;
class Graph;

class ConnectedComponent
{
public:
	//! Out of use!!!
	ConnectedComponent(Graph * G, compco * Comp);

	//! Constructor.
	ConnectedComponent(Graph * G, Node * N);

	//! Destructor.
	~ConnectedComponent() {
		free(m_Compco);
	}

	//! Get the Connected Component structure.
	compco * getCompcoStruct() const {
		return m_Compco;
	}

	//! Get the vector of nodes.
	std::vector<Node *> & getNodes() {
		return m_Nodes;
	}
	
	//! Get the ith node.
	Node * getNode(unsigned i) const {
		return m_Nodes[i];
	}
	
	//! Get the number of nodes in the connected component.
	unsigned getNumberOfNodes() const {
		return m_Nodes.size();
	}

	//! Get the lower bounds of the bounding box.
	const std::vector<double> & getLowerBBox() const {
		return bBoxMin;
	}

	//! Get the upper bounds of the bounding box.
	const std::vector<double> & getUpperBBox() const {
		return bBoxMax;
	}

	//! Get the volume of the bounding box.
	double getBBoxVolume() const {
		return bBoxVolume;
	}

	//! Get the id of the connected component.
	unsigned getId() const;

	//! Get the number of refinement nodes.
	unsigned getNumberOfRefinementNodes() const;

	//! Add the given number to the current number of refinement nodes.
	void updateNumberOfRefinementNodes();

	/**
	 * Add a node to the connected component.
	 * @param N the node to be added
	 */
	void addNode(Node * N);

	//! Remove the given node.
	void removeNode(Node * node) {
		m_Nodes.erase(std::find(m_Nodes.begin(), m_Nodes.end(), node));
	}

	/**
	 * Add the compco to the reachable Compco
	 */
	void addToReachableList(ConnectedComponent* Comp);
	
	/**
	 * Add the compco to the reachable Compco and update predecessors
	 */
	void addToReachableListAndUpdatePredecessors(ConnectedComponent* Comp);

	/**
	 * Merge CompcoPt with this compco and delete it 
	 * @param Pointer to the Connected component that will be freed
	 */
	void mergeWith(ConnectedComponent* CompcoPt);
	
	/**
	 * Can reach the compco
	 */
	bool isLinkedToCompco(ConnectedComponent* CompcoPt);
	
	//! Return the node whose configuration is the closest (in the C-space) to the given configuration.
    Node * nearestNeighbor(statePtr_t conf, bool backward=false);

	//! Return the k nodes whose configurations are the closest (in the C-space) to the given configuration.
	std::vector<Node *> kNearestNeighbors(unsigned k, statePtr_t conf);

	/**
	 * Return the nodes whose configurations are within a ball (in the C-space) having the given radius
	 * and centered on the given configuration.
	 */
	std::vector<Node *> neighborsInBall(statePtr_t conf, double radius);

	/**
	 * Nearest weithed Neigboor
	 */
	Node* nearestWeightNeighbour(std::shared_ptr<RobotState> q, bool weighted, int distConfigChoice);

	/**
	 * K Nearest Weighted Neighbors in connected component.
	 * Return the K Nearest Neighbors of the configuration q in the connected component within a minimum radius.
	 */
    std::vector<Node*> KNearestWeightNeighbour(statePtr_t config, int K, double radius, bool weighted,
											   int distConfigChoice);

    /**
	 * Search configuration in connected compco
	 */
	Node* searchConf(RobotState & q);

	//! Return a node, randomly chosen in the connected component.
	Node* randomNode();

	//! Get the temperature of the component.
	double getTemperature();

	//! Set the temperature of the component.
	void setTemperature(double temp);
	
private:
	compco*			m_Compco;
	Graph*			m_Graph;
	
	int m_Id;
	
	std::vector<Node*> m_Nodes;
	std::vector<ConnectedComponent*> m_CanReach;
	
	//! bounding box
	std::vector<double> bBoxMin;
	std::vector<double> bBoxMax;
	double bBoxVolume;
};
} //namespace move4d
#endif
