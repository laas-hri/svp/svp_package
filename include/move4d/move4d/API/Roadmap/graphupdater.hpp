#ifndef GRAPHUPDATER_HPP
#define GRAPHUPDATER_HPP

#include "move4d/API/Roadmap/node.hpp"
#include "move4d/API/Roadmap/edge.hpp"
#include "move4d/API/Trajectory/trajectory.hpp"
#include "move4d/API/Roadmap/graph.hpp"
#include <vector>
#include <set>


namespace move4d
{
class GraphUpdater
{
public:
  
	//contructors and destructor
	GraphUpdater(Graph * graph);

	double reduction(unsigned int param);

	bool connexity(unsigned int param);
	bool coverage(unsigned int param);

    bool correction(unsigned int param, double timelimit = -1);
    bool correction(Node* from, unsigned int param, double timelimit = -1);
    bool correction(statePtr_t from, unsigned int param, double timelimit = -1);

    std::vector<std::vector<Node*> > getAvailableTrajectories(statePtr_t from, bool computeNew = false);
    std::vector<std::vector<Node*> > getAvailableTrajectories(Node* from, bool computeNew = false);

    API::GeometricPath* getBestTrajectory(statePtr_t from, bool updateEnv = true);
    API::GeometricPath* getBestTrajectory(Node* from, bool updateEnv = true);

    std::vector<API::GeometricPath*> getTrajectories();

    void setCurrentTrajectory(API::GeometricPath* trajectory);
    API::GeometricPath* getCurrentTrajectory();

    void setCurrentConfiguration(statePtr_t configuration);
    Node* getPrevNode();
    Node* getNextNode();

private:
	Graph* graph;
	Node* start;
	Node* goal;

    API::GeometricPath* currentTrajectory;
    statePtr_t currentConfiguration;
    Node* prevNode;
    Node* nextNode;

    std::vector< std::vector<Node*> > trajectories;

	void updateTrajectories();
	std::vector<Node*> getChildrenOf(Graph* graph, std::vector<Node*> nodes);
    std::vector<Node*> getParentOf(Graph* graph, std::vector<Node*> nodes);
    unsigned int connectNodes(std::vector<Node*> nodes);
};

extern GraphUpdater* API_activeGraphUpdater;

} //namespace move4d
#endif
