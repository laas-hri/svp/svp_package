#ifndef GRAPH_HPP
#define GRAPH_HPP

#include "move4d/API/Roadmap/node.hpp"
#include "move4d/API/Roadmap/edge.hpp"
#include "move4d/API/Trajectory/trajectory.hpp"

#include <map>

#ifndef _ROADMAP_H
struct node;
struct edge;
struct graph;
struct list_node;
struct list_edge;
#endif

namespace move4d
{
#ifndef COMPCO_HPP
class ConnectedComponent;
#endif

/**
 @ingroup ROADMAP
 \brief Classe représentant un Graph pour un Robot
 @author Florian Pilardeau,B90,6349 <fpilarde@jolimont>
 */

class Graph
{
public:
  
	//contructors and destructor
	Graph() {}
	Graph(Robot* R, graph* ptGraph);
	Graph(Robot* R);
	Graph(graph* G);
	Graph(const Graph& G);
	~Graph();
  
	//! return the value of the Change flag
 	//! @return true if the graph has change since last export
	bool isGraphChanged() { return m_graphChanged; }
	
  	//! Creates a p3d_graph from Boost Graph
  	//graph* exportCppToGraphStruct(bool deleteGraphStruct = false);
	
	//Accessors
	/**
	 * obtient la structure p3d_graph stockée
	 * @return la structure p3d_graph stockée
	 */
	graph* getGraphStruct() const;
	
	/**
	 * stocke la structure p3d_graph
	 * @param G la structure à stocker
	 */
	void setGraph(graph* G);
	
	/**
	 * obtient le Robot pour lequel le Graph est créé
	 * @return le Robot pour lequel le Graph est créé
	 */
	Robot* getRobot() const;
	
	/**
	 * sets the Robot of the Graph
	 */
	void setRobot(Robot* R);
	
  	/**
	 * obtient la table des noeuds du Graph
	 * @return la table des noeuds du Graph
	 */
	std::map<node*, Node*> getNodesTable();
	
	/**
	 * obtient le nombre de Node dans le Graph
	 * @return le nombre de Node dans le Graph
	 */
	unsigned int getNumberOfNodes();
  
	/**
	 * obtient le vecteur des Node du Graph
	 * @return le vecteut des Node du Graph
	 */
	std::vector<Node*> getNodes() const;
        std::vector<Node*> getSortedNodes() const;
        std::vector<Node*> getNotTrajNodes() const;
	
	/**
	 * Get Node ith node in Graph
	 */
	Node* getNode(unsigned int i) const { return m_Nodes[i]; } 
	
	/**
	 * obtient le nombre de Node dans le Graph
	 * @return le nombre de Node dans le Graph
	 */
	unsigned int getNumberOfEdges() const;
	
	/**
	 * obtient le vecteur des Edge du Graph
	 * @return le vecteur des Edge du Graph
	 */
	std::vector<Edge*> getEdges() const;

    std::vector<Edge*> getEdges(std::shared_ptr<RobotState> q, bool from=false);
    std::vector<Edge*> getEdges(Node* nq,bool from=false);
	
	/**
	 * Returns the ith edge
	 */
	Edge* getEdge(unsigned int i)  const { return m_Edges[i]; } 
	
	/*
	 * Get the number of compco in the grap
	 */
	unsigned int getNumberOfCompco() const;
  
  	/**
	 * Get Compco
	 */
	ConnectedComponent* getConnectedComponent(int ith) const;
	
	/**
	 * Get Compcos
	 */
	std::vector<ConnectedComponent*> getConnectedComponents() const;
	
	/**
	 * obtient le Node correspondant au p3d_node
	 * @param N le p3d_node
	 * @return le Node correspondant; NULL si le Node n'existe pas
	 */
	Node* getNode(node* N);
	
	/**
	 * obtient le dernier Node ajouté au Graph
	 * @return le dernier Node ajouté au Graph
	 */
	Node* getLastNode();
	
	/**
	 * obtient le nom du Graph
	 * @return le nom du Graph
	 */
	std::string getName();
	
  	/**
	 * modifie le nom du Graph; un nom standard est alors mis
	 */
	void setName();
  
	/**
	 * modifie le nom du Graph
	 * @param Name le nouveau nom
	 */
	void setName(std::string Name);
	
	/**
	 * teste si deux Graph sont égaux en comparant leur listNode
	 * @param G le Graph à comparer
	 * @return les deux Graph sont égaux
	 */
	bool equal(Graph* G);
  
	/**
	 * teste si deux Graph ont le même nom
	 * @param G le Graph à comparer
	 * @return les deux Graph ont le même nom
	 */
	bool equalName(Graph* G);
	
	/**
	 * vérifie si un node est dans le Graph
	 * @param N le Node à tester
	 * @return le Node est dans le Graph
	 */
	bool isInGraph(Node* N);
	
	/**
	 * Looks if two Node make an edge in the Graph
	 * @param N1 the first node
	 * @param N2 the second node
	 * @return true if the node is the graph
	 */
	Edge* isEdgeInGraph(Node* N1,Node* N2);
	
	/**
	 * Gets the node that corresponds to a configuration
	 * @param q la RobotState recherchée
	 * @return le Node s'il existe, NULL sinon
	 */
	Node* searchConf(RobotState & q);
	
	/**
	 * New Compco 
	 * @param node to create a compco for
	 */
	void createCompco(Node* node);
	
	/**
	 * insert un Node dans le Graph
	 * @param node le Node à insérer
	 * @return le Node a été inséré
	 */
	Node* insertNode(Node* node);
	
	
	/**
	 * insert un Node de type ISOLATE
	 * @param node le Node à insérer
	 * @return le Node a été inséré
	 */
	Node* insertExtremalNode(Node* node);
	
	//! Add the given node to the storing structure of the graph (without creating any link).
	void addNode(Node * node);

	/**
	 * \brief Link a RobotState to a Node for the RRT Planner
	 * @param q the RobotState to link
	 * @param expansionNode the Node
	 * @param currentCost the current cost to reach the Node
	 * @param step the max distance for one step of RRT expansion
	 * @return the inserted Node
	 */
	Node* insertNode(Node* expansionNode, API::LocalPathBase& path);
	
	/**
	 * Link a RobotState to a Node for the RRT Planner
	 * @param q the RobotState to link
	 * @param from the Node
	 * @return the linked Node
	 */
	Node* insertConfigurationAsNode(std::shared_ptr<RobotState> q, Node* from, double step );
	
	/**
	 * trie les Edges en fonction de leur longueur
	 */
	void sortEdges();
	
    /**
     * trie les Nodes en fonction de leur distance à la position du robot via la trajectoire courante
     */

    void updateSortedNodes(Node* node);
    Node* getRandomNodeNearTraj();
    void updateSepareNodes(Node* n);
    void separateNodes();
    std::map<double, Node*> getNodesOnTraj(Node* near,bool neighbour = false);
    std::vector<Node*> getKNodesOnTraj(Node* near, uint k);
    void sortNodes(double init = -1);

	/**
	 * ajoute un Edge au Graph
	 * @param N1 le Node initial de l'Edge
	 * @param N2 le Node final de l'Edge
	 * @param Long la longueur de l'Edge
	 * NB: Obsolete. Should be replaced.
	 */
	Edge * addEdge(Node * source, Node * target, bool compute_length = true, double length = 0.,
			bool compute_cost = true, double cost = 0.);

	/**
	 * Add an edge to the graph, going from the given source to the given target.
	 * @param path: local path between the source and the target
	 * @return the created edge
	 */
	Edge * addEdge(Node * source, Node * target, API::localPathBasePtr_t path);

	/**
	 * ajoute deux Edge au Graph
	 * @param N1 l'une des extrémités des Edge
	 * @param N2 l'autre extrémité des Edge
	 * @param Long la longueur des Edge
	 * NB: Obsolete. Should be replaced.
	 */
	void addEdges(Node * N1, Node * N2, bool computeLength = true, double length = 0.,
			bool computeCost = true, double cost = 0.);
	
	/**
	 * Add two opposite edges between the nodes N1 and N2.
	 * @param N1: node already present in the graph and belonging to a connected component
	 * @param N2: new node that has to be linked to an existing node,
	 * 			or existing node that has to be moved to another connected component
	 * @param path: local path between N1 and N2
	 * @param addParent if true add parent edge
	 */
	void addEdges(Node * N1, Node * N2, API::localPathBasePtr_t path, bool addParent = false);

    /**
     * Add the oriented edge N1->N2.
     * @param N1: node
     * @param N2: node
     * @param path: local path from N1 to N2
     */
    void addOrientedEdge(Node * N1, Node * N2, API::localPathBasePtr_t path);

	/**
	 * Remove edge
	 */
	void removeEdge(Edge* E);

	/**
	 * Remove the edges between the nodes N1 and N2
	 */
	void removeEdges( Node* N1, Node* N2 );
	
	/**
	 * Sort nodes against one given node
	 * @param N le Node de référence
	 */
	void sortNodesByDist(Node* N);
  
  	/**
	 * Sort nodes against one given configuration
	 * @param N le Node de référence
	 */
  	void sortNodesByDist(statePtr_t q);

	/**
	 * ajoute un Node s'il est à moins d'une distance max du Graph
	 * @param N le Node à ajouter
	 * @param maxDist la distance max
	 */
	void addNode(Node* N, double maxDist);

	/**
	 * Ajout dans le graph avec un voisinage de k
	 * @param N le Node à ajouter
	 * @param k nombre de voisins
	 */
	void addNode(Node* N, int k, bool kino);


	/**
	 * ajoute des Node s'ils sont à moins d'une distance max du Graph
	 * @param N les Node à ajouter
	 * @param maxDist la distance max
	 */
	void addNodes(std::vector<Node*> N, double maxDist);

	//! Remove the given node from the graph.
	void removeNode(Node * node);

	/**
	 * lie un Node au Graph
	 * @param N le Node à lier
	 * @return le Node est lié
	 */
	bool linkNode(Node* N);
  
  	/**
   	 * Links a node to a compco with multisol
   	 */
  	bool linkNodeCompMultisol(Node *N, ConnectedComponent* compPt);
	
	/**
	 * Lie un node au Graph sans prendre en compte la distance max
	 * @param N le Node à lier
	 * @return le Node est lié
	 */
	bool linkNodeWithoutDist(Node* N);
	
	/**
	 * Lie un Node au Graph en prenant en compte la distance max
	 * @param N le Node à lier
	 * @return le Node est lié
	 */
	bool linkNodeAtDist(Node* N);
	
	/**
	 * lie un Node à tous les autres Nodes visibles
	 * @param N le Node à lier
	 * @return le Node est lié
	 */
	bool linkToAllNodes(Node* N);
	
	
	/**
	 * Function that computes if 
	 * a node is linked to another node
	 * @return true of the nodes can be linked
	 * @return the distance between the nodes
	 */
	bool areNodesLinked(Node* node1, Node* node2, double & dist);
	
	/**
	 * Link 2 nodes and merge them
	 */
	bool linkNodeAndMerge(Node* node1, Node* node2);
	
	/**
	 * crée des Node à des Configurations aléatoires
	 * @param NMAX le nombre de Node à crér
	 * @param (*fct_stop)(void) la fonction d'arret
	 * @param (*fct_draw)(void) la fonction d'affichage
	 */
	void createRandConfs(int NMAX);
	
	/**
	 * Gets Random node in the connected component
	 */
	Node* randomNodeFromComp(Node* comp);
	
	/**
	 * Function that trys to connect 
	 * a node to a given connected component
	 */
	bool connectNodeToCompco(Node* node, Node* compco);
  
  
  	/**
	 * Compute the K nearest nodes
	 * @param K the maximal number of neighbors
	 */
  	static void sortNodesByDist(std::vector<Node*>& nodes, statePtr_t config);
	
    /**
     * K Nearest Weighted Neighbors in graph.
     * Return the K Nearest Neighbors of the configuration q in the entire graph within a minimum radius.
     */
    std::vector<Node*> KNearestWeightNeighbour(statePtr_t q, int K, double radius, bool weighted,
											   int distConfigChoice);

	std::vector<Node *> kNearestNeighbours(unsigned k, statePtr_t conf, bool kino = false);

	/**
	 * Return the nodes whose configurations are within a ball (in the C-space) having the given radius
	 * and centered on the given configuration.
	 */
	std::vector<Node *> neighborsInBall(statePtr_t conf, double radius);
	
	/**
	 * obtient le plus proche voisin d'une composante connexe
	 * @param compco la composante connexe dans laquelle on cherche le Node
	 * @param C la RobotState dont on veut determier le plus proche voisin
	 * @param weighted
	 * @param distConfigChoice le type de calcul de distance
	 * @return le Node le plus proche de la RobotState appartenant à la composante connexe
	 */
	Node* nearestWeightNeighbour(Node* compco, statePtr_t C, bool weighted, int distConfigChoice);
	
	/**
	 * Get Ith Compco
	 */
	Node* getCompco(unsigned int ith);
	
	/**
	 * Vector of nodes in the same compco
	 */
	std::vector<Node*> getNodesInTheCompCo(Node* node);
	
	/**
	 * ajoute des Edges formant des cycles dans le Graph
	 * @param node le nouveau Node ajouté
	 * @param step la distance d'expansion
	 */
	void addCycles(Node* node, double step);

	//! Merge the connected components to which N1 and N2 belong.
	void mergeComponents(Node * N1, Node * N2);

	/*fonction mergeant les compco N1 et N2*/
	/**
	 * merge deux composantes connexes
	 * @param CompCo1 la première composante connexe
	 * @param CompCo2 la seconde composante connexe
	 * @param DistNodes la distance entre les deux composantes connexes
	 * @return les deux composantes sont mergées
	 */
	int mergeComp(Node* CompCo1, Node* CompCo2, double DistNodes, bool compute_edge_cost);
	
	/**
	 * Rebuild the connected component
	 * using the strongly connected component algo from the BGL
	 */
	int rebuildCompcoFromBoostGraph();
	
	/**
	 * teste si des composantes connexes doivent être merger et le fait le cas échéant
	 */
	void mergeCheck();
	
	/**
	 * Delete the compco
	 */
	void removeCompco(ConnectedComponent* CompCo);
	
	/**
	 * Check C and C++ Connected Components
	 */
	bool checkConnectedComponents();
	
	/**
	 * Recompute the Graph cost (Edges and Nodes)
	 */
	void recomputeCost();
	
	/**
	 * Recompute All Edges Valid
	 */
	bool checkAllEdgesValid();
    
	/**
	 * Extract best traj from qi that is closest to q_f
	 */
	API::GeometricPath* extractBestTrajSoFar(statePtr_t qi, statePtr_t qf);
  
	/**
	 * Extract best vector of nodes from qi that is closest to q_f
	 */
	std::pair<bool, std::vector<Node*> > extractBestNodePathSoFar(statePtr_t qi, statePtr_t qf);

	//! Return a trajectory (i.e. the shortest path) from initN to endN, using parenthood in a tree.
	API::GeometricPath * extractTreeShortestPath(Node * initN, Node * endN);

  	//! Compute the lengths of all the shortest paths having for source the node n, using Dijkstra.
	std::vector<double> computeDijkstraShortestPaths(Node * n);
	std::vector<double> computeDijkstraShortestPaths(Node * n, std::map<double, Node *> to);

	//! Return the shortest path (as a list of nodes) from initN to endN, using A*.
	std::vector<Node *> extractAStarShortestNodePaths(Node * initN, Node * endN);
    std::vector<Node *> extractAStarShortestNodePaths(std::vector<Node *> passingBy);

	//! Return the shortest path (as a list of nodes) from initC to endC, using A*.
	std::vector<Node *> extractAStarShortestNodePaths(statePtr_t initC, statePtr_t endC);

	//! Return a trajectory (i.e. the shortest path) from initN to endN, using A*.
	API::GeometricPath * extractAStarShortestPath(Node * initN, Node * endN);

	//! Return a trajectory (i.e. the shortest path) from initC to endC, using A*.
	API::GeometricPath * extractAStarShortestPath(statePtr_t initC, statePtr_t endC);

    /** Return the lenght of the shortest path (using A*) in the graph between initC and endC
        Return P3D_HUDGE if no such path exists
      */
    double getAStarShortestPathLength(statePtr_t initC, statePtr_t endC);

	//! Extract best traj.
	API::GeometricPath * extractBestTraj(statePtr_t qi, statePtr_t qf);

	//! Init Motion planning problem.
	void initMotionPlanning(node* start, node* goal);
	
	// BGL functions
	BGL_Vertex findVertexDescriptor(Node* N);
	BGL_Edge findEdgeDescriptor(Edge* E);
	void saveBGLGraphToDotFile(const std::string& filename);
	BGL_Graph& get_BGL_Graph() { return m_BoostGraph; }

	void draw();

    // Used for directed graphs
    void resetConnections();
    void unflagAll();
    unsigned int getNbScc();
    std::vector<std::vector<Node*> > getStronglyConnectedComponents();
    std::vector<double> sortSCC(unsigned int id,Node *N1,bool forward);
    void addScc(Node *node);
    bool areSccConnected(unsigned int i,unsigned int j);
    void connectScc(unsigned int i,unsigned int j);
    bool mergeScc(unsigned int i,unsigned int j);

	/**
	  * @brief Find all nodes within a radius and the closest node.
	  * @param node The query node.
	  * @param nearests The returned close nodes.
	  * @param dist The corresponding distances to the query node.
	  * @param deltaNear The radius to search within.
	  * @param kino use state if true, use conf otherwise
	  * @return The number of nodes returned.
	 */
	unsigned long findDeltaCloseAndClosest(statePtr_t node, std::vector<Node *> &nearests, double deltaNear, bool kino = true);

	/**
	 * @brief return the nearest node in neighbors of the nearest one in the sampled nodes.
	 * @details If nb node of graph is inf to nbNodes then nbSample = nbNodeInGraph / divideA. Otherwise nbSample = offset + nbNodeInGraph / divideB
	 * @param node The query node.
	 * @param dist The resulting distance between the closest point and the query point.
 	 * @param kino use state if true, use conf otherwise
 	 * @param nbNodes Nb node limit.
	 * @param divideA If nb node of graph is inf to nbNodes
	 * @param offset If nb node of graph is sup to nbNodes
	 * @param divideB If nb node of graph is sup to nbNodes
	 * @return The closest node.
	 */
	Node* getNearestLocalMinimal(statePtr_t node, double &dist, bool kino = true, unsigned nbNodes = 1000, int divideA = 5, int offset = 200, int divideB = 500);

	/**
	 * @brief return the nearest node.
	 * @param node The query node.
	 * @param dist The resulting distance between the closest point and the query point.
	 * @param kino use state if true, use conf otherwise
	 * @return The closest node.
	 */
	Node* getNearest(statePtr_t state, double &dist, bool kino = true);

	/**
	  * Remove neighbors of the node
	  * @param node The query node.
	  */
	void removeNeighbors(Node *node);
	
	/**
	  * Find the k closest nodes to the query point. This is performed using a graph search starting from sqrt(nr_points) random points.
	  * @brief Find the k closest nodes to the query point.
	  * @param node The query state.
	  * @param k The number to return.
	  * @return The vector of nodes actually returned.
	 */
	std::vector<Node *> findKClose(Node* node, unsigned k, bool kino);
private:
  
  void init();

	// BGL functions
	void initBGL();
	void unsetDescriptors();
	void drawEdge(BGL_Vertex v1, BGL_Vertex v2);
	void drawNode(BGL_Vertex v);

	void deleteGraphStruct();
	void updateCompcoFromStruct();
	void freeResources();
	static bool compareEdges(Edge* E1, Edge* E2);
	static bool compareNodes(Node* N1, Node* N2);

	// Old p3d graph
	graph* m_Graph;
	Robot* m_Robot;

	// Boost Graph Lib 
	BGL_Graph m_BoostGraph;

	// Flag that is set to false 
	// each time the graph is exported to p3d_graph
	bool m_graphChanged;bool m_initBGL;

	std::vector<Node*> m_Nodes;
    std::vector<Node*> m_SortedNodes;
    std::vector<Node*> m_tempTrajNodes;
    std::vector<Node*> m_TrajNodes;
    std::vector<Node*> m_NotTrajNodes;    
    std::vector<Node*> m_NeighbourNodes;    
    
	std::vector<Edge*> m_Edges;
	std::vector<ConnectedComponent*> m_Comp;

	// Maps between old and new nodes
	std::map<node*, Node*> m_NodesTable;

	// Graph name
	std::string m_Name;

    std::map<Node*,bool> m_UpdateList;

    // list of the strongly connected components
    std::vector<std::vector<Node*> > m_StronglyConnectedComponents;
    // adjacency matrix of the transitive closure of the graph of the strongly connected components
    std::vector<std::vector<bool> > m_Scc_TransitiveClosure;

};

extern Graph* API_activeGraph;

} //namespace move4d
#endif
