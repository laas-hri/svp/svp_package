#ifndef NODE_HPP
#define NODE_HPP

#include <memory>

#include "move4d/API/ConfigSpace/RobotState.hpp"
#include "move4d/API/ConfigSpace/localpath.hpp"
#include "move4d/planner/planEnvironment.hpp"
#include "p3d/env.hpp"

#ifndef _ROADMAP_H
struct node;
struct edge;
struct compco;
#endif
 
#include "move4d/API/Roadmap/BGL_Graph.hpp"
#include "move4d/API/Roadmap/edge.hpp"

namespace move4d
{
#ifndef EDGE_HPP
class Edge;
#endif

#ifndef COMPCO_HPP
class ConnectedComponent;
#endif

class Witness;
class Graph;

class NodeData 
{
  public :
  // Constructor & Destructor
  NodeData();
  virtual ~NodeData();
};

/**
 @ingroup ROADMAP
 \brief Classe représentant un Node d'un Graph
 @author Florian Pilardeau,B90,6349 <fpilarde@jolimont>
 */
class Node
{	
public:
	
  //Constructor and destructor
	Node();
	/**
	 * Constructeur de la classe
	 * @param G le Graph pour lequel le Node est créé
	 * @param C la RobotState stockée dans le Node
   * @param id the number in the graph (Graph->getNumberOfNodes)
	 */
	Node(Graph* G, std::shared_ptr<RobotState> C, bool newCompco=true);
	
	/**
	 * Constructeur de la classe
	 * @param G le Graph pour lequelle Node est créé
	 * @param N la structure de p3d_node qui sera stockée
	 */
	Node(Graph* G, node* N);
	
	/**
	 * destructeur de la classe
	 */
	~Node();
	
	/**
	 * Deletes the p3d node without deleting the 
	 * configuration allready stored in a std::shared_ptr
	 */
	void deleteNode();
	
	/**
	 * Equal operator
	 */
	bool operator==(Node& N);
	
  //Accessors
	/**
	 * obtient la structure p3d_node
	 * @return la structure p3d_node
	 */
	node* getNodeStruct();
	
	/**
	 * obtient le Graph pour lequel le Node a été créé
	 * @return le GRaph pour lequel le Node a été créé
	 */
	Graph* getGraph();
	
	/**
	 * obtient le Robot pour lequel le Node a été créé
	 * @return le Robot pour lequel le Node a été créé
	 */
	Robot* getRobot();
	
	/**
	 * obtient la RobotState stockée
	 * @return la RobotState stockée
	 */
	statePtr_t getConfiguration();
	
	/**
	 * modifie la valeur de activ
	 * @param b la nouvelle valeur de activ
	 */
	void activ(bool b);
  
	/**
	 * obtient la valeur de activ
	 * @return la valeur de activ
	 */
	bool isActiv();
	
	/**
	 * Returns the node id
	 */
	unsigned int getId() const;
	
	/**
	 * Sets the Id
	 */
	void setId(unsigned int id);
	
	/**
	 * returns a reference to the node parent when in a tree like structure
	 * the pointer is editable
	 */
	Node*& parent() { return m_parent; }
  
  /**
	 * returns a reference to the if leaf data
	 * the pointer is editable
	 */
	bool& isLeaf() { return m_is_leaf; }
	
	/**
	 * obtient le cout du Node
	 * @return le cout du Node
	 */
	double cost();
	
	/**
	 * Returns the Sum of cost along the path
	 */
	double& sumCost(bool recompute=false);

	/**
	 * Returns the cost along the path
	 * @param kino compute cost along a kinodynamic path
	 * @param recompute if true, recompute the cost
	 * @param parent if set, use the aggregation cost from this node
	 * @param lp if set, add this localpath to the aggregation computation
	 * @return the cost
	 */
	double& getCostFromStart(bool recompute = false, bool kino = false, Node *parent = NULL, API::LocalPathBase *lp = NULL);

	/**
	 * Get total duration from start
	 * @return total duration
	 */
	double& getDurationFromStart(bool recompute = false);

	/**
	 * obtient la temperature du Node
	 * @return la temperature du Node
	 */
	double getTemp();
	
	/**
	 * modifie la temperature du Node
	 * @param t la nouvelle temperature
	 */
	void setTemp(double t);
	
	/**
	 * obtient la distance au Node courant créé
	 * @return la distance au Node courant créé
	 */
	double getDist();
  
    /**
     * copy of p3d_APInode_dist_multisol
     */
    double distMultisol(Node *node) const;

    double metric(Node* node);

	/**
	 * Get the maximum integration duration for each DOF to reach the node
	 * @param node to reach
	 * return the maximum time to reach
	 */
	double kinoMetric(Node* node);

	/**
	 * Get the maximum integration duration for each DOF to reach the node
	 * @param node to reach
	 * return the maximum time to reach
	 */
	double kinoMetric(statePtr_t node) ;

	/**
	 * calcule la distance entre deux Node et stocke la valeur dans DistNew
	 * @param N le Node à partir duquel on veut calculer la distance
	 * @param kino if state = true, if config = false
	 * @return la distance entre les deux Node
	 */
	double dist(Node* N, bool kino = false) const;

	/**
	 * calcule la distance entre deux Node et stocke la valeur dans DistNew
	 * @param node le Node à partir duquel on veut calculer la distance
	 * @param kino Si la distance est entree deux états
	 * @return la distance entre les deux Node
	 */
	double squareDistance(statePtr_t node, bool kino = false);

	/**
	 * calcule la distance entre deux Node et stocke la valeur dans DistNew
	 * @param node le Node à partir duquel on veut calculer la distance
	 * @param kino Si la distance est entree deux états
	 * @return la distance entre les deux Node
	 */
	double squareDistance(Node *node, bool kino = false);

	/**
	 * Weighted distance
	 * @param node the seconde node
	 * @param w1 ponderation of distance
	 * @param w2 ponderation of velocity
	 * @param w3 ponderation of acceleration
	 * @return distance between to nodes
	 */
	double weightedSquareDistance(Node *node, float w1 = 0.25, float w2 = 0.5, float w3 = 1.0);

	/**
	 * Weighted distance
	 * @param state the seconde state
	 * @param w1 ponderation of distance
	 * @param w2 ponderation of velocity
	 * @param w3 ponderation of acceleration
	 * @return distance between to nodes
	 */
	double weightedSquareDistance(statePtr_t node, float w1 = 0.25, float w2 = 0.5, float w3 = 1.0);

	/**
	 * Compute the distance between the node and a configuration q and store it to DistNew
	 * @param q the configuration
	 * @param kino use state if true, use conf otherwise
	 * @return the distance
	 */
	double dist(statePtr_t q, bool kino = false) const;
	
	/**
	 * teste si deux Node sont égaux
	 * @param N le Node à tester
	 * @return les Node sont égaux
	 */
	bool equal(Node* N) const;
	
	/**
	 * teste si deux Node sont dans la même composante connexe
	 * @param N le Node à tester
	 * @return les deux Node sont dans la même composante connexe
	 */
	bool inSameComponent(Node* N) const;

	/**
	 * @brief Checks if two nodes can be linked
	 * @param node the node to test
	 * @param dist the distance between the nodes
	 * @return true if nodes can be linked
	 */
	bool isLinkable(Node* node, double* dist) const;
  
	/**
	 * @brief Check if two node are connectable
	 * @param node the node to test
	 * @param dist the distance between the nodes
	 * @return TRUE if connected FALSE otherwise
	 */
	bool isLinkableMultisol(Node* node, double* dist) const;
	
	/**
	 * vérifie si le poids max du Node a été atteint
	 */
	void checkStopByWeight();
	
	/**
	 * Gets the number of nodes
	 * in the connected componnent
	 */
	unsigned int getNumberOfNodesInCompco() const;
	
	/**
	 * obtient la structure de composante connexe à laquelle appartient le Node
	 * @return la structure de composante connexe à laquelle appartient le Node
	 */
	ConnectedComponent* getConnectedComponent() const {
		return m_Compco;
	}
	
	/**
	 * Sets the compco of the node
	 * @param the connected component in which is the node
	 */
	void setConnectedComponent(ConnectedComponent* compco);
	
	/**
	 * detruit la composante connexe
	 */
	void deleteCompco();
	
	/**
	 * teste si la composante connexe a atteint le nombre max de Nodes
	 * @return la composante connexe a atteint le nombre max de Nodes
	 */
	bool maximumNumberNodes();
	
	/**
	 * connect un Node à la composante connexe
	 * @param N le Node à connecter
	 * @return le Node est connecté
	 */
	bool connectNodeToCompco(Node* N, double step);
	
	/**
	 * merge deux composantes connexes
	 * @param compco la composante connexe à merger
	 */
	void merge(Node* compco);
	
	/**
	 * teste si deux composante connexe sont égales
	 * @param compco la composante connexe à tester
	 * @return les deux composantes sont égales
	 */
	bool equalCompco(Node* compco) const;
	
	/**
	 * tire un Node aléatoirement dans la composante connexe
	 * @return le Node tiré
	 */
	Node* randomNodeFromComp() const;

	//! Get the number of edges going out of this node.
	unsigned getNumberOfEdges() { return edges.size(); }

	//! Get the list of edges going out of this node.
	std::list<Edge *> & getEdges() { return edges; }

	//! Add the given edge to the list of edges going out of this node.
	void addEdge(Edge * edge) { edges.push_back(edge); }

	//! Remove the given edge from the list of edges going out of this node.
	void removeEdge(Edge * edge) { edges.remove(edge); }

	//! Recompute the list of edges going out of this node.
	std::vector<Edge *> computeEdges();

	//! Get the number of neighbors of this node.
	unsigned getNumberOfNeighbors() { return neighbors.size(); }

	//! Get the list of neighbors of this node.
	std::list<Node *> & getNeighbors() { return neighbors; }

	//! Add the given node to the list of neighbors of this node.
	void addNeighbor(Node * node) { neighbors.push_back(node); }

	//! Remove the given node from the list of neighbors of this node.
	void removeNeighbor(Node * node) { neighbors.remove(node); }

	//! Recompute the list of neighbors of this node.
	std::vector<Node *> computeNeighbors();

	/**
	 * Method for EST
	 */
	void setSelectCost(double Cost) { _SelectCost = Cost; }
	double getSelectCost() { 	return _SelectCost; }
	void setExpandFailed() { 	_nbExpan++; }
	int getNbExpandFailed() { return _nbExpan; }
	std::vector<Node*>& getSortedNodes() { return _SortedNodes; }
	void setSortedNodes(std::vector<Node*>& nodes) { 	_SortedNodes = nodes; }

	/**
	 * Prints the node to the standard output
	 */
	void print() const;

	//--------------------------------------
	// BGL
	BGL_Vertex getDescriptor();
	void setDescriptor(const BGL_Vertex& V);
	void unSetDescriptor();
	int useful;

    // Used for directed graphs
    bool isForwardConnectedToGoal();
    bool isBackwardConnectedToSource();
    void forwardConnectToGoal();
    void backwardConnectToSource();
    void resetConnections();
    bool isFlaged();
    void flag();
    void unflag();
    void unflagForward();
    void unflagBackward();
    void addForwardNeighbor(Node *N);
    void addBackwardNeighbor(Node *N);
    std::vector<Node*> getBackwardNeighbors();
    std::vector<Node*> getForwardNeighbors();
    void updateForward();
    void updateBackward();
    unsigned int getSccId();
    void setSccId(unsigned int i);

	/**
	 * Store the edge between this node and his parent node
	 * @param edge The edge
	 */
	void setParentEdge(Edge* edge) {
		m_parentEdge = edge;
	}

	/**
	 * Get the edge between this node and his parent node
	 * @return The edgee
	 */
	Edge* getParentEdge() {
		return m_parentEdge;
	}

	/**
	  * Get if the node is active or not
	  * @return true if is inactive, false otherwise
	  */
	bool isInactive() const {
		return inactive;
	}

	/**
	  * Set if the node, active or not
	  * @param inactive true if is inactive, false otherwise
	  */
	void setInactive(bool inactive) {
		this->inactive = inactive;
	}

	/**
	  * Get witness of this node
	  * @retun The witness
	  */
	Witness* getWitness() const {
		return witness;
	}

	/**
	  * Set witness of this node
	  * @retun The witness
	  */
	void setWitness(Witness *witness) {
		this->witness = witness;
	}

	/**
	 * Get children of this node
	 * @return List of children
	 */
	std::vector<Node *> getChildren() const {
		return children;
	}

	/**
	  * Add children of this node
	  * @param children new children
	  */
	void  addChildren(Node* children) {
		this->children.push_back (children);
	}

	// Get trajectory from start
	std::vector<Edge*> getEdgesFromStart();
	std::vector<API::LocalPathBase*> getLpFromStart();

private:
	
	// Old Node structure
	node* m_Node;
	NodeData* _specificNodeData;

	bool m_is_BGL_Descriptor_Valid;
	BGL_Vertex m_BGL_Descriptor;

	Graph* m_Graph;
	Robot* m_Robot;
	ConnectedComponent* m_Compco;

	//! edges going out of this node
	std::list<Edge *> edges;

	/**
	 * @brief The neighbor list for this node.
	 */
	std::list<Node *> neighbors;

	/**
	 * Witness is used for pruning
	 * @brief A special storage node used in SST
	 */
	Witness* witness;

	/**
	 * A flag for inclusion in the metric
	 */
	bool inactive;

	/**
	 * Childrens of this node
	 */
	std::vector<Node *> children;

	/**
	 * @brief The node represented.
	 */
	statePtr_t m_Configuration;

	bool _activ;

	double _SelectCost;
	int _nbExpan;
	std::vector<Node*> _SortedNodes;

	// Used for debug
	bool m_specificNode;

    // Used for directed graphs
    bool _isForwardConnectedToGoal;
    bool _isBackwardConnectedToSource;
    bool _flaged;
    std::vector<Node*> _ForwardNeighbors;
    std::vector<Node*> _BackwardNeighbors;
    // Id of the stongly connected component of the node
    unsigned int _SccId;

	// In tree graphs
	/**
	 * The edge between this node and his parent node
	 * @brief The parent edge.
	 */
	Edge* m_parentEdge;
	Node* m_parent;
	double m_costTraj;
	double m_durationTraj;
	bool m_is_leaf;
};

/**
 * @brief A special storage node for witness nodes for sparcity.
 */
class Witness {
protected:
	/**
	 * The best node in this area.
	 */
	Node *rep;

	/**
	 * The node that represents this sample.
	 */
	statePtr_t state;

public:
	/**
	 * Create a new witness
	 * @param node the witness node
	 */
	Witness(Node *node) {
		state = node->getConfiguration ();
		node->setWitness (this);
	}

	/**
	  * Create a new witness with represents
	  * @param node the witness node
	  * @param rep represents this sample
	  */
	Witness(Node *node, Node *rep) {
		state = node->getConfiguration ();
		node->setWitness (this);
		setRep (rep);
	}

	/**
	  * Get distance between this witness and other node
	  * @param node the node to test
	  * @param kino if use state
	  * @return square distance
	  */
	double getDistance(Node *node, bool kino = true) {
		double score;
		if(PlanEnv->getBool (PlanParam::kinoActivate) && ENV.getBool (Env::useMetric)) {
			score = node->kinoMetric (state);
		} else {
			score = node->squareDistance (state, kino && PlanEnv->getBool (PlanParam::kinoActivate) );
		}
		return score;
	}

	/**
	  * Get the node that represents this sample.
	  * @return the node that represents this sample
	  */
	Node *getRep() const {
		return rep;
	}

	/**
	  * Set the represents node
	  * @param the node that represents this sample
	  */
	void setRep(Node *rep) {
		this->rep = rep;
	}
};

} //namespace move4d
#endif
