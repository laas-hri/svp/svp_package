#ifndef VIRTUALIK_H
#define VIRTUALIK_H

#include <memory>
#include <map>
#include <string>
#include "move4d/Logging/Logger.h"

typedef double p3d_matrix4[4][4];

namespace move4d
{
class Robot;
class RobotState;

namespace IK {
typedef enum IkStatusType{
    ok,
    notAnAgent,
    notImplemented,
    IkIsInCollision,
    TooFar,
    KDLChainFailure,
    JointBoundFailure,
    unknownError
} IkStatusType;
}


class VirtualIk
{
    MOVE3D_STATIC_LOGGER;
public:
    VirtualIk();

    /**
      * Needs to be reimplemented
      * @brief initiallize the inverse kinematic method
      */
    virtual void init() = 0;

    /**
      * Needs to be reimplemented
      * @brief compute the inverse kinematic of the specified agent to the target
      * @param ag the agent on which to compute the inverse kinematic
      * @param q_start the starting position from which to compute the inverse kinematic
      * @note In case q_start == NULL or there is no solution with the specified q_start,
      * the algorithm will shoot random position to use as q_start.
      * @param target_pos the position on which we want the ikJoint to be
      * @param[out] q_solution output of the function.
      * @param armId the arm on which to use the IK
      * @param status raison of failure
      * @return true if the IK was found
      */

    virtual bool compute(Robot* ag,
                         std::shared_ptr<RobotState> q_start,
                         p3d_matrix4 target_pos,
                         std::shared_ptr<RobotState> &q_solution,
                         int armId,
                         IK::IkStatusType &status) = 0;

    /**
      * Needs to be reimplemented
      * @brief same as the previous one, a part from the armId replaced by the index of joints used
      * @param jntIndexes indexes of joints to use
      */
    virtual bool compute(Robot* ag,
                         std::shared_ptr<RobotState> q_start,
                         p3d_matrix4 target_pos,
                         std::shared_ptr<RobotState> &q_solution,
                         std::vector<int>& jntIndexes,
                         IK::IkStatusType &status) = 0;


    /**
      * calls compute, but set _withCollisionTesting and _maxIkLoop
      * to the specified values and set them back to their initial values
      *
      **/
    bool compute(Robot* ag,
                 std::shared_ptr<RobotState> q_start,
                 p3d_matrix4 target_pos,
                 std::shared_ptr<RobotState> &q_solution,
                 int armId,
                 IK::IkStatusType &status,
                 bool withCol,
                 int maxLoop);

    std::map<std::string,int> getCollisions() {return _collisions;}

    void setWithCollisionTesting(bool val){_withCollisionTesting = val;}
    void setMaxIkLoop(int val){_maxIkLoop = val;}

    bool getWithCollisionTesting(){return _withCollisionTesting;}
    int getMaxIkLoop(){return _maxIkLoop;}

protected:
    std::map<std::string,int> _collisions;

    bool _withCollisionTesting;

    int _maxIkLoop;

    bool test;
};

} //namespace move4d
#endif // VIRTUALIK_H
