#ifndef IKTYPES_HPP
#define IKTYPES_HPP

//list of all the hpp files
#include "move4d/API/IK/AIK/analiticalIk.hpp"


namespace move4d
{
namespace IK {
// enum of the ik types
typedef enum IkEnumTypes{
    analitical,
    tracik,
    unknownIk
} IkEnumTypes;
}

} //namespace move4d
#endif // IKTYPES_HPP
