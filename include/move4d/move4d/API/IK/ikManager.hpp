#ifndef IKMANAGER_HPP
#define IKMANAGER_HPP

#include "move4d/API/IK/ikTypes.hpp"
#include<string>
#include<map>

#include "move4d/Logging/Logger.h"

namespace move4d
{
class VirtualIk;

class IkManager
{
    MOVE3D_STATIC_LOGGER;
public:
    static IkManager* getInstance();
    ~IkManager(){}

    /**
     * @param ikType: the type of the ikType as a string
     * @return the enum value of the corresponding ikType
     */
    IK::IkEnumTypes getIkTypeFromString(std::string ikType);

    /**
     * @param ikType: the enum value of a ikEnumTypes
     * @return a string that describes the ikEnumTypes
     */
    std::string getIkTypeAsString(IK::IkEnumTypes ikType);

    /**
     * @param ikType: the enum value of a ikEnumTypes
     * @return an instanciation of the VirtualIk class
     */
    VirtualIk* createIkObject(IK::IkEnumTypes ikType);

    /**
     * @return returns all ik possible
     */
    std::vector<std::string> getAllIk();

    /**
      * @brief: get or create an object corresponding to the correct ikType
      * @param the ikType needed
      */
    VirtualIk* getIk(IK::IkEnumTypes ikType);
    VirtualIk* getCurrentIk() {return getIk(_currentIktype);}


    //getters
    std::map<IK::IkEnumTypes,VirtualIk*> getAllIkObjects() {return _ik;}
    std::map<std::string,IK::IkEnumTypes> getTypeMap() { return _stringEnumMap;}
    IK::IkEnumTypes getCurrentIkType() {return _currentIktype;}

    void setCurrentIkType(IK::IkEnumTypes s){_currentIktype = s;}

private:
    IkManager();
    static IkManager* _instance;

    std::map<std::string,IK::IkEnumTypes> _stringEnumMap;
    std::map<IK::IkEnumTypes,VirtualIk*> _ik;


    IK::IkEnumTypes _currentIktype;
};



} //namespace move4d
#endif // IKMANAGER_HPP
