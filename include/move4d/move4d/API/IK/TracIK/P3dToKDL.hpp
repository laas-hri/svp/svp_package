//
// Created by tollivie on 3/31/16.
//

#include <move4d/API/Device/joint.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include "kdl/segment.hpp"
#include "move4d/API/Device/joint.hpp"

#include <string>
#include <libmove3d/device.h>
#include "device.h"
#include <vector>

#ifndef LIBMOVE3D_PLANNERS_P3DTOKDL_H
#define LIBMOVE3D_PLANNERS_P3DTOKDL_H


namespace move4d
{
/**
 * this namespace provides functions to switch between p3d and KDL data
 */
namespace P3dToKDL
{

    /**
    * @brief convert the Robot P3D structure to KDL::Chain object
    * @param Robot* robot : the robot
    * @param lst_joints : the list of joints you want in the KDL::Chain
    * @param[out] KDL::Chain& chain : the chain you want
    * @return true if conversion worked
    */
    bool convertToKDLChain(Robot* robot,std::vector<int> lst_all_joints, KDL::Chain& chain);

    /**
    * @brief convert the Robot P3D structure to KDL::Chain object
    * @param Robot* robot : the robot
    * @param lst_joints : the list of joints you want in the KDL::Chain
    * @param end_effector_id : the id of the end_effector
    * @param[out] KDL::Chain& chain : the chain you want
    * @return true if conversion worked
    */
    bool convertToKDLChain(Robot* robot,std::vector<int> lst_all_joints, KDL::Chain& chain, uint end_effector_id);

    /**
    * @brief return the bounds of each joints of the robot in a KDL object
    * @param Robot* robot : the robot
    * @param lst_joints : the list of joints you want in the KDL::Chain
    * @param[out] KDL::JntArray& boundsMax : array of max bounds
    * @param[out] KDL::JntArray& boundsMin : array of min bounds
    * @return true if worked
    */
    bool getJointsBounds(Robot* robot,std::vector<int> lst_joints, KDL::JntArray& boundsMax, KDL::JntArray& boundsMin);

    /**
    * @brief convert an Eigen::VectorXd into a KDL::Array
    * @param Eigen::VectorXd joints : the vector you want to convert
    * @return the KDL::Array
    */
    KDL::JntArray getKDLJntArray(Eigen::VectorXd joints);

    /**
    * @brief build a KDL::Frame from two vectors containing the origin and the orientation
    * @param Eigen::Vector3d origin : the origin of the frame
    * @param Eigen::Vector3d orientation : the orientation of the frame
    * @return the KDL::Frame
    */
    KDL::Frame getKDLFrame(Eigen::Vector3d origin, Eigen::Vector3d orientation);

    /**
    * @brief convert a p3d_matrix4 to a KDL::Frame
    * @param p3d_matrix4 mat : the matrix
    * @return the KDL::Frame
    */
    KDL::Frame getKDLFrame(const p3d_matrix4 mat);

    /**
    * @brief convert a Eigen::Affine3d to a KDL::Frame
    * @param Eigen::Affine3d mat : the matrix
    * @return the KDL::Frame
    */
    KDL::Frame getKDLFrame(const Eigen::Affine3d mat);

    /**
    * @brief build a KDL::Twist from two Eigen vectors containing the vel and rot_vel
    * @param Eigen::Vector3d velocity : the translationnal velocity
    * @param Eigen::Vector3d rot_velocity : the rotation velocity
    * @return the KDL::Twist
    */
    KDL::Twist getKDLTwist(Eigen::Vector3d velocity, Eigen::Vector3d rot_velocity);

    /**
    * @brief convert a KDL::Array into an Eigen::VectorXd
    * @param KDL::JntArray joints : the vector you want to convert
    * @return the Eigen::VectorXd
    */
    Eigen::VectorXd getEigenVecXd(KDL::JntArray joints);

    RobotState jntArrayToState(Robot *r, std::vector<int> &jnt_list, KDL::JntArray &joints);

}
} //namespace move4d

#endif //LIBMOVE3D_PLANNERS_P3DTOK
