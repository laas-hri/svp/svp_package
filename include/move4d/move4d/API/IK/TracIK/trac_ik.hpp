/********************************************************************************
Copyright (c) 2015, TRACLabs, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, 
       this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its contributors
       may be used to endorse or promote products derived from this software 
       without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************/


#ifndef TRAC_IK_HPP
#define TRAC_IK_HPP

#include "move4d/API/IK/TracIK/nlopt_ik.hpp"
#include <kdl/chainjnttojacsolver.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/scoped_ptr.hpp>
#include <move4d/API/IK/virtualIk.hpp>
#include "move4d/Logging/Logger.h"
#include <map>

/**
* Théo OLLIVIER
* LAAS
*
* The class TRAC_IK offers a generic inverse kinematic solver for redondants robots
* It sould be used via the class VirtualIK in which you can choose between two diffrent solvers, one is the analitical solver and the other one is this one.
* The one you have to choose is depending on the number of dofs you want to command, if it is more than the degres in the constraint you sould use TRAC_IK.
*
* The architecture and the content of this class has a bit changed compare to its original version, but it is used for the same purposes
* - it has been adapted to be used with the ikManager in Move3d tranparently
* - all code related to ROS has been removed because we don't need it and it asks for dependances we do not use
* - the way of initialising has also changed as some atributes are part of VirtualIK
* - two class have been created (TracIKDataKey, TracIKData) to store the parameters in KDL structure with the aim of not converting the p3d struncture in KDL::Chain each time we use TRAC_IK
* - several methods called "compute" have been implemented to link TRAC_IK to VirtualIK
* - it's possible to find a solution by optimizing a cost in the costSpace within  Move3d
*
* NOTE : all methods added or modified will be commented
*
*/


namespace move4d
{
namespace TRAC_IK {


  enum SolveType { Speed, Distance, Manip1, Manip2};


     /**
     * @brief The TracIKDataKey class
     * @abstract To not compute the data of each different problem we store the data of each problem and use this class to recover them
     */
    class TracIKDataKey
    {
    public:
        /**
         * @brief TracIKDataKey Constructor of the key
         * @param name
         * @param lst_joints
         */
        TracIKDataKey(std::string name, std::vector<int> lst_joints);

        ~TracIKDataKey();

        bool operator==(TracIKDataKey key)
        {
            return (_name == key._name && _lst_joints == key._lst_joints);
        }

        // this is crappy but needed...
        bool operator<(const TracIKDataKey& key) const
        {
            return (_name == key._name ? _lst_joints < key._lst_joints : _name<key._name);
        }

    private:
        std::string _name;
        std::vector<int> _lst_joints;
    };

    /**
    * @brief TracIKData Class which contains all the needed parameters to solve an IK problem for a given robot and the list of joints
    */
    class TracIKData
    {
    public:
        TracIKData();

        /**
         * @brief TracIKData Constructor to store the data of a given problem
         * @param chain the kinematic chain
         * @param lb lower bounds
         * @param ub upper bounds
         * @param types types of the ddl
         * @param bounds twist bounds
         * @param robot the robot
         */
        TracIKData(KDL::Chain chain, KDL::JntArray lb, KDL::JntArray ub, std::vector<IK::BasicJointType> types, KDL::Twist bounds, Robot* robot);

        ~TracIKData();


        KDL::Chain getChain()
        {
            return _chain;
        }

        KDL::JntArray getLowerBounds()
        {
            return _lb;
        }

        KDL::JntArray getUpperBounds()
        {
            return _ub;
        }

        std::vector<IK::BasicJointType> getTypes()
        {
            return _types;
        }

        KDL::Twist getBounds()
        {
            return _bounds;
        }

        Robot* getRobot()
        {
            return _robot;
        }

    private:
        KDL::Chain _chain;
        KDL::JntArray _lb, _ub;
        std::vector<IK::BasicJointType> _types;
        KDL::Twist _bounds;
        Robot* _robot;
    };

      class TRAC_IK : public VirtualIk
      {
          MOVE3D_STATIC_LOGGER;
      public:
        TRAC_IK();

        ~TRAC_IK();

        /**
         * @brief init initialise the global parameters
         */
        void init();

        /**
         * @brief isInCollision check if the given config is in collision
         * @param q the config
         * @return True if in collision
         */
        bool isInCollision(KDL::JntArray q);

        /**
         * @brief compute compute the IK
         * @param ag the robot
         * @param q_start initial config
         * @param target_pos pos and orientation constraint
         * @param[out] q_solution out config
         * @param armId arm ID
         * @param status status
         * @return True if a solution has been found
         */
        bool compute(Robot* ag,
                       std::shared_ptr<RobotState> q_start,
                       p3d_matrix4 target_pos,
                       std::shared_ptr<RobotState> &q_solution,
                       int armId,
                       IK::IkStatusType& status);

        /**
         * @brief compute compute the IK
         * @param ag the robot
         * @param q_start initial config
         * @param target_pos pos and orientation constraint
         * @param[out] q_solution out config
         * @param jntIndexes list of joints
         * @param status status
         * @return True if a solution has been found
         */
        bool compute(Robot* ag,
                       std::shared_ptr<RobotState> q_start,
                       p3d_matrix4 target_pos,
                       std::shared_ptr<RobotState> &q_solution,
                       std::vector<int> &jntIndexes,
                       IK::IkStatusType& status);

        bool GetKDLChain(KDL::Chain& chain_) {
          chain_=chain;
          return initialized;
        }

        bool GetKDLLimits(KDL::JntArray& lb_, KDL::JntArray& ub_) {
          lb_=lb;
          ub_=ub;
          return initialized;
        }

        SolveType GetSolveType() {
            return solvetype;
        }

        double GetMaxTime() {
            return maxtime;
        }

        double GetEps() {
            return eps;
        }

        static double JointErr(const KDL::JntArray& arr1, const KDL::JntArray& arr2) {
          double err = 0;
          for (uint i=0; i<arr1.data.size(); i++) {
            err += pow(arr1(i) - arr2(i),2);
          }

          return err;
        }

        /**
         * @brief CartToJnt
         * @param q_init initial config
         * @param p_in pos and orientation constraint
         * @param[out] q_out out config
         * @param bounds twist bounds
         * @return
         */
        int CartToJnt(const KDL::JntArray &q_init, const KDL::Frame &p_in, KDL::JntArray &q_out, const KDL::Twist& bounds=KDL::Twist::Zero());

        inline void SetSolveType(SolveType _type) {
          solvetype = _type;
        }

        inline void SetMaxTime(double time_) {
            maxtime = time_;
        }

        inline void SetEps(double eps_) {
            eps = eps_;
        }

        void testPepper();

        void testPr2();

        void testRomeo();

      private:
        bool initialized;
        KDL::Chain chain;
        KDL::JntArray lb, ub;
        boost::scoped_ptr<KDL::ChainJntToJacSolver> jacsolver;
        double eps;
        double maxtime;
        SolveType solvetype;
        bool use_KDL_solver;

        Robot* robot;
        std::vector<int> _lst_joints;

        /**
        * map to store the data of each robots we want to solve a problem for
        */
        std::map<TracIKDataKey, TracIKData> _tracIKData;

        /**
        * @brief tracIKDataExists function to check if a given robot has already been converted
        * @param name robot's name
        * @param lst_joints list of joints
        * @result True if exists
        */
        bool tracIKDataExists(std::string name, std::vector<int> lst_joints);

        boost::scoped_ptr<NLOPT_IK::NLOPT_IK> nl_solver;
        boost::scoped_ptr<IK::ChainIkSolverPos_TL> iksolver;

        boost::posix_time::ptime start_time;
        unsigned int coltest,colfail;

        /**
         * @brief runKDL run the KDL IK solver
         * @param q_init initial conf
         * @param p_in pos and oriantion constraints
         * @return true if a solution has been found
         */
        bool runKDL(const KDL::JntArray &q_init, const KDL::Frame &p_in);

        /**
         * @brief runNLOPT run the NLOPT IK Solver
         * @param q_init initial conf
         * @param p_in pos and oriantion constraints
         * @return True if a solution has been found
         */
        bool runNLOPT(const KDL::JntArray &q_init, const KDL::Frame &p_in);

        void normalize_seed(const KDL::JntArray& seed, KDL::JntArray& solution);
        void normalize_limits(const KDL::JntArray& seed, KDL::JntArray& solution);



        std::vector<IK::BasicJointType> types;

        boost::mutex mtx_;
        std::vector<KDL::JntArray> solutions;
        std::vector<std::pair<double,uint> >  errors;


        boost::asio::io_service io_service;
        boost::thread_group threads;
        boost::asio::io_service::work work;
        KDL::Twist bounds;

        NLOPT_IK::OptType nlopt_type;

        bool unique_solution(const KDL::JntArray& sol);

        inline static double fRand(double min, double max)
        {
          double f = (double)rand() / RAND_MAX;
          return min + f * (max - min);
        }

        /* @brief Manipulation metrics and penalties taken from "Workspace
        Geometric Characterization and Manipulability of Industrial Robots",
        Ming-June, Tsia, PhD Thesis, Ohio State University, 1986.
        https://etd.ohiolink.edu/!etd.send_file?accession=osu1260297835
        */
        double manipPenalty(const KDL::JntArray&);
        double ManipValue1(const KDL::JntArray&);
        double ManipValue2(const KDL::JntArray&);

        inline bool myEqual(const KDL::JntArray& a, const KDL::JntArray& b) {
          return (a.data-b.data).isZero(1e-4);
        }

        /**
         * @brief solve run the IK solvers
         * @param ag the robot
         * @param q_start initial config
         * @param target_pos p_in pos and oriantion constraints
         * @param[out] q_solution out config
         * @param jntIndexes list of joints
         * @param status status
         * @return true if a solution has been found
         */
        bool solve(Robot* ag,
                   std::shared_ptr<RobotState> q_start,
                   p3d_matrix4 target_pos,
                   std::shared_ptr<RobotState> &q_solution,
                   std::vector<int> &jntIndexes,
                   IK::IkStatusType &status);

        /**
         * @brief initializeSolvers initialise the KDL and NLOPT IK solvers
         */
        void initializeSolvers();

      };
}

} //namespace move4d
#endif
