#ifndef FORWARDKYNEMATICSOLVER_H
#define FORWARDKYNEMATICSOLVER_H

#include "move4d/API/forward_declarations.hpp"
#include <Eigen/Geometry>
#include "kdl/chain.hpp"
#include "kdl/chainfksolvervel_recursive.hpp"
#include "move4d/Logging/Logger.h"

namespace move4d
{
class forwardKynematicSolver
{
    MOVE3D_STATIC_LOGGER;
public:
    forwardKynematicSolver(Robot* robot,std::vector<int> &lst_all_joints, uint end_effector_id);

    bool solve(const RobotState &conf, Eigen::Affine3d& pos, Eigen::Vector3d& trans_vel, Eigen::Vector3d& rot_vel);

private:
    bool init(Robot* robot,std::vector<int> &lst_all_joints, uint end_effector_id);

    Robot* robot;
    std::vector<int> lst_jnts;
    KDL::Chain chain;
    KDL::ChainFkSolverVel_recursive* fk_solver;
};

} //namespace move4d
#endif // FORWARDKYNEMATICSOLVER_H
