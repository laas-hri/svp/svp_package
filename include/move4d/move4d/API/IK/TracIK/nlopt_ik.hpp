/********************************************************************************
Copyright (c) 2015, TRACLabs, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, 
       this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its contributors
       may be used to endorse or promote products derived from this software 
       without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************/

#ifndef NLOPT_IK_HPP
#define NLOPT_IK_HPP
 

#include "move4d/API/IK/TracIK/kdl_tl.hpp"
#include <nlopt.hpp>
#include <move4d/API/Device/robot.hpp>
#include <move4d/utils/Geometry.h>
 

namespace move4d
{
namespace NLOPT_IK {


   /**
   * @brief Type used for solving
   * Joint : the out config will look like the desired provided one
   * DualQuat : minimize the euclidian distance
   * SumSq : minimize the euclidian distance
   * L2 : minimize the euclidian distance
   * Cost : minimise the cost used in the costmanager
   */
  enum OptType { Joint, DualQuat, SumSq, L2, Cost};


  class NLOPT_IK 
  {
    MOVE3D_STATIC_LOGGER;

    friend class TRAC_IK::TRAC_IK;
  public:
    /**
     * @brief NLOPT_IK Constructor of an IK solver using non linear optimization
     * @param chain kinematic chain of the robot
     * @param q_min min bounds of ddl
     * @param q_max max bounds of ddl
     * @param ag the robot
     * @param lst_joints the list of joints used
     * @param maxtime max time
     * @param eps tolerance
     * @param type solve type
     */
    NLOPT_IK(const KDL::Chain& chain,const KDL::JntArray& q_min, const KDL::JntArray& q_max, Robot* ag, std::vector<int> lst_joints, double maxtime=0.005, double eps=1e-3, OptType type=SumSq);

    ~NLOPT_IK() {};

    /**
     * @brief CartToJnt Get values of ddl from position and orientation constraints for the end effector
     * @param q_init initial configuration
     * @param p_in pos and orientation contraints
     * @param[out] q_out values of ddl to satisfy the constraints
     * @param bounds bounds of the ddl
     * @param q_desired desired configuration (not always used)
     * @return
     */
    int CartToJnt(const KDL::JntArray& q_init, const KDL::Frame& p_in, KDL::JntArray& q_out, const KDL::Twist bounds=KDL::Twist::Zero(), const KDL::JntArray& q_desired=KDL::JntArray());

    /**
     * @brief getPrevCost
     * @return previous cost
     */
    double getPrevCost();

    /**
     * @brief minJoints Compute the error between the config in param x and the desired config
     * @param x values of ddl
     * @param[out] grad gradiens
     * @return error
     */
    double minJoints(const std::vector<double>& x, std::vector<double>& grad);
    //  void cartFourPointError(const std::vector<double>& x, double error[]);

    /**
     * @brief cartSumSquaredError Compute the euclidian distance error
     * @param x values of ddl
     * @param[out] error
     * @abstract Now use the setAndUpdate() methode from Move3d, do not use KDL forward kynematic solver because of problems with some robots like PEPPER
     */
    void cartSumSquaredError(const std::vector<double>& x, double error[]);

    /**
     * @brief cartDQError Compute the euclidian distance error
     * @param x values of ddl
     * @param[out] error error
     */
    void cartDQError(const std::vector<double>& x, double error[]);

    /**
     * @brief cartL2NormError Compute the euclidian distance error
     * @param x values of ddl
     * @param[out] error error
     */
    void cartL2NormError(const std::vector<double>& x, double error[]);

    /**
    * @brief function used to build a RobotState depending on the vector x
    *
    * @note do not call this function by yourself, only a costFunc should call it
    *
    * @param std::vector<double>& x : the values of the dofs
    * @return a RobotState    */
    std::shared_ptr<RobotState> getActualConf(std::vector<double> x);

    /**
     * @brief setMaxtime
     * @param t time
     */
    inline void setMaxtime(double t) { maxtime = t; }

  private:

    inline void abort() {
      aborted = true;
    }

    inline void reset() {
      aborted = false;
    }

    std::vector<double> lb;
    std::vector<double> ub;

    const KDL::Chain chain;
    std::vector<double> des;

    KDL::ChainFkSolverPos_recursive fksolver;

    Robot* robot;
    std::vector<int> joints_indexes;

    double maxtime;
    double eps;
    int iter_counter; 
    OptType TYPE;

    KDL::Frame targetPose;
    KDL::Frame z_up ;
    KDL::Frame x_out;
    KDL::Frame y_out;
    KDL::Frame z_target;
    KDL::Frame x_target;
    KDL::Frame y_target;
    
    std::vector<IK::BasicJointType> types;

    nlopt::opt opt;

    KDL::Frame currentPose;

    std::vector<double> best_x;
    int progress;
    bool aborted;

    KDL::Twist bounds;

    double prev_cost;

    inline static double fRand(double min, double max)
    {
      double f = (double)rand() / RAND_MAX;
      return min + f * (max - min);
    }


  };

}
 
} //namespace move4d
#endif
