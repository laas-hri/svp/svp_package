#ifndef ANALITICALIK_HPP
#define ANALITICALIK_HPP

#include "move4d/API/IK/virtualIk.hpp"
#include "move4d/Logging/Logger.h"




namespace move4d
{
class AnaliticalIk : public VirtualIk
{
    MOVE3D_STATIC_LOGGER;
public:
    AnaliticalIk();

    void init();
    bool compute(Robot* ag,
                 std::shared_ptr<RobotState> q_start,
                 p3d_matrix4 target_pos,
                 std::shared_ptr<RobotState> &q_solution,
                 int armId,
                 IK::IkStatusType& status);

    bool compute(Robot* ag,
                 std::shared_ptr<RobotState> q_start,
                 p3d_matrix4 target_pos,
                 std::shared_ptr<RobotState> &q_solution,
                 std::vector<int> &jntIndexes,
                 IK::IkStatusType &status);
};

} //namespace move4d
#endif // ANALITICALIK_HPP
