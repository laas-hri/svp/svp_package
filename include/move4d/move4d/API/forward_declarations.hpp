#ifndef FORWARD_DECLARATIONS_HPP
#define FORWARD_DECLARATIONS_HPP
#include <memory>

namespace move4d {
class Project;
class Scene;
class RobotState;
typedef std::shared_ptr<RobotState> statePtr_t;
class Robot;
class Joint;


namespace API{
class Path;
typedef std::shared_ptr<Path> PathPtr;

class LocalPathBase;
class PathLocalPathInterface;
class PathWaypointInterface;
typedef std::shared_ptr<LocalPathBase> localPathBasePtr_t;
class LocalPath;
typedef std::shared_ptr<LocalPath> localPathPtr_t;

}

class PlanningTarget;
typedef std::shared_ptr<PlanningTarget> PlanningTargetPtr;

}


#endif // FORWARD_DECLARATIONS_HPP
