//
// Created by gbuisan on 23/05/18.
//

#ifndef LIBMOVE4D_PLANNERBASE_HPP
#define LIBMOVE4D_PLANNERBASE_HPP


#include <string>
#include <move4d/API/Trajectory/trajectory.hpp>
#include <boost/static_assert.hpp>
#include "forward_declarations.hpp"
#include "move4d/Logging/Logger.h"
#include "move4d/API/Device/robot.hpp"


namespace move4d {
    class Graph;


    /// base class for all (motion) planner
    class PlannerBase {
        public:

        virtual ~PlannerBase();

        virtual bool trajFound() = 0;

        /**
         * retourne le Robot activ
         * @return Le Robot activ
         */
        virtual Robot *getActivRobot() = 0;

        /**
         * place le Robot utilisé pour la planification
         * @param R le Robot pour lequel la planification va se faire
         */
        virtual void setRobot(Robot *R) = 0;

        virtual bool setInit(statePtr_t Cs) = 0;

        virtual bool setGoal(statePtr_t Cg) = 0;

        /**
         * Get init configuration
         */
        virtual statePtr_t getInitConf() = 0;

        /**
         * Get goal configuration
         */
        virtual statePtr_t getGoalConf() = 0;

        virtual bool getInitialized() = 0;

        /**
       * modifie la valeur du Booleen de test d'initialisation
       * @param b la valeur entrée
       */
        virtual void setInitialized(bool b) = 0;

        /**
         * Méthode d'initialisation du Planner
         */
        virtual unsigned init() = 0;

        /**
         * Run function
         */
        virtual unsigned int run() = 0;


        virtual API::Path *extractTrajectory() = 0;

        virtual API::Path* plan(statePtr_t from, statePtr_t to) = 0;
    };

    /// base class for planner factories
    class PlannerCreatorInterface
    {
        public:
        virtual PlannerBase *create() = 0;
    };

    template <class P>
    class PlannerCreator : public PlannerCreatorInterface
    {
        BOOST_STATIC_ASSERT(std::is_base_of<PlannerBase,P>::value);
        public:
        // Planner *create(Robot *r, Graph *g){return new P(r, g);}
        PlannerBase *create(){return new P();}
    };

    /**
     * @brief The PlannerRegister class holds factories for planners.
     *
     * The planners are created with the \ref create functions,
     * with the factories registered with \ref add. A default factory to use
     * in all the code can be specified with \ref setDefaultPlanner.
     *
     * The class is implemented as a singleton
     *
     * Recommended usage is to use the PLANNER_REGISTER_FACTORY macro to
     * register the factories at global scope.
     *
     * This class also enables registering planners from plugins.
     */
    class PlannerRegister
    {
        MOVE3D_STATIC_LOGGER;
        protected:
        PlannerRegister();
        public:
        /**
         * @brief instance access the singleton
         * @return
         */
        static PlannerRegister* instance();
        /**
         * @brief register a planner factory
         * @param name name used to create a planner from this factory
         * @param factory factory
         */
        void add(const std::string &name, PlannerCreatorInterface* factory);
        /**
         * @brief create a planner using the factory of specified name
         * @param name name the factory to use was registered with
         * @return
         */
        PlannerBase* create(const std::string &name) const;
        /**
         * @brief create a planner using the default factory
         * @return an instanciated planner, or nullptr
         * when no default factory type is set, uses variables of the ENV and PlanParam objects
         * to choose the planner to use (legacy from move3d-planners)
         */
        PlannerBase* create() const;
        const std::string &getDefaultPlanner() const;
        void setDefaultPlanner(const std::string &planner);
        std::vector<std::string> getAllAvailablePlannerNames() const;

        private:
        std::map<std::string,PlannerCreatorInterface* > _creators;
        std::string _defaultPlanner="";
    };


}

//credits to github.com/ros/class_loader for the macro
#define PLANNER_REGISTER_CREATOR_INTERNAL(Derived, UniqueID) \
namespace \
{\
  struct ProxyExec ## UniqueID \
  {\
    typedef  Derived _derived; \
    ProxyExec ## UniqueID() \
    { \
      ::move4d::PlannerRegister::instance()->add(#Derived, new ::move4d::PlannerCreator<_derived>()); \
    }\
  };\
  static ProxyExec ## UniqueID g_register_plugin_ ## UniqueID;\
}

//this one needed to have the needed supplementary level of macro expansion
#define PLANNER_REGISTER_CREATOR_INTERNAL_HOP1(Derived,UniqueID) \
    PLANNER_REGISTER_CREATOR_INTERNAL(Derived,UniqueID)
#define PLANNER_REGISTER_FACTORY(localpath_class)\
    PLANNER_REGISTER_CREATOR_INTERNAL_HOP1(localpath_class,__COUNTER__)



#endif //LIBMOVE4D_PLANNERBASE_HPP
