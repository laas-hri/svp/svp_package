#ifndef API_HUD_H
#define API_HUD_H

#include "Graphic-pkg.h"
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <set>
#include "move4d/API/Device/robot.hpp"
#include "move4d/Logging/Logger.h"
#ifdef MOVE4D_QT_LIBRARY
#include <QtCore/QObject>
#endif


namespace move4d
{
namespace API {

enum Type {T_DEFAULT, T_TRRT, T_TSST};

/**
 * @brief The SSTStatus struct groups data about a planner state
 */
struct SSTStatus {
    /**
     * @brief Nb of valid node
     */
    unsigned _validNode;

    /**
     * @brief Nb of total shoot
     */
    unsigned _totalShoot;

    /**
     * @brief Nb of accepted node (valid is not neceseraly accepted)
     */
    unsigned _accepted;

    /**
     * @brief number of reached node
     */
    unsigned _nbReached;

     /**
     * @brief number of witness node
     */
    unsigned long _witnesses;

    /**
     * @brief The best goal cost (store for speed)
     */
    double _bestGoalCost;
};

/**
 * @brief The PlanningStatus struct groups data about a planner state
 */
struct PlanningStatus
{
    PlanningStatus(bool plannerOk=false, Type type = T_DEFAULT, unsigned int nbFails=0,double gap=0,double initTargetDist=0,double temp=-1,bool found=false, SSTStatus* sstStat=NULL):
        plannerOk(plannerOk), type(type),nbFails(nbFails),gap(gap),initTargetDist(initTargetDist),temperature(temp), found(found), sstStat(sstStat) {}
    bool plannerOk;/// is there a planner ?
    Type type; ///< type of planner
    unsigned int nbFails; ///< nb of configurations shot and discarded (in collision or too expansive)
    double gap; ///< distance between the last added node and the goal, or something else when using Bi-RRT
    double initTargetDist; ///< distance separing the initial and target configurations (configuration space)
    double temperature; ///< for T-RRT, Simulated Annealing temperature
    bool found; ///< is the path found
    SSTStatus* sstStat;
};

/**
 * @brief The HUD class aims at providing, centralizing and managing information displayed in the scene, other than objects.
 *
 * Displayed data can be robot names, planning status, metadata about objects (text or geometry).
 */
class HUD
#ifdef MOVE4D_QT_LIBRARY
        : public QObject
#endif
{
#ifdef MOVE4D_QT_LIBRARY
    Q_OBJECT
#endif
    MOVE3D_STATIC_LOGGER;
public:
    typedef boost::function<void()> HudCallBack_t;
    typedef boost::function<void(bool)> HudInitCallBack_t;
    typedef std::pair<std::string, std::pair<HudCallBack_t,bool> > FunctionEntry_t;
    HUD();
    /// get the main HUD (acts like a singleton)
    static HUD* getHUD();
    /// initialize the HUD class so any existing or future HUD can be displayed (creates the interface with g3d)
    static void initHUD();
    virtual ~HUD();

#ifdef MOVE4D_QT_LIBRARY
public slots:
#endif
    /**
     * @brief setDraw to enable or disable this HUD
     * @param enable
     */
    void setDraw(bool enable);
    /**
     * @brief set global text size used for displaying 2d info on the screen
     */
    void setTextSize(double textSize);
    /// @brief set text size for drawing 3d text in the scene
    void setTextSize3d(double textSize3d);

    /// enable the drawing of agents shortnames above their head
    void setDrawAgentInfo(bool drawAgentInfo);
    /// draw the cost of the currently scene (through global_costSpace)
    void setDrawCost(bool drawCost);
    /// enable the drawing of advanced cost information (throug HRICS::CostManager)
    void setDrawAdvancedCost(bool drawAdvancedCost);
    /// enable the drawing of planning status (through global_Move3DPlanner, RRT, T-RRT)
    void setDrawSearchStatus(bool drawSearchStatus);
    /// enable the drawing of current joint velocity
    void setDrawVelocity(bool drawVelocity);
    /// ebnable the drawing of shoots
    void setDrawShoot(bool drawShoot);
    void setDrawFunction(const std::string &name,bool draw);

public:
    bool isDraw();
    static void drawHUD(void);

    double textSize() const;
    double textSize3d() const;

    void setAgentStaticInfoDefault();
    const std::map<Robot *, std::string> &agentStaticInfo() const;
    void setAgentStaticInfo(const std::map<Robot *, std::string> &agentStaticInfo);

    bool getDrawAgentInfo() const;
    bool getDrawCost() const;
    bool getDrawAdvancedCost() const;
    bool getDrawSearchStatus() const;
    bool getDrawVelocity() const;
    bool getDrawShoot() const;
    bool getDrawFunction(const std::string &name) const;

    /**
     * @brief getPlanningStatus fetch information about the planner
     * @return
     * get information about the planner (global_Move3DPlanner), according to its type (RRT, T-RRT,...).
     * @see PlanningStatus
     */
    static PlanningStatus getPlanningStatus();

    void addFunction(const std::string &name, HudCallBack_t f, bool draw=false);
    void addFunctionInit(const std::string &name, HudInitCallBack_t f);
    std::vector<std::string> getFunctionNames();
    std::map<std::string,std::pair<HudCallBack_t,bool> > const &getFunctions() const;

    void addCustomText( std::string label, std::string text);
    void clearCustomText(std::string label);

protected:

    /// draw this HUD taking into account its options
    virtual void draw();

    void printAgentInfo();
    void printCost();
    void printAdvancedCost();
    void printSearchStatus();
    void printVelocity();
    void printShoot(std::vector<statePtr_t> shoot);
    void printColorScale();
    void printTrajStat();
    void printCustomText();

private:
    static std::set<HUD*> _hud_to_draw; ///< possibility to have several HUDs...
    static HUD* __hud;

    std::map<Robot*,std::string> _agentStaticInfo;

    double _textSize;
    double _textSize3d;

    bool _drawAgentInfo;
    bool _drawCost;
    bool _drawAdvancedCost;
    bool _drawSearchStatus;

    bool _drawVelocity;
    bool _drawShoot;

    int _customTextStart;

    std::map<std::string, std::pair<HudCallBack_t,bool> > _functions;
    std::map<std::string, std::pair<HudInitCallBack_t,bool> > _functionsEnable; //the boolean indicates the last argument passed to the function

    std::map<std::string,std::string> _customText;
};

} // namespace API
} //namespace move4d

#endif // API_HUD_H
