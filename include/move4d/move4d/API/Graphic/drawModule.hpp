/*
 *  drawModule.h
 *  OOMove3D
 *
 *  Created by Jim Mainprice on 18/10/10.
 *  Copyright 2010 LAAS/CNRS. All rights reserved.
 *
 */

#ifndef GRAPHIC_M3D_MODULE_HPP
#define GRAPHIC_M3D_MODULE_HPP

namespace move4d
{
namespace Graphic 
{
	void initDrawFunctions();
}
} //namespace move4d
#endif
