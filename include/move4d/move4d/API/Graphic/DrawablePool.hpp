#ifndef MOVE4D_GRAPHIC_DRAWABLEPOOL_HPP
#define MOVE4D_GRAPHIC_DRAWABLEPOOL_HPP

#include <memory>
#include <vector>
#include <map>
#include <Eigen/Core>
#include "move4d/API/Grids/NDGrid.hpp"

namespace move4d {
namespace Graphic {

// drawable objects:
struct Grid3Dfloat{
    Grid3Dfloat(const std::string &name,const API::nDimGrid<float,3> &grid,bool autoscaleValues=false):
        name(name),
        grid(grid),
        autoscaleValues(autoscaleValues){}
    std::string name;
    API::nDimGrid<float,3> grid;
    bool autoscaleValues;

    static void filterCells(const std::string &name, const std::string &exportName, float minVal=0.f, float maxVal=1.f);
};
struct Grid2Dfloat{
    Grid2Dfloat(const std::string &name,const API::nDimGrid<float,2> &grid,bool autoscaleValues=false):
        name(name),
        grid(grid),
        autoscaleValues(autoscaleValues){}
    std::string name;
    API::nDimGrid<float,2> grid;
    bool autoscaleValues;
};
struct LinkedBalls2d
{
    std::string name;
    std::vector<std::pair<float,std::vector<Eigen::Vector2d> > > balls_values;
};
struct Arrows3d
{
    std::string name;
    std::vector<std::pair<Eigen::Vector3d,Eigen::Vector3d> > arrows;
};
struct LinesColored
{
    std::string name;
    std::vector<std::pair<std::pair<Eigen::Vector3d,Eigen::Vector3d> , Eigen::Vector3d> > lines_with_color;
};

template<typename T>
class DrawablePoolType
{
public:
    using ptr=std::shared_ptr<T>;
    void add(ptr v){mData[v->name] = v;}
    ptr pop(){if(mData.size()){auto it=mData.begin();ptr v=it->second;mData.erase(it);return v;} return ptr(nullptr);}
    ptr pop(const std::string &name){
        auto it=mData.find(name);
        if(it!=mData.end()){
            ptr v=it->second;
            mData.erase(it);
            return v;
        }
        return ptr(nullptr);
    }
    ptr get(const std::string &name){
        auto it=mData.find(name);
        if(it!=mData.end()){
            ptr v=it->second;
            return v;
        }
        return ptr(nullptr);
    }
    void clear(){mData.clear();}
    bool has(const std::string &name){return mData.count(name);}
private:
    std::map<std::string,ptr> mData;
};

#define DRAWABLE_POOL_MANAGE_TYPE(type) \
public:\
    static bool sAdd##type(DrawablePoolType<type>::ptr v){\
        if(getInstance()){getInstance()->add##type(v); return true;}\
        return false;\
    }\
    void add##type(DrawablePoolType<type>::ptr v){mPool##type.add(v);}\
    DrawablePoolType<type>::ptr pop##type(){ return mPool##type.pop(); }\
    DrawablePoolType<type>::ptr pop##type(const std::string &name){return mPool##type.pop(name);}\
    DrawablePoolType<type>::ptr get##type(const std::string &name){return mPool##type.get(name);}\
    void clear##type(){mPool##type.clear();}\
    bool has##type(const std::string &name){return mPool##type.has(name);}\
private:\
    DrawablePoolType<type> mPool##type;\



class DrawablePool
{
    DrawablePool();
    static DrawablePool *__instance;
public:
    static DrawablePool *createInstance();
    static DrawablePool *getInstance();

    DRAWABLE_POOL_MANAGE_TYPE(Grid3Dfloat)
    DRAWABLE_POOL_MANAGE_TYPE(Grid2Dfloat)
    DRAWABLE_POOL_MANAGE_TYPE(LinkedBalls2d)
    DRAWABLE_POOL_MANAGE_TYPE(Arrows3d)
    DRAWABLE_POOL_MANAGE_TYPE(LinesColored)

    //methods to add objects:
    //static bool sAdd2dGrid(std::shared_ptr<Drawable2dGrid> grid);
    //void add2dGrid(std::shared_ptr<Drawable2dGrid> grid);

    //methods to get objects to draw:
    //std::shared_ptr<Drawable2dGrid> pop2dGrid(const std::string &name);
    //std::shared_ptr<Drawable2dGrid> pop2dGrid();
    //void clearGrids();



    //const std::map<std::string, std::shared_ptr<Drawable2dGrid> > &getGrids() const;

private:
    //std::map<std::string,std::shared_ptr<Drawable2dGrid> > mGrids;

};

} // namespace Graphic
} // namespace move4d

#endif // MOVE4D_GRAPHIC_DRAWABLEPOOL_HPP
