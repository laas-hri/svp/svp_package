#ifndef VIRTUALGRASP_HPP
#define VIRTUALGRASP_HPP

#include "libmove3d/p3d_matrix.h"
#include "p3d/proto/p3d_matrix_proto.h"
#include "move4d/Logging/Logger.h"

#include <vector>
#include <map>
#include <memory>

#include "move4d/API/Grasps/graspTypes.hpp"

#include "move4d/API/IK/virtualIk.hpp"

namespace Json {
class Value;
}

namespace move4d
{
typedef struct struct_handDof
{
    int confId;
    int handId;
    double value;
} handDof;

typedef struct struct_graspApproach
{
    p3d_matrix4 rotation;
//    p3d_vector3 direction;
    std::vector<handDof> openConf;
    int id;
} graspApproach;


class RobotState;
class Robot;


/**
  * This virtual class correspond to the grasps.
  * The grasps are stored in the data folder as follow:
  *
  * /data
  *  /grasps
  *   /handMapping.json
  *   /graspMapping.json
  *   /grasps/<endEffectorType>/<graspName>.json
  *
  * handMapping.json conains the mapping between the robot arm and the end effector used
  * graspMapping.json maps each object to the right file (<graspName>.json)
  * the folder grasps/grasps/ contains different folder named following the different end effector types (pr2_gripper, romeo_hand, ...)
  * Each one of them contains json files where the grasps are actually stored.
  *
  * In this file, the grasps contains the Matrix _M the openConf, the Id, and the list of approach strategies, and can be retrieved into the
  * corresponding object.
  */
class VirtualGrasp
{
    MOVE3D_STATIC_LOGGER;
public:
    VirtualGrasp();
    VirtualGrasp(Robot* r, int armId, Robot* object);
    VirtualGrasp(VirtualGrasp* g);
    virtual ~VirtualGrasp();


    /**
      * @brief create configurations based on grasp data.
      * @param[out] graspConf grasping configuration to use
      * @param[out] graspOpenConf grasping configuration to use open gripper
      * @param[out] graspApproachConf the approaching config, using one of the approach strategies
      * @param[out] graspEscapeConf the escape config, following the z axis.
      * @param[out] failurePosition which of the different ik failed
      * @param[out] status the ik faillure status.
      * @param[out] nbGikCall the number of called ik
      * @param[in] approachDist the cartesian distance between graspConf and graspApproachConf
      * @param[in] escapeDist the cartesian distance between graspConf and graspEscapeConf
      * @return true if a solution is found
      */
    virtual bool computeManipulationConfs(std::shared_ptr<RobotState> & graspConf,
                                          std::shared_ptr<RobotState> & graspOpenConf,
                                          std::shared_ptr<RobotState> & graspApproachConf,
                                          std::shared_ptr<RobotState> & graspEscapeConf,
                                          std::string& failurePosition,
                                          IK::IkStatusType& status,
                                          int& nbGikCall,
                                          double& ikTime,
                                          double approachDist = 0.1,
                                          double escapeDist = 0.1) =0;



    /**
      * @brief read basic elements from json: _M, _openConf, _id, _approachStrategies
      * @param root json value containing the basics
      * @return true if it was successfully red
      */
    bool readBasicFromJson(Json::Value root);

    /**
      * @brief write basic elements to json format: _M, _openConf, _id, _approachStrategies
      * @return true if it was successfully transformed to json
      */
    Json::Value writeBasicToJson();

    /**
      * @brief read all the needed elements from the json value
      * @param root json value containing the elements
      * @return true if it was successfully red
      */
    virtual bool readFromJson(Json::Value root) = 0;

    /**
      * @brief write all elements to json format
      * @return true if it was successfully transformed to json
      */
    virtual Json::Value writeToJson() = 0;

    /**
      * @brief draw the grasp using the related end effector
      */
    virtual void draw(bool close, int strat) = 0;

    /**
      * @brief test hand col in position.
      * @return true if collision false is not
      */
    virtual std::vector<Robot*> isHandInColForGrasp() = 0;

    /**
      * @brief Compute the current matrix between the gripper and the object
      */
    void computeMatrix();

    /**
      * @brief retrieve the current hand conf
      * @return the hand conf
      */
    std::vector<handDof> getCurrentConf();

    /**
      * @brief Save the current config of the gripper
      */
    void saveConf();

    /**
      * @brief Compute the current approach strategy
      */
    void computeApproachStrat();

    /**
      * @brief Compute the inverse kinematic of the given matrix
      */
    std::shared_ptr<RobotState> computeIk(p3d_matrix4 M, IK::IkStatusType& status, double zOffset = 0);




    //getters and setters
    Robot* getRobot() {return _r;}
    void setRobot(Robot* r) {_r = r;}

    Robot* getGripper() {return _gripper;}
    void setGripper(Robot* g) {_gripper = g;}

    Robot* getObject() {return _object;}
    void setObject(Robot* o) {_object = o;}

    int getArmId() {return _armId;}
    void setArmId(int armId) {_armId = armId;}

    void getMatrix(p3d_matrix4& m) { p3d_mat4Copy(_M,m);}
    void setMatrix(p3d_matrix4 m) { p3d_mat4Copy(m,_M); }

    std::vector<handDof> getHandClosedConf() {return _closedConf;}
    void setHandClosedConf(std::vector<handDof> v) {_closedConf = v;}

    int getId() { return _id;}
    void setId(int i) { _id = i;}

    Grasps::graspTypeEnum getGType() {return _gType;}
    void setGType(Grasps::graspTypeEnum gType) {_gType = gType;}

    std::map<int,graspApproach> getApproachStategies() {return _approachStrategies;}
    graspApproach getApproachStategy(int id) {return _approachStrategies[id];}
    void addApproachStategy(graspApproach a ) {_approachStrategies[a.id] = a;}

protected:
    Grasps::graspTypeEnum _gType;

    Robot* _r;
    Robot* _gripper;
    Robot* _object;
    int _armId;

    p3d_matrix4 _M;
    std::vector<handDof> _closedConf;
    int _id;
    std::map<int,graspApproach> _approachStrategies;

};

} //namespace move4d
#endif // VIRTUALGRASP_HPP
