#ifndef GRASPMANAGER_HPP
#define GRASPMANAGER_HPP

#include "move4d/Logging/Logger.h"

#include <vector>
#include<string>
#include<map>

namespace move4d
{
class Robot;
class VirtualGrasp;

class GraspManager
{
    MOVE3D_STATIC_LOGGER;
public:
    static GraspManager* getInstance();
    ~GraspManager(){}


    /**
      * @brief retrieve the grasps from the file if they are not already retrived in _allGrasps
      * @param r The robot concerned with the grasp
      * @param armId the arm with which to grasp
      * @param object the object to grasp
      * @param overWrite remove previous grasps if true, otherwise, add the new ones to the previous ones
      * @return true if it is in _allGrasps, false otherwise (no file for example)
      */
    bool retrieveGrasps(Robot* r, int armId, Robot* object, bool overWrite = true);

    /**
      * @brief retrieve (from file or from _allGrasps) a specific grasp
      * @param r The robot concerned with the grasp
      * @param armId the arm with which to grasp
      * @param object the object to grasp
      * @param id the grasp id
      * @return the corresponding grasp (null if the grasp does not exist)
      */
    VirtualGrasp* getGrasp(Robot* r, int armId, Robot* object, int id);

    /**
      * @brief get the list of grasp ids
      * @param r The robot concerned with the grasp
      * @param armId the arm with which to grasp
      * @param object the object to grasp
      * @return the list of grasps ids
      */
    std::vector<int> getGraspsIds(Robot* r, int armId, Robot* object);

    /**
      * @brief If the grasp are in _allGrasps, it returns it, otherwise it load it from the file, and then returns it
      * @param r The robot concerned with the grasp
      * @param armId the arm with which to grasp
      * @param object the object to grasp
      * @return the grasps available for this robot/arm/object
      */
    std::vector<VirtualGrasp*> getGrasps(Robot* r, int armId, Robot* object);

    /**
      * @brief clear _allGrasps to force file reading
      */
    void resetGrasps();

    void setDrawAll(Robot *r,int armId,Robot *obj,bool enable);

    void drawAllGrasps_();
    static void drawAllGrasps();

    /**
      * @brief form the filepath from the grasps inputs
      * @param r The robot concerned with the grasp
      * @param armId the arm with which to grasp
      * @param object the object to grasp
      * @return the file path
      */
    std::string getNominalFilePath(Robot* r, int armId, Robot* object);

    /**
      * @brief form the filepath from the grasps inputs and the objectMapping.json file
      * @param r The robot concerned with the grasp
      * @param armId the arm with which to grasp
      * @param object the object to grasp
      * @return the file path
      */
    std::string getMappedFilePath(Robot* r, int armId, Robot* object);


    /**
      * @brief retrieve the grasps from the corresponding file
      * @param r The robot concerned with the grasp
      * @param armId the arm with which to grasp
      * @param object the object to grasp
      * @return a map with the graspIds as keys and the grasps as values
      */
    std::vector<VirtualGrasp*> readFile(Robot* r, int armId, Robot* object);

    /**
      * @brief save the content of a specific list of grasp in a file
      * @param r The robot concerned with the grasp
      * @param armId the arm with which to grasp
      * @param object the object to grasp
      * @param filePath the path where to write the file
      * @return true if the file is written successfully
      */
    bool saveGraspsToFile(Robot* r, int armId, Robot* object, std::string filePath);

    /**
      * @brief add a grasp to the corresponding list of grasps
      * @param g The grasp to add
      */
    void addGrasp(VirtualGrasp* g);

    /**
      * @brief retriver from file the list of arms for the specified robot
      * @param r The robot concerned with the grasp
      * @return the list of arm ids
      */
    std::vector<int> getRobotArms(Robot* r);

    /**
      * @brief create a new grasp
      * @param gType type of grasp to create
      * @param r The robot concerned with the grasp
      * @param armId the arm to use for the grasp
      * @param object the object to grasp
      * @return a virtualGrasp
      */
    VirtualGrasp* createGrasp(std::string gType, Robot* r, int armId, Robot* object);

    /**
      * @brief create a new grasp from a previous grasp
      * @param g the vitual grasp to duplicate
      * @return a duplicated virtualGrasp
      */
    VirtualGrasp* duplicateGrasp(VirtualGrasp* g);

    bool convertFromOld(std::string gType, Robot *r, int armId, Robot *object);


private:
    GraspManager();
    static GraspManager* _instance;

//    std::map<Robot*,std::map<Robot*,std::map<int,VirtualGrasp*> > > _allGrasps;
    std::vector<VirtualGrasp*> _allGrasps;

    Robot *_robotToDraw,*_objectToDraw;
    int _armIdToDraw;
};

} //namespace move4d
#endif // GRASPMANAGER_HPP
