#ifndef GRASPTYPES_HPP
#define GRASPTYPES_HPP

#include <string>
#include <map>

#include "move4d/Logging/Logger.h"

namespace move4d
{

class VirtualGrasp;
class Robot;

namespace Grasps {
typedef enum graspTypeEnum{
    fullGrasp,
    cylindricGrasp,
    unknownGrasp
} graspTypeEnum;
}

class GraspTypes
{
    MOVE3D_STATIC_LOGGER;
public:
    static GraspTypes* getInstance();
    ~GraspTypes(){}

    /**
      * @param gType the grasp type as a string
      * @return the graspTypeEnum corresponding to the string
      */
    Grasps::graspTypeEnum getGraspTypeFromString(std::string gType);

    /**
      * @param gType the grasp type as a graspTypeEnum
      * @return the string corresponding to the graspTypeEnum
      */
    std::string getStringFromGraspType(Grasps::graspTypeEnum gType);

    /**
      * @brief createan object of the corresponding type
      * @param gtype the type of grasp to create
      * @param r the robot going to grasp
      * @param gripper the gripper of the grasp
      * @param object the object to grasp
      * @return the grasp under the form of a virtualGrasp
      */
    VirtualGrasp* createGrasp(Grasps::graspTypeEnum gType,
                              Robot* r,
                              int armId,
                              Robot* object);

    /**
      * @brief same as createGrasp(Grasps::graspTypeEnum gType,...) with string as input
      */
    VirtualGrasp* createGrasp(std::string gType,
                              Robot* r,
                              int armId,
                              Robot* object);

    /**
      * @brief duplicate an existing grasp
      * @param g the grasp to duplicate
      * @return the duplicated grasp
      */
    VirtualGrasp* duplicateGrasp(VirtualGrasp* g);

    /**
      * @brief _stringGraspMapgetter
      */
    std::map<std::string,Grasps::graspTypeEnum> getStringGraspMap() {return _stringGraspMap;}

    /**
      * @brief _defaultGraspType getter
      */
    Grasps::graspTypeEnum getDefaultGraspType() {return _defaultGraspType;}

private:
    GraspTypes();
    static GraspTypes* _instance;

    std::map<std::string,Grasps::graspTypeEnum> _stringGraspMap;

    Grasps::graspTypeEnum _defaultGraspType;
};

} //namespace move4d
#endif // GRASPTYPES_HPP
