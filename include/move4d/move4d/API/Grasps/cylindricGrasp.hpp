#ifndef CYLINDRICGRASP_HPP
#define CYLINDRICGRASP_HPP

#include "move4d/API/Grasps/virtualGrasp.hpp"

namespace move4d
{
class CylindricGrasp : public VirtualGrasp
{
    MOVE3D_STATIC_LOGGER;
public:
    CylindricGrasp();
    CylindricGrasp(Robot *r,int armId,Robot *object);
    CylindricGrasp(CylindricGrasp*g);

    // VirtualGrasp interface
public:
    virtual bool computeManipulationConfs(std::shared_ptr<RobotState> &graspConf, std::shared_ptr<RobotState> &graspOpenConf, std::shared_ptr<RobotState> &graspApproachConf, std::shared_ptr<RobotState> &graspEscapeConf, std::string &failurePosition, IK::IkStatusType &status, int &nbGikCall, double &ikTime, double approachDist, double escapeDist);
    virtual bool readFromJson(Json::Value root);
    virtual Json::Value writeToJson();
    virtual void draw(bool close, int start){return draw(close,start,NULL);}
    virtual void draw(bool close, int strat, p3d_matrix4 *M);
    virtual std::vector<Robot *> isHandInColForGrasp();
    virtual bool isHandInColForGrasp(double angle);

private:
    double _increment;
};

} //namespace move4d
#endif // CYLINDRICGRASP_HPP
