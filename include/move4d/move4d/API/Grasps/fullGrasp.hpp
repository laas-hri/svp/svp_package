#ifndef FULLGRASP_HPP
#define FULLGRASP_HPP

#include "move4d/API/Grasps/virtualGrasp.hpp"

namespace move4d
{
class FullGrasp : public VirtualGrasp
{
    MOVE3D_STATIC_LOGGER;
public:
    FullGrasp();
    FullGrasp(Robot* r, int armId, Robot* object);
    FullGrasp(FullGrasp* g);
    virtual ~FullGrasp(){}

    bool computeManipulationConfs(std::shared_ptr<RobotState> & graspConf,
                                  std::shared_ptr<RobotState> & graspOpenConf,
                                  std::shared_ptr<RobotState> & graspApproachConf,
                                  std::shared_ptr<RobotState> & graspEscapeConf,
                                  std::string& failurePosition,
                                  IK::IkStatusType& status,
                                  int& nbGikCall,
                                  double& ikTime,
                                  double approachDist = 0.1,
                                  double escapeDist = 0.1) ;

    bool readFromJson(Json::Value root);
    Json::Value writeToJson();
    void draw(bool close, int strat);
    std::vector<Robot*> isHandInColForGrasp();
};

} //namespace move4d
#endif // FULLGRASP_HPP
