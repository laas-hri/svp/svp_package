#ifndef UNKNOWN_ROBOT_HPP
#define UNKNOWN_ROBOT_HPP

#include <stdexcept>
namespace move4d{
namespace API{
class unknown_robot : public std::logic_error
{
public:
    explicit unknown_robot(const std::string &robot_name) :
        std::logic_error("no robot known as "+robot_name)
    {}
};
}
}

#endif // UNKNOWN_ROBOT_HPP
