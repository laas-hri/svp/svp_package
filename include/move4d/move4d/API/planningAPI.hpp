#ifndef PLANNING_API_HPP_
#define PLANNING_API_HPP_

/**
 * C++ basic headers (they all have name spaces)
 */
#include <iostream>
#include <iomanip>
#include <iosfwd>
#include <sstream>
#include <fstream>
#include <string>
//#include <vector>
#include <set>
#include <map>
#include <list>
#include <utility>
#include <cstdlib>
#include <limits>
#include <algorithm>
#include <memory>

/**
 * Environment has to be included before anything (weird)
 */
#include "libmove3d/p3d/env.hpp"

/**
 * The CPP API so that
 * Robot is first and Graph is last (kind of tricky because its backwards)
 */

#include <Eigen/Core>
#define EIGEN_USE_NEW_STDVECTOR
#include <Eigen/StdVector>
#include <Eigen/Geometry> 
#include <Eigen/LU>

// RobotState has no dependencies and is used by most other classes
#include "move4d/API/ConfigSpace/RobotState.hpp"
#include "move4d/API/ConfigSpace/localpath.hpp"

// Dependency to robot
#include "move4d/API/Device/robot.hpp"

// Graph, Node, Edge, Robot and Localpath have no interdependencies
#include "move4d/API/Roadmap/node.hpp"
#include "move4d/API/Roadmap/edge.hpp"
//#include "move4d/API/Roadmap/compco.hpp"
#include "move4d/API/Roadmap/graph.hpp"

#include "move4d/API/Search/AStar/AStar.hpp"
#include "move4d/API/Search/Dijkstra/dijkstra.hpp"

#include "move4d/API/Trajectory/PathFactory.hpp"
#include "move4d/API/ConfigSpace/localpath_factory.hpp"
//#include "move4d/planner/TrajectoryOptim/Classic/costOptimization.hpp"
//#include "move4d/planner/TrajectoryOptim/Classic/smoothing.hpp"
#include "move4d/API/Trajectory/trajectory.hpp"

#include "move4d/API/scene.hpp"
#include "move4d/API/project.hpp"

#endif
