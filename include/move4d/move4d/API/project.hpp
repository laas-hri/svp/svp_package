#ifndef WORKSPACE_HPP
#define WORKSPACE_HPP

#include "move4d/API/scene.hpp"
#include "move4d/Logging/Logger.h"

/**
  * @ingroup NEW_CPP_MODULE
  * @defgroup CPP_API C++ Planning API
  * @brief Implements in C++ an interface to the more low level functionalities
  */

namespace move4d
{
namespace API{class CollisionInterface;}

/**
 * @ingroup CPP_API
 * \brief Classe représentant l'espace de travail de l'application
 * @author Florian Pilardeau,B90,6349 <fpilarde@jolimont>
 */
class Project {

    MOVE3D_STATIC_LOGGER;
public:
	/**
	 * Class constructor
	 */
    Project(Scene* sc);
    Project();

	/**
	 * Class Destructor
	 */
	~Project();

    static Project *createGlobalProject(int argc, char *argv[]);
    static Project *createGlobalProject(const std::string &p3d_file,
                                        const std::string &sce_file=std::string(""),
                                        const std::vector<std::string> &modules_to_activ=std::vector<std::string>(0),
                                        const std::vector<std::string> &plugins=std::vector<std::string>(0));
    static Project *createGlobalProject(int argc, char *argv[],boost::function<Robot*(rob*)> robotCreator);
    static Project *createGlobalProject(boost::function<Robot*(rob*)> robotCreator,
                                        const std::string &p3d_file,
                                        const std::string &sce_file=std::string(""),
                                        const std::vector<std::string> &modules_to_activ=std::vector<std::string>(0),
                                        const std::vector<std::string> &plugins=std::vector<std::string>(0));

    /**
     * @brief initializes the project by initializing modules.
     * cf. addModule(), which sets the modules to start.
     *
     */
    bool init();
    /**
     * @brief add a module to the project.
     * @param name name of the module
     *
     * The module will be initialized and enabled when init() is called.
     */
    void addModule(const std::string &name);

	/**
     * get the active Scene
     * @return the active Scene
     * @deprecated see setActiveScene()
	 */
	Scene* getActiveScene();
	
    /**
      * modifie la Scene active
      * @param name the name of the new active Scene
      * @deprecated useless/probably very unstable
	  */
    void setActiveScene(std::string name);

	/**
     * insert a new Scene to the list of Scenes
     * @param E the new Scene
	 */
	void insertScene(Scene* E);

    /**
      * returns the user-set number of jobs, or the number of cores - 1
      * return value is guaranteed to be >=1
      */
    unsigned int getNumberOfJobs();

    API::CollisionInterface *getCollision() const;
    void setCollision(API::CollisionInterface *Collision);

    int diagnose() const;

protected:
    void loadPlugins(const std::vector<std::string> &pathes);
    bool loadPlugins();

private:
        std::vector<Scene*> m_Scenes;/*!< le vecteur des Scene chargés*/
        std::string m_activeScene;/*!< le nom de l'Scene actif*/
        API::CollisionInterface *m_Collision;
};

extern Project* global_Project;

} //namespace move4d
#endif
