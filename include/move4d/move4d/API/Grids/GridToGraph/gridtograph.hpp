#ifndef GRIDTOGRAPH_H
#define GRIDTOGRAPH_H

#include "move4d/API/Grids/ThreeDGrid.hpp"

#include "move4d/API/Device/robot.hpp"
#include "move4d/API/Roadmap/graph.hpp"

namespace move4d
{
/**
  @ingroup GRID
  */
class GridToGraph : public API::ThreeDGrid
{
public:
    GridToGraph();
    GridToGraph( Eigen::Vector3i size );
    GridToGraph( double pace, std::vector<double> envSize );

    API::ThreeDCell* createNewCell(unsigned int index,unsigned  int x,unsigned  int y,unsigned  int z );

    void putGridInGraph();

private:
    Robot* _Robot;
    Graph* _Graph;
};

} //namespace move4d
#endif // GRIDTOGRAPH_H
