#ifndef MOVE4D_API_TWODCOSTGRID_HPP
#define MOVE4D_API_TWODCOSTGRID_HPP

#include "move4d/API/Grids/TwoDGrid.hpp"
#include "move4d/API/Grids/TwoDCostCell.hpp"


namespace move4d {
namespace API {

class TwoDCostGrid : public TwoDGrid
{
public:
    TwoDCostGrid();
    TwoDCostGrid(const Eigen::Vector2i &size, const std::vector<double> &envsize);
    TwoDCostGrid(double samplingRate, const std::vector<double> &envsize);

    virtual Eigen::MatrixXd matrix();

protected:
    virtual TwoDCell *createNewCell(unsigned int index, unsigned int x, unsigned int y) override;
};

} // namespace API
} // namespace move4d

#endif // MOVE4D_API_TWODCOSTGRID_HPP
