#ifndef NDGRID_HPP
#define NDGRID_HPP

#include <iostream>
#include <vector>

#include <Eigen/Core>

namespace move4d
{
/**
 * Base class for a Grid
 */
namespace API
{
template <typename T,int NUM_DIMS >
class nDimGrid
{

public:
    using ArrayValues     = typename std::vector<T>; //dynamic storage
    using ArrayCoord      = std::array<size_t,NUM_DIMS>;
    using SpaceCoord      = std::array<float,NUM_DIMS>;

    using value_type      = typename ArrayValues::value_type;      // T
    using reference       = typename ArrayValues::reference;       // T&
    using const_reference = typename ArrayValues::const_reference; // const T&
    using size_type       = typename ArrayValues::size_type;       // size_t
    using iterator        = typename ArrayValues::iterator;        // random access iterator
    using const_iterator  = typename ArrayValues::const_iterator;

    static const int Dimension=NUM_DIMS;

    //exceptions
    using out_of_grid     = std::out_of_range;
    /**
         * Constructors
         */
    nDimGrid() { }
    nDimGrid( SpaceCoord cellSize, bool adjust, std::vector<double> envSize)
    {
        if(envSize.size() != NUM_DIMS*2){
            throw std::invalid_argument("dimension mismatch in nDimGrid");
        }
        for (unsigned int  i=0; i< envSize.size() ; i += 2)
        {
            m_originCorner[i/2] = envSize.at(i);
        }

        uint n_cell{1};
        if(!adjust){
            for ( unsigned int i=0;i<NUM_DIMS; ++i){
                double l=std::abs(envSize.at(i*2+1) - envSize.at(i*2));
                m_cellSize[i]=cellSize[i];
                m_nbOfCell[i]=std::floor(l / cellSize[i]);
                n_cell*=m_nbOfCell[i];
            }
        }else{
            for (unsigned int  i=0; i< envSize.size() ; i += 2){
                double l=std::abs(envSize.at(i+1) - envSize.at(i));
                m_nbOfCell[i/2] = std::round(l/cellSize[i/2]);
                m_cellSize[i/2] = l / m_nbOfCell[i/2];
                n_cell*=m_nbOfCell[i/2];
            }
        }
        values_.assign(n_cell,value_type());
    }
    nDimGrid( float samplingRate, bool adjust, std::vector<double> envSize)
    {
        SpaceCoord cellsize;
        cellsize.fill(samplingRate);
        *this=nDimGrid(cellsize,adjust,envSize);
    }
    nDimGrid( ArrayCoord size, std::vector<double> envSize )
    {
        //std::swap(m_nbOfCell,size);
        for(size_t  i=0;i<size.size();++i){
            m_nbOfCell[i]=size[i];
        }
        if(envSize.size() != NUM_DIMS*2){
            throw std::invalid_argument("dimension mismatch in nDimGrid");
        }

        for (unsigned int  i=0; i< envSize.size() ; i += 2)
        {
            m_cellSize[i/2] = (envSize.at(i+1) - envSize.at(i)) / size[i/2] ;
            m_originCorner[i/2] = envSize.at(i);
        }
        size_t n_cell=1;
        for (size_t d : m_nbOfCell){
            n_cell *= d;
        }
        values_.assign(n_cell,value_type());
    }
    nDimGrid( SpaceCoord origin, ArrayCoord size, SpaceCoord cellSize)
    {
        for(size_t  i=0;i<size.size();++i){
            m_nbOfCell[i]=size[i];
        }
        for(size_t  i=0;i<cellSize.size();++i){
            m_cellSize[i]=cellSize[i];
        }
        for(size_t  i=0;i<origin.size();++i){
            m_originCorner[i]=origin[i];
        }
        size_t n_cell=1;
        for (size_t d : m_nbOfCell){
            n_cell *= d;
        }
        values_.assign(n_cell,value_type());
    }
    template<typename other_t>
    nDimGrid(const nDimGrid<other_t,NUM_DIMS> &other):
        m_cellSize(other.getCellSize()),
        m_nbOfCell(other.shape()),
        m_originCorner(other.getOriginCorner())
    {
        values_.reserve(getNumberOfCells());
        for(other_t o : other){
            values_.push_back(static_cast<value_type>(o));
        }
    }

    iterator       begin()        { return values_.begin();  }
    const_iterator begin()  const { return values_.begin();  }
    const_iterator cbegin() const { return values_.cbegin(); }
    iterator       end()          { return values_.end();    }
    const_iterator end()    const { return values_.end();    }
    const_iterator cend()   const { return values_.cend();   }

    reference       operator[] (size_type idx)       { return values_[idx]; }
    const_reference operator[] (size_type idx) const { return values_[idx]; }

    reference operator[] (const ArrayCoord& coord) {
        return getCell(coord);
    }
    const_reference operator[] (const ArrayCoord& coord) const {
        return const_cast<reference>(static_cast<const nDimGrid&>(*this)[coord]);
    }

    /**
         * Destructor
         */
    virtual ~nDimGrid() { }

    /**
         * Returns the dimension of on cell
         */
    SpaceCoord getCellSize() const { return m_cellSize; }
    SpaceCoord getOriginCorner() const {return m_originCorner;}

    /**
         * Accessors to cells
         */
    const_reference getCell(unsigned int i) const
    {
        return values_[i];
    }
    reference getCell(unsigned int i)
    {
        return values_[i];
    }
    const_reference getCell(const ArrayCoord &coordinates) const
    {
        return (*this)[coordinates];
    }
    reference getCell(const ArrayCoord & coordinate)
    {
        return getCell(getCellIndex(coordinate));
    }
    size_type getCellIndex(const ArrayCoord & coordinate) const
    {
        unsigned int j=0;
        unsigned int coeff=1;

        for (unsigned int i=0; i<m_nbOfCell.size(); i++)
        {
            if(coordinate[i]<0 || coordinate[i] >= m_nbOfCell[i])
            {
                throw out_of_grid("nDimGrid Error : out of bands in dimension ");
            }

            j += coeff*coordinate[i];
            coeff *= m_nbOfCell[i];
        }

        return j;
    }
    reference getCell(const SpaceCoord & pos)
    {
        return getCell(getCellCoord(pos));
    }

    /**
         * Returns the coordinate of a cell in the grid
         */
    ArrayCoord getCellCoord(unsigned int index)const
    {
        ArrayCoord coordinate;

        unsigned int coeff=1.0;

        for (unsigned int j=0; j<m_nbOfCell.size(); j++)
        {
            coordinate[j] = (index/coeff) % (m_nbOfCell[j]);
            coeff *= m_nbOfCell[j];
        }

        assert(index == getCellIndex(coordinate));
        return coordinate;
    }
    ArrayCoord getCellCoord(const SpaceCoord &pos) const
    {
        ArrayCoord coordinate;

        for (unsigned int i=0; i<NUM_DIMS; i++)
        {
            coordinate[i] = static_cast<unsigned int>(floor((pos[i]-m_originCorner[i])/m_cellSize[i]));
        }
        return coordinate;
    }

    /**
         * Returns the number of cell in the grid
         */
    unsigned int getNumberOfCells() const
    {
        return values_.size();
    }

    /**
         * Returns the neighbor cells
         * of the cell at coordinate 'pos'
         */
    ArrayCoord getNeighbour( const ArrayCoord & coordinate, int ith_neigh) const
    {
        unsigned int totDirections=neighboursNumber();
        unsigned int dimension = NUM_DIMS;


        if( ith_neigh<0 || ith_neigh>=totDirections )
        {
            throw out_of_grid("not that much neighbours in nDimGrid");
        }
        else
        {
            if(ith_neigh>=(totDirections/2)) ith_neigh++; // it would be  the cell itself

            int coeff=1;

            ArrayCoord NeighboorCoord;

            for (int i=0; i<dimension; i++)
            {
                NeighboorCoord[i] = coordinate[i] + ((ith_neigh/coeff) % 3 - 1);
                coeff *= 3;
            }

            return NeighboorCoord;
        }
    }
    size_t neighboursNumber() const {return std::pow(3,NUM_DIMS)-1;}

    /**
         * Function to display
         * the grid
         */
    virtual void draw() {}

    SpaceCoord getCellCenter(const ArrayCoord &coord) const
    {
        SpaceCoord pos;
        for (size_t i=0;i<NUM_DIMS;++i){
            pos[i] = m_originCorner[i] + m_cellSize[i] * coord[i]; //that's the corner
            pos[i] += m_cellSize[i]/2; //that's the center
        }
        return pos;
    }

    ArrayCoord shape() const {return m_nbOfCell;}

    const ArrayValues &getValues() const { return values_; }

protected:
    /**
         * All cells are stored
         * in this array
         */
    ArrayValues values_;

    /**
         * Origin corner coordinate
         */
    SpaceCoord m_originCorner;

    /**
         * One cell dimension (Size)
         */
    SpaceCoord m_cellSize;

    /**
         * Number of Cell per dimension
         */
    ArrayCoord m_nbOfCell;
};


}

} //namespace move4d
#endif // NDGRID_HPP
