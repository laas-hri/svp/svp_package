#ifndef NDGRIDALGO_HPP
#define NDGRIDALGO_HPP

#include "move4d/Logging/Logger.h"
#include "move4d/API/Grids/NDGrid.hpp"
#include <limits>

namespace move4d{ namespace API{ namespace ndGridAlgo{

template<typename size_type,typename dist_t>
struct DijkstraCell{
    size_type cell;
    size_type previous;
    dist_t dist=std::numeric_limits<dist_t>::infinity();
    bool operator>(const DijkstraCell<size_type,dist_t> &other){return dist > other.dist;}
    bool operator==(const DijkstraCell<size_type,dist_t> &other){return (cell==other.cell);}
    bool operator!=(const DijkstraCell<size_type,dist_t> &other){return (cell!=other.cell);}
    explicit operator float(){return static_cast<float>(dist);}
};
struct IsValidBool
{
    template<typename T>
    bool operator () (T v){return v;}
};
template <class G, class dist_type, class IsValid=IsValidBool>
class Dijkstra{
    MOVE3D_STATIC_LOGGER;
public:
    using dist_t=dist_type;
    using Grid = G;
    using size_type=typename Grid::size_type;
    using DCell=DijkstraCell<size_type,dist_t>;
    using DGrid=nDimGrid<DCell, Grid::Dimension >;
    using reference=typename DGrid::reference;
    using ArrayCoord=typename G::ArrayCoord;
    using SpaceCoord=typename G::SpaceCoord;

    Dijkstra(): mGrid(nullptr){}
    Dijkstra(Grid *grid, const ArrayCoord &from, dist_t max_dist=dist_t(),IsValid valid=IsValid(), uint max_depth=0):
        mGrid(grid),
        mIsValid(valid)
    {
        mDGrid = DGrid(grid->getOriginCorner(),grid->shape(),grid->getCellSize());
        run(from,max_dist);
    }

    const DGrid &getGrid() const {return mDGrid;}

    dist_t getCost(size_type cell_ref){
        return mDGrid[cell_ref].dist;
    }
    dist_t getCost(const ArrayCoord &cell_ref){
        return mDGrid[cell_ref].dist;
    }
    dist_t getCostPos(const SpaceCoord &pos){
        return mDGrid.getCell(pos).dist;
    }

protected:
    struct Compare{
    bool operator()(DCell *a,DCell *b){return a->dist > b->dist;}
    };
    dist_t dist(const SpaceCoord &a,const SpaceCoord &b)
    {
        dist_t sq{0};
        for(size_type i =0;i<a.size();++i){
            sq+=std::pow(a[i]-b[i],2);
        }
        return std::sqrt(sq);
    }
    void run(const ArrayCoord &from, dist_t max_dist)
    {
        using namespace std;
        vector<DCell*> pq;

        for(size_type i=0;i<mGrid->getNumberOfCells();++i){
            reference c = mDGrid[i];
            c.cell=i;
            c.previous=i;
            if(mIsValid(mGrid->getCell(i)))
                pq.push_back(&c);
        }
        mDGrid[from].dist=0;
        make_heap(pq.begin(),pq.end(),Compare());

        while(!pq.empty()){
            size_type u=pq.front()->cell;
            DCell &u_cell=mDGrid[u];
            pop_heap(pq.begin(),pq.end(),Compare());
            pq.pop_back();
            if(max_dist && u_cell.dist > max_dist) continue; //skip neighbours of cells already further than max_dist
            for(size_type i=0;i<mDGrid.neighboursNumber();++i){
                try{
                ArrayCoord v=mDGrid.getNeighbour(mDGrid.getCellCoord(u),i);
                dist_t c=dist(mDGrid.getCellCenter(v),mDGrid.getCellCenter(mDGrid.getCellCoord(u)));//dist u-v
                DCell &v_cell=mDGrid[v];
                if(v_cell.dist > u_cell.dist + c && mIsValid(mGrid->getCell(v_cell.cell))){
                    //decrease dist of v
                    v_cell.dist=u_cell.dist+c;
                    v_cell.previous=u;
                    //find v in pq:
                    typename vector<DCell*>::size_type search_i=0;
                    while(*pq[search_i]!=v_cell){++search_i;}
                    //pq[search_i] is v_cell
                    //reorder the heap
                    while(search_i!=0 && *pq[(search_i-1)/2] > *pq[search_i]){
                        //swap heap father and son
                        swap(pq[search_i],pq[(search_i-1)/2]);
                        search_i=(search_i-1)/2;
                    }
                }
                }catch(typename Grid::out_of_grid&){/*that's fine*/}
            }
        }
    }

    Grid *mGrid;
    DGrid mDGrid;
    IsValid mIsValid;
};
}}}

#endif // NDGRIDALGO_HPP
