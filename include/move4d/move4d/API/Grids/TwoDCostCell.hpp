#ifndef MOVE4D_API_TWODCOSTCELL_HPP
#define MOVE4D_API_TWODCOSTCELL_HPP

#include "move4d/API/Grids/TwoDCell.hpp"


namespace move4d {
namespace API {

class TwoDCostCell : public TwoDCell
{
public:
    TwoDCostCell();
    TwoDCostCell(int i, const Eigen::Vector2d &corner, TwoDGrid *grid);

    double cost() const;
    void setCost(double cost);

protected:
    double mCost;
};

} // namespace API
} // namespace move4d

#endif // MOVE4D_API_TWODCOSTCELL_HPP
