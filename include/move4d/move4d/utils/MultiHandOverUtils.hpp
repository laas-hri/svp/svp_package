#ifndef MULTIHANDOVERUTILS_HPP
#define MULTIHANDOVERUTILS_HPP

#include <Eigen/Core>

#include "P3d-pkg.h"
#include "move4d/API/Device/robot.hpp"
#include "move4d/HRI_costspace/Grid/HRICS_MultiAgentGrid.hpp"
#include "move4d/utils/multiHandOver/LazyWAstar.h"
#include "move4d/utils/multiHandOver/SimpleGraph.h"
//#include "move4d/utils/MultiHandOverAlgos.h"
//#include "move4d/HRI_costspace/HRICS_MultiHandOver.h"
#include "move4d/utils/multiHandOver/graphAlgorithms.h"
#include "move4d/Logging/Logger.h"

#include "move4d/GTP.bkp/taskManagerInterface.hpp"


namespace move4d
{
extern taskManagerInterface *TMI;


namespace HRICS {
class MultiHandOver;

namespace MHO{
class AbstractSolutionTools;
class AbstracHandOverObjectCheck;
class ToolSet;


/**
 * @brief The AlgoLoggerInterface class defines dummy logging methods
 * that will be used for textual log and display of classes in this file.
 * The class have to be derived in order to provide effective logging
 */
class AlgoLoggerInterface{
public:
    AlgoLoggerInterface()
    {
    }
    virtual ~AlgoLoggerInterface(){}
    //virtual void textlog(std::stringstream ss){}
    virtual void drawcells(MultiAgentCell* c1,MultiAgentCell* c2,bool marker){}
    virtual void draw() {}
    virtual void resetdisp() {}
    //virtual void drawcellsdist(std::vector<MultiAgentCell*> &cells) =0;
};

/**
 * @brief The ConfigFileParser class parses a stream representing data as an XML file.
 *
 * Everything in it is optional and no error will be prompted
 *
 * Sample configuration file :
 *
 * \verbatim
<!DOCTYPE xml>
<MHOSettings>
    <!-- speed is in m/s (meters per second), use cost is in [0:1] or inf, name should exactly match, or will be ignored-->
    <agent speed="1" use_cost="2" name="HERAKLES_HUMAN0"/> <!--use_cost will be set to 1 -->
    <agent speed="1" use_cost="inf" name="HERAKLES_HUMAN2"/>
    <agent name="PR2_ROBOT4"/> <!-- default speed and cost are 1-->
    <agent use_cost="infinity" name="PR2_ROBOT3"/>
    <agent speed="10" use_cost="0.1" name="PR2_ROBOT0"/>
    <agent speed="1" use_cost="-1" name="PR2_ROBOT2"/>
        <!-- use_cost="-1" will parse as infinity or inf. Any negative value and non-double value will parse as inf.-->
    <cost_factors distance="0" hri="1" time="1" agent="6"/>
    <stop_conditions iterations="50" time="-1" overheat="100"/>
        <!-- for random search only, defaults are -1 for all (infinie loop), -1 means no limit, time is in seconds -->
    <from_to from="PR2_ROBOT4" to="HERAKLES_HUMAN0" object="RED_TAPE"/> <!-- target and holder agents, object manipulated-->
    <algo_opts>
        <frontier_search rrt="1" dummy_smooth="1"/> <!-- use RRT for frontier checking or
            default alternative to it, do smoothing on these trajectories (int values
            are casted into bool)-->
        <search_algo name="lwa"/> <!-- one of "lwa" or "random", case insensitive-->
        <lwa_star epsilon="1.3"/> <!-- for "lwa only, should be >=1 -->
        <grid cellsize="0.20"/>     <!-- grid cell size in meters -->
    </algo_opts>
</MHOSettings>
\endverbatim
 *
 */
class ConfigFileParser
{
public:
    ConfigFileParser(MultiHandOver *mho);
    bool configure_from_xml(std::istream & is);
private:
    MultiHandOver* _mho;
};

class MultiHandOverAgent{
public:
    MultiHandOverAgent(Robot *r,double use_cost,double speed,unsigned int index);
    Robot *robot;
    bool is_human;
    unsigned int index;
    std::string shortname;
    double use_cost;
    double speed;
    double safe_dist;///< for human only, the minimal distance to feel safe about surrounding robots
    double handover_comfort_dist;///< for human only, optimal arm extension distance to get an object (must be in [0..reach])
    /// the reach distance of the arm of the agent. -inf if not set/known
    double reach();
    /// return the shoulder height
    /// @todo is dummy for robots. It's a nasty bug resolution waiting for a better way of computing object check (constant to be abble to HO through windows)
    double shoulderHeight();
    bool have_final_position;
};

class MultiHandOver3dPlanner
{
public:
    MultiHandOver3dPlanner(Robot * robot, bool is_human);

    bool grabConfIK(configPt &q, statePtr_t robotPos, const Eigen::Vector3d & grab_point);

    /**
     * @brief given a 2D position for the robot and a 3d position for the object, find a free grabing conf
     * @param q [out] the configuration (null if not found)
     * @param robot_pos the position (x,y) for the robot
     * @param grab_point the point to grab
     * @return true if found, false otherwise
     * sets the orientation of the robot so it faces the point
     */
    bool grabConfCheck(configPt &q, const Eigen::Vector2d robot_pos, const Eigen::Vector3d & grab_point);
    bool grabConfIK_human(configPt &q, statePtr_t const humanPos, Eigen::Vector3d const & grab_point);

    bool computeHandOverTrajectory(statePtr_t q_start, statePtr_t q_end);
    void clearTraj(statePtr_t conf= statePtr_t((RobotState *)0));
    bool computeTrajectory(statePtr_t start_conf, statePtr_t nav_conf, std::vector<Eigen::Vector2d,Eigen::aligned_allocator<Eigen::Vector2d> > const& path2d, statePtr_t end_conf);
    bool computePath2dToPath3dShortCut(std::vector<Eigen::Vector2d,Eigen::aligned_allocator<Eigen::Vector2d> > const& path2d, std::vector<Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > &path3d );

private:
    Robot * _robot;
    bool _is_human;
};



class MultiHandOverSolution{
public:
    class Action{
    private:
        //Robot *agentA,*agentB;
        //FrontierPtr frontier;
        //std::vector<MultiAgentCell*> &pathA, &pathB;
        //double &distA,&distB;
        unsigned int _f;
        MultiHandOverSolution * _s;
        static void checkI(unsigned int i){if(i>1){throw(std::invalid_argument("argument must be 0 or 1"));}}
    public:
        Action(unsigned int frontier_index,MultiHandOverSolution * solution);
        Robot* robot(unsigned int i){checkI(i);return _s->agents[_f+i]->robot;}
        MultiHandOverAgent * agent(unsigned int i){checkI(i);return _s->agents[_f+i];}
        std::vector<MultiAgentCell*> & path(unsigned int i){ checkI(i); return _s->navigation_trajs[_f*2+i];}
        void setPath(unsigned int i,std::vector<MultiAgentCell*> &path){ checkI(i); _s->navigation_trajs[_f*2+i]=path;}
        ///the cell the agent come from (previous action position, if any, 0 otherwise)
        MultiAgentCell* from(unsigned int i){return (i<2 && _s->navigation_trajs[_f*2+i].size() ? _s->navigation_trajs[_f*2+i][0] : 0);}
        ///the cell the agent goes to for the next action, if any, 0 otherwise
        MultiAgentCell* to(unsigned int i){return (i<2 && _s->navigation_trajs[_f*2+i].size() ? _s->navigation_trajs[_f*2+i].back() : 0);}
        double dist(unsigned int i){return (i<2 ? _s->nav_dists[_f*2+i]: -1 );}
        void setDist(unsigned int i,double d){(i<2 ? _s->nav_dists[_f*2+i]=d: -1 );}
        double startDate(unsigned int i){checkI(i); return _s->nav_start_dates[_f*2+i];}
        void setStartDate(unsigned int i, double date_s){checkI(i); _s->nav_start_dates[_f*2+i]=date_s;}
        double startDateHO(){return _s->ho_start_end_dates[_f].first;}
        void setStartDateHO(double start_s){_s->ho_start_end_dates[_f].first=start_s;}
        double endDateHO(){return _s->ho_start_end_dates[_f].second;}
        void setEndDateHO(double end_s){_s->ho_start_end_dates[_f].second=end_s;}
        double navTime(unsigned int i){checkI(i); return _s->nav_times[_f*2+i];}
        void setNavTime(unsigned int i, double time_s){checkI(i); _s->nav_times[_f*2+i] = time_s;}
        RobotState & initQ(unsigned int i){checkI(i); return _s->initial_confs[_f + i];}
        FrontierPtr frontier(){return _s->frontiers[_f];}
        MultiHandOverSolution* getSolution(void){return _s;}
        statePtr_t objectPos();
        statePtr_t agentConf(unsigned int i);
    };
    struct CostData{
        CostData():dist(-1),time(-1),use_cost(-1),handover_cost(-1),handovers(0),marker_is_opt(0),agent_names(0){}
        CostData(MultiHandOverSolution* sol);
        double dist;
        double time;
        double use_cost;
        double handover_cost;
        unsigned int handovers;
        unsigned int go_to_target_nb;
        bool marker_is_opt;
        std::vector<std::string> agent_names;
    };

    MultiHandOverSolution(void);
    MultiHandOverSolution *cloneAsSon(void);
    MultiHandOverSolution *clone(void);
    unsigned int getId(void){return _id;}
    bool addHandOverAction(FrontierPtr frontier);

    double getTotalDist();
    double getTotalUseCost();

    double cost;///< the cost (estimated) of the solution
    double time;///< the estimation of the execution time of the plan
    double cumul_time_nav;///< estimation of cumulated navigation of times of all robots
    double hri_cost;
    std::vector<RobotState> initial_confs,target_confs;
    //trajectories traj; ///< the complete solution as trajectories for each agents
    std::vector<MultiHandOverAgent*> agents; ///< list of agents used (in the order they have the object)
    std::vector<Action> actions;
    std::vector<std::shared_ptr<FrontierCells> > frontiers; ///< the places (frontiers) where the object is transferred

    std::vector<double> avail_delay;   ///< estimation of the time each agent have to start its plan
                                        ///< after the first agent starts (begining of the plan execution)
                                        ///
    /// estimations of the start date (seconds after the 1st nav begining)
    /// of each navigation step
    std::vector<double> nav_start_dates;
    std::vector<std::pair<double,double> > ho_start_end_dates;///< start and end dates of each hand over
    std::vector<double> handover_times; ///< duration of each handover
    std::vector<double> handover_hri_costs; ///< HRI related costs of each handover @todo define what is included
    std::vector<std::vector<MultiAgentCell*> > navigation_trajs;///< the navigation trajectories. @see nav_costs for the ordering
    std::vector<double> nav_dists;///< costs for each deplacement made (2 nav costs for each handover, the first is the one of the agent holding the object)
                                  ///< if the agents list is A B C D (3 HO), the nav costs will correspond to A B B C C D
    std::vector<double> nav_times;///< the duration of each navigation
    std::vector<std::vector<MultiAgentCell*> > navigation_trajs_to_target;///< the navigation trajectories to reach final target positions.
    std::vector<double> nav_dists_to_target;
    std::vector<std::pair<double,double> > nav_start_end_times_to_target;///< nav times of the path to reach final target as begin and end dates in seconds
    bool is_optimized;///a marker for debug
    friend std::ostream& operator<<(std::ostream& os, const MultiHandOverSolution& sol);
    friend bool operator<(const MultiHandOverSolution &left,const MultiHandOverSolution &right);
    friend bool operator>(const MultiHandOverSolution &left,const MultiHandOverSolution &right);
    static bool less(MultiHandOverSolution const *left,MultiHandOverSolution const *right);
    std::string print_long();

    //solution modification graph
    std::vector<MultiHandOverSolution*> sons;
    MultiHandOverSolution * parent;
    MultiHandOverSolution * oldest_parent;

    Robot* object;

    static void printFamiliesTrees(std::vector<MultiHandOverSolution*> &solutions, std::ofstream &output);
    static void resetID(void){MultiHandOverSolution::_current_id=0;}
private:
    static unsigned int _current_id;
    unsigned int _id;
};


class SubTask{
public:
    typedef enum {UNDEF=0, HO_GIVE, HO_GET, NAV_WITH_OBJ, NAV_TO_HO, NAV_TO_TARGET, NAV_SYNC, NAV_OTHER,IDLE} TaskType_t;
    typedef enum {SHO_NONE=0,     ///< not a HO task
                  SHO_G_GIVE,       ///< giver extends arm
                  SHO_R_PICK,       ///< receiver extends arm to reach object
                  SHO_G_RELEASE,    ///< giver open gripper
                  SHO_R_GRAB,       ///< receiver close gripper
                  SHO_G_DISENGAGE,  ///< giver disengage its arm
                  SHO_R_TAKE,       ///< receiver disengage arm with the object
                  SHO_ENUM_SIZE} HandoverStepType_t;
    typedef enum {IS_2D=0, HAS_FULL_TRAJ, COMPLETED, RUNNING, SIZE} MarkerList_t;
    SubTask();
    SubTask(MultiHandOverAgent* agent, TaskType_t type, double beginDate, double endDate, double cost=0, HandoverStepType_t hostepType = SHO_NONE);
    virtual ~SubTask();

    void resetMarker();
    bool getMarker(MarkerList_t m);
    void setMarker(MarkerList_t m, bool b);

    TaskType_t type() const ;
    HandoverStepType_t hoStepType() const;
    bool isNav()     const ;
    bool isManip()   const ;

    double beginDate();
    double endDate();
    double duration();
    void extendToEndDate(double new_end_date);
    void extendDuration(double new_duration);
    void replan(double new_begin);
    double cost();

    MultiHandOverAgent *agent();

    std::vector<statePtr_t> & waypoints();
    std::vector<Eigen::Vector2d> & waypoints2d();
    void addWaypoint(statePtr_t q);
    void addWaypoint(Eigen::Vector2d v, double date=-std::numeric_limits<double>::infinity());
    std::vector<double> & waypointDates();
    API::GeometricPath &traj();
    void traj(API::GeometricPath traj);
    void spreadIdlePositionToNext();

    void addPrevious(SubTask* prev);
    void addNext(SubTask* next);
    std::vector<SubTask*> & getPrev(){return this->prev;}
    std::vector<SubTask*> & getNext(){return this->next;}
    void substitutePrev(SubTask *prev,SubTask *new_prev);
    void substituteNext(SubTask *next,SubTask *new_next);

    unsigned int getId(){return this->_id;}
    static bool compareLessBeginDate(SubTask &t1,SubTask &t2){return t1.beginDate() < t2.beginDate();}
    static bool compareLessBeginDatePtr(SubTask *t1,SubTask *t2){return t1->beginDate() < t2->beginDate();}

    static std::string typeString(SubTask::TaskType_t t);

    //TODO for debug:
    std::string comment;
private:
    TaskType_t _type;
    HandoverStepType_t _handoverStepType;
    double _begin_date,_end_date;
    double _duration;
    double _cost;
    MultiHandOverAgent *_agent;
    std::vector<statePtr_t> _wp;
    std::vector<Eigen::Vector2d> _2d_wp;
    std::vector<double> _dates_wp;
    API::GeometricPath _traj;

    std::vector<SubTask*> prev;///< these tasks must end before this starts
    std::vector<SubTask*> next;///< these tasks can start when this ends

    std::vector<bool> _markers;

    unsigned int _id;
    static unsigned int _count_id;
};

class Task{
public:
    class state_iterator
    {
    public:
        state_iterator(Task* task);
        bool advance();
        std::map<MHO::MultiHandOverAgent*,Eigen::Vector2d> & state();
        SubTask* getSubTask(MultiHandOverAgent* agent);
        double date();
        bool isEnd();

    private:
        Task* _task;
        bool _is_end_iterator;
        HRICS::MultiAgentGrid* _grid;
        std::map<MHO::MultiHandOverAgent*,Eigen::Vector2d>  _state_current;
        double _time;
        std::map<MHO::SubTask*,std::pair<int, double> > _trak; ///< saves the current/reached waypoint index of each SubTask and the time associated
        std::map<MHO::SubTask*,std::pair<int, double> > _next; ///< saves the next waypoint index of each SubTask and the time associated
        std::map<MHO::MultiHandOverAgent*,MHO::SubTask*> _sub_task;
    };

    Task(MultiHandOverSolution* sol);
    ~Task();
    SubTask* getSubTask(int i);
    std::vector<HRICS::MHO::MultiHandOverAgent*> const & getAgents();
    std::vector<SubTask *> const & getSubTasks();
    void addSubTask(SubTask* st, bool no_sort=false);
    /// erase a sub task from the list
    /// also delete the existing references to it in the other sub tasks of this task
    void eraseSubTask(SubTask* st);
    /// use gnuplot to plot the gantt chart of the task (create new independent process/window)
    bool plotGanttChart();
    /// uses graphviz (dot) to display the (directed) graph representing the time constraints of the task
    /// nodes represent actions begining
    /// edges are labeled with the time between the two begining dates
    /// A -- [12] --> B means "B starts 12 secs after A start" which can be "A takes 12 secs to complete and B starts immediately after"
    bool displayGraph();
    void removeIdles();
    /// create new IDLE sub tasks when needed
    /// manages previous/next relations
    void fillWithIdle(std::map<MultiHandOverAgent *, MultiAgentCell *> &state_begin);
    state_iterator begin();

    void refineHandOvers();
    void refineHandOver(SubTask *st1,SubTask *st2);

    void setObject(Robot* obj){_object=obj;}
    Robot *getObject(){return _object;}

private:
    std::vector<SubTask*> _sub_tasks;
    std::vector<MultiHandOverAgent*>  _agents;
    Robot* _object;
};

/**
 * @brief The SubTaskMHP struct is to pass results to MHP as simplified SubTask
 */
struct SubTaskMHP{
    //SubTaskMHP(SubTask *st);
    std::string robotname;
    std::vector<int> nexts;
    int index;
    double begin,end;
    std::vector<statePtr_t> wp;
    SubTask::TaskType_t type;
};

/// \brief The MultiHandOverMhpHO struct defines data for a handover to be exported in MHP
struct MultiHandOverMhpHO
{
    MultiHandOverMhpHO(MultiHandOverSolution *sol,unsigned int step);
    std::string g_name; ///< giver agent name
    Eigen::Vector3d g_pose;///< giver 3d pose (x,y,theta)
    enum {ARM_LEFT,ARM_RIGHT,ARM_UNDEF} g_arm;///<giver arm
    std::vector<double> g_arm_conf;///< giver arm configuration
    double g_torso;///< giver torso joint

    std::string r_name; ///< receiver agent name
    Eigen::Vector3d r_pose;///< receiver 3d pose (x,y,theta)
    double r_torso;
    std::vector<double> r_arm_conf;

    Eigen::Vector3d obj_pose;///< object 3D position (x,y,z)

    double r_start_time;
    double g_start_time;

};

/**
 * @brief The MultiHandOverHO class
 * describes a hand over, and implements method to compute costs and improve a handover
 * The class should implement static methods to find good handover according to several situations parameters
 */
class MultiHandOverHO
{
public:
    MultiHandOverHO(MultiHandOverSolution::Action *action, MultiHandOver* mho);
    MultiHandOverHO(MultiHandOverAgent* a1,MultiHandOverAgent *a2,MultiAgentCell* c1,MultiAgentCell* c2, MultiHandOver* mho);
    MultiHandOverHO(MultiHandOverAgent *a1, MultiHandOverAgent *a2, double d, MultiHandOver* mho);
    /// @brief compute cost and time duration of the handover
    double computeCost();
    double humanRobotDistanceRelatedCost();

    //getters / setters
    double getCost(void) const;
    void setCost(double cost);
    double getTime(void) const;
    void setTime(double time_sec);

    Robot* robot(unsigned int i);
    MultiHandOverAgent* agent(unsigned int i);
    //FrontierPtr frontier(void);
    MultiAgentCell *cell(unsigned int i);

    //methods to find a handover
    static MultiHandOverHO handoverBetween2pos(MultiAgentCell* cellA,MultiAgentCell* cellB,unsigned int agentA, unsigned int agentB);
    static void getOptimalCost(MultiHandOverAgent *a1, MultiHandOverAgent *a2,double &cost,double &duration,MultiHandOver* mho);

private:
    double cost;
    double time;
    MultiHandOverAgent *_a1,*_a2;
    MultiAgentCell *_c1,*_c2;
    double _d;
    MultiHandOver* _mho;
    //MultiHandOverSolution::Action *_action;

};

class HandOverFinder
{
public:
    HandOverFinder(MultiHandOver* mho,MultiAgentGrid *grid,unsigned int agent1,unsigned int agent2,
                   MultiAgentCell* cell1_O,MultiAgentCell* cell2_O,MultiAgentCell* cell1_T,MultiAgentCell* cell2_T);
    FrontierPtr findFrom2Points();
    //void setCostFunction(double (*func)(double,unsigned int)){_cost_function=func;}

private:
    MultiAgentGrid *_grid;
    MultiHandOver *_mho;
    unsigned int _agent1,_agent2;///< the 2 agents
    MultiAgentCell *_cell1_ori,*_cell2_ori,*_cell1_dest,*_cell2_dest; ///< destinations and origins for each agent

    //double (*_cost_function)(double ,unsigned int);
};

/**
 * @brief The MultiHandOverHeuristic class provides an heuristic based on euclidian distance and
 * minimal cost by selecting agents who can make successive handovers.
 */
class MultiHandOverHeuristic{
public:
    MultiHandOverHeuristic(MultiHandOver *mho, MHO::AbstractSolutionTools *s_tools);
    virtual ~MultiHandOverHeuristic(){}
    /**
     * @brief compute
     * @param c1
     * @param a1
     * @param c2
     * @param a2
     * @return
     *
     * @todo the target of the heuristic is always the same, save Dijkstra result (reverse)
     */
    virtual double compute(MultiAgentCell* c1,unsigned int a1,MultiAgentCell* c2,unsigned int a2);
    std::vector<double> getTimes(){return _times;}
protected:
    void doTheGraph();
    mho::ReverseDisjkstra& getRevDijkstra(){return rev_dijk;}
    MultiHandOver* getMho(){return _mho;}
    MHO::AbstractSolutionTools* getSolutionTools(){return _tools;}
    mho::Node* getAgentNode(unsigned int i){return _agents_nodes[i];}
private:
    MultiHandOver *_mho;
    mho::SimpleGraph _graph;
    std::vector<mho::Node*> _agents_nodes;
    MHO::AbstractSolutionTools *_tools;
    //TODO remove related
    std::vector<double> _times;
    mho::ReverseDisjkstra rev_dijk;
};

class MultiHandOverHeuristicClosestFrontier: public MultiHandOverHeuristic{
public:
    MultiHandOverHeuristicClosestFrontier(MultiHandOver *mho,MHO::AbstractSolutionTools *s_tools);

    virtual ~MultiHandOverHeuristicClosestFrontier(){}
    virtual double compute(MultiAgentCell *c1, unsigned int a1, MultiAgentCell *c2, unsigned int a2);

protected:
    /**
     * @brief findMinimalFrontier searches for the frontier between a1 and a2 that minimizes the distance agents have to run.
     * @param c1i
     * @param c1g
     * @param a1
     * @param c2i
     * @param c2g
     * @param a2
     * @return
     *
     * It uses real distances, as got from MultiAgentCell::getDistanceAny())
     * It tries to minimize the sum of the folowing distances:
     *   * a1 from c1i to Frontier
     *   * a1 from frontier to c1g
     *   * a2 from c2i to frontier
     *   * a2 from frontier to c2g
     */
    virtual double findMinimalFrontier(MultiAgentCell* c1i,MultiAgentCell* c1g, unsigned int a1,
                                            MultiAgentCell *c2i,MultiAgentCell* c2g, unsigned int a2);
    Eigen::Vector2d findMinimalFrontier2pt(Eigen::Vector2d c1, double f1, Eigen::Vector2d c2, double f2);
    Eigen::Vector2d findMinimalFrontier3pt(Eigen::Vector2d ci,Eigen::Vector2d cg, double f1, Eigen::Vector2d c2, double f2);
    Eigen::Vector2d findMinimalFrontier4pt(Eigen::Vector2d c1i,Eigen::Vector2d c1g, double f1, Eigen::Vector2d c2i, Eigen::Vector2d c2g, double f2);

};

class LazyWeightedAstarNode : public mho::LWANodeBase{
    MOVE3D_STATIC_LOGGER;
public:
    LazyWeightedAstarNode(MultiAgentCell* cell, unsigned int agent,MultiHandOverHeuristic *heuristic, ToolSet *tool_set);
    LazyWeightedAstarNode(LazyWeightedAstarNode *other);
    virtual ~LazyWeightedAstarNode();
    virtual LWANodeInterface *clone();

    std::vector<LWANodeInterface*> sons();

    virtual void g(double g);
    virtual double computeEndDate();
    /// returns true if the agent is used by the ancestors of this
    virtual bool isAgentAlreadyUsed();
    virtual double edgeCostEstimationFrom(LWANodeInterface *parent);
    virtual double computeHeuristic(LWANodeInterface *goal);
    virtual double computeTrueCost(LWANodeInterface *parent);
    virtual bool isSameConf(LWANodeInterface *other);

    static LazyWeightedAstarNode *cast(LWANodeInterface* p){return dynamic_cast<LazyWeightedAstarNode*>(p);}

    MultiAgentCell *cell(){return _cell;}
    void cell(MultiAgentCell* c){_cell=c;}
    unsigned int agent(){return _agent;}
    void agent(unsigned int a){_agent=a;}
    void parent(LazyWeightedAstarNode* p);
    LWANodeInterface* parent(){return mho::LWANodeBase::parent();}

private:
    MultiAgentCell *_cell;
    unsigned int _agent;
    MultiHandOverHeuristic *_heuristic_class;
    MHO::ToolSet *_toolset;
    std::vector<bool> _agents_in_parents;///< v[i] is true when agent i is in a predecessor of this
    double _time,_last_computed_time,_last_computed_cost;


};

class DisplayLWAstarInGrid : public mho::DisplaySearchAstarInterface
{
public:
    DisplayLWAstarInGrid(MultiAgentGrid *grid, bool showtext, bool draw=true);
    virtual ~DisplayLWAstarInGrid(){}
    void onOpen(mho::LWANodeInterface* open);
    void onClose(mho::LWANodeInterface* closed);
    void onFound(mho::LWANodeInterface* goal);
    void onEnd(std::vector<std::pair<unsigned int,double> > times);
    void setDraw(bool b){_draw=b;}
    std::vector<std::pair<unsigned int,double> > getTimes();

private:
    MultiAgentGrid *_grid;
    bool _marker_is_first_call;
    bool _showText,_draw;
    std::vector<MultiAgentCell*> _cells;
    double _max_draw_f;
    double count_open,count_closed;
    std::vector<std::pair<unsigned int,double> > _times;
};

}
}
} //namespace move4d
#endif // MULTIHANDOVERUTILS_HPP
