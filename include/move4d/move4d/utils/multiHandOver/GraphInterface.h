#ifndef GRAPHINTERFACE_H
#define GRAPHINTERFACE_H

#include <utility>
#include <vector>
#include "move4d/utils/multiHandOver/NodeInterface.h"
#include "move4d/utils/multiHandOver/EdgeInterface.h"

namespace move4d
{
namespace mho{
/**
 * template interface for a graph
 */
class GraphInterface
{
public:

    /**
     * @brief isDirected
     * @return true if directed graph
     */
    virtual bool isDirected(void) =0;

    virtual NodeInterface * addNode(NodeInterface *n) =0;

    /**
     * @brief connectNodes connect 2 nodes.
     * @param n1 first node
     * @param n2 second node
     * @param c cost
     * if directed graph, makes an arrow n1 -> n2
     * otherwise n1 <-> n2
     */
    virtual void connectNodes(NodeInterface* n1, NodeInterface* n2, cost_t c) =0;

    /**
     * @brief at returns the node at given index
     * @param i index
     * @return reference to node at i
     */
    virtual NodeInterface* at(unsigned int i) =0;
    virtual std::vector<NodeInterface*> getNodes(void)=0;
    virtual std::vector<NodeInterface*> sonsNodes(NodeInterface * i) = 0;
    virtual std::vector<EdgeInterface*> sons(NodeInterface* i) =0;

    /**
     * @brief size the number of nodes
     * @return
     */
    virtual unsigned int size(void) =0;

    virtual unsigned int edgesNumber(void) =0;

    /**
     * @brief cost
     * @param n1
     * @param n2
     * @return the cost of edge from n1 to n2, if it exists, numeric_limit<cost_t>::max() if not
     */
    //virtual cost_t cost(unsigned int n1,unsigned int n2) =0;

};
}
} //namespace move4d
#endif // GRAPHINTERFACE_H
