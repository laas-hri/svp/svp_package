#ifndef MHO_EDGEINTERFACE_H
#define MHO_EDGEINTERFACE_H


#include <vector>
#include "move4d/utils/multiHandOver/GraphCommon.h"

namespace move4d
{
namespace mho {

class NodeInterface;

class EdgeInterface
{
public:

    //EdgeInterface();
    //EdgeInterface(NodeInterface *n1, NodeInterface *n2, cost_t c);

    virtual NodeInterface*  first(void) =0;
    virtual NodeInterface*  second(void) =0;
    virtual NodeInterface*  oppositeSideOf(NodeInterface* n) =0;
    virtual cost_t getCost(void) =0;
    virtual void setCost(cost_t c) =0 ;
    virtual void setFirst(NodeInterface* n) =0;
    virtual void setSecond(NodeInterface* n) =0;

};

} // namespace mho


} //namespace move4d
#endif // MHO_EDGEINTERFACE_H
