#ifndef MHO_NODEINTERFACE_H
#define MHO_NODEINTERFACE_H

#include <vector>
#include "move4d/utils/multiHandOver/GraphCommon.h"

namespace move4d
{
namespace mho {
class EdgeInterface;
class GraphInterface;
\
class NodeInterface
{
public:

    //NodeInterface();
    //virtual ~NodeInterface();

    virtual void connect(EdgeInterface *e) = 0;
    //virtual std::vector<EdgeInterface*> sons(void) = 0;
    virtual int getId(void) =0;

    virtual GraphInterface* getGraph(void)=0;
    virtual void setGraph(GraphInterface * g)=0;

    virtual NodeInterface* clone(void)=0;

};

} // namespace mho

} //namespace move4d
#endif // MHO_NODEINTERFACE_H
