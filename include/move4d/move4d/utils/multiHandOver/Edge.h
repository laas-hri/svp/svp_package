#ifndef MHO_EDGE_H
#define MHO_EDGE_H

#include <utility>
#include "move4d/utils/multiHandOver/EdgeInterface.h"

namespace move4d
{
namespace mho {

class Node;

class Edge : public EdgeInterface
{
public:
    Edge();
    Edge(NodeInterface* n1, NodeInterface*  n2, cost_t c);
    virtual ~Edge(){}

    NodeInterface* first(void);
    NodeInterface* second(void);

    NodeInterface* oppositeSideOf(NodeInterface* n);


    cost_t getCost(void);
    void setCost(cost_t c);
    void setFirst(NodeInterface* n);
    void setSecond(NodeInterface* n);

private:
    std::pair<NodeInterface*,NodeInterface*> _nodes;
    cost_t _cost;

};

} // namespace mho

} //namespace move4d
#endif // MHO_EDGE_H
