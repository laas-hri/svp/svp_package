#ifndef GRAPHALGORITHMS_H
#define GRAPHALGORITHMS_H

#include "move4d/utils/multiHandOver/GraphInterface.h"
#include <vector>
#include <deque>
#include <map>
#include <utility>


namespace move4d
{
namespace mho {

/**
 * @brief enumerate all paths starting from x
 * @param g the graph
 * @param x the starting node
 * @param destination the node to reach (if NULL, search all paths)
 * @param max_cost maximum cost of a path. If max_cost=NaN, not used
 * @return a list of paths represented as vectors of pointers to nodes
 *
 * if a destination parameter is given, a path will be added only when it is met, so that
 * all returned paths end with this node.
 * complexity = huge! (depends on graph connectivity)
 */
std::vector<std::pair<cost_t,std::vector<NodeInterface*> > > enumerate_paths(GraphInterface *g, NodeInterface *x, NodeInterface *destination, cost_t max_cost);
/**
 * @brief enumerate_paths calls enumerate_paths() with max_cost= NaN
 */
std::vector<std::pair<cost_t,std::vector<NodeInterface*> > > enumerate_paths(GraphInterface *g, NodeInterface *x, NodeInterface *destination=0);
std::pair<cost_t, std::vector<NodeInterface *> > random_path(GraphInterface *g, NodeInterface *x, NodeInterface *destination,cost_t max_cost);
/**
 * @brief random_path calls random_path() with max_cost= NaN
 */
std::pair<cost_t, std::vector<NodeInterface *> > random_path(GraphInterface *g, NodeInterface *x, NodeInterface *destination);

std::pair<cost_t,std::deque<NodeInterface*> > dijkstra(GraphInterface *g,NodeInterface* n1,NodeInterface* n2);
std::pair<cost_t,std::vector<NodeInterface*> > dijkstra(GraphInterface *g,NodeInterface* n1,NodeInterface* n2,bool vect);

class ReverseDisjkstra{
public:
    ReverseDisjkstra(GraphInterface *g=0);
    void setGraph(GraphInterface *g);
    void setTarget(NodeInterface *target);
    NodeInterface* getTarget(){return target;}
    void compute(NodeInterface *target);
    void compute();
    std::pair<cost_t,std::deque<NodeInterface*> > getPathDeque(NodeInterface *start);
    std::pair<cost_t,std::vector<NodeInterface*> > getPathVector(NodeInterface *start);

private:
    GraphInterface *g;
    NodeInterface *target;
    std::map<NodeInterface*,cost_t> dist;
    std::map<NodeInterface*,NodeInterface*> pred;
};


}
} //namespace move4d
#endif // GRAPHALGORITHMS_H
