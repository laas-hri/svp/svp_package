#ifndef SIMPLEGRAPH_H
#define SIMPLEGRAPH_H
#define _DEBUG_SIMPLEGRAPH_H
#include <map>
#include <vector>
#include <deque>

#ifdef _DEBUG_SIMPLEGRAPH_H
#include <iostream>
#endif

#include "move4d/utils/multiHandOver/GraphInterface.h"
#include "move4d/utils/multiHandOver/GraphCommon.h"
#include "move4d/utils/multiHandOver/Node.h"
#include "move4d/utils/multiHandOver/Edge.h"

namespace move4d
{
namespace mho {

class SimpleGraph : public GraphInterface
{
public:
    SimpleGraph(bool directed=false);
    virtual ~SimpleGraph();
    SimpleGraph *clone(void);

    bool isDirected();

    /**
     * @brief addNode
     * @param n
     * makes a local copy of the node
     */
    NodeInterface * addNode(NodeInterface *n);
    NodeInterface * addNode(Node *n);

    virtual void connectNodes(NodeInterface* n1, NodeInterface* n2, cost_t c);
    /**
     * @brief update the cost of an edge specified by its pre and post node, eventually its cost
     * @param n1 pre node
     * @param n2 post node (may be swapped in undirected graphs)
     * @param newcost cost to assign
     * @param oldcost current cost of the edge
     * @param onlySpecifiedCost ignore oldcost if false and change cost of all edges from n1 to n2
     */
    void updateCost(NodeInterface* n1,NodeInterface* n2,cost_t newcost,cost_t oldcost, bool onlySpecifiedCost=true);
    void updateCost(NodeInterface* n1,NodeInterface* n2,cost_t newcost);
    void updateCost(EdgeInterface *edge,cost_t cost);


    NodeInterface* at(unsigned int i);
    EdgeInterface* edgeAt(unsigned int i);
    std::vector<NodeInterface*> sonsNodes(NodeInterface *i);
    std::vector<EdgeInterface*> sons(NodeInterface* i);
    std::vector<NodeInterface*> getNodes(void);


    unsigned int size(void);
    unsigned int edgesNumber(void);

    void clear(void);

#ifdef _DEBUG_SIMPLEGRAPH_H
    friend std::ostream& operator<<(std::ostream& os, SimpleGraph& g);
#endif

protected:

    std::vector<Edge*> _edges;
    std::vector<Node*> _nodes;
    bool _is_directed;
    unsigned int _count;
};
}
}
#endif // SIMPLEGRAPH_H
