#ifndef NODE_H
#define NODE_H

#include <vector>
#include <utility>

#include "move4d/utils/multiHandOver/NodeInterface.h"
#include "move4d/utils/multiHandOver/GraphCommon.h"

namespace move4d
{
namespace mho {

class Edge;

class Node : public NodeInterface
{
public:
    Node();
    Node(int id);
//    Node(NodeInterface &ni);
    virtual ~Node();
    void connect(EdgeInterface *e);
    std::vector<EdgeInterface *> &sons(void);
    int getId(void){return _id;}

    GraphInterface * getGraph(){return this->_graph;}
    void setGraph(GraphInterface * g){this->_graph = g;}

    virtual NodeInterface* clone(void);


private:
    std::vector<EdgeInterface*> _edges;
    int _id;
    GraphInterface * _graph;

    static int __current_id;

};

}

} //namespace move4d
#endif // NODE_H
