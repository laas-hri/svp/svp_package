#ifndef TRIANGLEFORM_H
#define TRIANGLEFORM_H

#include "move4d/utils/geometricTools/geometricForm.hpp"


namespace move4d
{
class TriangleForm : public GeometricForm
{
    MOVE3D_STATIC_LOGGER;
public:
    TriangleForm(p3d_point a, p3d_point b, p3d_point c, p3d_vector3 norm);
    virtual ~TriangleForm(){}

    bool isPointInForm(p3d_point p);
    double distHToPoint(p3d_point p);

    double getDistanceFromPoint(p3d_point p);
    double getPlannarDistanceFromPoint(p3d_point p);
    p3d_point getCenter();
    double getHeight();

    p3d_point getRandomPointInForm();
    std::vector<p3d_point> getFormGrid(double sampleRate);
    GeometricForm* getTransformedForm(p3d_matrix4 M);

    void draw();

private:
    p3d_point _a;
    p3d_point _b;
    p3d_point _c;
};


} //namespace move4d
#endif // TRIANGLEFORM_H
