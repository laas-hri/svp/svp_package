#ifndef GEOMETRICFORMS_H
#define GEOMETRICFORMS_H

#include "move4d/utils/geometricTools/rectangleForm.hpp"
#include "move4d/utils/geometricTools/triangleForm.hpp"
#include "move4d/utils/geometricTools/circleForm.hpp"
#include "move4d/utils/geometricTools/parallelepipedForm.hpp"

#endif // GEOMETRICFORMS_H
