#ifndef CIRCLEFORM_H
#define CIRCLEFORM_H

#include "move4d/utils/geometricTools/geometricForm.hpp"

namespace move4d
{
class CircleForm : public GeometricForm
{
    MOVE3D_STATIC_LOGGER;
public:
    CircleForm(p3d_point c, double r, p3d_vector3 norm);
    virtual ~CircleForm(){}

    bool isPointInForm(p3d_point p);
    double distHToPoint(p3d_point p);


    double getDistanceFromPoint(p3d_point p);
    double getPlannarDistanceFromPoint(p3d_point p);

    p3d_point getRandomPointInForm();
    std::vector<p3d_point> getFormGrid(double sampleRate);
    GeometricForm* getTransformedForm(p3d_matrix4 M);
    p3d_point getCenter();
    double getHeight();

    void draw();

private:
    p3d_point _c;
    double _r;
};
} //namespace move4d
#endif // CIRCLEFORM_H
