#ifndef PARALLELEPIPEDFORM_H
#define PARALLELEPIPEDFORM_H

#include "move4d/utils/geometricTools/geometricForm.hpp"

namespace move4d
{

/**  c----d
  * /|   /|
  *a----b h
  *|/g  |/
  *e----f
  *b and c should be opposite
  */
class ParallelepipedForm : public GeometricForm
{
    MOVE3D_STATIC_LOGGER;
public:
    ParallelepipedForm(p3d_point a, p3d_point b, p3d_point c, p3d_point d,
                       p3d_point e, p3d_point f, p3d_point g, p3d_point h);
    virtual ~ParallelepipedForm(){}
    bool isPointInForm(p3d_point p);
    double distHToPoint(p3d_point p);

    double getDistanceFromPoint(p3d_point p);
    double getPlannarDistanceFromPoint(p3d_point p);

    p3d_point getRandomPointInForm();
    std::vector<p3d_point> getFormGrid(double sampleRate);
    p3d_point getCenter();
    double getHeight();

    GeometricForm* getTransformedForm(p3d_matrix4 M);

    void draw();

private:
    p3d_point _a;
    p3d_point _b;
    p3d_point _c;
    p3d_point _d;
    p3d_point _e;
    p3d_point _f;
    p3d_point _g;
    p3d_point _h;
};


} //namespace move4d
#endif // PARALLELEPIPEDFORM_H
