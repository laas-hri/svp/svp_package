#ifndef GEOMETRICTOOLS_HPP
#define GEOMETRICTOOLS_HPP

#include "P3d-pkg.h"
#include "move4d/API/Device/robot.hpp"
#include "move4d/Logging/Logger.h"


namespace move4d
{
class ParallelepipedForm;


class GeometricComputations
{
    MOVE3D_STATIC_LOGGER;
public:
    GeometricComputations();

    p3d_point getRandomPointInTriangle(p3d_point A, p3d_point B, p3d_point C);


    /**
      *a----b
      *|    |
      *c----d   .p
      *b and c should be opposite
      */
    p3d_point getRandomPointInRectangle(p3d_point A, p3d_point B, p3d_point C, p3d_point D);

    /**
      * get lower BB points of the object
      */
    std::vector<p3d_point> getLowerPointsInObject(Robot* obj, double steepness);

    /**
      * get higher BB points of the object
      */
    std::vector<p3d_point> getUpperPointsInObject(Robot* obj, double steepness);


    /**
      * @return: the center point of the BB
      */
    p3d_point getCenterOfObj(Robot* obje);

    /**
      * @return: the center point of the botom rectangle of the BB
      */
    p3d_point getBotomCenterOfObj(Robot* obje, double threshhold);

    double compute_triangle_area_x_y2(p3d_point a, p3d_point b, p3d_point c);

    /**
      *a----b
      *|    |
      *c----d   .x
      *b and c should be opposite
      */
    bool point_in_rect2(p3d_point a, p3d_point b, p3d_point c, p3d_point d, p3d_point x);

    /**
      * @return: true if x is between a and b, false otherwise.
      */
    bool point_in_segment2d(p3d_point a, p3d_point b, p3d_point x);

    /**
      * @return: distance between c and [a,b]
      */
    double DistanceFromSegment(double cx, double cy, double ax, double ay ,
                               double bx, double by);

    /**
      * @return: euclidien distance between a and b
      */
    double distBetweenPoints(p3d_point a, p3d_point b);

    /**
      * distance between the points in the p3d_matrix4
      */
    double distBetweenMatrix4(p3d_matrix4 a, p3d_matrix4 b);

    /**
      * @return: euclidien distance between a and b (in 2d, not accounting for z)
      */
    double distBetweenPoints2D(p3d_point a, p3d_point b);

    /**
      * test if four points of hexahedron y are in hexahedron x
      */
    bool hexahedron_in_hexahedron(p3d_point x1,p3d_point x2, p3d_point x3, p3d_point x4,
                                  p3d_point x5, p3d_point x6, p3d_point x7, p3d_point x8,
                                  p3d_point y1,p3d_point y2, p3d_point y3, p3d_point y4,
                                  p3d_point y5, p3d_point y6, p3d_point y7, p3d_point y8);

    /**  c----d
      * /|   /|
      *a----b h
      *|/g  |/
      *e----f   .x
      *b and c should be opposite
      */
    bool point_in_hexahedron(p3d_point a, p3d_point b, p3d_point c, p3d_point d,
                             p3d_point e, p3d_point f, p3d_point g, p3d_point h,
                             p3d_point x);

    /**
      *a----b
      *| .h |
      *c----d
      *b and c should be opposite
      */
    double compute_pyramid_area(p3d_point a, p3d_point b, p3d_point c, p3d_point d, p3d_point h);

    std::vector<p3d_point> getRandomPointsInHexahedron(double nbPoints, p3d_point a, p3d_point b, p3d_point c, p3d_point d, p3d_point e, p3d_point f, p3d_point g, p3d_point h);

    p3d_point getClosestBBPointFromPoint(Robot* e, p3d_point s);

    ParallelepipedForm getBoxFromObjects(std::vector<Robot*> vr, Robot* gripper);

    /**
      * The euclidian distance between 2 confs
      */
    double distConfs(statePtr_t q1, statePtr_t q2);

    /**
      * sample more finely a list of points
      */
    std::vector<p3d_point> getSampled(std::vector<p3d_point> vp, double sampleRate);

    /**
      * vHP is the list of human positions (z is the orientation) and vp is the list of points to asses (can be only one point)
      * @return: human aware cost for a set of robot positions.
      */
    double computeCost(std::vector<p3d_point> vHP, std::vector<p3d_point> vp, double angleWeight, double distHWeight, double distWeight);


    /**
      * vHP is the list of human positions (z is the orientation) and origTraj is the list of points to smooth
      * @return a smoothed traj, based on human aware costs.
      */
    std::vector<p3d_point> smoothTraj(std::vector<p3d_point> origTraj, std::vector<p3d_point> vHP, Robot *r, int nbIterMax, double gainLim, double trajDistWeight, double distToHWeight, double angleWeight);

};







} //namespace move4d
#endif // GEOMETRICTOOLS_HPP
