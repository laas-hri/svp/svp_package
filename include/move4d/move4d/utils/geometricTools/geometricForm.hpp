#ifndef GEOMETRICFORM_H
#define GEOMETRICFORM_H

#include "P3d-pkg.h"
//#include "move4d/API/Device/robot.hpp"
#include "Graphic-pkg.h"
#include "move4d/Logging/Logger.h"


namespace move4d
{

class GeometricForm
{
    MOVE3D_STATIC_LOGGER;
public:
    GeometricForm() {}
    virtual ~GeometricForm();

    /**
      * @return: true if p is in the form
      */
    virtual bool isPointInForm(p3d_point p) = 0;

    /**
      * @return: return the distance between the form and p, in the direction of the norm
      * TODO: for now it is just the z dist.
      */
    virtual double distHToPoint(p3d_point p) = 0;

    /**
      * @return: smallest dist between the form and p
      */
    virtual double getDistanceFromPoint(p3d_point p) = 0;

    /**
      * @return: smallest distance between the form and p in 2D
      */
    virtual double getPlannarDistanceFromPoint(p3d_point p) = 0;

    /**
      * @return: a point from the form
      */
    virtual p3d_point getRandomPointInForm() = 0;

    /**
      * @return: get the form center
      */
    virtual p3d_point getCenter() = 0;


    /**
      * @return: the average height of all points.
      */
    virtual double getHeight() = 0;


    /**
      * @return: create a grid inside the form and return its points
      */
    virtual std::vector<p3d_point> getFormGrid(double sampleRate) = 0;

    /**
      * @brief create a new form rotate by M compared to this one.
      */
    virtual GeometricForm* getTransformedForm(p3d_matrix4 M) = 0;

    /**
      * @brief draw the form
      */
    virtual void draw() = 0;

    /**
     * @brief transformPoint
     * @return p3d_point
     * Transform a point position based on the given mat4
     */
    p3d_point transformPoint(p3d_point a, p3d_matrix4 M);

    /**
     * @brief transformVector: applies the transformation to the vector
     * @param src[in]
     * @param M[in]
     * @param res[out]
     *
     * do not apply the translation
     */
    void transformVector(p3d_vector3 src, p3d_matrix4 M, p3d_vector3 res);

    /**
      * @return v: a copy of the form normal
      */
    void getNorm(p3d_vector3 v){p3d_vectCopy(_norm,v);}


    /**
      * the type of the form
      */
    std::string type;

    /**
      * is the form a support or not
      */
    bool isSupport;

    /**
      * a map of internal geometric form
      */
    std::map<std::string,GeometricForm*> supportAreas;

protected:

    /**
      * the form normal
      */
    p3d_vector3 _norm;

};

} //namespace move4d
#endif // GEOMETRICFORM_H
