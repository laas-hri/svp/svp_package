#ifndef RECTANGLEFORM_H
#define RECTANGLEFORM_H

#include "move4d/utils/geometricTools/geometricForm.hpp"

namespace move4d
{
/**
  * @brief a form of a rectangle, b and c should be diagonally oposite
  *a----b
  *|    |
  *c----d
  */
class RectangleForm : public GeometricForm
{
    MOVE3D_STATIC_LOGGER;
public:
    RectangleForm(p3d_point a, p3d_point b, p3d_point c, p3d_point d, p3d_vector3 norm);
    virtual ~RectangleForm(){}

    bool isPointInForm(p3d_point p);
    double distHToPoint(p3d_point p);

    double getDistanceFromPoint(p3d_point p);
    double getPlannarDistanceFromPoint(p3d_point p);
    p3d_point getRandomPointInForm();
    std::vector<p3d_point> getFormGrid(double sampleRate);
    p3d_point getCenter();
    double getHeight();


    GeometricForm* getTransformedForm(p3d_matrix4 M);

    void draw();

    p3d_point getA(){return _a;}
    p3d_point getB(){return _b;}
    p3d_point getC(){return _c;}
    p3d_point getD(){return _d;}

protected:
    p3d_point _a;
    p3d_point _b;
    p3d_point _c;
    p3d_point _d;


};

} //namespace move4d
#endif // RECTANGLEFORM_H
