#ifndef MHO_MULTIHANDOVERDISPLAY_H
#define MHO_MULTIHANDOVERDISPLAY_H

#include "move4d/utils/MultiHandOverUtils.hpp"

namespace move4d
{
namespace HRICS {
namespace MHO {

class MultiHandOverDisplay
{
public:
    MultiHandOverDisplay();
    ~MultiHandOverDisplay();
    void playSolutionTrajSyncRT(Task *task);

    ///return false when nothing to display (end)
    bool updateSolutionTrajSyncStep(Task* task, double time);

    ///return false when nothing to display (end)
    /// if a task is given, resets the display
    bool stepForwardSolutionTrajSync(Task* task=0);

    /// set the number of frame per second
    /// used by playSolutionTrajSyncRT
    void setFps(double fps){this->fps=fps;}

private:
    double fps;
    Task* _current_task;
    double _current_time;
};

} // namespace MHO
}

} //namespace move4d
#endif // MHO_MULTIHANDOVERDISPLAY_H
