#ifndef CHRONOMETER_HPP
#define CHRONOMETER_HPP

#include <time.h>
#include "time.h"
#include <exception>
#include <string>

namespace move4d
{
class Chronometer
{
public:
    class ChronometerException: std::exception{
    public:
        ChronometerException(){}
        const char * what(){return _what;}
    private:
        static char _what[50];
    };

    Chronometer();

    void reset(void);
    long int getNanoSec(void);
    double getMilliSec(void);
    long int getMicroSec(void);
    timespec getTimespec(void);

private:
    timespec _ts;
};

} //namespace move4d
#endif // CHRONOMETER_HPP
