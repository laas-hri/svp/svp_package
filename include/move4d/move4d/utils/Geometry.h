#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "libmove3d/P3d-pkg.h"
#include "move4d/API/Device/robot.hpp"
#include <Eigen/Eigen>

namespace move4d
{
/**
 * this namespace provides functions to switch between p3d and Eigen data,
 * and tools for geometric operations with RobotState too, like rotation, etc...
 */
namespace m3dGeometry
{

    /** @name EigenToP3d
     */
    ///@{

    p3d_point eigenToP3d(const Eigen::Vector3d & v);
    /// convert a position (like in Joint::getAbsPos())
    void eigenToP3d(const Eigen::Affine3d & f, p3d_matrix4 &out);
    void eigenToP3d(const Eigen::Matrix4d & f, p3d_matrix4 &out);
    ///@}


    /** @name p3dToEigen
     */
    ///@{

    Eigen::Vector3d p3dToEigen(const p3d_vector3 v);
    Eigen::Vector3d p3dToEigen(const p3d_point v);
    /// convert a position (like in Joint::getAbsPos())
    Eigen::Affine3d p3dToEigen(const p3d_matrix4 pos);

    ///@}

    /** @name stlToEigen
     */
    ///@{
    Eigen::VectorXd stlToEigen(const std::vector<double> &v);

    /// @name position
    ///@{

    /// returns the translation part of a transform represented as 4x4 matrix
    p3d_point getTranslation(const p3d_matrix4 t);

    /// returns the base 2d position (1st DOF)
    Eigen::Vector2d getConfBase2DPos(const RobotState &q);

    /// set only base rotation based on a rotation matrix
    /// (wrt Rx=Ry=Rz=0, NOT rotation from actual position)
    void setBaseOrientation(Robot* rob,const Eigen::Matrix3d & rot_mat);
    /// set from angles Rx Ry Rz given in a vector
    void setBaseOrientation(Robot *rob, const Eigen::Vector3d & angles);
    /// set only base position in 3D (X,Y,Z translations from origin)
    void setBasePosition(Robot *rob, const Eigen::Vector3d & pos);
    /// does not change z position
    void setBasePosition2D(Robot *rob, const Eigen::Vector2d & pos);
    /// as setBaseOrientation() and setBasePosition() with angles Rx,Ry,Rz specified
    /// in a vector
    void setBase6D(Robot *rob,const Eigen::Vector3d &pos, const Eigen::Vector3d &angles);
    /// as setBase6D() but set Rx and Ry to 0, Rz to theta
    void setBase4D(Robot *rob, const Eigen::Vector3d &pos, double theta);
    /// as setBase6D() but with a transform
    void setBaseTransform(Robot* rob, const Eigen::Affine3d & tf);

    /// change the current configuration of rob so that its base joint position
    /// matches the given conf one.
    void setBaseFromConf(Robot *rob, const RobotState & conf);
    ///@}

    /// @name transformations
    ///@{

    /// computes a postion in a new frame (posF2) from the postion in the original frames and the
    /// 2 frames positions
    /// @note it is the SAME position but in a different frame
    /// @note you can get absolute frame with m3dGeometry::absoluteFramePos()
    Eigen::Affine3d changeFrame(const Eigen::Affine3d &posF1,
                                const Eigen::Affine3d &F1,
                                const Eigen::Affine3d &F2);

    Eigen::Vector3d changeFrameVector(const Eigen::Vector3d &vec,
                                      const Eigen::Affine3d &F1,
                                      const Eigen::Affine3d &F2);
    /**
     * Same as changeFrame, but only with 3D coordinates
     */
    Eigen::Vector3d changeFramePoint(const Eigen::Vector3d p1,
                                     const Eigen::Affine3d &F1,
                                     const Eigen::Affine3d &F2);

    /**
     * Returns a null transformation (no rotation, no translation, no scalling),
     * i.e. an identity matrix
     */
    Eigen::Affine3d absoluteFramePos();

    double angle(const Eigen::Vector2d &v1, const Eigen::Vector2d &v2);
    /// equivalent to angle(v,UnitX())
    double angle(const Eigen::Vector2d &v);

    ///@}

    void test();

}

} //namespace move4d
#endif // GEOMETRY_H
