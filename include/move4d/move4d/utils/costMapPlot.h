#ifndef COSTMAPPLOT_H
#define COSTMAPPLOT_H

#include <set>
#include "move4d/API/Device/robot.hpp"

#include "move4d/Logging/Logger.h"
#include "move4d/Logging/socketplot.h"


namespace move4d
{
/**
 * @brief The CostMapPlot class samples the configuration space on a few DoF to plot a costmap
 */
class CostMapPlot
{
    MOVE3D_STATIC_LOGGER;
public:
    CostMapPlot(const std::string &name);
    ~CostMapPlot();

    void start();

    /**
     * @brief plot the cost of the trajcetory projected into the selected DoFs
     */
    void plotCurrentTraj();

    void addDof(Robot *r,Joint* j, int dof_index, double min=-std::numeric_limits<double>::infinity(), double max=std::numeric_limits<double>::infinity(), double step=0);

    /**
     * @brief firstSample move joints to the initial position for the sampling and compute cost
     * @param[out] dof_values the DoF values after the initialisation (position)
     * @return cost of the new sample
     */
    double firstSample(std::vector<double> &dof_values);
    /**
     * @brief nextSample move joints to next sample position
     * @param[out] dof_values the DoF values after the initialisation (position)
     * @return cost of the new sample
     */
    double nextSample(std::vector<double> &dof_values);
    /**
     * @brief moveJointNextSample
     * @param i index of the DoF
     * @param[out] reached_extreme true when this dof has reached its min/max value, and been reset to its max/min value, resp.
     * @return
     */
    double moveJointNextSample(unsigned int i,bool &reached_extreme);
    double getDofValue(unsigned int i);

    void updateRobots();
    void saveRobotConfs();
    void restoreRobotConfs();

    unsigned int defaultSampleNbPerJnt() const;
    void setDefaultSampleNbPerJnt(unsigned int defaultSampleNbPerJnt);

    void setAndUpdateJointDof(Joint* j,int dof,double val);

    static void test1();
    static void test2();

private:
    SocketPlot *_socketPlot;
    SocketPlot *_socketPlotTraj;
    std::vector<std::pair<Joint*,int> > _dofs;
    std::vector<double> _curValues;
    std::set<Robot*> _robots_to_update;
    bool _reached_end;
    unsigned int _defaultSampleNbPerJnt;
    std::vector<RobotState> _savedConf;

    std::vector<double> _steps,_min,_max;
    std::string _name,_name_traj;
};

} //namespace move4d
#endif // COSTMAPPLOT_H
