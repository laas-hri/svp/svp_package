#ifndef MATHFUNC_HPP
#define MATHFUNC_HPP

template<typename T>
T affine(T value, T inlow=T(0), T inhigh=T(1), T outmin=T(0), T outmax=T(1)){
    return (value-inlow) / (inhigh - inlow) * (outmax-outmin) + outmin;
}

template<typename T>
T sigmoid(T value, T inlow=T(-6), T inhigh=T(6), T outmin=T(0), T outmax=T(1)){
    return (outmax - outmin) * ( 1 / (1 + std::exp(-affine<T>(value,inlow,inhigh,T(-6),T(6))))) + outmin;
}

template<typename T>
T bounded_affine(T value, T inlow=T(0), T inhigh=T(1), T outmin=T(0), T outmax=T(1)){
    return std::min<T>(std::max<T>(affine(value,inlow,inhigh,outmin,outmax),outmin),outmax);
}

template<typename T>
T exp_inv(T value, T inlow=T(0), T inhigh=T(1), T outmin=T(0), T outmax=T(1)){
    T xnorm=(value-inlow)/(inhigh - inlow);
    T c=(outmax-outmin)* (1 - std::exp(-xnorm*5)) + outmin;
    return std::max<T>(c,outmin);
}

template<typename T>
T gauss_inv(T x, T inlow, T inhigh, T outmin=T(0), T outmax=T(1)){
    T u((inhigh+inlow)/2); //center
    T s((inhigh - u)/3); //steppiness
    T sq((x-u)/s);
    T c(1-std::exp(-0.5*sq*sq)); //value
    return (outmax-outmin)*c+outmin; //scale to output range
}

#endif // MATHFUNC_HPP
