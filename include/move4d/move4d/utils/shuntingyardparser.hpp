#ifndef SHUNTINGYARDPARSER_HPP
#define SHUNTINGYARDPARSER_HPP


#include "move4d/utils/shuntingyardparser.hpp"
#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <deque>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include <boost/foreach.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>

#include "move4d/planner/cost_space.hpp"
#include "move4d/HRI_costspace/HRICS_CostManager.h"

namespace move4d
{
class ShuntingYardToken {
public:
    enum Type {
        Unknown = 0,
        Number,
        Operator,
        LeftParen,
        RightParen,
        Function,
        CostFunction,
        Comma,
    };

    ShuntingYardToken(Type t, const std::string& s, int prec = -1, bool ra = false, int nbpar = 2)
        : type ( t ), str ( s ), precedence ( prec ), rightAssociative ( ra ), nparam ( nbpar )
    {}

    Type type ;
    std::string str ;
    int precedence ;
    bool rightAssociative ;
    int nparam ;
};

template<typename T,typename F=boost::function<double (T)>, typename FF=boost::function<double (std::string,T)> >
class ShuntingYardParser{
    MOVE3D_STATIC_LOGGER;
public:
    typedef T Operand_t;
    //typedef boost::function<double (Operand_t)> Function_t;
    typedef F Function_t;
    typedef FF MetaFunction_t;

    ShuntingYardParser(){}

protected:

    void debugReport(const ShuntingYardToken& token, const std::deque<ShuntingYardToken>& queue, const std::vector<ShuntingYardToken> &stack, const std::string& comment = "") {
        return;
        std::ostringstream ossQueue;
        BOOST_FOREACH(const ShuntingYardToken& t , queue) {
            ossQueue << " " << t.str;
        }

        std::ostringstream ossStack;
        BOOST_FOREACH(const ShuntingYardToken& t , stack) {
            ossStack << " " << t.str;
        }

        printf("|%-3s|%-32s|%10s| %s\n"
               , token.str.c_str()
               , ossQueue.str().c_str()
               , ossStack.str().c_str()
               , comment.c_str()
        );
    }

    bool my_isnumber(const char *p,const char *b,const std::deque<ShuntingYardToken> &prev_tokens){
        if(isdigit(*p) || *p=='.') return true;
        if(*p=='-' && p==b){ // - may be negative mark if at the begining of the string
            if(prev_tokens.size()==0) return true;
            if(prev_tokens.back().type == ShuntingYardToken::Number || prev_tokens.back().type == ShuntingYardToken::RightParen) return false; else return true;
        }
        return false;
    }

    std::deque<ShuntingYardToken> exprToTokens(const std::string& expr) {
        INIT_MOVE3D_LOG("move4d.utils.shuntingyard");
        std::deque<ShuntingYardToken> tokens;

        for(const char* p = expr.c_str(); *p; ++p) {
            if(my_isnumber(p,p,tokens)) {
                const char* b = p;
                for(; my_isnumber(p,b,tokens) ; ++p) {
                    ;
                }
                const std::string s = std::string(b, p);
                tokens.push_back(ShuntingYardToken ( ShuntingYardToken::Number, s ));
                --p;
            }else if(isalpha(*p)){
                const char* b = p;
                for(; *p!='(' ; ++p);
                const std::string s = std::string(b,p);
                tokens.push_back(ShuntingYardToken ( ShuntingYardToken::Function, s ));
            }else if(*p=='$'){
                ++p;
                const char *b=p;
                for(; isalpha(*p) || *p=='_' ; ++p);
                const std::string s = std::string(b,p);
                tokens.push_back(ShuntingYardToken ( ShuntingYardToken::CostFunction, s ));
                --p;

            }else if(isspace(*p)){
                ;
            } else {
                ShuntingYardToken::Type t = ShuntingYardToken::Unknown;
                int pr = -1;            // precedence
                bool ra = false;        // rightAssociative
                switch(*p) {
                default:                                    break;
                case '(':   t = ShuntingYardToken::LeftParen;     break;
                case ')':   t = ShuntingYardToken::RightParen;    break;
                case '^':   t = ShuntingYardToken::Operator;      pr = 4; ra = true;  break;
                case '*':   t = ShuntingYardToken::Operator;      pr = 3; break;
                case '/':   t = ShuntingYardToken::Operator;      pr = 3; break;
                case '+':   t = ShuntingYardToken::Operator;      pr = 2; break;
                case '-':   t = ShuntingYardToken::Operator;      pr = 2; break;
                case ',':   t = ShuntingYardToken::Comma;         pr=0; break;
                }
                if ( t==ShuntingYardToken::Unknown ){
                    M3D_ERROR("unknown token "<<*p<<"("<<p<<")");
                }
                tokens.push_back(ShuntingYardToken (
                                     t, std::string(1, *p), pr, ra
                                     ));
            }
        }
        return tokens;
    }


    std::deque<ShuntingYardToken> shuntingYard(const std::deque<ShuntingYardToken>& tokens) {
        INIT_MOVE3D_LOG("move4d.utils.shuntingyard");
        std::deque<ShuntingYardToken> queue;
        std::vector<ShuntingYardToken> stack;

        // While there are tokens to be read:
        BOOST_FOREACH(const ShuntingYardToken &token , tokens) {
            // Read a token
            switch(token.type) {
            case ShuntingYardToken::CostFunction:
            case ShuntingYardToken::Number:
                // If the token is a number, then add it to the output queue
                queue.push_back(token);
                break;

            case ShuntingYardToken::Function:
                stack.push_back(token);
                break;
            case ShuntingYardToken::Comma:
            case ShuntingYardToken::Operator:
            {
                // If the token is operator, o1, then:
                const ShuntingYardToken &o1 = token;

                // while there is an operator token,
                while(!stack.empty()) {
                    // o2, at the top of stack, and
                    const ShuntingYardToken &o2 = stack.back();

                    // either o1 is left-associative and its precedence is
                    // *less than or equal* to that of o2,
                    // or o1 if right associative, and has precedence
                    // *less than* that of o2,
                    if(
                            (! o1.rightAssociative && o1.precedence <= o2.precedence)
                            ||  (  o1.rightAssociative && o1.precedence <  o2.precedence)
                            ) {
                        // then pop o2 off the stack,
                        stack.pop_back();
                        // onto the output queue;
                        queue.push_back(o2);

                        continue;
                    }

                    // @@ otherwise, exit.
                    break;
                }

                // push o1 onto the stack.
                stack.push_back(o1);
            }
                break;

            case ShuntingYardToken::LeftParen:
                // If token is left parenthesis, then push it onto the stack
                stack.push_back(token);
                break;

            case ShuntingYardToken::RightParen:
                // If token is right parenthesis:
            {
                bool match = false;
                int commas(0);
                while(! stack.empty()) {
                    // Until the token at the top of the stack
                    // is a left parenthesis,
                    const ShuntingYardToken &tos = stack.back();
                    if(tos.type != ShuntingYardToken::LeftParen && tos.type != ShuntingYardToken::Function) {
                        //std::cout<<tos.str<<" ";
                        // pop operators off the stack
                        stack.pop_back();
                        if(tos.type == ShuntingYardToken::Comma)
                            ++commas;
                        else
                            // onto the output queue.
                            queue.push_back(tos);
                    }else if(stack.size()){
                        //std::cout<<"__"<<stack.back().str<<std::endl;
                        // Pop the left parenthesis from the stack,
                        // but not onto the output queue.
                        if(stack.back().type == ShuntingYardToken::Function){
                            stack.back().nparam=commas+1;
                            queue.push_back(stack.back());
                        }
                        stack.pop_back();
                        match = true;
                        break;
                    }
                }

                if(!match && stack.empty()) {
                    // If the stack runs out without finding a left parenthesis,
                    // then there are mismatched parentheses.
                    M3D_ERROR("RightParen error ("<< token.str.c_str()<<")");
                    return std::deque<ShuntingYardToken>(0,ShuntingYardToken(ShuntingYardToken::Unknown,"error"));
                }
            }
                break;

            default:
                M3D_ERROR("unknown token: "<<token.str.c_str());
                return std::deque<ShuntingYardToken>(0,ShuntingYardToken(ShuntingYardToken::Unknown,"error"));
                break;
            }

            debugReport(token, queue, stack);
        }

        // When there are no more tokens to read:
        //   While there are still operator tokens in the stack:
        while(! stack.empty()) {
            // If the operator token on the top of the stack is a parenthesis,
            // then there are mismatched parentheses.
            if(stack.back().type == ShuntingYardToken::LeftParen) {
                M3D_ERROR("Mismatched parentheses error");
                return std::deque<ShuntingYardToken>(0,ShuntingYardToken(ShuntingYardToken::Unknown,"error"));
            }

            // Pop the operator onto the output queue.
            queue.push_back(stack.back());
            stack.pop_back();
        }

        debugReport(ShuntingYardToken ( ShuntingYardToken::Unknown, "End" ), queue, stack);

        //Exit.
        return queue;
    }


public:
    Function_t parse (const std::string &expr,MetaFunction_t cost_functions) {
        INIT_MOVE3D_LOG("move4d.utils.shuntingyard");
    //boost::function<double (Operand_t&)> parse (const std::string &expr) {
        //const std::string expr = "3+4*2/(1-5)^2^3"; // Wikipedia's example
        //const std::string expr = "20-30/3+4*2^3";
        //const std::string default_expr = "sin(3.14*2.5+5^(2-1))";
        //std::string expr;

        M3D_DEBUG("Shunting-yard:"<<expr);
        //printf("|%-3s|%-32s|%-10s|\n", "Tkn", "Queue", "Stack");

        const std::deque<ShuntingYardToken> tokens = exprToTokens(expr);
        std::deque<ShuntingYardToken> queue = shuntingYard(tokens);
        std::vector<Function_t > stack;

        //printf("\nCalculation\n");
        //printf("|%-3s|%-32s|%-10s|\n", "Tkn", "Queue", "Stack");

        //boost::function<double (Operand_t &)> function;

        while(! queue.empty()) {
            std::string op;

            const ShuntingYardToken & token = queue.front();
            queue.pop_front();
            switch(token.type) {
            case ShuntingYardToken::Number:
                //stack.push_back(std::stod(token.str));
                stack.push_back(boost::bind (std::plus<double>(), 0,std::atof(token.str.c_str()) ));
                op = "Push " + token.str;
                break;
            case ShuntingYardToken::CostFunction:
                stack.push_back(boost::bind(cost_functions,token.str,_1));
                break;
            case ShuntingYardToken::Function:
            {
                if(token.str == "sin" && token.nparam == 1 ){
                    Function_t arg = stack.back();
                    stack.pop_back();
                    stack.push_back(boost::bind(sin,boost::bind(arg,_1)));
                    //op = "Push "+token.str+"("+std::to_string(arg)+")";
                }else if (token.str=="max" && token.nparam ==2){
                    Function_t arg1 = stack.back();
                    stack.pop_back();
                    Function_t arg2 = stack.back();
                    stack.pop_back();
                    stack.push_back(boost::bind(std::max<double>,boost::bind(arg1,_1),boost::bind(arg2,_1)));
                    //op = "Push "+token.str+"("+std::to_string(arg1)+","+std::to_string(arg2)+")";
                }
                break;
            }

            case ShuntingYardToken::Operator:
            {
                Function_t rhs = stack.back();
                stack.pop_back();
                Function_t lhs = stack.back();
                stack.pop_back();

                stack.push_back(boost::bind( HRICS::CostManager::elementaryOperation(token.str), boost::bind(lhs,_1), boost::bind(rhs,_1)));
                // switch(token.str[0]) {
                // default:
                //     printf("Operator error [%s]\n", token.str.c_str());
                //     exit(0);
                //     break;
                // case '^':
                //     stack.push_back(static_cast<int>(pow(lhs, rhs)));
                //     break;
                // case '*':
                //     stack.push_back(lhs * rhs);
                //     break;
                // case '/':
                //     stack.push_back(lhs / rhs);
                //     break;
                // case '+':
                //     stack.push_back(lhs + rhs);
                //     break;
                // case '-':
                //     stack.push_back(lhs - rhs);
                //     break;
                // }
                // op = "Push " + std::to_string(lhs) + " " + token.str + " " + std::to_string(rhs);
            }
                break;

            default:
                M3D_ERROR("Token error ("<<token.str<<")");

                return Function_t();
            }

            //debugReport(token, queue, stack, op);
        }
        assert(stack.size()==1);
        return stack.back();

        //printf("\n  result = %f\n", stack.back());
    }
};

//template<typename T>
//INIT_MOVE3D_STATIC_LOGGER(ShuntingYardParser<T>,"move4d.utils.shuntingyard");

} //namespace move4d
#endif // SHUNTINGYARDPARSER_HPP
