#ifndef HRICSDATA_COSTMANAGER_H
#define HRICSDATA_COSTMANAGER_H

#include "move4d/Logging/Logger.h"
#include "move4d/database/DatabaseInterface.hpp"

namespace move4d
{
namespace HRICS{
class CostManager;
}
namespace hricsdata {

class CostManagerData : public DatabaseInterface
{
    MOVE3D_STATIC_LOGGER;
public:
    CostManagerData(HRICS::CostManager *costManager, DatabaseReader *dbReader=NULL, bool proactive=false);
    ~CostManagerData();

    bool fetchData();
    using DatabaseInterface::fetchData;
    bool parseJson(const Json::Value &root, const std::string &name);

    HRICS::CostManager *costManager() const;

private:
    HRICS::CostManager *_costManager;
};
} // namespace hricsdata

} //namespace move4d
#endif // HRICSDATA_COSTMANAGER_H
