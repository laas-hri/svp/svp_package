#ifndef HRICSDATA_BODYWEIGHTS_H
#define HRICSDATA_BODYWEIGHTS_H

#include "move4d/Logging/Logger.h"
#include "move4d/database/DatabaseInterface.hpp"

namespace move4d
{
namespace HRICS{
namespace CF {
class CostFunctionCommon;
}
}
namespace hricsdata {

class BodyWeights : public DatabaseInterface
{
    MOVE3D_STATIC_LOGGER;
public:
    BodyWeights(HRICS::CF::CostFunctionCommon *common, DatabaseReader *dbReader=NULL, bool proactive=false);
    ~BodyWeights();

    bool fetchData();
    using DatabaseInterface::fetchData;
    bool parseJson(const Json::Value &root, const std::string &name);

private:
    HRICS::CF::CostFunctionCommon *_common;

};

} // namespace hricsdata

} //namespace move4d
#endif // HRICSDATA_BODYWEIGHTS_H
