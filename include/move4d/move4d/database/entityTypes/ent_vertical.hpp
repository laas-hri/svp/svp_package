#ifndef ENT_VERTICAL_H
#define ENT_VERTICAL_H

#include "move4d/API/Device/robot.hpp"
#include <jsoncpp/json/json.h>

#include "move4d/Logging/Logger.h"


namespace move4d
{
namespace Ent {


/**
  * @brief Vertical lists the objects that should stay in a certain orientation (e.g. filled glass, bottle,...)
  *
  * usefile: EntityTypes/Vertical.json
  */
class Vertical
{
    MOVE3D_STATIC_LOGGER;
public:
    Vertical( std::string json_path);
    ~Vertical();


    //json import/export
    //.global
    std::string getPath();

    //.import
    bool fetchData();
    bool importJsonFile(const std::string &path);
    bool createVerticalConst(Json::Value const & value);

    //.json conversion
    bool setToObjRob(Robot *rob, const Json::Value &value);


private:
    std::string _json_path;
};

} // namespace Ent

} //namespace move4d
#endif // ENT_VERTICAL_H
