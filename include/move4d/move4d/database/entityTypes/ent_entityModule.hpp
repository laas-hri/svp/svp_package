#ifndef ENT_ENTITYMODULE_H
#define ENT_ENTITYMODULE_H

#include "move4d/Logging/Logger.h"
#include "move4d/API/moduleBase.hpp"

namespace move4d
{
namespace Ent {

class EntitiesModule : public ModuleBase
{
    MOVE3D_STATIC_LOGGER;
protected:
    EntitiesModule();
public:
    static EntitiesModule *getInstance();
    ~EntitiesModule();
    void initialize();
    std::string getName();
    static std::string name();

private:
    static EntitiesModule *_instance;
};

} // namespace Ent

} //namespace move4d
#endif // ENT_ENTITYMODULE_H
