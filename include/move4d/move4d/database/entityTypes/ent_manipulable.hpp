#ifndef SEM_MANIPULABLE_H
#define SEM_MANIPULABLE_H

#include "move4d/API/Device/robot.hpp"
#include <jsoncpp/json/json.h>

//#include <string>
//#include <vector>
//#include <map>

#include "move4d/Logging/Logger.h"




namespace move4d
{
namespace Ent {

/**
  * @brief Manipulable find the list of manipulable objects and their stable rotations
  *
  * usefile: EntityTypes/Manipulable.json
  */
class Manipulable
{
    MOVE3D_STATIC_LOGGER;
public:
    Manipulable( std::string json_path);
    ~Manipulable();


    //json import/export
    //.global
    std::string getPath();

    //.import
    bool fetchData();
    bool importJsonFile(const std::string &path);
    bool createManipulableObjects(Json::Value const & value);

    //.json conversion
    bool setToObjRob(Robot *rob, const Json::Value &value);


private:
    std::string _json_path;
};

} // namespace Ent


} //namespace move4d
#endif // SEM_MANIPULABLE_H
