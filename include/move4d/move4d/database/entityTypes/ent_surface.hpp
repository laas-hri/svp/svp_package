#ifndef ENT_SURFACE_H
#define ENT_SURFACE_H

#include "move4d/API/Device/robot.hpp"
#include <jsoncpp/json/json.h>

//#include <string>
//#include <vector>
//#include <map>

#include "move4d/Logging/Logger.h"




namespace move4d
{
namespace Ent {


/**
  * @brief Surface Find the list of surfaces per object
  *
  * usefile: EntityTypes/Surface.json
  */
class Surface
{
    MOVE3D_STATIC_LOGGER;
public:
    Surface( std::string json_path);
    ~Surface();


    //json import/export
    //.global
    std::string getPath();

    //.import
    bool fetchData();
    bool importJsonFile(const std::string &path);
    bool createSurfaceInObjects(Json::Value const & value);

    //.json conversion
    bool setToObjRob(Robot *rob, const Json::Value &value);


private:
    std::string _json_path;
};

} // namespace Ent

} //namespace move4d
#endif // ENT_SURFACE_H
