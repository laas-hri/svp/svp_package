#ifndef ENT_ENTITYMANAGER_H
#define ENT_ENTITYMANAGER_H

#include "move4d/Logging/Logger.h"
#include "move4d/database/entityTypes/ent_manipulable.hpp"
#include "move4d/database/entityTypes/ent_surface.hpp"
#include "move4d/database/entityTypes/ent_agent.hpp"
#include "move4d/database/entityTypes/ent_vertical.hpp"
#include "move4d/database/entityTypes/ent_virtual.hpp"

namespace move4d
{
namespace Ent {


/**
  * @brief EntityManager: Manage all the entity type reader classes
  */
class EntityManager
{
    MOVE3D_STATIC_LOGGER;
public:
    EntityManager();
    ~EntityManager();

    static EntityManager* mainEntityManager();

    Manipulable *manipulables() const;
    void setManipulables(Manipulable *manip);

    Surface *surfaces() const;
    void setSurfaces(Surface *surface);

    Agent *agents() const;
    void setAgents(Agent *agent);

    Vertical *verticals() const;
    void setVerticals(Vertical *vertical);

    Virtual *virtuals() const;
    void setVirtuals(Virtual *virtua);


    std::string globalJsonPath() const;
    void setGlobalJsonPath(const std::string &globalJsonPath);

    bool updateEntities();
    bool fetchData();

protected:
    void createModules();


private:
    static EntityManager *__mainEntityManager;

    std::string _globalJsonPath;

    Manipulable* _manipulable;
    Surface* _surface;
    Agent* _agent;
    Vertical* _vertical;
    Virtual* _virtual;

};



} //namespace Ent

} //namespace move4d
#endif // ENT_ENTITYMANAGER_H
