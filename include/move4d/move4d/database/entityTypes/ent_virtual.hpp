#ifndef ENT_VIRTUAL_H
#define ENT_VIRTUAL_H

#include "move4d/API/Device/robot.hpp"
#include <jsoncpp/json/json.h>

#include "move4d/Logging/Logger.h"


namespace move4d
{
namespace Ent {


/**
  * @brief Virtual distinguish between virtual and normal objects
  *
  * usefile: EntityTypes/Virtual.json
  */
class Virtual
{
    MOVE3D_STATIC_LOGGER;
public:
    Virtual( std::string json_path);
    ~Virtual();


    //json import/export
    //.global
    std::string getPath();

    //.import
    bool fetchData();
    bool importJsonFile(const std::string &path);
    bool createVirtualObjects(Json::Value const & value);

    //.json conversion
    bool setToObjRob(Robot *rob);


private:
    std::string _json_path;
};

} // namespace Ent

} //namespace move4d
#endif // ENT_VIRTUAL_H
