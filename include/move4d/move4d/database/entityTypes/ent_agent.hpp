#ifndef ENT_AGENT_H
#define ENT_AGENT_H
#include "move4d/database/DatabaseInterface.hpp"
#include <jsoncpp/json/json.h>

#include "move4d/Logging/Logger.h"


namespace move4d
{
class Robot;

namespace Ent {

/**
  * @brief Agent distinguish between agents and objects
  *
  * usefile: EntityTypes/Agent.json
  */
class Agent : public DatabaseInterface
{
    MOVE3D_STATIC_LOGGER;
public:
    Agent(DatabaseReader *dbReader=NULL, bool proactive=false);
    ~Agent();


    //json import/export
    //.global
    std::string getPath();

    //.import
    using DatabaseInterface::fetchData;
    bool fetchData();
    bool createAgentInRob(Json::Value const & value);

    bool parseJson(const Json::Value &root, const std::string &name);

    //.json conversion
    bool setToObjRob(Robot *rob, const Json::Value &value);
};

} // namespace Ent

} //namespace move4d
#endif // ENT_AGENT_H
