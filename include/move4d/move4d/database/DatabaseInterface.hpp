#ifndef DATABASEINTERFACE_H
#define DATABASEINTERFACE_H

#include <string>
#include <jsoncpp/json/json.h>

namespace move4d
{
class DatabaseReader;

/**
 * @brief The DatabaseInterface class provides interface and generic methods for database classes.
 *
 * You will need to reimplement fetchData() and parseJson(Json::Value const &,std::string const &).
 *
 * And of course add your own data as class attributes, accessors,...
 *
 * @ingroup database
 */
class DatabaseInterface
{
public:
    DatabaseInterface(DatabaseReader *dbReader=NULL);
    virtual ~DatabaseInterface();
    /** @brief import a specific file given by path (by the user)
     */
    virtual bool readFile(const std::string &path);
    /** @brief alias for readFile
     */
    virtual bool importJsonFile(const std::string &path);
    /**
     * @brief fetch all required data automatically, looking at several places (using DatabaseParser).
     * @return true if data is found and correct.
     * this method needs to be reimplemented for each class (need to know which data to fetch)
     * it can make use of \ref fetchData(const std::string&)
     * @see DatabaseParser
     */
    virtual bool fetchData() = 0;
    /**
     * @brief as fetchData() but specify a file name / partial path
     * @param[in] name name or final part of the file path. The ".json" extension may be omitted.
     * @return true if data is found and correct.
     * e.g. fetchData("action/pick") will search for all available files wich path ends by "action/pick.json"
     */
    virtual bool fetchData(const std::string &name);
    /**
     * @brief parse a Json::Value object. It's the core function of any database class.
     * Needs to be reimplemented.
     */
    virtual bool parseJson(const Json::Value &root, const std::string &name) =0;

    /**
     * @brief getter for the proactive option.
     * when true, fetch all possibly needed data immediately (or on creation)
     * when false, fetch data when needed
     */
    bool proactive() const;
    /** @brief setter of the proactive option.
     * @see proactive()
     */
    void setProactive(bool proactive);

    DatabaseReader *dbReader() const;
    void setDbReader(DatabaseReader *dbReader);

private:
    bool _proactive;

    DatabaseReader *_dbReader;
};

} //namespace move4d
#endif // DATABASEINTERFACE_H
