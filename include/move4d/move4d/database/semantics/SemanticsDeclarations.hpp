/**
 * @file SemanticsDeclarations.hpp
 * @brief declares classes of Semantics module to avoid including other headers.
**/

#ifndef SEMANTICSDECLARATIONS_HPP
#define SEMANTICSDECLARATIONS_HPP

namespace move4d
{
namespace Sem{
class SemanticsManager;
class PlanParts;
class PlanningPart;
class PlanningType;
class Actions;
class ActionsManager;
class ActionDefinition;
class ActionType;
}

} //namespace move4d
#endif // SEMANTICSDECLARATIONS_HPP

