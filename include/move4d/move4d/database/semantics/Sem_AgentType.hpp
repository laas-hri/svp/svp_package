#ifndef SEM_AGENTTYPE_H
#define SEM_AGENTTYPE_H

#include <string>
#include <vector>
#include <map>

namespace move4d
{
class Robot;

namespace Sem {

class AgentType
{
public:
    AgentType();
    AgentType(const std::string &s);
    AgentType(Robot *rob);
    virtual ~AgentType();

    static AgentType fromRobot(Robot *rob);

    bool isDefined() const;

    friend bool operator==(const AgentType &t1, const AgentType &t2);
    friend bool operator==(const AgentType &t1, const std::string &s2);
    friend bool operator!=(const AgentType &t1, const AgentType &t2);
    friend bool operator!=(const AgentType &t1, const std::string &s2);
    friend bool operator<(const AgentType &t1, const AgentType &t2);
    friend bool operator>(const AgentType &t1, const AgentType &t2);

    operator bool() const;
    operator std::string() const;

    friend std::ostream &operator<<(std::ostream&,const AgentType &t);

    std::string toString() const ;

private:
    static std::vector<std::string> _agent_types_str;

    int _val;
};

} // namespace Sem

} //namespace move4d
#endif // SEM_AGENTTYPE_H
