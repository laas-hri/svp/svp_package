#ifndef SEM_SEMANTICSMANAGER_H
#define SEM_SEMANTICSMANAGER_H

#include "move4d/Logging/Logger.h"

#include "move4d/database/semantics/SemanticsDeclarations.hpp"

namespace move4d
{
class DatabaseReader;

/**
 * @brief namespace for classes managing information about semantics.
 *
 * Semantics module is mainly about giving names to geometric objects or groups of geometric objects, as well as some additional data.
 *
 * For instance, Sem::PlanParts links a group of joints to a named planning part (Sem::PlanningPart).
 * Sem::Actions links an action name to the necessary and optional PlanningPart
 *
 *
 * @ingroup database
 */
namespace Sem {

class SemanticsManager
{
    MOVE3D_STATIC_LOGGER;
public:
    SemanticsManager();
    ~SemanticsManager();

    static SemanticsManager* mainSemanticsManager();

    PlanParts *planParts() const;
    void setPlanParts(PlanParts *planParts);

    Actions *actions() const;
    void setActions(Actions *actions);

    bool proactive() const;
    void setProactive(bool proactive);

    ActionsManager *actionsManager() const;
    void setActionsManager(ActionsManager *actionsManager);

    DatabaseReader *dbReader() const;
    void setDbReader(DatabaseReader *dbReader);

protected:
    void createModules();

private:
    static SemanticsManager *__mainSemanticsManager;

    DatabaseReader *_dbParser;
    bool _proactive;

    PlanParts *_planParts;
    Actions* _actions;
    ActionsManager *_actionsManager;
};

} // namespace Sem

} //namespace move4d
#endif // SEM_SEMANTICSMANAGER_H
