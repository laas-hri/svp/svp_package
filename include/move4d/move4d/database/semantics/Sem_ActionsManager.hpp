#ifndef SEM_ACTIONSMANAGER_H
#define SEM_ACTIONSMANAGER_H

#include <map>
#include <vector>
#include "move4d/Logging/Logger.h"


namespace move4d
{
namespace Sem {

class Actions;
class ActionType;
class PlanningPart;
class PlanParts;
class AgentType;

/**
 * @brief The ActionsManager class.
 *
 * Example use:
 * ~~~~~~~~{.cpp}
 *     std::string sem_repport;
 *     Sem::ActionType act_t("pick")
 *     if(!Sem::SemanticsManager::mainSemanticsManager()->actionsManager()->isActionPossible(act_t,mainAgent->getHriAgent()->type,sem_repport)){
 *         cout << sem_repport<<endl;
 *         return false;
 *     }
 *
 *     Json::Value json;
 *     std::vector<Sem::PlanningPart> planParts =
 *             Sem::SemanticsManager::mainSemanticsManager()->actionsManager()->getJointsForAction(Sem::ActionType("pick"),
 *                                                                                                 mainAgent->getHriAgent()->type,
 *                                                                                                 false);
 *     for (std::vector<Sem::PlanningPart>::const_iterator it = planParts.begin(); it!= planParts.end();it++){
 *         //...
 *     }
 * ~~~~~~~~
 */
class ActionsManager
{

    MOVE3D_STATIC_LOGGER;
public:
    ActionsManager(Actions *actions, PlanParts *planparts);
    ~ActionsManager();

    /**
     * @brief ActionsManager::isActionPossible
     * @param act_type[in]
     * @param agent_type[in]
     * @param report[out]
     * @return
     *
     * possible reports (and reasons why it returns false):
     * - "action <action type> not defined" : no definition found for this action (in json files)
     * - "action <action type> is marked as not feasible by agents of type <agent type>" : the agent type is in the exlcude list,
     *   or not in the include list, and one of them exists;
     * - "agent type <agent type> has no plan part for planning type <planning type> required for the action <action type>" : there is a planning part required by
     *   the action that is not defined for the agent type.
     * - "ok" : if and only if return true
     */
    bool isActionPossible(ActionType &act_type, Sem::AgentType agent_type, std::string &report);

    /**
     * @brief getJointsForAction uses Actions and PlanParts to retrieve set of joints to use.
     * @param act_type
     * @param agent_type
     * @param is_hri
     * @return a vector of PlanningPart, each of them is an alternative (e.g. left arm/right arm).
     */
    std::vector<PlanningPart> getJointsForAction(ActionType &act_type, Sem::AgentType agent_type, bool is_hri);

    /**
      * @brief read from files various variables needed by the action
      * @param act_type the action type
      * @return a map of string,double, where the strings identify the variable and the double its value
      */
    std::map<std::string,double> getActionValues(ActionType &act_type);

    Actions *actions() const;
    void setActions(Actions *actions);

    PlanParts *planParts() const;
    void setPlanParts(PlanParts *planParts);

private:
    Actions *_actions;
    PlanParts *_planParts;
};

} // namespace Sem

} //namespace move4d
#endif // SEM_ACTIONSMANAGER_H
