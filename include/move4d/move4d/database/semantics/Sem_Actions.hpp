#ifndef SEM_ACTIONS_H
#define SEM_ACTIONS_H

#include <string>
#include <vector>
#include <map>
#include <set>

#include "move4d/Logging/Logger.h"
#include "move4d/database/semantics/Sem_PlanParts.hpp" //PlanningType
#include "move4d/database/DatabaseInterface.hpp"

namespace move4d
{
class DatabaseReader;

namespace Sem {

class ActionType
{
    MOVE3D_STATIC_LOGGER;
public:
    ActionType();
    ActionType(const std::string &s);
    virtual ~ActionType();
    bool isDefined() const;

    std::string toString() const;

    friend bool operator==(ActionType const &v1 , ActionType const &v2) {return v1._value==v2._value;}
    friend bool operator!=(ActionType const &v1 , ActionType const &v2) {return v1._value!=v2._value;}
    friend bool operator<(ActionType const &v1 , ActionType const &v2) {return v1._value<v2._value;}
    friend bool operator>(ActionType const &v1 , ActionType const &v2) {return v1._value>v2._value;}

    static std::vector<ActionType> currentlyDefinedActionTypes();

protected:
    ActionType(int val);
    static std::vector<std::string> __stringValues;
private:
    int _value;
};

class ActionDefinition
{
    MOVE3D_STATIC_LOGGER;
public:
    ActionDefinition();
    ActionDefinition(ActionType type);
    virtual ~ActionDefinition();

    std::vector<PlanningType> const &requiredParts() const;
    void setRequiredParts(const std::vector<PlanningType> &requiredParts);

    ActionType actionType() const;
    void setActionType(const ActionType &actionType);

    std::vector<PlanningType> hriParts() const;
    void setHriParts(const std::vector<PlanningType> &hriParts);


    const std::set<AgentType> &include() const;
    std::set<AgentType> &include();
    void setInclude(const std::set<AgentType> &include);
    void addInclude(AgentType agent_type);

    const std::set<AgentType> &exclude() const;
    std::set<AgentType> &exclude();
    void setExclude(const std::set<AgentType> &exclude);
    void addExclude(AgentType agent_type);

    /// consider include and exclude lists
    bool isWhiteListed(AgentType agent_type) const;

    std::map<std::string,double> actionVariables() const;
    void setActionVariables(const std::map<std::string,double> &actVar);
    void addActionVariable(std::string key, double value);

private:
    ActionType _actionType;
    std::vector<PlanningType> _requiredParts;
    std::vector<PlanningType> _hriParts;

    std::set<AgentType> _include;
    std::set<AgentType> _exclude;

    std::map<std::string,double> _actionVariables;

};

class Actions : public DatabaseInterface
{
    MOVE3D_STATIC_LOGGER;
public:
    Actions(DatabaseReader *dbReader=NULL, bool proactive=false);
    ~Actions();

    ActionDefinition const* getActionDefinition(ActionType t );

    //json import/export
    //.global
    std::string getPath();
    std::string getPath(ActionType t);
    //.import
    bool fetchData();
    using DatabaseInterface::fetchData;
    bool fetchAction(ActionType t);
    bool parseJson(const Json::Value &root, const std::string &name);

    bool addActionDef(ActionType t, Json::Value const & value);
    void addActionDef(ActionDefinition *action);

    //.export
    bool save();
    bool appendActionToJson(Json::Value &root,ActionDefinition const & act);

    //.json conversion
    Json::Value actionToJson(ActionDefinition const &act);
    ActionDefinition *jsonToActionDefinition(ActionType t,Json::Value const & value);

private:
    std::map<ActionType,ActionDefinition*> _actions;
};

} // namespace Sem

} //namespace move4d
#endif // SEM_ACTIONS_H
