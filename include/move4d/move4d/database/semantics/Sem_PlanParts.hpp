#ifndef SEM_PLANPARTS_H
#define SEM_PLANPARTS_H

#include <jsoncpp/json/json.h>

//#include "move4d/API/Device/robot.hpp"
#include "move4d/Logging/Logger.h"
#include "move4d/database/semantics/Sem_AgentType.hpp"
#include "move4d/database/DatabaseInterface.hpp"

namespace move4d
{
class Robot;

namespace Sem {

typedef struct armStruct {
    int armId;
    int ikJoint;
    std::string label;
    std::vector<int> jointIds;
    std::vector<int> fixedJointIds;
    int shoulderJoint;
    double reach_tolerance;
} armStruct;

class PlanningType
{
public:
    PlanningType();
    PlanningType(std::string s);
    virtual ~PlanningType();
    std::string toString() const;
    bool isDefined() const {return _value >=0;}

    friend bool operator==(PlanningType const &p1, PlanningType const &p2);
    friend bool operator!=(PlanningType const &p1, PlanningType const &p2);
    friend bool operator<(PlanningType const &p1, PlanningType const &p2);
    friend bool operator>(PlanningType const &p1, PlanningType const &p2);

    static std::vector<PlanningType> currentlyDefinedPlanningTypes();

protected:
    static std::vector<std::string> __strs;
private:
    int _value;
};

class PlanningPart
{
    MOVE3D_STATIC_LOGGER;
public:
    PlanningPart();
    PlanningPart(AgentType agent_type,PlanningType plan_type,const std::vector<int> &joint_ids,const std::vector<int> & arm_ids);
    /**
     * @brief PlanningPart concatenation constructor
     * @param planning_parts
     */
    PlanningPart(std::vector<PlanningPart*> planning_parts);
    virtual ~PlanningPart();

    void concat(const PlanningPart &p);

    std::vector<int> getAllJointIds(bool remove_fixed=true) const;
    std::vector<int> getJointsOfArmId(int armId, bool remove_fixed=true) const;


    /**
     * @brief use active robot active joints to create a new joint set
     * @return
     */
    static PlanningPart* createFromActiveJoints(PlanningType plan_type);

    PlanningType planType() const;
    void setPlanType(const PlanningType &planType);

    /**
     * @brief jointsIndices
     * @return
     *
     * @warn use getAllJointsIds to get joint indices to use (incl. arms, for instance)
     */
    const std::vector<int> &jointsIndices() const;
    void setJointsIndices(const std::vector<int> &jointsIndices);


    AgentType agentType() const;
    void setAgentType(const AgentType &agentType);

    const std::vector<int> &armIds() const;
    void setArmIds(const std::vector<int> &armIds);

    static Robot* getRobotOfType(AgentType t);

    bool operator==(PlanningPart const &other) const ;
    bool operator!=(PlanningPart const &other) const ;

private:
    AgentType _agentType;
    PlanningType _planType;
    std::vector<int> _jointsIndices;
    std::vector<int> _armIds;
};

class PlanParts : public DatabaseInterface
{
    MOVE3D_STATIC_LOGGER;
public:

    PlanParts(DatabaseReader *dbReader=NULL, bool proactive=false);
    ~PlanParts();

    bool fetchData();
    //use DatabaseInterface::fetchData other overload (std::string name)
    using DatabaseInterface::fetchData;
    //bool fetchData(const std::string &name);
    void clearData(AgentType t);

    bool createFromActiveJoints(PlanningType plan_type);

    bool parseJson(const Json::Value &root,const std::string &name);

    //bool getJsonRoot(Json::Value &root,const std::string &file_path);
    bool readJsonAgent(AgentType t);
    bool readJsonAgent(const Json::Value &root,const std::string &file);
    bool parsePlanPart(AgentType agn_type, std::string const &plan_type_str, Json::Value const &joint_set_json);
    bool parsePlanPart(AgentType agn_type, PlanningType plantype, Json::Value const &joint_set_json);

    void save();
    void writeJson(AgentType t);
    void appendJointsSetToJson(Json::Value &planning_parts_dict,  const PlanningPart *jointsset);

    std::string getPath(AgentType t);

    void addPlanPart(AgentType agent_type, PlanningType plan_type, std::vector<int> const & joints, std::vector<int> const & arm_ids);
    /**
     * @brief addPlanPart
     * @param jointsset
     * @note it only adds if the jointsset is not already present in the list, thus avoiding duplicates. it uses JointsSet::operator==.
     */
    void addPlanPart(PlanningPart * jointsset);


    /**
      * @brief retrive the arms data from p3d or/and from json file
      * @param r the robot for which to retrive the data
      */
    static void retrieveJointsForRobot(Robot *r);

    /**
      * @brief if not retrieved yet, retrive arm data and return corresponding jointIds
      * @param armId the arm id for which to get the data
      * @param r the concerned robot
      * @return the list of joint ids corresponding to the armId
      */
    static std::vector<int> getJointsOfArmId(int armId, Robot *r, bool remove_fixed=true);

    /**
      * @param armLabel the arm label for which to get the data
      * @param r the concerned robot
      * @return armId
      */
    static int getArmIdFromArmLabel(std::string armLabel, Robot *r);

    /**
      * @param armId the arm id for which to get the data
      * @param r the concerned robot
      * @return the arm label
      */
    static std::string getArmLabelFromArmId(int armId, Robot *r);

    /**
      * @brief retrieve all the possible armIds for given robot
      * @param r the concerned robot
      * @return the armIds
      */
    static std::vector<int> getRobotArmIds(Robot *r);

    /**
      * @brief get the robot armId ik joint
      * @param r the concerned robot
      * @param armId the robot arm
      * @return the ik joint id
      */
    static int getArmIkJoint(Robot *r, int armId);

    /**
      * @brief get the robot armId shoulder joint
      * @param r the concerned robot
      * @param armId the robot arm
      * @return the shoulder joint id
      */
    static int getArmShoulderJoint(Robot *r, int armId);

    /**
      * @brief get the robot armId reach
      * @param r the concerned robot
      * @param armId the robot arm
      * @return the arm reach
      */
    static double getArmReach(Robot *r, int armId);

    /**
      * @brief retrive others data from json file
      * @param r the robot for which to retrive the data
      */
    static void retrieveOtherJointsForRobot(Robot *r);

    /**
      * @brief if not retrieved yet, retrive others data and return corresponding jointIds
      * @param otherLabel the label of the concerned other part
      * @param r the concerned robot
      * @return the list of joint ids corresponding to the other part
      */
    static std::vector<int> getJointsOfOther(std::string otherLabel, Robot *r, bool remove_fixed=true);

    std::vector<PlanningPart *> getPlanningParts(AgentType agent_type, PlanningType plan_type);

    //std::vector<int> getPlanPart(Sem::AgentType agent_type, PlanningType plan_type, bool *found=0,bool *have_other=0);

    // static std::string planningTypeToString(PlanningType t);
    // static PlanningType stringToPlanningType(std::string const &s);

private:
    //static std::map<PlanningType,std::string> _planningTypeStrings;

private:
    std::string _global_json_path;
    std::map<AgentType, std::map<PlanningType,std::vector<PlanningPart *> > > _planParts;

    /**
      * @brief contains the robot arms description (id, label, and joints)
      */
    static std::map<Robot*,std::vector<armStruct> > armsDefinition;

    /**
      * @brief contains the robot other parts
      */
    static std::map<Robot*,std::map<std::string,std::pair<bool,std::vector<int> > > > othersjoints;

    // DatabaseInterface interface
};

} // namespace Sem

} //namespace move4d
#endif // SEM_PLANPARTS_H
