#ifndef DATABASEREADER_H
#define DATABASEREADER_H

#include <vector>
#include <string>
#include <jsoncpp/json/json.h>
#include "move4d/Logging/Logger.h"

namespace move4d
{
/**
 * @defgroup database
 * @brief data parsing tools for non-geometric information
 */

/**
 * @brief The DatabaseReader class
 * @ingroup database
 */
class DatabaseReader
{
    MOVE3D_STATIC_LOGGER;
public:
    static DatabaseReader *getInstance(const std::string &global_path="");

    DatabaseReader(const std::string &path);
    ~DatabaseReader();

    std::string path() const;
    void setPath(const std::string &path);
    std::string getParamPath() const;

    /**
     * @brief findFile search for a file in the different locations
     * @param name
     * @return
     */
    std::string findFile(const std::string &relPath);

    bool fileExists(const std::string &path);

    /**
     * @brief read find and parse json files by their relative path
     * @param relPath
     * @return
     */
    Json::Value read(const std::string &relPath);
    /**
     * @brief readDir find all json files in given directory, in all defined places
     * @param relPath the name of the directory to read (e.g. "Semantics/Actions")
     * @return a vector of file names
     *
     * @warning not recursive, returns only filenames (basename)
     *
     * e.g. files returned by this method can be accessed by calling read(relPath+filename)
     */
    std::vector<std::string> readDir(const std::string &relPath);
    /**
     * @brief readDirAbs find all json files in given directory (absolute path)
     * @param absPath the path to the directory to read, may be relative to "."
     * @return a vector of file names
     *
     * @warning not recursive, returns only filenames (basename)
     */
    std::vector<std::string> readDirAbs(const std::string &absPath);

    Json::Value readAbsolute(const std::string &absPath);
    Json::Value readLocal(const std::string &relPath);
    Json::Value readGlobal(const std::string &relPath);

    Json::Value parseFile(const std::string &absPath);

private:

    static DatabaseReader *__mainDbReader;
    enum STATE_t {PLACE_LOCAL,PLACE_ENV,PLACE_ARG,PLACE_GLOBAL};
    std::vector<STATE_t> _stateMachine;

    std::string _path;


};

} //namespace move4d
#endif // DATABASEREADER_H
