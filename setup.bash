#!/usr/bin/bash

export SVP_INSTALL_PATH=`pwd`
export SRC_PATH=$SVP_INSTALL_PATH/src
export ROS_WS=$SVP_INSTALL_PATH/ros_ws
export LD_LIBRARY_PATH=$SVP_INSTALL_PATH/lib:$LD_LIBRARY_PATH
export PATH=$SVP_INSTALL_PATH/lib:$PATH
export CMAKE_PREFIX_PATH=$SVP_INSTALL_PATH:$CMAKE_PREFIX_PATH
export MOVE4D_PLUGIN_LIBRARIES=$SVP_INSTALL_PATH/lib/libsvp_planner_move4d.so
export HOME_MOVE4D=$SVP_INSTALL_PATH/share/move4d

if [ -f $ROS_WS/devel/setup.bash ]; then
	source $ROS_WS/devel/setup.bash
else
	echo "You must run catkin_make in $ROS_WS and source $ROS_WS/devel/setup.bash"\
		"Sourcing setup.bash will be done automatically next time you source this file"
fi
