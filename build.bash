#!/usr/bin/env bash

dir=`dirname $0`

source $dir/setup.bash
(cd $ROS_WS && catkin_make)
